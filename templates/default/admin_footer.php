</div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?=SITE_URL.DEFAULT_ASSETS?>bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=SITE_URL.DEFAULT_ASSETS?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=SITE_URL.DEFAULT_ASSETS?>bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?=SITE_URL.DEFAULT_ASSETS?>dist/js/sb-admin-2.js"></script>
    
    <script src="<?=SITE_URL?>js/jquery.validate.js"></script>
<!-- Page specific javascript will be included here -->
<?php
if(!empty($mtPageScripts)){
    foreach($mtPageScripts as $script){
        echo '<script src="'.SITE_URL."page_javascript/".$script.'"></script>';
    }
}
?>
</body>

</html>