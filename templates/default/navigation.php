<?php
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Process Active Tab
--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
//$userPages = $this->page->getUserPages($_SESSION['auth_userinfo']['userLevel']);
?>
<!-- BEGIN HORIZANTAL MENU -->
<div class="hor-menu hor-menu-light hidden-sm hidden-xs">
  <ul class="nav navbar-nav"> 
     
    <?php
	$p = $this->uri->uri_string();
    $currentPage = $this->page->getPageByUrl($p); // $p obtained from index.php
	$userPermissions = $this->user->getUserPermisstions();
	$userPermissionIDs = array_keys($userPermissions);
	
    $navCheck	 = 'AND pages.addToNavigation = 1';
	$levelCheck = '';
    $userPages   = $this->page->getUserPages($navCheck.' AND pages.parentID=0 ');
	if($userPermissionIDs) {
		$levelCheck = "AND ( pages.permID IN (".implode(", ",$userPermissionIDs).") OR pages.permID = '-1')";	
		//$levelCheck	 = 'AND roles.userLevel >= '.$this->session->userdata('userLevel');
		
		$userPages   = $this->page->getUserPages($navCheck.' AND pages.parentID=0 '.$levelCheck);
		$summary = $this->general->defectsSummary();
		foreach ($userPages as $page) {
			if (empty($page['pageUrl'])){
				$pageUrl = "javascript:;";
			} else {
				$pageUrl = $this->url->navigation($page['pageUrl']);
			}
			// --------------- Check if pages have child pages ---------------->
			$subNav = '';
			$pageName = $page['pageDisplayName'];
			$childPages = $this->page->getUserPages($navCheck.' AND pages.parentID='.$page['pageID'].' '.$levelCheck, "pages.pageName");
			$subHeadings = $this->page->getUserPages($navCheck.' AND pages.parentID='.$page['pageID'].' AND pages.isHeadingOnly = 1  '.$levelCheck);
			$activeClass = ($page['pageID'] == @$currentPage['pageID'] OR $page['pageID'] == @$currentPage['parentID']) ? 'active' : '';
				
				?>
				<?php if (empty($childPages)) { ?>
					<li class="classic-menu-dropdown <?=$activeClass?>">
						<a href="<?php echo $pageUrl; ?>">
							<i class="<?=$page['iconClass']?>"></i><? echo $page['pageDisplayName']; ?> <?php if ($activeClass == 'active') { ?> <span
								class="selected"></span><?php } ?>
						</a>
					</li>
				<?php } else if (empty($subHeadings)) { ?>
					<li class="classic-menu-dropdown <?=$activeClass?>">
						<a data-toggle="dropdown" href="javascript:;">
							<i class="<? echo $page['iconClass']; ?>"></i> <? echo $page['pageName']; ?> <i class="fa fa-angle-down"></i> <?php if ($activeClass == 'active') { ?> <span
								class="selected"></span><?php } ?>
						</a>
						<ul class="dropdown-menu pull-left">
							<?php
							foreach ($childPages as $childPage) {
									?>
									<li>
										<a href="<?= $this->url->navigation($childPage['pageUrl']) ?>">
											<i class="<?=$childPage['iconClass']?>"></i><? echo $childPage['pageDisplayName']; ?> 
										</a>
									</li>
								<?php
							}
							?>
						</ul>
					</li>
				<?php } else { ?>
					<li class="mega-menu-dropdown mega-menu-full <?=$activeClass?>">
						<a data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
							<i class="<?=$page['iconClass']?>"></i><? echo $page['pageDisplayName']; ?> <i class="fa fa-angle-down"></i> <?php if ($activeClass == 'active') { ?> <span
								class="selected"></span><?php } ?>
						</a>
						<?php
						$totalSubHeadings = count($subHeadings);
						$colClass = ($totalSubHeadings == 0) ? 'col-md-12' : 'col-md-' . floor(12 / $totalSubHeadings);
						?>
						<ul class="dropdown-menu">
							<li>
								<!-- Content container to add padding -->
								<div class="mega-menu-content">
									<div class="row">
										<?php
										foreach ($subHeadings as $subHeading) {
											?>
											<div class="<?= $colClass ?>">
												<ul class="mega-menu-submenu">
													<li class="c-heading-col">
														<h3><?= $subHeading['pageDisplayName'] ?></h3>
													</li>
													<?php
													$subHeadingsPages = $this->page->getUserPages($navCheck.' AND pages.headingID='.$subHeading['pageID'].' '.$levelCheck, "pages.pageName");
													if (!empty($subHeadingsPages)) {
														foreach ($subHeadingsPages as $subHeadingsPage) {
															?>
															<li>
																<a href="<?= $this->url->navigation($subHeadingsPage['pageUrl']) ?>">
																	<i class="<?=$subHeadingsPage['iconClass']?>"></i><?=$subHeadingsPage['pageDisplayName']?>
																</a>
															</li>
														<?php
														}
													}
													?>
												</ul>
											</div>
										<?php
										}
										?>
									</div>
									<?php if($page['pageID']==10){ ?>
									<hr style="margin: 0px 0px 15px 0px;">
									<div class="row">
										<div class="col-sm-2">
											<ul class="mega-menu-submenu">
												<li class="c-heading-col">
													<h2 class="margin-0">Todays Stats</h2>
												</li>
											</ul>
										</div>
										<div class="col-sm-3">
											<ul class="mega-menu-submenu">
												<li class="c-heading-col">
													<h3>Defects Received:
													<span class="defect-summary-receiving"><?php echo $summary['totalDefects'];?></span></h3>
												</li>
											</ul>
										</div>
										<div class="col-sm-3">
											<ul class="mega-menu-submenu">
												<li class="c-heading-col">
													<h3>Inspections:
													<span class="defect-inspection-receiving"><?php echo $summary['inspectionsReceived']; ?></span></h3>
												</li>
											</ul>
										</div>
										<div class="col-sm-3">
											<ul class="mega-menu-submenu">
												<li class="c-heading-col">
													<h3>Potential Costs:
													<span class="defect-potential-cost"><?php echo costFormat($summary['totalDefectCost']); ?></span></h3>
												</li>
											</ul>
										</div>
									</div>
									<?php } ?>
								</div>
							</li>
						</ul>
					</li>
				<?php
				}
		
		}
	}
?>
    
  </ul>
</div>
<!-- END HORIZANTAL MENU -->