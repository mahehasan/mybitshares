-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2016 at 08:11 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `mybitshares`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `parent_category` int(11) NOT NULL,
  `category_title` varchar(255) NOT NULL,
  `category_description` text NOT NULL,
  `showing_order` int(11) NOT NULL,
  `category_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `parent_category`, `category_title`, `category_description`, `showing_order`, `category_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 0, 'Earnings Menuu', '', 1, 1, 1369335800, 1460916351, 1),
(2, 0, 'Offerwall Menu ', '', 2, 1, 1369335813, 1460854694, 1),
(3, 0, 'Referral, Advertise and Upgrade Menu', '', 3, 1, 1369335821, 1460854717, 1),
(4, 1, 'Share To Click', '', 4, 1, 1369337772, 1461000501, 1),
(5, 1, 'Paid To Click', '', 5, 1, 1369337799, 1460854804, 1),
(6, 0, 'Account, Cash and Payout Menu', 'test', 4, 1, 1431288356, 1460854731, 1),
(7, 0, 'Other Info!', 'test', 0, 1, 1460853760, 1460854749, 1),
(8, 1, 'Company Products', 'Users can purchase Company products here', 0, 1, 1462470167, 1462470167, 1),
(9, 1, 'Buy And Sell', 'Buy And Sell, just like ebay', 0, 1, 1462470205, 1462470205, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;