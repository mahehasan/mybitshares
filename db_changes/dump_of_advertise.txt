-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2016 at 02:29 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mybitshares`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertise`
--
DROP TABLE IF EXISTS `advertise`;
CREATE TABLE `advertise` (
  `advertise_id` int(11) NOT NULL,
  `site_server_code` varchar(255) DEFAULT NULL,
  `site_server_advertise_id` int(11) NOT NULL,
  `advertise_title` varchar(255) NOT NULL,
  `advertise_description` text NOT NULL,
  `advertise_price_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `showing_order` int(11) NOT NULL,
  `advertise_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advertise`
--

INSERT INTO `advertise` (`advertise_id`, `site_server_code`, `site_server_advertise_id`, `advertise_title`, `advertise_description`, `advertise_price_id`, `menu_id`, `showing_order`, `advertise_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, '100', 1, '', 'Click Here to get unlimited cash', 2, 5, 0, 1, 2016, 0, 1),
(2, '100', 2, '', 'Planet Traffic Guaranteed Signups', 2, 5, 0, 1, 2016, 0, 1),
(3, '100', 3, '', 'ADZ Bazar earnings', 1, 4, 0, 1, 2016, 0, 1),
(4, '100', 4, '', 'Earn Tinder Bitcoin On Click', 2, 4, 0, 1, 2016, 0, 1),
(5, NULL, 6, '', 'find out how banners like this make income', 1, 4, 0, 1, 2016, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertise`
--
ALTER TABLE `advertise`
  ADD PRIMARY KEY (`advertise_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertise`
--
ALTER TABLE `advertise`
  MODIFY `advertise_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
