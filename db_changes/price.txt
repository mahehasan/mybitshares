-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2016 at 06:25 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `mybitshares`
--

-- --------------------------------------------------------

--
-- Table structure for table `price_lookup`
--

CREATE TABLE `price_lookup` (
  `price_id` int(11) NOT NULL,
  `price_title` varchar(255) NOT NULL,
  `price_description` text NOT NULL,
  `price_sell` double(11,2) NOT NULL,
  `price_advertise` double(11,2) NOT NULL,
  `showing_order` int(11) NOT NULL,
  `price_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `price_lookup`
--

INSERT INTO `price_lookup` (`price_id`, `price_title`, `price_description`, `price_sell`, `price_advertise`, `showing_order`, `price_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 'AD Package 1', 'advertisement', 500.00, 450.00, 0, 1, 1461255757, 0, 1),
(2, 'AD Package 2', 'Bulk advertisement', 1000.00, 900.00, 0, 1, 1461255853, 1461255874, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `price_lookup`
--
ALTER TABLE `price_lookup`
  ADD PRIMARY KEY (`price_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `price_lookup`
--
ALTER TABLE `price_lookup`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;