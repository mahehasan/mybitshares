-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2016 at 09:48 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mybitshares`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertise`
--

CREATE TABLE `advertise` (
  `advertise_id` int(11) NOT NULL,
  `site_server_code` varchar(255) DEFAULT NULL,
  `site_server_advertise_id` int(11) NOT NULL,
  `advertise_title` varchar(255) NOT NULL,
  `advertise_description` text NOT NULL,
  `advertise_price_id` int(11) NOT NULL,
  `menu1_id` int(11) NOT NULL,
  `menu1_click` int(11) NOT NULL,
  `menu2_click` int(11) NOT NULL,
  `menu2_id` int(11) NOT NULL,
  `showing_order` int(11) NOT NULL,
  `advertise_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `expire_date` int(11) DEFAULT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advertise`
--

INSERT INTO `advertise` (`advertise_id`, `site_server_code`, `site_server_advertise_id`, `advertise_title`, `advertise_description`, `advertise_price_id`, `menu1_id`, `menu1_click`, `menu2_click`, `menu2_id`, `showing_order`, `advertise_publish`, `created_date`, `expire_date`, `modified_date`, `created_by`) VALUES
(1, NULL, 1, 'Unlimited Cash', 'Click Here to get unlimited cash', 1, 5, 0, 0, 0, 0, 1, 2016, 0, 1463828850, 1),
(2, NULL, 2, 'Planet Traffic', 'Planet Traffic Guaranteed Signups', 2, 5, 0, 0, 0, 0, 1, 2016, 0, 1463828949, 1),
(3, '100', 3, '', 'ADZ Bazar earnings', 1, 4, 0, 0, 0, 0, 1, 2016, 0, 0, 1),
(4, '100', 4, '', 'Earn Tinder Bitcoin On Click', 2, 4, 0, 0, 0, 0, 1, 2016, 0, 0, 1),
(5, NULL, 6, '', 'find out how banners like this make income', 1, 4, 0, 0, 0, 0, 1, 2016, 0, 0, 1),
(6, '100', 8, '', 'des', 0, 5, 500, 500, 4, 0, 1, 2016, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `app_users`
--

CREATE TABLE `app_users` (
  `userID` bigint(20) NOT NULL,
  `userRoleID` int(11) NOT NULL DEFAULT '0' COMMENT 'table app_user_roles',
  `email` varchar(250) NOT NULL,
  `password` varchar(40) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `user_type_id` int(10) NOT NULL DEFAULT '1',
  `phone` varchar(50) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `town` varchar(30) NOT NULL,
  `country` int(10) NOT NULL,
  `postCode` varchar(12) NOT NULL,
  `timeStamp` bigint(30) UNSIGNED DEFAULT NULL,
  `lastUpdated` bigint(30) UNSIGNED DEFAULT NULL,
  `lastUpdatedBy` int(10) UNSIGNED DEFAULT NULL,
  `lastLogin` bigint(30) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `password_date` date DEFAULT NULL,
  `sendPass` varchar(50) DEFAULT NULL,
  `pay_type` varchar(100) NOT NULL,
  `publish_flag` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_users`
--

INSERT INTO `app_users` (`userID`, `userRoleID`, `email`, `password`, `firstName`, `lastName`, `user_type_id`, `phone`, `address1`, `town`, `country`, `postCode`, `timeStamp`, `lastUpdated`, `lastUpdatedBy`, `lastLogin`, `active`, `password_date`, `sendPass`, `pay_type`, `publish_flag`) VALUES
(1, 1, 'admin@mybitshares.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Super', 'Admin', 1, '', '', '', 18, '', 0, 1440738787, 12, 1466845894, 1, '2019-08-07', NULL, '', 1),
(2, 0, 'mahehasan@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Mahedi', 'Hasan', 1, '', 'West Razabazar', 'Dhaka', 18, '1215', 1460360808, NULL, NULL, 1467008358, 1, '2016-04-11', NULL, 'Payza', 1),
(3, 0, 'khasan.hss@gmail.com', '202cb962ac59075b964b07152d234b70', 'Md. Khairul', 'Hasan', 1, '', 'Mohammadpur', 'Dhaka', 18, '1000', 1463819368, NULL, NULL, 1463819644, 1, '2016-05-21', NULL, 'Payza', 1),
(4, 0, 'user1@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'user1', 'user1', 1, '', 'here', 'test', 15, '1000', 1463986112, NULL, NULL, NULL, 1, '2016-05-23', NULL, 'Payza', 1),
(5, 0, 'user2@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'user2', 'user2', 1, '', 'here', 'Dhaka', 1, '1200', 1463986175, NULL, NULL, NULL, 1, '2016-05-23', NULL, 'Payza', 1),
(6, 0, 'user3@gmail.com', '202cb962ac59075b964b07152d234b70', 'Mehedi', 'Hasan', 1, '', 'West Razabazar', 'Dhaka', 1, '1000', 1463986214, NULL, NULL, NULL, 1, '2016-05-23', NULL, 'Payza', 1),
(7, 0, 'user4@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Mehedi', 'user1', 1, '', 'here', 'asd', 16, '2323', 1463986253, NULL, NULL, NULL, 1, '2016-05-23', NULL, 'Payza', 1),
(8, 0, 'k@yahoo.com', '202cb962ac59075b964b07152d234b70', 'adasd', 'asdas', 1, '', 'dasd', 'asd', 1, 'asasd', 1464593174, NULL, NULL, NULL, 1, '2016-05-30', NULL, 'Payza', 1),
(9, 0, 'a@b.com', '202cb962ac59075b964b07152d234b70', 'asdasd', 'asdas', 1, '', 'd', '', 1, '', 1464593443, NULL, NULL, NULL, 1, '2016-05-30', NULL, 'Payza', 1),
(10, 0, 'b@c.com', '202cb962ac59075b964b07152d234b70', 'zxz', 'czxc', 1, '', 'zxczx', 'czxc', 1, 'zxc', 1464593702, NULL, NULL, NULL, 1, '2016-05-30', NULL, 'Payza', 1),
(11, 0, 'b@c.com', '202cb962ac59075b964b07152d234b70', 'zxz', 'czxc', 1, '', 'zxczx', 'czxc', 1, 'zxc', 1464593929, NULL, NULL, NULL, 1, '2016-05-30', NULL, 'Payza', 1),
(12, 0, 'a@c.com', '202cb962ac59075b964b07152d234b70', 'asdas', 'asd', 1, '', 'dasd', 'asd', 1, '', 1464593956, NULL, NULL, NULL, 1, '2016-05-30', NULL, 'Payza', 1),
(13, 0, 'a@c.com', '202cb962ac59075b964b07152d234b70', 'asdas', 'asd', 1, '', 'dasd', 'asd', 1, '', 1464594343, NULL, NULL, NULL, 1, '2016-05-30', NULL, 'Payza', 1),
(14, 0, 'a@c.com', '202cb962ac59075b964b07152d234b70', 'asdas', 'asd', 1, '', 'dasd', 'asd', 1, '', 1464594403, NULL, NULL, NULL, 1, '2016-05-30', NULL, 'Payza', 1),
(15, 0, 'a@c.com', '202cb962ac59075b964b07152d234b70', 'asdas', 'asd', 1, '', 'dasd', 'asd', 1, '', 1464594406, NULL, NULL, NULL, 1, '2016-05-30', NULL, 'Payza', 1),
(16, 0, 'a@c.com', '202cb962ac59075b964b07152d234b70', 'asdas', 'asd', 1, '', 'dasd', 'asd', 1, '', 1464594409, NULL, NULL, NULL, 1, '2016-05-30', NULL, 'Payza', 1),
(17, 0, 'a@c.com', '202cb962ac59075b964b07152d234b70', 'asdas', 'asd', 1, '', 'dasd', 'asd', 1, '', 1464594655, NULL, NULL, NULL, 1, '2016-05-30', NULL, 'Payza', 1),
(18, 0, 'mirpur1272@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Mutawaqqil', 'Billah', 1, '', 'mirpur', 'dhaka', 18, '1216', 1465636574, NULL, NULL, 1465637780, 1, '2016-06-11', NULL, 'Payza', 1),
(19, 0, 'a@yahoo.com', '827ccb0eea8a706c4c34a16891f84e7b', 'dfdsfdsf', 'sfsdfsd', 1, '', 'sdfsdf', 'sdfsdf', 18, '12345', 1466151581, NULL, NULL, NULL, 1, '2016-06-17', NULL, 'Payza', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart_detail`
--

CREATE TABLE `cart_detail` (
  `cart_master_id` bigint(20) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `publish_flag` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_detail`
--

INSERT INTO `cart_detail` (`cart_master_id`, `product_id`, `quantity`, `unit_price`, `publish_flag`) VALUES
(0, 9, 1, 1, 1),
(0, 8, 1, 0, 1),
(0, 4, 1, 2, 1),
(0, 5, 1, 1, 1),
(0, 10, 3, 1, 1),
(6, 14, 1, 1, 1),
(7, 5, 1, 1, 1),
(8, 5, 3, 1, 1),
(9, 5, 2, 1, 0),
(10, 5, 1, 1, 1),
(10, 4, 1, 2, 1),
(11, 4, 1, 2, 1),
(11, 3, 2, 1, 1),
(12, 3, 1, 1, 0),
(12, 6, 1, 2, 0),
(12, 4, 1, 2, 0),
(14, 5, 6, 1, 0),
(15, 5, 1, 1, 0),
(16, 5, 1, 1, 0),
(17, 5, 1, 1, 1),
(18, 5, 3, 1, 1),
(19, 4, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart_master`
--

CREATE TABLE `cart_master` (
  `id` bigint(20) NOT NULL,
  `secret` varchar(300) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `checked_out` decimal(1,0) NOT NULL DEFAULT '0',
  `publish_flag` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_master`
--

INSERT INTO `cart_master` (`id`, `secret`, `created_on`, `checked_out`, `publish_flag`) VALUES
(6, '46ca7de8078b77b970db15cce6c4fc18', '2016-06-17 07:25:56', '0', 0),
(7, '0792c4e168da414057c792ebbd9ebc56', '2016-06-17 07:31:36', '0', 0),
(8, 'b722ff34d69904e27a06ad047b4ad46a', '2016-06-18 07:05:17', '0', 0),
(9, '8e59b54e60b6dcbd8005337ec2a9a7c4', '2016-06-18 08:15:58', '0', 0),
(10, '5ba6dab9271afb8903c7a42b0ce2471a', '2016-06-18 08:33:58', '0', 0),
(11, 'd7384145829cea5ba1dc8ca06d3b606a', '2016-06-18 08:38:51', '0', 0),
(12, '34fc6db683139fdd4315e16dcf811cd5', '2016-06-18 09:21:39', '0', 0),
(13, '63ada968fdd576df26477426487b5eb0', '2016-06-18 09:25:24', '0', 1),
(14, '9ac474f13ff5cf54591e760249e13b16', '2016-06-18 10:26:20', '0', 0),
(15, '9ac474f13ff5cf54591e760249e13b16', '2016-06-18 10:29:39', '0', 0),
(16, '9ac474f13ff5cf54591e760249e13b16', '2016-06-18 10:46:29', '0', 0),
(17, 'e25d3da66e96c92ae5bb90188e0af298', '2016-06-19 09:40:00', '0', 1),
(18, '75d9e8642454a516833e99bb1ca107f6', '2016-06-19 09:53:30', '0', 1),
(19, '84bd51712a8d5a15080ea90c0e4d554d', '2016-06-19 09:58:44', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cash_withdraw_history`
--

CREATE TABLE `cash_withdraw_history` (
  `withdraw_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `date` int(11) NOT NULL,
  `payment_method` varchar(150) NOT NULL,
  `transactionID` int(11) NOT NULL,
  `other_infos` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_withdraw_history`
--

INSERT INTO `cash_withdraw_history` (`withdraw_id`, `user_id`, `amount`, `date`, `payment_method`, `transactionID`, `other_infos`) VALUES
(1, 2, 100, 1465979016, '1', 1, '1'),
(2, 2, 100, 1465979308, '1', 2, '11'),
(3, 2, 50, 1465979451, '2', 22, '2'),
(4, 2, 25, 1465979507, '3', 5, '50'),
(5, 2, 10, 1465979652, 'PayPal Credit Card', 50, '0'),
(6, 2, 10, 1465979744, 'Solid Trustpay', 111, 'test info');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `parent_category` int(11) NOT NULL,
  `category_title` varchar(255) NOT NULL,
  `category_description` text NOT NULL,
  `showing_order` int(11) NOT NULL,
  `category_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `parent_category`, `category_title`, `category_description`, `showing_order`, `category_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 0, 'Earnings Menu', 'earnings', 1, 1, 1369335800, 1460916351, 1),
(2, 0, 'Offerwall Menu ', 'offerwall', 2, 1, 1369335813, 1460854694, 1),
(3, 0, 'Referral, Advertise and Upgrade Menu', 'ref_add_upg', 3, 1, 1369335821, 1460854717, 1),
(4, 1, 'Share To Click', 'share_to_click', 4, 1, 1369337772, 1461000501, 1),
(5, 1, 'Paid To Click', 'paid_to_click', 5, 1, 1369337799, 1460854804, 1),
(6, 0, 'Account, Cash and Payout Menu', 'account_cash_payout', 4, 1, 1431288356, 1460854731, 1),
(7, 0, 'Other Info!', 'Others', 0, 1, 1460853760, 1460854749, 1),
(8, 1, 'Company Products', 'purchase_product', 0, 1, 1462470167, 1462470167, 1),
(9, 1, 'Buy And Sell', 'buy_sell', 0, 1, 1462470205, 1462470205, 1),
(10, 7, 'Share Earnings Info', 'share_info', 0, 1, 1463126869, 1463126869, 1),
(11, 7, 'Log-Out', 'log_out', 0, 1, 1463126892, 1463126892, 1),
(12, 7, 'Cancel Account', 'cancel_account', 0, 1, 1463126912, 1463126912, 1),
(13, 6, 'Earning stats', 'earning_stats', 0, 1, 1463132497, 1463132497, 1),
(14, 6, 'Withdraw / Cashout', 'withdraw_cashout', 0, 1, 1463132533, 1463132533, 1),
(15, 6, 'Last Daily earnings', 'last_daily_earnings', 0, 1, 1463132573, 1463132573, 1),
(16, 6, 'Account Info', 'account_info', 0, 1, 1463132603, 1463132603, 1),
(17, 3, 'Referral Links', 'referral_link', 0, 1, 1464590742, 1464590742, 1),
(19, 3, 'Your Referrals', 'my_referrals', 0, 1, 1465531797, 1465531797, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `click_calculation`
--

CREATE TABLE `click_calculation` (
  `click_calculation_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `price_id` bigint(20) NOT NULL,
  `current_click_share` int(11) NOT NULL,
  `current_click_paid` int(11) NOT NULL,
  `advertise_id` int(11) NOT NULL,
  `date` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `click_calculation`
--

INSERT INTO `click_calculation` (`click_calculation_id`, `user_id`, `price_id`, `current_click_share`, `current_click_paid`, `advertise_id`, `date`) VALUES
(1, 2, 1, 2, 0, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `client_cash`
--

CREATE TABLE `client_cash` (
  `cash_id` int(11) NOT NULL,
  `site_server_code` varchar(255) NOT NULL,
  `site_server_advertise_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `cash_amount` double(11,2) NOT NULL,
  `client_cash_publish` tinyint(1) NOT NULL DEFAULT '1',
  `click_date` int(11) NOT NULL,
  `ip_address` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_cash`
--

INSERT INTO `client_cash` (`cash_id`, `site_server_code`, `site_server_advertise_id`, `client_id`, `cash_amount`, `client_cash_publish`, `click_date`, `ip_address`) VALUES
(1, '0', 0, 2, 0.05, 1, 1462816301, '192.168.1.1'),
(2, '0', 0, 3, 0.05, 1, 1462817187, '192.168.1.2'),
(3, '0', 0, 2, 0.04, 1, 1462816301, '192.168.1.1'),
(4, '0', 0, 3, 0.04, 1, 1462817187, '192.168.1.2'),
(5, '0', 0, 4, 0.05, 1, 1462817187, '192.168.1.2'),
(6, '0', 0, 4, 0.04, 1, 1462817187, '192.168.1.2'),
(7, '0', 0, 5, 0.05, 1, 1462817187, '192.168.1.2'),
(8, '0', 0, 5, 0.04, 1, 1462817187, '192.168.1.2'),
(9, '0', 0, 6, 0.04, 1, 1462817187, '192.168.1.2'),
(10, '0', 0, 6, 0.05, 1, 1462817187, '192.168.1.2'),
(11, '100', 2, 2, 0.05, 0, 1467010798, '::1'),
(12, '100', 2, 2, 0.05, 0, 1467012269, '::1'),
(13, '100', 2, 2, 0.05, 0, 1467012610, '::1'),
(14, '100', 2, 2, 0.05, 0, 1467012695, '::1'),
(15, '100', 2, 2, 0.05, 0, 1467013057, '::1'),
(16, '100', 2, 2, 0.05, 0, 1467013057, '::1'),
(17, '100', 2, 2, 0.05, 0, 1467013117, '::1'),
(18, '100', 2, 2, 0.05, 0, 1467013117, '::1'),
(19, '100', 2, 2, 0.05, 0, 1467013201, '::1'),
(20, '100', 2, 2, 0.05, 0, 1467013201, '::1'),
(21, '100', 2, 2, 0.05, 0, 1467013251, '::1'),
(22, '100', 2, 2, 0.05, 0, 1467013251, '::1'),
(23, '100', 2, 2, 0.05, 0, 1467013269, '::1'),
(24, '100', 2, 2, 0.05, 0, 1467013269, '::1'),
(25, '100', 2, 2, 0.05, 0, 1467013322, '::1'),
(26, '100', 2, 2, 0.05, 0, 1467013322, '::1'),
(27, '100', 2, 2, 0.05, 0, 1467013659, '::1'),
(28, '100', 2, 2, 0.05, 0, 1467013659, '::1'),
(29, '100', 2, 2, 0.05, 0, 1467013919, '::1'),
(30, '100', 2, 2, 0.05, 0, 1467013919, '::1'),
(31, '100', 2, 2, 0.05, 0, 1467013975, '::1'),
(32, '100', 2, 2, 0.05, 0, 1467013975, '::1'),
(33, '100', 2, 2, 0.05, 0, 1467014029, '::1'),
(34, '100', 2, 2, 0.05, 0, 1467014029, '::1'),
(35, '100', 2, 2, 0.05, 0, 1467014231, '::1'),
(36, '100', 2, 2, 0.05, 0, 1467014231, '::1'),
(37, '100', 2, 2, 0.05, 0, 1467014345, '::1'),
(38, '100', 2, 2, 0.05, 0, 1467014345, '::1'),
(39, '100', 2, 2, 0.05, 0, 1467014558, '::1'),
(40, '100', 2, 2, 0.05, 0, 1467014558, '::1'),
(41, '100', 2, 2, 0.05, 0, 1467014745, '::1'),
(42, '100', 2, 2, 0.05, 0, 1467014745, '::1'),
(43, '100', 2, 2, 0.05, 0, 1467014776, '::1'),
(44, '100', 2, 2, 0.05, 0, 1467014776, '::1'),
(45, '100', 2, 2, 0.05, 0, 1467014868, '::1'),
(46, '100', 2, 2, 0.05, 0, 1467014869, '::1'),
(47, '100', 2, 2, 0.05, 0, 1467015021, '::1'),
(48, '100', 2, 2, 0.05, 0, 1467015021, '::1'),
(49, '100', 2, 2, 0.05, 0, 1467015075, '::1'),
(50, '100', 2, 2, 0.05, 0, 1467015075, '::1'),
(51, '100', 2, 2, 0.05, 0, 1467015102, '::1'),
(52, '100', 2, 2, 0.05, 0, 1467015102, '::1'),
(53, '100', 2, 2, 0.05, 0, 1467015167, '::1'),
(54, '100', 2, 2, 0.05, 0, 1467015476, '::1'),
(55, '100', 2, 2, 0.05, 0, 1467015483, '::1'),
(56, '100', 2, 2, 0.05, 0, 1467015732, '::1'),
(57, '100', 2, 2, 0.05, 0, 1467016104, '::1'),
(58, '100', 2, 2, 0.05, 0, 1467016187, '::1'),
(59, '100', 2, 2, 0.05, 0, 1467016266, '::1'),
(60, '100', 2, 2, 0.05, 0, 1467016281, '::1'),
(61, '100', 2, 2, 0.05, 0, 1467016355, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `client_shares`
--

CREATE TABLE `client_shares` (
  `shares_id` int(11) NOT NULL,
  `site_server_code` varchar(255) NOT NULL,
  `site_server_advertise_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `shares_amount` double(11,2) NOT NULL,
  `client_shares_publish` tinyint(1) NOT NULL DEFAULT '1',
  `click_date` int(11) NOT NULL,
  `ip_address` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_shares`
--

INSERT INTO `client_shares` (`shares_id`, `site_server_code`, `site_server_advertise_id`, `client_id`, `shares_amount`, `client_shares_publish`, `click_date`, `ip_address`) VALUES
(1, '', 0, 2, 0.05, 1, 1462816406, '192.168.0.1'),
(2, '', 0, 2, 0.25, 1, 1462817185, ''),
(3, '', 0, 1, 0.00, 1, 1466938338, '192.168.0.1'),
(4, '', 0, 1, 0.00, 0, 1466938892, '::1'),
(5, '', 0, 1, 0.00, 0, 1466939201, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `property_name` varchar(150) NOT NULL,
  `property_value` varchar(150) NOT NULL,
  `configuration_publish` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `property_name`, `property_value`, `configuration_publish`) VALUES
(1, 'enable_time', '250h', 1),
(2, 'share_sell_percentage', '5', 1),
(3, 'share_buy_percentage', '5', 1),
(4, 'minimum_amount_to_see_sell', '0', 1),
(5, 'daily_income_system_share', '2', 1),
(6, 'minimum_share_to_join_sell', '2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` bigint(20) NOT NULL,
  `country_name` varchar(64) NOT NULL,
  `country_code` char(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country_name`, `country_code`) VALUES
(1, 'Andorra', 'AD'),
(2, 'United Arab Emirates', 'AE'),
(3, 'Afghanistan', 'AF'),
(4, 'Antigua and Barbuda', 'AG'),
(5, 'Anguilla', 'AI'),
(6, 'Albania', 'AL'),
(7, 'Armenia', 'AM'),
(8, 'Netherlands Antilles', 'AN'),
(9, 'Angola', 'AO'),
(10, 'Argentina', 'AR'),
(11, 'American Samoa', 'AS'),
(12, 'Austria', 'AT'),
(13, 'Australia', 'AU'),
(14, 'Aruba', 'AW'),
(15, 'Azerbaijan', 'AZ'),
(16, 'Bosnia and Herzegowina', 'BA'),
(17, 'Barbados', 'BB'),
(18, 'Bangladesh', 'BD'),
(19, 'Belgium', 'BE'),
(20, 'Burkina Faso', 'BF'),
(21, 'Bulgaria', 'BG'),
(22, 'Bahrain', 'BH'),
(23, 'Burundi', 'BI'),
(24, 'Benin', 'BJ'),
(25, 'Bermuda', 'BM'),
(26, 'Brunei Darussalam', 'BN'),
(27, 'Bolivia', 'BO'),
(28, 'Brazil', 'BR'),
(29, 'Bahamas', 'BS'),
(30, 'Bhutan', 'BT'),
(31, 'Bouvet Island', 'BV'),
(32, 'Botswana', 'BW'),
(33, 'Belarus', 'BY'),
(34, 'Belize', 'BZ'),
(35, 'Canada', 'CA'),
(36, 'Cocos (Keeling) Islands', 'CC'),
(37, 'Central African Republic', 'CF'),
(38, 'Congo', 'CG'),
(39, 'Switzerland', 'CH'),
(40, 'Cote D''Ivoire', 'CI'),
(41, 'Cook Islands', 'CK'),
(42, 'Chile', 'CL'),
(43, 'Cameroon', 'CM'),
(44, 'China', 'CN'),
(45, 'Colombia', 'CO'),
(46, 'Costa Rica', 'CR'),
(47, 'Cuba', 'CU'),
(48, 'Cape Verde', 'CV'),
(49, 'Christmas Island', 'CX'),
(50, 'Cyprus', 'CY'),
(51, 'Czech Republic', 'CZ'),
(52, 'Germany', 'DE'),
(53, 'Djibouti', 'DJ'),
(54, 'Denmark', 'DK'),
(55, 'Dominica', 'DM'),
(56, 'Dominican Republic', 'DO'),
(57, 'Algeria', 'DZ'),
(58, 'Ecuador', 'EC'),
(59, 'Estonia', 'EE'),
(60, 'Egypt', 'EG'),
(61, 'Western Sahara', 'EH'),
(62, 'Eritrea', 'ER'),
(63, 'Spain', 'ES'),
(64, 'Ethiopia', 'ET'),
(65, 'Finland', 'FI'),
(66, 'Fiji', 'FJ'),
(67, 'Falkland Islands (Malvinas)', 'FK'),
(68, 'Micronesia, Federated States of', 'FM'),
(69, 'Faroe Islands', 'FO'),
(70, 'France', 'FR'),
(71, 'France, Metropolitan', 'FX'),
(72, 'Gabon', 'GA'),
(73, 'Grenada', 'GD'),
(74, 'Georgia', 'GE'),
(75, 'French Guiana', 'GF'),
(76, 'Ghana', 'GH'),
(77, 'Gibraltar', 'GI'),
(78, 'Greenland', 'GL'),
(79, 'Gambia', 'GM'),
(80, 'Guinea', 'GN'),
(81, 'Guadeloupe', 'GP'),
(82, 'Equatorial Guinea', 'GQ'),
(83, 'Greece', 'GR'),
(84, 'South Georgia and the South Sandwich\r\n Islands', 'GS'),
(85, 'Guatemala', 'GT'),
(86, 'Guam', 'GU'),
(87, 'Guinea-bissau', 'GW'),
(88, 'Guyana', 'GY'),
(89, 'Hong Kong', 'HK'),
(90, 'Heard and Mc Donald Islands', 'HM'),
(91, 'Honduras', 'HN'),
(92, 'Croatia', 'HR'),
(93, 'Haiti', 'HT'),
(94, 'Hungary', 'HU'),
(95, 'Indonesia', 'ID'),
(96, 'Ireland', 'IE'),
(97, 'Israel', 'IL'),
(98, 'India', 'IN'),
(99, 'British Indian Ocean Territory', 'IO'),
(100, 'Iraq', 'IQ'),
(101, 'Iran (Islamic Republic of)', 'IR'),
(102, 'Iceland', 'IS'),
(103, 'Italy', 'IT'),
(104, 'Jamaica', 'JM'),
(105, 'Jordan', 'JO'),
(106, 'Japan', 'JP'),
(107, 'Kenya', 'KE'),
(108, 'Kyrgyzstan', 'KG'),
(109, 'Cambodia', 'KH'),
(110, 'Kiribati', 'KI'),
(111, 'Comoros', 'KM'),
(112, 'Saint Kitts and Nevis', 'KN'),
(113, 'Korea, Democratic People''s\r\n Republic of', 'KP'),
(114, 'Korea, Republic of', 'KR'),
(115, 'Kuwait', 'KW'),
(116, 'Cayman Islands', 'KY'),
(117, 'Kazakhstan', 'KZ'),
(118, 'Lao People''s Democratic Republic', 'LA'),
(119, 'Lebanon', 'LB'),
(120, 'Saint Lucia', 'LC'),
(121, 'Liechtenstein', 'LI'),
(122, 'Sri Lanka', 'LK'),
(123, 'Liberia', 'LR'),
(124, 'Lesotho', 'LS'),
(125, 'Lithuania', 'LT'),
(126, 'Luxembourg', 'LU'),
(127, 'Latvia', 'LV'),
(128, 'Libyan Arab Jamahiriya', 'LY'),
(129, 'Morocco', 'MA'),
(130, 'Monaco', 'MC'),
(131, 'Moldova, Republic of', 'MD'),
(132, 'Madagascar', 'MG'),
(133, 'Marshall Islands', 'MH'),
(134, 'Macedonia, The Former Yugoslav\r\n Republic of', 'MK'),
(135, 'Mali', 'ML'),
(136, 'Myanmar', 'MM'),
(137, 'Mongolia', 'MN'),
(138, 'Macau', 'MO'),
(139, 'Northern Mariana Islands', 'MP'),
(140, 'Martinique', 'MQ'),
(141, 'Mauritania', 'MR'),
(142, 'Montserrat', 'MS'),
(143, 'Malta', 'MT'),
(144, 'Mauritius', 'MU'),
(145, 'Maldives', 'MV'),
(146, 'Malawi', 'MW'),
(147, 'Mexico', 'MX'),
(148, 'Malaysia', 'MY'),
(149, 'Mozambique', 'MZ'),
(150, 'Namibia', 'NA'),
(151, 'New Caledonia', 'NC'),
(152, 'Niger', 'NE'),
(153, 'Norfolk Island', 'NF'),
(154, 'Nigeria', 'NG'),
(155, 'Nicaragua', 'NI'),
(156, 'Netherlands', 'NL'),
(157, 'Norway', 'NO'),
(158, 'Nepal', 'NP'),
(159, 'Nauru', 'NR'),
(160, 'Niue', 'NU'),
(161, 'New Zealand', 'NZ'),
(162, 'Oman', 'OM'),
(163, 'Panama', 'PA'),
(164, 'Peru', 'PE'),
(165, 'French Polynesia', 'PF'),
(166, 'Papua New Guinea', 'PG'),
(167, 'Philippines', 'PH'),
(168, 'Pakistan', 'PK'),
(169, 'Poland', 'PL'),
(170, 'St. Pierre and Miquelon', 'PM'),
(171, 'Pitcairn', 'PN'),
(172, 'Puerto Rico', 'PR'),
(173, 'Portugal', 'PT'),
(174, 'Palau', 'PW'),
(175, 'Paraguay', 'PY'),
(176, 'Qatar', 'QA'),
(177, 'Reunion', 'RE'),
(178, 'Romania', 'RO'),
(179, 'Russian Federation', 'RU'),
(180, 'Rwanda', 'RW'),
(181, 'Saudi Arabia', 'SA'),
(182, 'Solomon Islands', 'SB'),
(183, 'Seychelles', 'SC'),
(184, 'Sudan', 'SD'),
(185, 'Sweden', 'SE'),
(186, 'Singapore', 'SG'),
(187, 'St. Helena', 'SH'),
(188, 'Slovenia', 'SI'),
(189, 'Svalbard and Jan Mayen Islands', 'SJ'),
(190, 'Slovakia (Slovak Republic)', 'SK'),
(191, 'Sierra Leone', 'SL'),
(192, 'San Marino', 'SM'),
(193, 'Senegal', 'SN'),
(194, 'Somalia', 'SO'),
(195, 'Suriname', 'SR'),
(196, 'Sao Tome and Principe', 'ST'),
(197, 'El Salvador', 'SV'),
(198, 'Syrian Arab Republic', 'SY'),
(199, 'Swaziland', 'SZ'),
(200, 'Turks and Caicos Islands', 'TC'),
(201, 'Chad', 'TD'),
(202, 'French Southern Territories', 'TF'),
(203, 'Togo', 'TG'),
(204, 'Thailand', 'TH'),
(205, 'Tajikistan', 'TJ'),
(206, 'Tokelau', 'TK'),
(207, 'Turkmenistan', 'TM'),
(208, 'Tunisia', 'TN'),
(209, 'Tonga', 'TO'),
(210, 'East Timor', 'TP'),
(211, 'Turkey', 'TR'),
(212, 'Trinidad and Tobago', 'TT'),
(213, 'Tuvalu', 'TV'),
(214, 'Taiwan', 'TW'),
(215, 'Tanzania, United Republic of', 'TZ'),
(216, 'Ukraine', 'UA'),
(217, 'Uganda', 'UG'),
(218, 'United Kingdom', 'UK'),
(219, 'United States Minor Outlying\r\n Islands', 'UM'),
(220, 'United States', 'US'),
(221, 'Uruguay', 'UY'),
(222, 'Uzbekistan', 'UZ'),
(223, 'Vatican City State (Holy See)', 'VA'),
(224, 'Saint Vincent and the Grenadines', 'VC'),
(225, 'Venezuela', 'VE'),
(226, 'Virgin Islands (British)', 'VG'),
(227, 'Virgin Islands (U.S.)', 'VI'),
(228, 'Viet Nam', 'VN'),
(229, 'Vanuatu', 'VU'),
(230, 'Wallis and Futuna Islands', 'WF'),
(231, 'Samoa', 'WS'),
(232, 'Yemen', 'YE'),
(233, 'Mayotte', 'YT'),
(234, 'Yugoslavia', 'YU'),
(235, 'South Africa', 'ZA'),
(236, 'Zambia', 'ZM'),
(237, 'Zaire', 'ZR'),
(238, 'Zimbabwe', 'ZW');

-- --------------------------------------------------------

--
-- Table structure for table `current_share_price`
--

CREATE TABLE `current_share_price` (
  `price_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `per_share_price` double NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `current_share_price`
--

INSERT INTO `current_share_price` (`price_id`, `user_id`, `per_share_price`, `date`) VALUES
(1, 3, 2, 1463738449),
(2, 2, 5, 1464692754);

-- --------------------------------------------------------

--
-- Table structure for table `manage_client_advertise`
--

CREATE TABLE `manage_client_advertise` (
  `manage_client_advertise_id` int(11) NOT NULL,
  `advertise_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `manage_client_advertise_publish` tinyint(1) NOT NULL DEFAULT '0',
  `click_time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manage_client_advertise`
--

INSERT INTO `manage_client_advertise` (`manage_client_advertise_id`, `advertise_id`, `client_id`, `manage_client_advertise_publish`, `click_time`) VALUES
(11, 4, 2, 0, 1463738449),
(1, 3, 2, 0, 1463738449),
(10, 3, 2, 0, 1463738449);

-- --------------------------------------------------------

--
-- Table structure for table `price_lookup`
--

CREATE TABLE `price_lookup` (
  `price_id` int(11) NOT NULL,
  `price_title` varchar(255) NOT NULL,
  `price_description` text NOT NULL,
  `price_sell` double(11,2) NOT NULL,
  `price_advertise` double(11,2) NOT NULL,
  `showing_order` int(11) NOT NULL,
  `price_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `price_lookup`
--

INSERT INTO `price_lookup` (`price_id`, `price_title`, `price_description`, `price_sell`, `price_advertise`, `showing_order`, `price_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 'AD Package 1', 'advertisement', 0.50, 0.25, 0, 1, 1461255757, 1466150183, 1),
(2, 'AD Package 2', 'Bulk advertisement', 0.10, 0.05, 0, 1, 1461255853, 1461255874, 1),
(3, 'a', 'a', 1.00, 0.20, 0, 0, 1466150233, 0, 1),
(4, 'b', 'b', 20.05, 2.05, 0, 0, 1466150615, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_itemid` varchar(255) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `product_company_id` int(11) NOT NULL,
  `product_brand_id` int(11) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_price` double(11,2) NOT NULL,
  `product_discount` double(11,2) NOT NULL,
  `product_discount_percentage` int(11) NOT NULL,
  `product_special` tinyint(1) DEFAULT NULL,
  `menu_id` int(11) NOT NULL,
  `new_arrivals` tinyint(1) DEFAULT NULL,
  `best_sellers` tinyint(1) DEFAULT NULL,
  `comming_soon` tinyint(1) DEFAULT NULL,
  `product_image` varchar(200) NOT NULL,
  `product_thumbimage` varchar(250) NOT NULL,
  `product_thumbimage1` varchar(250) NOT NULL,
  `short_description` text NOT NULL,
  `product_description` text NOT NULL,
  `product_sellscount` int(11) NOT NULL,
  `product_shares` int(11) NOT NULL DEFAULT '0',
  `showing_order` tinyint(1) NOT NULL,
  `product_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_itemid`, `product_category_id`, `product_company_id`, `product_brand_id`, `product_quantity`, `product_price`, `product_discount`, `product_discount_percentage`, `product_special`, `menu_id`, `new_arrivals`, `best_sellers`, `comming_soon`, `product_image`, `product_thumbimage`, `product_thumbimage1`, `short_description`, `product_description`, `product_sellscount`, `product_shares`, `showing_order`, `product_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 'DSLR', '0', 0, 0, 0, 100, 0.00, 5.00, 5, 0, 0, 0, 0, 0, 'upload/images/products/1f16e4524598c2b0aa096c86cb996ac3.jpg', 'upload/images/products/thumbs/1f16e4524598c2b0aa096c86cb996ac3_thumb.jpg', 'upload/images/products/thumbs/1f16e4524598c2b0aa096c86cb996ac3_thumb.jpg', 'Camera', 'Canon or nikon', 0, 10, 0, 1, 1462298097, 1462985628, 1),
(2, 'Beautiful Sights', '0', 0, 0, 0, 3, 0.00, 1.00, 1, NULL, 0, 0, NULL, NULL, 'upload/images/products/c2a1c10068de8e3588825543b13a89a6.jpg', 'upload/images/products/thumbs/c2a1c10068de8e3588825543b13a89a6_thumb.jpg', 'upload/images/products/thumbs/c2a1c10068de8e3588825543b13a89a6_thumb.jpg', 'Scenery', 'Scenery Art', 0, 10, 0, 1, 1461693628, 1464601581, 1),
(3, 'Desert Sniper', '0', 25, 0, 0, 50, 1.00, 1.00, 1, 1, 0, 0, 1, 1, '/upload/images/products/98c47da419e7cb1e23f5a267094d230b.JPEG', 'upload/images/products/thumbs/98c47da419e7cb1e23f5a267094d230b_thumb.JPEG', 'upload/images/products/thumbs/98c47da419e7cb1e23f5a267094d230b_thumb.JPEG', 'This Is A Game', 'Sniper Game', 0, 10, 0, 1, 1461692177, 0, 1),
(4, 'Shirt', '0', 5, 0, 0, 100, 2.00, 5.00, 5, 0, 0, 0, 0, 0, '/upload/images/products/ca2c09758aef206c68a8168b0414a954.jpg', 'upload/images/products/thumbs/ca2c09758aef206c68a8168b0414a954_thumb.jpg', 'upload/images/products/thumbs/ca2c09758aef206c68a8168b0414a954_thumb.jpg', 'Cotton Shirt', 'Made in bangladesh', 0, 10, 0, 1, 1462298049, 0, 1),
(5, 'Shoe', '0', 5, 0, 0, 100, 1.00, 5.00, 5, 0, 0, 0, 0, 0, '/upload/images/products/6934fcdc2c7ea964e8a423b259abbc5b.jpg', 'upload/images/products/thumbs/6934fcdc2c7ea964e8a423b259abbc5b_thumb.jpg', 'upload/images/products/thumbs/6934fcdc2c7ea964e8a423b259abbc5b_thumb.jpg', 'Leather Shoe', 'Made in bangladesh', 0, 10, 0, 1, 1462297987, 0, 1),
(6, 'Chocolate', '0', 4, 0, 0, 100, 2.00, 3.00, 3, 0, 0, 0, 0, 0, '/upload/images/products/cba74aaab99da8302e889776935c9dcf.png', 'upload/images/products/thumbs/cba74aaab99da8302e889776935c9dcf_thumb.png', 'upload/images/products/thumbs/cba74aaab99da8302e889776935c9dcf_thumb.png', 'Tasty', 'Chocolate bars imported, don''t grow in bangladesh', 0, 10, 0, 1, 1462298187, 0, 1),
(7, 'Woolen shoes', '0', 0, 0, 0, 0, 1.00, 5.00, 1, NULL, 0, NULL, NULL, NULL, 'upload/images/products/4bbe783d2727badf8408b9a09a589ebf.JPG', 'upload/images/products/thumbs/4bbe783d2727badf8408b9a09a589ebf_thumb.JPG', 'upload/images/products/thumbs/4bbe783d2727badf8408b9a09a589ebf_thumb.JPG', 'made of wool', 'asd', 0, 10, 0, 0, 1464600546, 0, 1),
(8, 'Woolen shoes', '0', 1, 1, 0, 100, 0.00, 5.00, 1, NULL, 0, NULL, NULL, NULL, 'upload/images/products/baeffdea46bf8b0e6bbb559dfeb98737.JPG', 'upload/images/products/thumbs/baeffdea46bf8b0e6bbb559dfeb98737_thumb.JPG', 'upload/images/products/thumbs/baeffdea46bf8b0e6bbb559dfeb98737_thumb.JPG', 'hgfhfg', 'cvxvbxbvxbv', 0, 10, 0, 1, 1464602229, 1464604114, 1),
(9, 'Playstation 3', '0', 1, 1, 0, 100, 1.00, 1.00, 1, NULL, 0, NULL, NULL, NULL, 'upload/images/products/4aa91f5283a2186c1dbf9941eda4a0fa.png', 'upload/images/products/thumbs/4aa91f5283a2186c1dbf9941eda4a0fa_thumb.png', 'upload/images/products/thumbs/4aa91f5283a2186c1dbf9941eda4a0fa_thumb.png', 'sadasdas', 'dasdasdasdasdasdasd', 0, 10, 0, 1, 1464604212, 0, 1),
(10, 'Smart Watch', '0', 1, 1, 0, 1, 1.00, 2.00, 2, NULL, 0, NULL, NULL, NULL, 'upload/images/products/3eb3a6dce55069ac231ef8820225bc84.png', 'upload/images/products/thumbs/3eb3a6dce55069ac231ef8820225bc84_thumb.png', 'upload/images/products/thumbs/3eb3a6dce55069ac231ef8820225bc84_thumb.png', 'sdsda', 'sdsadas', 0, 10, 0, 1, 1465537456, 0, 1),
(11, 'test', '0', 1, 1, 0, 1, 1.00, 1.00, 1, NULL, 0, NULL, NULL, NULL, 'upload/images/products/0346bc7aabde418e07ceafae5d2b9340.jpg', 'upload/images/products/thumbs/0346bc7aabde418e07ceafae5d2b9340_thumb.jpg', 'upload/images/products/thumbs/0346bc7aabde418e07ceafae5d2b9340_thumb.jpg', '1', '1', 0, 10, 0, 1, 1465810759, 0, 1),
(12, 't', '0', 1, 1, 0, 1, 1.00, 1.00, 1, NULL, 0, NULL, NULL, NULL, 'upload/images/products/a24ca8014b5ec47cae3a53e2de6a1343.jpg', 'upload/images/products/thumbs/a24ca8014b5ec47cae3a53e2de6a1343_thumb.jpg', 'upload/images/products/thumbs/a24ca8014b5ec47cae3a53e2de6a1343_thumb.jpg', '1', '1', 0, 10, 0, 1, 1465810837, 0, 1),
(13, 't', '0', 1, 1, 0, 1, 1.00, 1.00, 1, NULL, 0, NULL, NULL, NULL, 'upload/images/products/f87820ca08c156a58dc197dbea927e11.jpg', 'upload/images/products/thumbs/f87820ca08c156a58dc197dbea927e11_thumb.jpg', 'upload/images/products/thumbs/f87820ca08c156a58dc197dbea927e11_thumb.jpg', '1', '1', 0, 10, 0, 1, 1465810905, 0, 1),
(14, 'tt', '0', 1, 1, 0, 1, 1.00, 1.00, 1, NULL, 0, NULL, NULL, NULL, 'upload/images/products/49af8e65ed8a9104fe7a5d9c0f794612.jpg', 'upload/images/products/thumbs/49af8e65ed8a9104fe7a5d9c0f794612_thumb.jpg', 'upload/images/products/thumbs/49af8e65ed8a9104fe7a5d9c0f794612_thumb.jpg', '1', '1', 0, 10, 0, 1, 1465811456, 0, 1),
(15, 't2', '0', 1, 1, 0, 1, 1.00, 1.00, 1, NULL, 0, NULL, NULL, NULL, 'upload/images/products/fac5be37a93229663042c53fdc90b8ba.JPEG', 'upload/images/products/thumbs/fac5be37a93229663042c53fdc90b8ba_thumb.JPEG', 'upload/images/products/thumbs/fac5be37a93229663042c53fdc90b8ba_thumb.JPEG', '1', '1', 0, 10, 0, 1, 1465811671, 0, 1),
(16, 't4', '0', 1, 1, 0, 1, 1.00, 1.00, 1, NULL, 0, NULL, NULL, NULL, 'upload/images/products/45b3d580c3a9c549f5a35e6d8544df3c.png', 'upload/images/products/thumbs/45b3d580c3a9c549f5a35e6d8544df3c_thumb.png', 'upload/images/products/thumbs/45b3d580c3a9c549f5a35e6d8544df3c_thumb.png', '1', '1', 0, 10, 0, 1, 1465811759, 0, 1),
(17, 'fdgdfgd', '0', 1, 1, 0, 1, 1.00, 1.00, 1, NULL, 0, NULL, NULL, NULL, 'upload/images/products/935065b7e8af92c1c2b23342ba075332.png', 'upload/images/products/thumbs/935065b7e8af92c1c2b23342ba075332_thumb.png', 'upload/images/products/thumbs/935065b7e8af92c1c2b23342ba075332_thumb.png', 'sfsfsdf', 'sfsdfs', 0, 10, 0, 1, 1466150057, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_cart_list`
--

CREATE TABLE `product_cart_list` (
  `product_cart_list_id` int(11) NOT NULL,
  `product_order_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `product_price` double(11,2) NOT NULL,
  `product_thumbimage` varchar(250) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_totalprice` double(11,2) NOT NULL,
  `created_date` varchar(250) NOT NULL,
  `modified_date` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_cart_list`
--

INSERT INTO `product_cart_list` (`product_cart_list_id`, `product_order_id`, `category_id`, `product_id`, `product_name`, `product_price`, `product_thumbimage`, `quantity`, `product_totalprice`, `created_date`, `modified_date`) VALUES
(1, 1, 19, 1, 'product1', 10.00, 'Jellyfish1_thumb.jpg', 1, 10.00, '2013-03-14', '2013-03-14'),
(2, 2, 19, 1, 'product1', 10.00, 'Jellyfish1_thumb.jpg', 2, 20.00, '2013-03-14', '2013-03-14'),
(3, 2, 19, 2, 'product2', 10.00, 'Hydrangeas1_thumb.jpg', 3, 30.00, '2013-03-14', '2013-03-14'),
(4, 3, 19, 1, 'product1', 10.00, 'Jellyfish1_thumb.jpg', 2, 20.00, '2013-03-14', '2013-03-14'),
(5, 4, 19, 1, 'product1', 10.00, 'Jellyfish1_thumb.jpg', 1, 10.00, '2013-03-20', '2013-03-20');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `product_category_id` int(11) NOT NULL,
  `category_title` varchar(255) NOT NULL,
  `category_description` text NOT NULL,
  `showing_order` int(11) NOT NULL,
  `category_publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`product_category_id`, `category_title`, `category_description`, `showing_order`, `category_publish`, `created_date`, `modified_date`, `created_by`) VALUES
(1, 'Services', 'Special services on our site', 1, 1, 0, 0, 0),
(2, 'Photography', 'photography related items will be kept here', 0, 1, 1463586644, 1463586644, 1),
(3, 'Food', 'Food Items', 0, 1, 1463587125, 1463587125, 1),
(4, 'Beverage', 'sdadasd11111111111113223232\r\n', 0, 1, 1464609228, 1464609228, 1),
(5, 'Beverage', 'sdadasd23424324', 0, 0, 1464609236, 1464609236, 1),
(6, 'Beverage', 'sdadasd23424324345345435', 0, 0, 1464609249, 1464609249, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_company`
--

CREATE TABLE `product_company` (
  `product_company_id` int(11) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_description` varchar(500) NOT NULL,
  `created_date` int(11) NOT NULL,
  `company_publish` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_company`
--

INSERT INTO `product_company` (`product_company_id`, `company_name`, `company_description`, `created_date`, `company_publish`) VALUES
(1, 'Samsung', 'n', 0, 1),
(2, 'RFL', 'sssd asdwc casd ', 0, 1),
(3, 'Sony', 'Japanese Company', 1464609761, 1),
(4, 'RFL', 'ssxzczxczxc cccc', 1464609836, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE `product_order` (
  `product_order_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `shipping_person_name` varchar(150) NOT NULL,
  `location` varchar(250) NOT NULL,
  `delivery_date` date NOT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `city` varchar(150) NOT NULL,
  `state` varchar(150) NOT NULL,
  `zip` varchar(100) NOT NULL,
  `country` varchar(256) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `day_phone_a` varchar(20) DEFAULT NULL,
  `services_name` varchar(200) DEFAULT NULL,
  `comments` text,
  `services_rate` double(11,2) DEFAULT NULL,
  `payment_getway` varchar(50) DEFAULT NULL,
  `credit_card_type` varchar(100) DEFAULT NULL,
  `cardholder_name` varchar(250) DEFAULT NULL,
  `creadit_cardno` varchar(50) DEFAULT NULL,
  `expiration_date` int(11) DEFAULT NULL,
  `card_security_code` varchar(50) DEFAULT NULL,
  `subtotal_amount` double(11,2) NOT NULL,
  `tax_amount` double(11,2) DEFAULT NULL,
  `coupon_value` double(11,2) DEFAULT NULL,
  `deposit_value` double(11,2) DEFAULT NULL,
  `tip_value` double(11,2) DEFAULT NULL,
  `delivery_value` double(11,2) DEFAULT NULL,
  `payment_getway_amount` double(11,2) DEFAULT NULL,
  `total_amount` double(11,2) NOT NULL,
  `order_status` varchar(50) DEFAULT NULL,
  `shipping_status` tinyint(1) DEFAULT '0',
  `publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(250) NOT NULL,
  `modified_date` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`product_order_id`, `member_id`, `shipping_person_name`, `location`, `delivery_date`, `phone`, `city`, `state`, `zip`, `country`, `email`, `day_phone_a`, `services_name`, `comments`, `services_rate`, `payment_getway`, `credit_card_type`, `cardholder_name`, `creadit_cardno`, `expiration_date`, `card_security_code`, `subtotal_amount`, `tax_amount`, `coupon_value`, `deposit_value`, `tip_value`, `delivery_value`, `payment_getway_amount`, `total_amount`, `order_status`, `shipping_status`, `publish`, `created_date`, `modified_date`) VALUES
(1, 2, 'Mehedi Hasan', 'Farmgate', '0000-00-00', '3423433333333333', 'dhaka', '', '323232', '18', '', '', 'test', NULL, NULL, 'Mehedi Hasan', '1', '', '', 0, '', 1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, '1', 1, 1, '1466250501', '1466250501'),
(2, 2, 'Mehedi Hasan', 'Farmgate', '0000-00-00', '3423433333333333', 'dhaka', '', '323232', '18', '', '', 'test', NULL, NULL, 'Mehedi Hasan', '1', '', '', 0, '', 1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, '1', 1, 1, '1466250536', '1466250536'),
(3, 2, 'Mehedi Hasan', 'Farmgate', '0000-00-00', '3423433333333333', 'dhaka', '', '323232', '18', '', '', 'test', NULL, NULL, 'Mehedi Hasan', '1', '', '', 0, '', 1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, '1', 1, 1, '1466250751', '1466250751'),
(4, 0, 'Mehedi Hasan', 'Farmgate', '0000-00-00', '3423433333333333', 'dhaka', '', '323232', '18', '', '', 'test', NULL, NULL, 'Mehedi Hasan', '1', '', '', 0, '', 1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, '1', 1, 1, '1466321936', '1466321936'),
(5, 2, 'Mehedi Hasan', 'Farmgate', '0000-00-00', '3423433333333333', 'dhaka', '', '323232', '18', '', '', 'test', NULL, NULL, 'Mehedi Hasan', '1', '', '', 0, '', 1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, '1', 1, 1, '1466322011', '1466322011'),
(6, 0, 't', '', '0000-00-00', '', '', '', '', 'NULL', '', '', 'test', NULL, NULL, 't', '1', '', '', 0, '', 3.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3.00, '1', 1, 1, '1466323057', '1466323057'),
(7, 0, 'dd', 'fghfghfg', '0000-00-00', 'fhfghf', 'fhfghf', '', 'fhfghf', '18', '', '', 'test', NULL, NULL, 'dd', '1', 'fhfghf', 'fhfghf', 0, 'fhff', 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2.00, '1', 1, 1, '1466323169', '1466323169');

-- --------------------------------------------------------

--
-- Table structure for table `referral_price`
--

CREATE TABLE `referral_price` (
  `referral_price_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `price` double NOT NULL,
  `date` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referral_price`
--

INSERT INTO `referral_price` (`referral_price_id`, `user_id`, `price`, `date`) VALUES
(1, 2, 0.025, 1464341349),
(2, 2, 0.025, 1464341383),
(3, 2, 0.025, 1464341545),
(4, 2, 0.025, 1464585628),
(5, 2, 0.025, 1465551982),
(6, 2, 0.025, 1465556322),
(7, 2, 0.025, 1465626823),
(8, 2, 0.025, 1465626934),
(9, 2, 0.025, 1465626983),
(10, 2, 0.025, 1465627046),
(11, 1, 0, 1466080929),
(12, 1, 0, 1466081072);

-- --------------------------------------------------------

--
-- Table structure for table `referral_price_lookup`
--

CREATE TABLE `referral_price_lookup` (
  `price_lookup_id` smallint(6) NOT NULL,
  `level_number` smallint(6) NOT NULL,
  `price_percentage` double NOT NULL,
  `add_date` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referral_price_lookup`
--

INSERT INTO `referral_price_lookup` (`price_lookup_id`, `level_number`, `price_percentage`, `add_date`) VALUES
(1, 1, 50, 0),
(2, 2, 40, 0),
(3, 3, 30, 0),
(4, 4, 20, 0),
(5, 5, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `referral_relations`
--

CREATE TABLE `referral_relations` (
  `relations_id` bigint(20) NOT NULL,
  `parent_id` bigint(20) NOT NULL,
  `child_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referral_relations`
--

INSERT INTO `referral_relations` (`relations_id`, `parent_id`, `child_id`) VALUES
(1, 2, 3),
(2, 3, 4),
(3, 4, 5),
(4, 5, 6),
(5, 6, 7),
(7, 2, 8),
(8, 3, 9),
(9, 4, 10),
(10, 4, 11),
(11, 5, 12),
(12, 5, 13),
(13, 18, 19);

-- --------------------------------------------------------

--
-- Table structure for table `saved_cart`
--

CREATE TABLE `saved_cart` (
  `saved_cart_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cart_master_id` bigint(20) NOT NULL,
  `date` int(11) NOT NULL,
  `publish_flag` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saved_cart`
--

INSERT INTO `saved_cart` (`saved_cart_id`, `user_id`, `cart_master_id`, `date`, `publish_flag`) VALUES
(4, 2, 9, 1466231193, 0),
(5, 2, 11, 1466234098, 0),
(6, 2, 12, 1466234509, 0),
(7, 2, 13, 1466234725, 0),
(8, 2, 14, 1466238521, 0),
(9, 2, 14, 1466238579, 0),
(10, 2, 15, 1466239579, 0),
(11, 2, 16, 1466239594, 0);

-- --------------------------------------------------------

--
-- Table structure for table `share_bonus`
--

CREATE TABLE `share_bonus` (
  `share_bonus_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `fund_amount` double NOT NULL,
  `bonus_publish` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `share_bonus`
--

INSERT INTO `share_bonus` (`share_bonus_id`, `amount`, `fund_amount`, `bonus_publish`) VALUES
(1, 0, 0, 0),
(2, 5, 0, 0),
(3, 10, 1, 0),
(4, 15, 5, 0),
(5, 20, 2, 0),
(6, 1, 10, 1),
(7, 2, 25, 1),
(8, 5, 50, 1),
(9, 7, 100, 1),
(10, 10, 250, 1);

-- --------------------------------------------------------

--
-- Table structure for table `share_buy`
--

CREATE TABLE `share_buy` (
  `share_buy_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `number_of_shares` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `system_percentage_sell` double NOT NULL,
  `system_percentage_buy` double NOT NULL,
  `total_share_price` double NOT NULL,
  `amount_paid_by_buyer` double NOT NULL,
  `amount_paid_by_seller_to_system` double NOT NULL,
  `used_site_income` tinyint(4) DEFAULT NULL COMMENT '1 if site income used to buy or 0 for credit card payment',
  `transaction_id` int(11) DEFAULT NULL COMMENT 'for site income use, this will be empty',
  `payment_method` varchar(150) DEFAULT NULL COMMENT 'for site income use, this will be empty'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `share_buy`
--

INSERT INTO `share_buy` (`share_buy_id`, `user_id`, `number_of_shares`, `buyer_id`, `date`, `system_percentage_sell`, `system_percentage_buy`, `total_share_price`, `amount_paid_by_buyer`, `amount_paid_by_seller_to_system`, `used_site_income`, `transaction_id`, `payment_method`) VALUES
(1, 2, 100, 2, 1464680747, 5, 5, 0, 0, 0, NULL, NULL, NULL),
(2, 2, 100, 2, 1464681190, 5, 5, 200, 210, 10.5, NULL, NULL, NULL),
(3, 2, 100, 2, 1464681268, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(4, 2, 100, 2, 1464686436, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(5, 2, 100, 2, 1464686510, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(6, 2, 100, 2, 1464686695, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(7, 2, 100, 2, 1464686718, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(8, 3, 100, 2, 1464687138, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(9, 3, 100, 2, 1464687189, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(10, 3, 100, 2, 1464687355, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(11, 3, 100, 2, 1464687429, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(12, 3, 100, 2, 1465541122, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(13, 3, 100, 2, 1465541127, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(14, 3, 100, 2, 1465541243, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(15, 3, 100, 2, 1465554405, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(16, 3, 100, 2, 1465557423, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(17, 3, 100, 2, 1465557570, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(18, 3, 100, 2, 1465557837, 5, 5, 200, 210, 10, NULL, NULL, NULL),
(19, 3, 50, 2, 1466060262, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(20, 3, 50, 2, 1466060310, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(21, 3, 50, 2, 1466060363, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(22, 3, 50, 2, 1466060393, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(23, 3, 50, 2, 1466060440, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(24, 3, 50, 2, 1466060466, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(25, 3, 50, 2, 1466060669, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(26, 3, 50, 2, 1466061577, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(27, 3, 50, 2, 1466061626, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(28, 3, 50, 2, 1466061678, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(29, 3, 50, 2, 1466062216, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(30, 3, 50, 2, 1466062428, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL),
(31, 3, 50, 2, 1466062522, 5, 5, 2, 2.1, 0.1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `share_price_history`
--

CREATE TABLE `share_price_history` (
  `price_history_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `per_share_price` double NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `share_price_history`
--

INSERT INTO `share_price_history` (`price_history_id`, `user_id`, `per_share_price`, `date`) VALUES
(1, 2, 5, 1464692589),
(2, 2, 400, 1464692754);

-- --------------------------------------------------------

--
-- Table structure for table `share_sell`
--

CREATE TABLE `share_sell` (
  `share_sell_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `number_of_shares` int(11) NOT NULL,
  `amount` double NOT NULL,
  `date` int(11) NOT NULL,
  `share_sell_publish` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `share_sell`
--

INSERT INTO `share_sell` (`share_sell_id`, `seller_id`, `number_of_shares`, `amount`, `date`, `share_sell_publish`) VALUES
(1, 3, 100, 1, 1463738449, 1),
(2, 3, 50, 2, 1463738449, 0),
(4, 2, 10, 1, 1464691863, 1),
(5, 2, 5, 1, 1464692589, 1),
(6, 2, 5, 1, 1464692753, 1),
(7, 2, 5, 1, 1464694650, 1),
(8, 2, 10, 50, 1466055697, 1);

-- --------------------------------------------------------





--
-- Table structure for table `site_servers`
--

CREATE TABLE `site_servers` (
  `site_server_id` int(11) NOT NULL,
  `site_server_code` varchar(255) NOT NULL,
  `site_server_url` varchar(150) NOT NULL,
  `publish_flag` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_servers`
--

INSERT INTO `site_servers` (`site_server_id`, `site_server_code`, `site_server_url`, `publish_flag`) VALUES
(1, '100', 'http://bikrimela.com/', 1),
(2, '200', 'www.yahoo.com', 0),
(3, '2001', 'www.yahoo.com1', 0),
(4, '2001111', 'www.yahoo.com1111', 0),
(5, '200', 'www.yahoo.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_daily_income`
--

CREATE TABLE `system_daily_income` (
  `income_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_daily_income`
--

INSERT INTO `system_daily_income` (`income_id`, `amount`, `date`) VALUES
(2, 0, 1465626823),
(3, 196, 1465626934),
(4, 294, 1465626983),
(5, 400, 1465627046),
(6, 3, 1467098141),
(7, 3, 1467098256);

-- --------------------------------------------------------

--
-- Table structure for table `system_user_grant`
--

CREATE TABLE `system_user_grant` (
  `grant_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_user_grant`
--

INSERT INTO `system_user_grant` (`grant_id`, `user_id`, `amount`, `date`) VALUES
(1, 3, 1.8090452261307, 1467092196),
(2, 3, 1.8090452261307, 1467092365),
(3, 0, 0.90452261306533, 1467092365),
(4, 2, 1.1859296482412, 1467092365),
(5, 1, 0.10050251256281, 1467092365),
(6, 3, 1.8090452261307, 1467092643),
(7, 0, 0.90452261306533, 1467092643),
(8, 2, 1.1859296482412, 1467092643),
(9, 1, 0.10050251256281, 1467092643),
(10, 3, 0, 1467092648),
(11, 0, 0, 1467092649),
(12, 2, 0, 1467092649),
(13, 1, 0, 1467092649),
(14, 3, 0, 1467092654),
(15, 0, 0, 1467092654),
(16, 2, 0, 1467092654),
(17, 1, 0, 1467092654),
(18, 3, 0, 1467092659),
(19, 0, 0, 1467092659),
(20, 2, 0, 1467092660),
(21, 1, 0, 1467092660),
(22, 3, 0, 1467092665),
(23, 0, 0, 1467092665),
(24, 2, 0, 1467092665),
(25, 1, 0, 1467092665),
(26, 3, 0, 1467092671),
(27, 0, 0, 1467092671),
(28, 2, 0, 1467092671),
(29, 1, 0, 1467092671),
(30, 3, 0, 1467092676),
(31, 0, 0, 1467092676),
(32, 2, 0, 1467092676),
(33, 1, 0, 1467092677),
(34, 3, 0, 1467092682),
(35, 0, 0, 1467092682),
(36, 2, 0, 1467092682),
(37, 1, 0, 1467092682),
(38, 3, 1.8090452261307, 1467092684),
(39, 0, 0.90452261306533, 1467092684),
(40, 2, 1.1859296482412, 1467092684),
(41, 1, 0.10050251256281, 1467092684),
(42, 3, 1.8090452261307, 1467092689),
(43, 0, 0.90452261306533, 1467092689),
(44, 2, 1.1859296482412, 1467092689),
(45, 1, 0.10050251256281, 1467092690),
(46, 3, 0, 1467092695),
(47, 0, 0, 1467092695),
(48, 2, 0, 1467092695),
(49, 1, 0, 1467092695),
(50, 3, 0, 1467092700),
(51, 0, 0, 1467092700),
(52, 2, 0, 1467092700),
(53, 1, 0, 1467092700),
(54, 3, 0, 1467092706),
(55, 0, 0, 1467092706),
(56, 2, 0, 1467092706),
(57, 1, 0, 1467092706),
(58, 3, 0, 1467092711),
(59, 0, 0, 1467092711),
(60, 2, 0, 1467092711),
(61, 1, 0, 1467092711),
(62, 3, 0, 1467092717),
(63, 0, 0, 1467092717),
(64, 2, 0, 1467092717),
(65, 1, 0, 1467092717),
(66, 3, 0, 1467092722),
(67, 0, 0, 1467092722),
(68, 2, 0, 1467092722),
(69, 1, 0, 1467092722),
(70, 3, 0, 1467092728),
(71, 0, 0, 1467092728),
(72, 2, 0, 1467092728),
(73, 1, 0, 1467092728),
(74, 3, 0, 1467092733),
(75, 0, 0, 1467092733),
(76, 2, 0, 1467092733),
(77, 1, 0, 1467092733),
(78, 3, 0, 1467092739),
(79, 0, 0, 1467092739),
(80, 2, 0, 1467092739),
(81, 1, 0, 1467092739),
(82, 3, 0, 1467092744),
(83, 0, 0, 1467092744),
(84, 2, 0, 1467092744),
(85, 1, 0, 1467092744),
(86, 3, 0, 1467092750),
(87, 0, 0, 1467092750),
(88, 2, 0, 1467092750),
(89, 1, 0, 1467092750),
(90, 3, 0, 1467092755),
(91, 0, 0, 1467092755),
(92, 2, 0, 1467092755),
(93, 1, 0, 1467092755),
(94, 3, 0, 1467092761),
(95, 0, 0, 1467092761),
(96, 2, 0, 1467092761),
(97, 1, 0, 1467092761),
(98, 3, 0, 1467092766),
(99, 0, 0, 1467092766),
(100, 2, 0, 1467092766),
(101, 1, 0, 1467092766),
(102, 3, 0, 1467092772),
(103, 0, 0, 1467092772),
(104, 2, 0, 1467092772),
(105, 1, 0, 1467092772),
(106, 3, 0, 1467092777),
(107, 0, 0, 1467092777),
(108, 2, 0, 1467092777),
(109, 1, 0, 1467092777),
(110, 3, 0, 1467092783),
(111, 0, 0, 1467092783),
(112, 2, 0, 1467092783),
(113, 1, 0, 1467092783),
(114, 3, 0, 1467092788),
(115, 0, 0, 1467092788),
(116, 2, 0, 1467092788),
(117, 1, 0, 1467092788),
(118, 3, 0, 1467092794),
(119, 0, 0, 1467092794),
(120, 2, 0, 1467092794),
(121, 1, 0, 1467092794),
(122, 3, 0, 1467092799),
(123, 0, 0, 1467092799),
(124, 2, 0, 1467092799),
(125, 1, 0, 1467092799),
(126, 3, 0, 1467092805),
(127, 0, 0, 1467092805),
(128, 2, 0, 1467092805),
(129, 1, 0, 1467092805),
(130, 3, 1.8090452261307, 1467092809),
(131, 0, 0.90452261306533, 1467092809),
(132, 2, 1.1859296482412, 1467092809),
(133, 1, 0.10050251256281, 1467092809),
(134, 3, 1.8090452261307, 1467092963),
(135, 0, 0.90452261306533, 1467092963),
(136, 2, 1.1859296482412, 1467092963),
(137, 1, 0.10050251256281, 1467092964),
(138, 3, 0, 1467092969),
(139, 0, 0, 1467092969),
(140, 2, 0, 1467092969),
(141, 1, 0, 1467092969),
(142, 3, 0, 1467092974),
(143, 0, 0, 1467092974),
(144, 2, 0, 1467092975),
(145, 1, 0, 1467092975),
(146, 3, 0, 1467092980),
(147, 0, 0, 1467092980),
(148, 2, 0, 1467092980),
(149, 1, 0, 1467092980),
(150, 3, 0, 1467092985),
(151, 0, 0, 1467092985),
(152, 2, 0, 1467092985),
(153, 1, 0, 1467092986),
(154, 3, 0, 1467092991),
(155, 0, 0, 1467092991),
(156, 2, 0, 1467092991),
(157, 1, 0, 1467092991),
(158, 3, 0, 1467092996),
(159, 0, 0, 1467092996),
(160, 2, 0, 1467092997),
(161, 1, 0, 1467092997),
(162, 3, 1.8090452261307, 1467094195),
(163, 0, 0.90452261306533, 1467094195),
(164, 2, 1.1859296482412, 1467094195),
(165, 1, 0.10050251256281, 1467094195),
(166, 3, 1.8090452261307, 1467094776),
(167, 0, 0.90452261306533, 1467094776),
(168, 2, 1.1859296482412, 1467094776),
(169, 1, 0.10050251256281, 1467094776),
(170, 3, 1.8090452261307, 1467098111),
(171, 0, 0.90452261306533, 1467098111),
(172, 2, 1.1859296482412, 1467098111),
(173, 1, 0.10050251256281, 1467098111),
(174, 3, 1.8090452261307, 1467098141),
(175, 0, 0.90452261306533, 1467098141),
(176, 2, 1.1859296482412, 1467098141),
(177, 1, 0.10050251256281, 1467098141),
(178, 3, 1.8090452261307, 1467098255),
(179, 0, 0.90452261306533, 1467098255),
(180, 2, 1.1859296482412, 1467098256),
(181, 1, 0.10050251256281, 1467098256),
(182, 3, 1.8090452261307, 1467098418),
(183, 0, 0.90452261306533, 1467098419),
(184, 2, 1.1859296482412, 1467098419),
(185, 1, 0.10050251256281, 1467098419),
(186, 3, 2.713567839196, 1467098446),
(187, 0, 1.356783919598, 1467098446),
(188, 2, 1.7788944723618, 1467098446),
(189, 1, 0.15075376884422, 1467098446);

-- --------------------------------------------------------

--
-- Table structure for table `total_current_cash`
--

CREATE TABLE `total_current_cash` (
  `cash_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `current_amount` double NOT NULL,
  `bonus_amount` double DEFAULT NULL,
  `date` int(11) NOT NULL,
  `system_bonus` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_current_cash`
--

INSERT INTO `total_current_cash` (`cash_id`, `user_id`, `current_amount`, `bonus_amount`, `date`, `system_bonus`) VALUES
(1, 2, 1.7788944723618, NULL, 1467098446, 16.010050251256),
(2, 3, 2.713567839196, NULL, 1467098446, 26.231155778897),
(3, 0, 1.356783919598, NULL, 1467098446, 12.211055276382),
(4, 1, 0.15075376884422, NULL, 1467098446, 1.3567839195979);

-- --------------------------------------------------------

--
-- Table structure for table `user_fund`
--

CREATE TABLE `user_fund` (
  `user_fund_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fund_amount` int(11) NOT NULL,
  `bonus_id` int(11) NOT NULL,
  `payment_method` varchar(50) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `gateway_company` varchar(50) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_fund`
--

INSERT INTO `user_fund` (`user_fund_id`, `user_id`, `fund_amount`, `bonus_id`, `payment_method`, `transaction_id`, `gateway_company`, `date`) VALUES
(1, 2, 0, 2, '1', 3, 'xx', 1464699834);

-- --------------------------------------------------------

--
-- Table structure for table `user_price_history`
--

CREATE TABLE `user_price_history` (
  `user_id` bigint(20) NOT NULL,
  `price` double NOT NULL,
  `share` int(11) NOT NULL,
  `referral_price` double NOT NULL,
  `date` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_price_history`
--

INSERT INTO `user_price_history` (`user_id`, `price`, `share`, `referral_price`, `date`) VALUES
(2, 0.05, 0, 0, 1464339080),
(2, 0.05, 0, 0, 1464340211),
(2, 0.05, 0, 0, 1464340376),
(2, 0.05, 0, 0, 1464340405),
(2, 0.05, 0, 0, 1464340542),
(2, 0.05, 0, 0.025, 1464340618),
(2, 0.05, 0, 0.025, 1464340776),
(2, 0.05, 0, 0.025, 1464340924),
(2, 0.05, 0, 0.025, 1464341349),
(2, 0.05, 0, 0.025, 1464341383),
(2, 0.075, 0, 0.025, 1464341545),
(2, 0.1, 0, 0.025, 1464585628),
(2, 0.125, 0, 0.025, 1465551982),
(2, 0.15, 0, 0.025, 1465556322),
(2, 0.175, 0, 0.025, 1465626822),
(2, 0.2, 0, 0.025, 1465626934),
(2, 0.225, 0, 0.025, 1465626983),
(2, 0.25, 0, 0.025, 1465627046),
(1, 0, 0, 0, 1466080929),
(1, 0, 0, 0, 1466081072);

-- --------------------------------------------------------

--
-- Table structure for table `user_share_balance`
--

CREATE TABLE `user_share_balance` (
  `user_share_balance_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `current_share_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_share_balance`
--

INSERT INTO `user_share_balance` (`user_share_balance_id`, `user_id`, `date`, `current_share_number`) VALUES
(5, 3, 1466062522, 900),
(9, 0, 1466060310, 450),
(10, 2, 1466322011, 590),
(11, 1, 1466323169, 50);

-- --------------------------------------------------------

--
-- Table structure for table `user_shipping`
--

CREATE TABLE `user_shipping` (
  `userID` int(11) NOT NULL,
  `address` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` int(11) NOT NULL,
  `zip_code` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_shipping`
--

INSERT INTO `user_shipping` (`userID`, `address`, `city`, `country`, `zip_code`, `phone`, `name`) VALUES
(1, 'fghfghfg', 'fhfghf', 18, 'fhfghf', 'fhfghf', 'dd'),
(2, 'Farmgate', 'dhaka', 18, '323232', '3423433333333333', 'Mehedi Hasan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertise`
--
ALTER TABLE `advertise`
  ADD PRIMARY KEY (`advertise_id`);

--
-- Indexes for table `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`userID`),
  ADD KEY `user_id` (`email`);

--
-- Indexes for table `cart_master`
--
ALTER TABLE `cart_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_withdraw_history`
--
ALTER TABLE `cash_withdraw_history`
  ADD PRIMARY KEY (`withdraw_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `click_calculation`
--
ALTER TABLE `click_calculation`
  ADD PRIMARY KEY (`click_calculation_id`);

--
-- Indexes for table `client_cash`
--
ALTER TABLE `client_cash`
  ADD PRIMARY KEY (`cash_id`);

--
-- Indexes for table `client_shares`
--
ALTER TABLE `client_shares`
  ADD PRIMARY KEY (`shares_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `current_share_price`
--
ALTER TABLE `current_share_price`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `manage_client_advertise`
--
ALTER TABLE `manage_client_advertise`
  ADD PRIMARY KEY (`manage_client_advertise_id`);

--
-- Indexes for table `price_lookup`
--
ALTER TABLE `price_lookup`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_cart_list`
--
ALTER TABLE `product_cart_list`
  ADD PRIMARY KEY (`product_cart_list_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`product_category_id`);

--
-- Indexes for table `product_company`
--
ALTER TABLE `product_company`
  ADD PRIMARY KEY (`product_company_id`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`product_order_id`);

--
-- Indexes for table `referral_price`
--
ALTER TABLE `referral_price`
  ADD PRIMARY KEY (`referral_price_id`);

--
-- Indexes for table `referral_price_lookup`
--
ALTER TABLE `referral_price_lookup`
  ADD PRIMARY KEY (`price_lookup_id`);

--
-- Indexes for table `referral_relations`
--
ALTER TABLE `referral_relations`
  ADD PRIMARY KEY (`relations_id`);

--
-- Indexes for table `saved_cart`
--
ALTER TABLE `saved_cart`
  ADD PRIMARY KEY (`saved_cart_id`);

--
-- Indexes for table `share_bonus`
--
ALTER TABLE `share_bonus`
  ADD PRIMARY KEY (`share_bonus_id`);

--
-- Indexes for table `share_buy`
--
ALTER TABLE `share_buy`
  ADD PRIMARY KEY (`share_buy_id`);

--
-- Indexes for table `share_price_history`
--
ALTER TABLE `share_price_history`
  ADD PRIMARY KEY (`price_history_id`);

--
-- Indexes for table `share_sell`
--
ALTER TABLE `share_sell`
  ADD PRIMARY KEY (`share_sell_id`);



--
-- Indexes for table `site_servers`
--
ALTER TABLE `site_servers`
  ADD PRIMARY KEY (`site_server_id`);

--
-- Indexes for table `system_daily_income`
--
ALTER TABLE `system_daily_income`
  ADD PRIMARY KEY (`income_id`);

--
-- Indexes for table `system_user_grant`
--
ALTER TABLE `system_user_grant`
  ADD PRIMARY KEY (`grant_id`);

--
-- Indexes for table `total_current_cash`
--
ALTER TABLE `total_current_cash`
  ADD PRIMARY KEY (`cash_id`);

--
-- Indexes for table `user_fund`
--
ALTER TABLE `user_fund`
  ADD PRIMARY KEY (`user_fund_id`);

--
-- Indexes for table `user_share_balance`
--
ALTER TABLE `user_share_balance`
  ADD PRIMARY KEY (`user_share_balance_id`);

--
-- Indexes for table `user_shipping`
--
ALTER TABLE `user_shipping`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertise`
--
ALTER TABLE `advertise`
  MODIFY `advertise_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `app_users`
--
ALTER TABLE `app_users`
  MODIFY `userID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `cart_master`
--
ALTER TABLE `cart_master`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `cash_withdraw_history`
--
ALTER TABLE `cash_withdraw_history`
  MODIFY `withdraw_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `click_calculation`
--
ALTER TABLE `click_calculation`
  MODIFY `click_calculation_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `client_cash`
--
ALTER TABLE `client_cash`
  MODIFY `cash_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `client_shares`
--
ALTER TABLE `client_shares`
  MODIFY `shares_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=239;
--
-- AUTO_INCREMENT for table `current_share_price`
--
ALTER TABLE `current_share_price`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `manage_client_advertise`
--
ALTER TABLE `manage_client_advertise`
  MODIFY `manage_client_advertise_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `price_lookup`
--
ALTER TABLE `price_lookup`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `product_cart_list`
--
ALTER TABLE `product_cart_list`
  MODIFY `product_cart_list_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `product_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `product_company`
--
ALTER TABLE `product_company`
  MODIFY `product_company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
  MODIFY `product_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `referral_price`
--
ALTER TABLE `referral_price`
  MODIFY `referral_price_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `referral_price_lookup`
--
ALTER TABLE `referral_price_lookup`
  MODIFY `price_lookup_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `referral_relations`
--
ALTER TABLE `referral_relations`
  MODIFY `relations_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `saved_cart`
--
ALTER TABLE `saved_cart`
  MODIFY `saved_cart_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `share_bonus`
--
ALTER TABLE `share_bonus`
  MODIFY `share_bonus_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `share_buy`
--
ALTER TABLE `share_buy`
  MODIFY `share_buy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `share_price_history`
--
ALTER TABLE `share_price_history`
  MODIFY `price_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `share_sell`
--
ALTER TABLE `share_sell`
  MODIFY `share_sell_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--

--
-- AUTO_INCREMENT for table `site_servers`
--
ALTER TABLE `site_servers`
  MODIFY `site_server_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `system_daily_income`
--
ALTER TABLE `system_daily_income`
  MODIFY `income_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `system_user_grant`
--
ALTER TABLE `system_user_grant`
  MODIFY `grant_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT for table `total_current_cash`
--
ALTER TABLE `total_current_cash`
  MODIFY `cash_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_fund`
--
ALTER TABLE `user_fund`
  MODIFY `user_fund_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_share_balance`
--
ALTER TABLE `user_share_balance`
  MODIFY `user_share_balance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
