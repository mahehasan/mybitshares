<?php
/**
 * HtmlElements Class
 *
 * This class includes all core functions that are being used to display HTML elements 
 *
 * @author Mahedi Hasan
 */
class html {
	var $CR;

	function __construct(){
		$this->CR =& get_instance();
	}

	/**
	 * show icon link
	 *
	 * This function will be used to display icon link
	 *
	 * @param $link
	 * @param $title = ''
	 * @param $useAjax=''
	 *
	 * @author azizabbas
	 */
	public  function showIconLink($url, $text, $icon_name, $css_class='', $title= '', $link_separator='', $extraParams=''){
		$icon_path = "<img src='".SITE_URL."images/icons/$icon_name' border='0' align='absmiddle'>&nbsp;";
		return $link_separator.'<a href="'.$url.'" title="'.$title.'" class="'.$css_class.'" '.$extraParams.' >'.$icon_path.$text.'</a>';
	}

	/**
	 * show drop down
	 *
	 * This function will be used to display dropdowns and it also accomodate onchange javascript events
	 *
	 * @param $name
	 * @param $dataArray
	 * @param $showEmptyOption=true
	 * @param $selected=''
	 * @param $styleClass='form-control'
	 * @param $id=''
	 * @param $extraParams=''
	 *
	 * @author azizabbas
	 */		
	public  function showDropDown($name, $dataArray, $showEmptyOption=true, $selected='', $styleClass='form-control select2', $id='', $extraParams=''){
		$cssStyle 	= ($styleClass) ? ' class="'.$styleClass.'"' : '';
		$id			= (empty($id)) ? $name : $id;

		//---- output drop down menu
		if(!count($dataArray)) {
			$placeholder = 'data-placeholder="Select..."';
		} else {
			$placeholder = "";
		}

		$rt = '<select name="'.$name.'" id="'.$id.'" '.$cssStyle.' '.$extraParams.' '.$placeholder.'>';
		if($showEmptyOption) $rt .= '<option value=""> Select </option>';

		foreach($dataArray as $key=>$value){
			$rt .= '<option value="'.$key.'"';
            if(is_array($selected) && in_array($key,$selected)){
                $rt .= ' selected ';
            }else if(!is_array($selected) && $selected == $key){
				$rt .= ' selected ';
			}
			$rt .= '>'.$value.'</option>';
		}
		$rt .= '</select>';

		return $rt;
	}

	/**
 	 * show weeks dropdown
	 *
	 * This function will be used to display weeks dropdown
	 *
	 * @param $showWeekOneToTwelve=true
	 * @param $extraParams=''
	 * @param $selectedValue=''
	 * @param $name='week'
	 * @param $id=''
	 * @param $cssClass='form-control select2me'
	 *
	 * @author azizabbas
	 */
	public  function showWeeksDropdown($showWeekOneToTwelve=true, $extraParams='', $selectedValue='', $name='week', $id='', $cssClass='form-control select2me'){

		//---- variables
		$lastWeekOfYear = $this->CR->general->getLastWeekOfYear();
		$startIndex 	= ($showWeekOneToTwelve) ? 0 : 1;
		$firstOptionVal = ($showWeekOneToTwelve) ? -1 : 0;
		$id = empty($id) ? $name : $id;

		//---- compile dropdown
		$select = '<select name="'.$name.'" id="'.$id.'" class="'.$cssClass.'" data-placeholder="Select Week" '.$extraParams.'>
		 	    	 <option value="'.$firstOptionVal.'">Select Week</option>';
		for($i=$startIndex; $i<=$lastWeekOfYear; $i++){
			$optText = ($i==0) ? "Week 1-12" : "Week $i";
			if($selectedValue==$i){$selected="selected";}else{$selected="";}
			$select .= '<option value="'.$i.'" '.$selected.'>'.$optText.'</option>';
		}
		$select .= "</select>";

		return $select;
	}
	/**
	 * show years dropdown
	 *
	 * This function will be used to display years dropdown
	 *
	 * @param $showNoYear=true
	 * @param $extraParams=''
	 * @param $selectedValue=''
	 *
	 * @author azizabbas
	 */
	public  function showYearsDropdown($showNoYear=true, $extraParams='', $selectedValue='', $name='year'){

		//---- variables
		$firstOptionVal = ($showNoYear) ? -1 : 0;

		//---- compile dropdown
		$select = '<select name="'.$name.'" id="'.$name.'" class="form-control select2me" data-placeholder="Select Year" '.$extraParams.'>
		 	    	 <option value="'.$firstOptionVal.'">Select Year</option>';
		if($showNoYear){
			$select .= ' <option value="0">No Year</option>';
		}

		for($i=2011; $i<=date("Y"); $i++){
			if($selectedValue==$i){$selected="selected";}else{$selected="";}
			$select .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
		}
		$select .= "</select>";

		return $select;
	}
	/**
	 * show months dropdown
	 *
	 * This function will be used to display months dropdown
	 *
	 * @param $selectedMonth=''
	 *
	 * @author azizabbas
	 */
	public  function showMonthsDropdown($selectedMonth=''){

		$monthsArr = array('January', 'February', 'March', 'April', 'May', 'June', 'July ', 'August', 'September', 'October', 'November', 'December');

		//---- compile dropdown
		$select = '<select name="month" id="month" class="form-control select2me" data-placeholder="Select Month" '.$extraParams.'>
		 	    	 <option value="">Select Month</option>';
		foreach($monthsArr as $month){
			$selected = ($selectedMonth==$month) ? "selected" : "";
			$select .= '<option value="'.$month.'" '.$selected.'>'.$month.'</option>';
		}
		$select .= "</select>";

		return $select;
	}


	/**
	 * show success msg
	 *
	 * This function is used to display Success Message, user can hide this message once displayed
	 *
	 * @param $message
	 * @param $extraParams
	 * @param $extra_css
	 *
	 * @author azizabbas
	 */	
	public  function showSuccessMsg($message="", $extraParams="", $extra_css=""){
		return '<div '.$extraParams.' class="alert alert-success '.$extra_css.'"><button class="close" data-close="alert"></button>'.$message.'</div>';
	}

	/**
	 * show success note
	 *
	 * This function is used to display Success Note, 
	 *
	 * @param $message
	 * @param $extraParams
	 * @param $extra_css
	 *
	 * @author azizabbas
	 */	
	public  function showSuccessNote($message="", $extraParams="", $extra_css=""){
		return '<div '.$extraParams.' class="note note-success '.$extra_css.'">'.$message.'</div>';
	}

	/**
	 * show error msg
	 *
	 * This function is used to display Error Message, user can hide this message once displayed
	 *
	 * @param $message
	 * @param $extraParams
	 * @param $extra_css
	 *
	 * @author azizabbas
	 */	
	public  function showErrorMsg($message="", $extraParams="", $extra_css=""){
		return '<div '.$extraParams.' class="alert alert-danger '.$extra_css.'"><button class="close" data-close="alert"></button>'.$message.'</div>';
	}

	/**
	 * show error note
	 *
	 * This function is used to display Error Note
	 *
	 * @param $message
	 * @param $extraParams
	 * @param $extra_css
	 *
	 * @author azizabbas
	 */	
	public  function showErrorNote($message="", $extraParams="", $extra_css=""){
		return '<div '.$extraParams.' class="note note-danger '.$extra_css.'">'.$message.'</div>';
	}

	/**
	 * show info msg
	 *
	 * This function is used to display Info Message, user can hide this message once displayed
	 *
	 * @param $message
	 * @param $extraParams
	 * @param $extra_css
	 *
	 * @author azizabbas
	 */
	public  function showInfoMsg($message="", $extraParams="", $extra_css=""){
		return '<div '.$extraParams.' class="alert alert-info '.$extra_css.'"><button class="close" data-close="alert"></button>'.$message.'</div>';
	}

	/**
	 * show info note
	 *
	 * This function is used to display Info Note
	 *
	 * @param $message
	 * @param $extraParams
	 * @param $extra_css
	 *
	 * @author azizabbas
	 */
	public  function showInfoNote($message="", $extraParams="", $extra_css=""){
		return '<div '.$extraParams.' class="note note-info '.$extra_css.'">'.$message.'</div>';
	}

	/**
	 * show warning msg
	 *
	 * This function is used to display Warning Message, user can hide this message once displayed
	 *
	 * @param $message
	 * @param $extraParams
	 * @param $extra_css
	 *
	 * @author azizabbas
	 */	
	public  function showWarningMsg($message="", $extraParams="", $extra_css=""){
		return '<div '.$extraParams.' class="alert alert-warning '.$extra_css.'"><button class="close" data-close="alert"></button>'.$message.'</div>';
	}

	/**
	 * show warning note
	 *
	 * This function is used to display Warning Note
	 *
	 * @param $message
	 * @param $extraParams
	 * @param $extra_css
	 *
	 * @author azizabbas
	 */	
	public  function showWarningNote($message="", $extraParams="", $extra_css=""){
		return '<div '.$extraParams.' class="note note-warning '.$extra_css.'">'.$message.'</div>';
	}

	/**
	 * Get Currency Symbol
	 *
	 * This method will be used to get currency symbol so that we can display on pages
	 *
	 * @param $pid
	 *
	 * @author azizabbas
	 */	
	public  function getCurrencySymbol($pid=""){
		if($pid=="javascript"){
			return "\u00A3";
		}
		elseif($pid=="graphs"){
			//return utf8_encode('?');
			return '�';
		}
		else{
			return "&#xA3;";	// http://webdesign.about.com/od/localization/l/blhtmlcodes-cur.htm
		}
	}
	
	
	public function radioButtons($name, $options, $checked='', $container_id='', $error_div_id='') {

		$html = '		
		<div class="radio-list" data-error-container="#'.$error_div_id.'" id="'.$container_id.'">';
			foreach($options as $option) {
				$title = $option['title'];
				$value = $option['value'] != '' ? $option['value'] : $option['title'];
				$id = $option['id'] != '' ? $option['id'] : str_replace(" ","_",$title.'_'.$value);

				if(trim($checked) == trim($value)) {
					$markChecked = ' checked="checked"';
				} else {
					$markChecked = '';
				}
				
				$html .= '
				<label class="radio-inline">
				<input type="radio" name="'.$name.'" id="'.$id.'" '.$markChecked.' value="'.$option['value'].'"> '.$option['title'].' </label>';
			}
		$html .= '</div>';
		$html .= '<div id="'.$error_div_id.'"></div>';
		return $html;
	}

    public  function displayInlineRadioButtonsArray($name, $dataArr, $checked='', $style='', $class=''){

        $html = '
		<div class="radio-list" data-error-container="#form_2_membership_error">';
        foreach($dataArr as $option=>$v) {
            $value = $v ;
            $id = $option;

            if(trim($checked) == trim($id)) {
                $markChecked = ' checked="checked"';
            } else {
                $markChecked = '';
            }

            $html .= '
				<label class="radio-inline" style="'.$style.'" class="'.$class.'">
				<input type="radio" name="'.$name.'" id="'.$id.'" '.$markChecked.' value="'.$id.'"> '.$value.' </label>';
        }
        $html .= '</div>';
        $html .= '<div id="form_2_membership_error"></div>';

        return $html;
    }
	
	public function yesNoRadioButtons($name, $checked='', $container_id = '', $error_div_id='', $valYes='Yes', $valNo='No', $idYes='', $idNo='') {
		$idYes = $idYes != '' ? $idYes : str_replace(' ','-',$name).''.'Yes';
		$idNo = $idNo != '' ? $idNo : str_replace(' ','-',$name).''.'No';
		
		$options = array(
				array(
					'title' => 'Yes',
					'value' => $valYes,
					'id' => $idYes),
				array(
					'title' => 'No',
					'value' => $valNo,
					'id' => $idNo),
			);
		return $this -> radioButtons($name, $options, $checked, $container_id, $error_div_id);
	}
	
	/**
	 * Show UI radio buttons
	 *
	 * This method will be used to show UI radio buttons
	 *
	 * @param $name
	 * @param $checked=2
	 * @param $active_class_1='custom-ui-state-active-green'
	 * @param $active_class_2='custom-ui-state-active-grey'
	 * @param $val1='Yes',$val2='No'
	 * @param $val2='No'
	 *
	 * @author waqasmushtaq
	 */	
	public  function showRadioButtons($name,$checked=2,$active_class_1='custom-ui-state-active-green',$active_class_2='custom-ui-state-active-grey',$val1='Yes',$val2='No'){
		?>
		<div id="<?=$name?>_div">
		<input type="radio" name="<?=$name?>" id="<?=$name?>_radio1" value="<?=$val1?>" <?php if ( $checked == 1 ){echo 'checked="checked"';} ?> /> <label for="<?=$name?>_radio1" id="<?=$name?>_radio1-label" <?php if ( $checked == 1 ){echo "class='".$active_class_1."'";} ?>><?=$val1?></label> 
		<input type="radio" name="<?=$name?>" id="<?=$name?>_radio2" value="<?=$val2?>" <?php if ( $checked == 2 ){echo 'checked="checked"';} ?> /> <label for="<?=$name?>_radio2" id="<?=$name?>_radio2-label" <?php if ( $checked == 2 ){echo "class='".$active_class_2."'";} ?>><?=$val2?></label>
		</div> 
		
		<script type="text/javascript">
		jQuery(function($){
			$('#<?=$name?>_div').buttonset();
			$('#<?=$name?>_radio1-label').removeClass('ui-state-default');
			$('#<?=$name?>_radio2-label').removeClass('ui-state-default');
			$('#<?=$name?>_radio1-label').addClass('custom-ui-state-disable');
			$('#<?=$name?>_radio2-label').addClass('custom-ui-state-disable');

			$('input[name="<?=$name?>"]').click(function(){
				var this_label_id =$(this).attr('id')+"-label";

				//----- remove disable state clss
				$("#"+this_label_id).removeClass("custom-ui-state-disable");

				if(this_label_id.match("_radio2")){
					other_label_id=this_label_id.replace("radio2","radio1");
					$("#"+this_label_id).addClass("<?=$active_class_2?>");
					$("#"+other_label_id).addClass("custom-ui-state-disable");
					$("#"+other_label_id).removeClass("<?=$active_class_1?>");
				}
				else if(this_label_id.match("_radio1")){
					other_label_id=this_label_id.replace("radio1","radio2");
					$("#"+this_label_id).addClass("<?=$active_class_1?>");
					$("#"+other_label_id).addClass("custom-ui-state-disable");
					$("#"+other_label_id).removeClass("<?=$active_class_2?>");
				}
			});
		});
		</script>
		<?php
	}

	/**
     * display header logo
     *
     * This method will be used to show UI radio buttons
     *
     * @author waqasmushtaq
     */
	public  function displayHeaderLogo(){
		return '<img src="'.SITE_URL.'images/logo_amey.png" alt="logo" class="logo-default" style="padding:0px; margin:0px;"/>';
	}

	/**
     * Display Inline Checkboxes
     *
     * This method will be used to display inline checkboxes as per metronic theme
	 *
	 * @param $name
	 * @param $dataArr
	 * @param $checked=''
     *
     * @author azizabbas
     */
	public  function displayInlineChceckBoxes($name, $dataArr, $checked=''){
		$html = '<ul class="list-inline c-checkbox-panel c-padding-2">';
		if(!empty($dataArr)){
			foreach($dataArr as $k=>$v){
				$html .= '<li class="c-padding-2">
						    <input type="checkbox" name="'.$name.'" id="'.substr($name, 0, strlen($name)-2).'_'.$k.'" value="'.$k.'"> '.$v.'
						  </li>';	
			}
		}
		$html .= '</ul>';

		return $html;
	}

    /**
     * This function will generate check box where we can get multiple selected value
     * @param $name
     * @param $dataArr
     * @param string $checked
     * @return string
     *
     * @author Razib Ahmed
     */
    public  function displayInlineChceckBoxesArray($name, $dataArr, $checked='', $style='', $class=''){
		
        $valueArr = '';
        if($checked != '') {
            if (strpos($checked, ',') !== false) {
                $valueArr = explode(',', $checked); // array return
                foreach($valueArr as $key => $value){
                    $valueArr[$key] = trim($value); // reduce white space form value that we are getting through database.
                }
            } else {
                $valueArr = trim($checked); // string return
            }
        }
		
		
        $html = '<ul class="list-inline c-checkbox-panel c-padding-2">';
        if(!empty($dataArr)){
            foreach($dataArr as $k=>$v){
                if($v == "ALL"){
                    $html .= '<li class="c-padding-2" style="'.$style.'"><input type="checkbox" class="'.$class.'" name="' . $name . 'All" id="' . substr($name, 0, strlen($name) - 2) . '_' . $k . '" value="' . $k . '"';
                }else {
                    $html .= '<li class="c-padding-2" style="'.$style.'"><input type="checkbox" class="'.$class.'" name="' . $name . '[]" id="' . substr($name, 0, strlen($name) - 2) . '_' . $k . '" value="' . $k . '"';
                }
                if($checked != '') {
                    if (is_array($valueArr)) {
                        if (in_array($k, $valueArr)) {
                            $html .= " Checked";
                        }
                    } else {
                        if ($valueArr == 'ALL') {
                            $html .= " Checked";
                        } else {							
                            if ($k == $valueArr) { 
								
								$html .= " Checked"; 
							}
                        }
                    }
                }
                $html .= '> <label style="margin-top:-1px;" for="'.substr($name, 0, strlen($name) - 2) . '_' . $k .'">' . $v . '</label></li>';
            }
        }
        $html .= '</ul>';

        return $html;
    }

	/**
     * Display Download CSV Button
     *
     * This method will be used to display download csv button
     *
     * @author azizabbas
     */
	public  function displayDownloadCsvButton($url, $title='Click to download CSV'){
		return '<a href="'.$url.'" class="btn green-meadow" title="'.$title.'">
				  <i class="glyphicon glyphicon-download-alt"> </i> Download CSV
				</a>';
	}


	public  function showHighwayAuthDropDown($userID='', $name='highway_auth', $id='', $cssClass="form-control select2me filter"){

		$result = $this->CR->defect_model->getHighWayAuthList();
		$dataArray = array();
		if(count($result)) {
			foreach($result as $ha) {
				$dataArray[$ha] = $ha;
			}
		}
		$id = empty($id) ? $name : $id;

		//---- compile and return dropdown html
		return $this->showDropDown($name, $dataArray, $showEmptyOption=true, $userID, $cssClass, $id=$id, $extraParams='');
	}

	/**
	 * Show UI radio buttons
	 *
	 * This method will be used to show UI radio buttons
	 * @author ikram
	 */	
	public  function showRadioButtonsNew($name,$checked="",$label1='Yes',$label2='No',$val1='Yes',$val2='No'){
		$yesCheck =  ($checked == $val1)?'checked="checked"':"";
		$noCheck =($checked == $val2)?'checked="checked"':"";

		$html =	'<label class="radio-inline">
                      <input type="radio" name="'.$name.'" id="'.$name.'1" value="'.$val1.'" '.$yesCheck.'>
                      '.$label1.'</label>
                    <label class="radio-inline">
                      <input type="radio" name="'.$name.'" id="'.$name.'2" value="'.$val2.'" '.$noCheck.'>
                      '.$label2.'</label>';

		return $html;

	}

	/**
	 * Show UI calender datePicking
	 *
	 * This method will be used to show calender datePicking
	 * @author ikram
	 */	
	public  function showDatePickingInput($name, $value="", $formate = true, $required=false, $id="", $helper_text='Select date', $readonly = "readonly"){
		if($value == '0000-00-00' || $value == ''){
			$value = '';
		}
		elseif($formate && $value){
			$value =  date('d-M-Y',strtotime($value));
		}
		$required = $required ? 'required' : '';
		if($required) {
			$required = 'required';
			$error = '<div for="'.$name.'" class="error" style="display: none;">This field is required.</div>';
			
			$error = '<div for="'.$name.'" class="error help-block help-block-error" style="display: none;">This field is required.</div>';
		} else {
			$required = '';
			$error = '';
		}
		$id = ( $id != "" ) ? $id : $name;
		$html =	'<div class="input-group date date-picker datepicker" data-date-format="dd-mm-yyyy">
                    <span class="input-group-btn">
                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                    </span> 
					<input type="text" class="form-control" '.$readonly.'  '.$required.' name="'.$name.'" id="'.$id.'" value="'.$value.'">
                    </div>
                    <span class="help-block">'.$helper_text.'</span>
                    '.$error;
		return $html;
	}

	/**
	 * Show bootstrap alert message
	 *
	 * This function can be used instead of showWarningMsg, showInfoMsg separately
	 * 
	 * @author Waqas
	 */	
	public function showMsg($message="", $type, $extra_params="", $extra_css=""){
		return '<div '.$extra_params.' class="alert alert-'.$type.' '.$extra_css.'"><button class="close" data-close="alert"></button>'.$message.'</div>';
	}

    /**
     * Show User Detail
     *
     * This function can be used to show detail of a surveyor, DSP on tooltip
     *
     * @Usman
     */
    public function showUserDetail($userID = 0){
        $html = "";
        if($userID>0){
            $userDetail = $this->CR->user_model->getUserById($userID);
            if(!empty($userDetail)){
                $html = '<table><tr><th>Name: </th><td>'.$userDetail['firstName'].' '.$userDetail['LastName'].'</td></tr><tr><th>Email: </th><td>'.$userDetail["email"].'</td></tr><tr><th>Phone: </th><td>'.$userDetail["phone"].'</td></tr></table>';
            }
        }
        return $html;
    }

    /**
     * Show Inspector Detail
     *
     * This function can be used to show Inspector detail on tooltip
     *
     * @Usman
     */
    public function showInspectorDetail($inspectorID = 0){
        $inspector = "";
        if($inspectorID>0){
            $inspectorDetail = $this->CR->Inspector_model->getById($inspectorID);
            if(!empty($inspectorDetail)){
                $inspector = '<table><tr><th>Name: </th><td>'.$inspectorDetail['name'].'</td></tr><tr><th>Email: </th><td>'.$inspectorDetail["email"].'</td></tr><tr><th>Phone: </th><td>'.$inspectorDetail["phone"].'</td></tr></table>';
            }
        }
        return $inspector;
    }

    /**
     * Show Inspector Detail
     *
     * This function can be used to show Inspector detail on tooltip
     *
     * @Usman
     */
    public function generalTooltipDetail($name="", $email="", $phone=""){
        $html = '<table><tr><th>Name: </th><td>'.$name.'</td></tr><tr><th>Email: </th><td>'.$email.'</td></tr><tr><th>Phone: </th><td>'.$phone.'</td></tr></table>';
        return $html;
    }

    /**
     * Show toolTip Detail
     *
     * This function can be used to show Tooltip data in tabular format
     * It creates only 2 columns. $key is used as name and value is used to be the value of that field.
     *
     * @Usman
     */
    public function generalTooltip($data){
        $html = '';
        if(is_array($data)){
            $html .= '<table>';
            foreach($data as $key=>$value){
                $html .= '<tr><th>'.$key.': </th><td>'.$value.'</td></tr>';
            }
            $html .= '</table>';
        }
        return $html;
    }
    
    public function get_parent_category($parent_categoryval){
    	$str_option = "";
    	$sqlcategory = "select * from category where category_publish = 1 and parent_category = 0 order by category_id asc";
    	$result = $this->CR->category_model->getallrowbysqlquery($sqlcategory);
    	if(count($result)>0 && $result !=''){
    		foreach($result as $row){
    			$category_id = $row->category_id;
    			$category_title = stripslashes($row->category_title);
    
    			$secondstr = '';
    			$secondresult = $this->CR->category_model->getallrowbysqlquery("select * from category where category_publish=1 and parent_category = $category_id");
    			if(count($secondresult)>0 && $secondresult !=''){
    				foreach($secondresult as $secondrow){
    					$secondcategory_id = $secondrow->category_id;
    					$secondcategory_title = stripslashes($secondrow->category_title);
    					$secondselected = '';
    					$thirdstr = '';
    					$thirdresult = $this->CR->category_model->getallrowbysqlquery("select * from category where category_publish=1 and parent_category = $secondcategory_id");
    					if(count($thirdresult)>0 && $thirdresult !=''){
    						foreach($thirdresult as $thirdrow){
    							$thirdcategory_id = $thirdrow->category_id;
    							$thirdcategory_title = stripslashes($thirdrow->category_title);
    							$thirdselected = '';
    							$classname1 = 'selectthirdarrow';
    							if($thirdcategory_id=='$parentcategoryval'){$thirdselected = ' selected="selected"';}
    							$thirdstr .= "<option class=\"$classname1\" value=\"$thirdcategory_id\" $thirdselected>$thirdcategory_title</option>";
    						}
    					}
    					if($thirdstr !=''){$classname1 = 'optionChild';}
    					else{$classname1 = 'selectsecondarrow';}
    					if($secondcategory_id=='$parent_categoryval'){$secondselected = ' selected="selected"';}
    					$secondstr .= "<option class=\"$classname1\" value=\"$secondcategory_id\" $secondselected>$secondcategory_title</option>".$thirdstr;
    				}
    			}
    			if($secondstr !=''){$classname1 = 'optionGroup';}
    			else{$classname1 = 'selectfirstarrow';}
    			$firstselected = '';
    			if($category_id==$parent_categoryval){$firstselected = ' selected="selected"';}
    				
    			$firstparentstr = '<option class="'.$classname1.'" value="'.$category_id.'" '.$firstselected.'>'.$category_title.'</option>';
    
    			$str_option .= $firstparentstr.$secondstr;
    		}
    		return $str_option;
    	}
    }

}

?>