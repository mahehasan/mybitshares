<?php
class Query_manager{

	/**
	 * Class Variables
	 */
	var $CR;
	var $timeStamp;
	public  $counter = 0;
	public $data = array();
	var $insertVals = array();
	var $insertKeys = array();
	var $updateVals;
	var $updateWhere;

	function __construct(){
			//global $CR;
			$this->CR =& get_instance();
	}
	/**
	 * select tbl
	 *
	 * This function is being used to select a db table
	 *
	 * @param $tblName
	 */
	public function selectTbl($tblName){
		$this->tblName = $tblName;
		$this->timeStamp = time();
	}

	/**
	 * get fields
	 *
	 * This function is being used to get fields/columns from a db table
	 *
	 * @param $tblName
	 */
	public function getFields($tblName){
		$this->data = NULL;
		$this->counter = 0;
		$result = $this->CR->db ->getTableFields($tblName);
		if(!$result){
			echoError("Failed getting field names for '".$tblName."'|".__FUNCTION__."|".__LINE__."|".get_class($this)."|".$tblName."|".mysql_error());
			return false;
		}
		else{
			$this->counter =  count($result);
			$this->data = $result;
			return true;
		}
	}
	
	/**
	 * get fields public
	 *
	 * This function is being used to get fields/columns from a db table. Its same as getFields(), its just Public version of it.
	 *
	 * @param $tblName
	 */
	public function getFieldsPublic($tblName){
		return $this->getFields($tblName);
	}

	/**
	 * check table
	 *
	 * This function is being used to check if a db table exists or not
	 *
	 * @param $tblName
	 */
	public function checkTable($tblName){
		$sql = "show tables from `".$this->CR->db->dbname."` ";
		$this->CR->db->query($sql);
		$result = $this->CR->db->resultset();
		if(!$result){
			echoError("Failed query showing tables|".__FUNCTION__."|".__LINE__."|".get_class($this)."|".$tblName."|".mysql_error());
			return false;
		}
		else{
			foreach($result as $obj){
				if($obj['Tables_in_'.$this->CR->db->dbname] == $tblName){
					return true;
					break;
				}
			}
			$result->close();
		}
	}

	/**
	 * check table public
	 *
	 * This function is being used to check if a db table exists or not. Its same as checkTable(), its just Public version of it.
	 *
	 * @param $tblName
	 */
	public function checkTablePublic($tblName){
		return $this->checkTable($tblName);
	}

	/**
	 * save record
	 *
	 * This function is being used to insert new record in db table
	 *
	 * @param $postedData
	 */
	public function saveRecord($postedData){
		
		$postedData['lastEdit'] = date("Y-m-d H:i:s");

		if(!$check = $this->checkTable($this->tblName)){
			echoError("Invalid table name ''".$this->tblName."'' requested|".__FUNCTION__."|".__LINE__."|".get_class($this)."||");
			exit;
		}
		
		if(!$getFieldCheck = $this->getFields($this->tblName)){
			echoError("Failed getting field names for '".$this->tblName."'|".__FUNCTION__."|".__LINE__."|".get_class($this)."||");
			exit;
		}
		
		if(is_array($postedData)){
			foreach($postedData as $key => $value){
				if(in_array($key,$this->data)){
					$value = $db->escape_string($value);
					$insertVals[] = $this->validateSqlInput($key,$value);
					$insertKeys[] = $key;
				}
			}
		}
		else{
			echoError("Sending empty posted array values|".__FUNCTION__."|".__LINE__."|".get_class($this)."|".var_dump($postedData)."|");
			exit;
		}
		
		$sql = "insert into ".$this->tblName." (".implode(",",$insertKeys).") values ('".implode("','",$insertVals)."')";
		$result = $this->CR->db -> runSQL($sql);
		if(!$result){
			echoError("Failed insert query|".__FUNCTION__."|".__LINE__."|".get_class($this)."|".$sql."|".mysql_error());
			return false;
		}
		else{
			return $db->insert_id;
		}
	}
	
	/**
	 * save record v2
	 *
	 * This is improved version of saveRecord(),  In this function we are not calling validateSqlInput() because that is causing issues for normal db inserttions
	 *
	 * @param $postedData
	 */
	public function saveRecordV2($postedData){
		
		$result = $this->CR->db->insert($this->tblName,$postedData);
		if(!$result){
			$this->phpAlert("Failed insert query|".__FUNCTION__."|".__LINE__."|".get_class($this)."|".$sql."|".mysql_error());
			return false;
		}else{
			return $result;
		}
	}

    public function saveRecordV3($postedData){

        $result = $this->CR->db->insert($this->tblName,$postedData);
        if(!$result){
            $this->phpAlert("Failed insert query|".__FUNCTION__."|".__LINE__."|".get_class($this)."|".$sql."|".mysql_error());
            return false;
        }else{
            return $this->CR->db->insert_id();
        }
    }
	
	/**
	 * update record
	 *
	 * This function is being used to update existing record from a db table
	 *
	 * @param $updateData
	 * @param $primaryKeyField	represents primary field to be used to update record(s)
	 */
	public function updateRecord($updateData, $primaryKeyField='ID'){
		
		$updateData['lastEdit'] = date("Y-m-d H:i:s");

		if(!$check = $this->checkTable($this->tblName)){
			echoError("Invalid table name ''".$this->tblName."'' requested|".__FUNCTION__."|".__LINE__."|".get_class($this)."||");
			exit;
		}
		
		if(!$getFieldCheck = $this->getFields($this->tblName)){
			echoError("Failed getting field names for '".$this->tblName."'|".__FUNCTION__."|".__LINE__."|".get_class($this)."||");
			exit;
		}
		
		if(is_array($updateData)){
			foreach($updateData as $key => $value){
				if(in_array($key,$this->data)){
					$value = $db->escape_string($value);
					if($key!=$primaryKeyField){
						$value = $this->validateSqlInput($key,$value);
						$updateVals .= "`$key`='$value',";
					}else{
						$updateWhere .= "`$primaryKeyField`='$value'";
						$ID = $value;
					}
				}
			}
			$updateVals = rtrim($updateVals,',');
		}
		else{
			echoError("Sending empty posted array values|".__FUNCTION__."|".__LINE__."|".get_class($this)."|".var_dump($updateData)."|");
			exit;
		}
		
		$sql = "update ".$this->tblName." set ".$updateVals." where ".$updateWhere;
		$result = $this->CR->db -> runSQL($sql);
		if(!$result){
			echoError("Failed update query|".__FUNCTION__."|".__LINE__."|".get_class($this)."|".$sql."|".mysql_error());
			return false;
		}
		else{
			return $ID;
		}
	}

	/**
	 * update record v2
	 *
	 * This is improved version of updateRecord(),  In this function we are not calling validateSqlInput() because that is causing issues for normal db inserttions
	 *
	 * @param $updateData
	 * @param $primaryKeyField	represents primary field to be used to update record(s)
	 */	
	function updateRecordV2($updateData, $primaryKeyField='ID'){
		$updateWhere[$primaryKeyField] = $updateData[$primaryKeyField];
		unset($updateData[$primaryKeyField]);
		$result = $this->CR->db ->update($this->tblName, $updateData, $updateWhere);
		return $result;
		
	}

	/**
	 * validate sql input
	 *
	 * This function is being used to apply some validation and filtering on SQL input data types
	 *
	 * @param $key
	 * @param $value
	 */
	public function validateSqlInput($key,$value){
		if($value=='DATESTAMP'){
			$value = $this->timeStamp;
		}
		elseif(is_numeric($value)){
			$value = $value;
		}
		elseif($key=='pwd'){
			$value = strrev(base64_encode($value));
		}
		elseif($key=='password'){
			$value = md5($value);
		}
		else{
			$value = htmlentities($value);
			$this->autolink($value);
			$value = nl2br($value);
			$value = (get_magic_quotes_gpc()) ? $value : addslashes($value);
		}
		
		return $value;
	}
	
	/**
	 * delete record
	 *
	 * This function is being used to delete an existing record from a db table
	 *
	 * @param $ID				value of the primary field as mentioned in second parameter $primaryKeyField
	 * @param $primaryKeyField	represents primary field to be used to delete record against $ID
	 */
	public function deleteRecord($ID, $primaryKeyField='ID'){
		
		
		if(!$check = $this->checkTable($this->tblName)){
			echoError("Invalid table name ''".$this->tblName."'' requested|".__FUNCTION__."|".__LINE__."|".get_parent_class($this)."||");
			exit;
		}
		
		if(!$getFieldCheck = $this->getFields($this->tblName)){
			echoError("Failed getting field names for '".$this->tblName."'|".__FUNCTION__."|".__LINE__."|".get_parent_class($this)."||");
			exit;
		}

		if(empty($ID)==true){
			echoError("ID is empty|".__FUNCTION__."|".__LINE__."|".get_parent_class($this)."|".$ID."|");
			exit;
		}

		$ID = $db->escape_string($ID);
		$sql = "DELETE FROM ".$this->tblName." WHERE $primaryKeyField='$ID'";
		$result = $this->CR->db -> runSQL($sql);
		if(!$result){
			echoError("Failed delete query|".__FUNCTION__."|".__LINE__."|".get_parent_class($this)."|".$sql."|".mysql_error());
			return false;
		}
		else{
			return true;
		}
	}
	
	/**
	 * db query raw
	 *
	 * This function is being used to execute sql statement. This is most widely used function to execute an SQL statement across the application
	 *
	 * @param $rawSql
	 */
	public function dbQueryRaw($rawSql){
        if($rawSql == '') return false;
		
		$result = $this->CR->db -> runSQL($rawSql);
		if(!$result){
			echoError("Failed raw sql query|".__FUNCTION__."|".__LINE__."|".get_parent_class($this)."|".$rawSql."|".mysql_error());
			return false;
		}
		else{
			return $result;
		}
	}
	
	/**
	 * php alert
	 *
	 * This function is being used to show a javascript alert message. Mostly its being used when some SQL operation throws an error.
	 *
	 * @param $rawSql
	 */
	public  function phpAlert($message){
 		echo '<SCRIPT LANGUAGE="JavaScript">';
		echo 'alert("'.$message.'");';
		echo '</SCRIPT>';
	}
	
	/**
	 * autolink
	 *
	 * @param $text
	 * @param $target
	 * @param $nofollow
	 */
	public function autolink( &$text, $target='_blank', $nofollow=true ){
		// grab anything that looks like a URL...
		$urls  =  $this->_autolink_find_URLS( $text );
		if( !empty($urls) ){ // i.e. there were some URLS found in the text
			@array_walk( $urls, '_autolink_create_html_tags', array('target'=>$target, 'nofollow'=>$nofollow) );
			$text  =  strtr( $text, $urls );
		}
	}
	
	/**
	 * _autolink_find_URLS
	 *
	 * @param $text
	 */
	public function _autolink_find_URLS( $text ){
		// build the patterns
		$scheme         =       '(http:\/\/|https:\/\/)';
		$www            =       'www\.';
		$ip             =       '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}';
		$subdomain      =       '[-a-z0-9_]+\.';
		$name           =       '[a-z][-a-z0-9]+\.';
		$tld            =       '[a-z]+(\.[a-z]{2,2})?';
		$the_rest       =       '\/?[a-z0-9._\/~#&=;%+?-]+[a-z0-9\/#=?]{1,1}';            
		$pattern        =       "$scheme?(?(1)($ip|($subdomain)?$name$tld)|($www$name$tld))$the_rest";
	
		$pattern        =       '/'.$pattern.'/is';
		$c              =       preg_match_all( $pattern, $text, $m );
		unset( $text, $scheme, $www, $ip, $subdomain, $name, $tld, $the_rest, $pattern );
		if( $c ){
			return( array_flip($m[0]) );
		}
		return( array() );
	}
	
	/**
 	 * check table entry
	 *
	 * This function is being used to check whether a record exists in a table or not against provided primary KEY of the table. 
	 * It will return total number of rows found against provided tblField and value.
	 *
	 * @param $queryTbl 		name of the table to be queried
	 * @param $fieldValue 	value of the field to look for
	 * @param $tblField		table field, by default its ID
	 * @param $qry_options	QRY OPTIONS, by default empty
	 *
	 * @author: azizabbas
	 */	
	public  function checkTableEntry($queryTbl, $fieldValue, $tblField="ID", $qry_options=""){
		
		
		//---- check if database table exists or not
		if(!$check = $this->checkTable($queryTbl)){
			echoError("Invalid table name ''".$queryTbl."'' requested|".__FUNCTION__."|".__LINE__."|".get_parent_class($this)."|");
			exit;
		}
		
		//---- run qry and return number of rows
		$sql = "SELECT ".$tblField." FROM ".$queryTbl." WHERE ".$tblField." = '".$fieldValue."' ".$qry_options;
		$result = $this->CR->db -> runSQL($sql);
		return $result->num_rows;
	}
	
	/**
 	 * get key value from table
	 *
	 * This function is being used to get key/value array from a specified table as per provided parameter.
	 *
	 * @param $key 			
	 * @param $value 		
	 * @param $table		
	 * @param $qry_options	
	 *
	 * @author: azizabbas
	 */	
	public  function getKeyValueFromTable($key, $value, $table, $qry_options="", $compileStr=false){
		
		
		$result = array();
		
		//---- run qry and return number of rows
		$sql = "SELECT `".$key."`, `".$value."` FROM `".$table."` ".$qry_options;
		$rs = $this->CR->db -> runSQL($sql);
		foreach($rs as $arr){
			$index = ($compileStr) ? general::compileStringValue($arr[$key]) : $arr[$key];
			$result[$index] = $arr[$value];
		}

		return $result;
	}
	
	 function table_fields($table_name){
	  $query = $this->CR->db->query('show columns from '.$table_name);
	  $result = $query->result();
	
	  return $result;
	 }
	/////////////////////////////////////////
	
	###This returns fields in array. You can use it this way.
	
	//////////////////////////////////////////////////
	function prepare_mysql_data($table_name,$array){
	
	  $table_fields = $this->table_fields($table_name);
	
	  foreach ($table_fields as $field){
	   	$field_array[] = $field->Field;
	  }
	
	  // initialize mysql entry array
	
	  $data = array();
	  foreach ($array as $key => $value){
	   // check if mysql field is there
	   if ( in_array($key, $field_array) ){
		if ( is_array($array[$key]) ){
		 $data[$key] = implode(',',$array[$key]);
		}else{
		 $data[$key] = ($value);
		}
	   }
	  }
	  return $data;
	 }
}
?>