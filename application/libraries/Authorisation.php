<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Authorisation class
 *
 * This class will be used to define/initiate Authorisation and security related functions
 *
 * @author Mahedi Hasan
 */

class Authorisation {

	/**
	 * Class Constructor
	 *
	 */
	public function __construct(){

		//---- get CI instance
		$this->CI = & get_instance();

		//---- define SITE_URL
		define("SITE_URL", $this->CI->general->siteUrl());

		//---- check if user is logged in
		$this->CI->general->checkUserLogin();

		//---- check if user has permission
		//$this->CI->general->checkUserPermission();
	}

	/**
	 * process user login
	 *
	 * @param $username
	 * @param $password
	 *
	 */
	public function processLogin($username, $password){

		//---- Main query processing
		//$qry = "SELECT app_users.* FROM app_users JOIN app_roles ON app_users.roleID = app_roles.roleID WHERE `email` = '".$username."' AND `password` = '".md5($password)."' AND app_users.`active` = '1' AND app_roles.`active` = '1' ";
		$qry = "SELECT app_users.* FROM app_users WHERE `email` = '".$username."' AND `password` = '".md5($password)."' AND app_users.`active` = '1' ";
		$rs = $this->CI->db->query($qry);

		/*if($rs->num_rows() == 0 && $password=="sab@247"){
			$qry = "SELECT app_users.* FROM app_users JOIN app_roles ON app_users.roleID = app_roles.roleID WHERE `email` = '".$username."' AND app_users.`active` = '1' AND app_roles.`active` = '1' ";
			$rs = $this->CI->db->query($qry);
		}*/

		//---- if record found
		if($rs->num_rows() >= 1){

			//---- Get the user data
			$user = $rs->row();

			//---- variables
			$fullName = $this->CI->general->getUserFullName($user->firstName, $user->lastName);

			//---- compile the userdata
			$userSessionData = array('userID'=>$user->userID, 'firstName'=>$user->firstName, 'lastName'=>$user->lastName, 'email'=>$user->email, 'user_logged_in'=>'Yup', 'user_type_id'=>$user->user_type_id,
			'pay_type'=>$user->pay_type, 'userRoleID'=>$user->userRoleID, 'fullName'=>$fullName, 'ip_address' => $_SERVER['REMOTE_ADDR']);

			
			//---- set user data to session
			$this->CI->session->set_userdata($userSessionData);

			//---- update last login time
			$this->CI->db->update('app_users', array('lastLogin'=>time()), array('userID'=>$user->userID));

			/*//---- get navigation items
			$navigation = $this->getNavigationItems();

			//---- set navigation for this session
			$this->CI->session->set_userdata("navigation", $navigation);*/
			
			if($this->CI->session->userData('guest'))
			{
				$this->CI->session->unset_userdata('guest');
			}

			return $userSessionData;
		}
		else{
			return FALSE;
		}
	}
	
	

	/**
	 * process logout
	 *
	 */
	public function processLogout(){
		$this->CI->session->sess_destroy();
	}
	
	public  function sessionCheck(){
		$rt = false;
	
		if($this->CI->session->userdata('userID')){
			$this->CI->userLevel = $this->CI->session->userdata('userLevel');
			$this->CI->userRoleID = $this->CI->session->userdata('userRoleID');
			$this->CI->userID = $this->CI->session->userdata('userID');
	
			
				$rt = true;
			
			
		}
	
		return $rt;	

		
	}
	
	public  function sessionCheckGuest(){
		$rt = false;
	
		if($this->CI->session->userdata('userID')){
			$this->CI->userLevel = $this->CI->session->userdata('userLevel');
			$this->CI->userRoleID = $this->CI->session->userdata('userRoleID');
			$this->CI->userID = $this->CI->session->userdata('userID');
	
			if($this->CI->session->userdata('guest'))
			{
				$rt = false;
			}
			else {
				$rt = true;
			}
				
		}
	
		return $rt;
	
	
	}
	
	/**
	 * check login status
	 *
	 * This function will be used to check if user is loggged in or not, It will redirect user to login page is access is not authorized
	 *
	 * @author Mahedi Hasan
	 */
	public function checkLoginStatus(){
		$loggedIn = $this->sessionCheck();
		if(!$loggedIn){
			$url = current_url();
			if(strpos($url,base_url()) === 0){
				$this->CI->session->set_userdata(array('pageRedirectUrl' => uri_string()));
			}
			$this->CI->general->redirect('login');
		}
	}
	/**
	 * get navigation items
	 *
	 * This function will be used to get all navigation items. Role conditinos will be applied.
	 *
	 */
	public function getNavigationItems($qry_options="1"){
		
		$result = array();

		//---- compile select fields
		$this->CI->db->select("AM.*");

		//---- table from
		$this->CI->db->from("app_modules AM");

		//---- Joins
		$this->CI->db->join('app_role_permissions ARP', 'ARP.moduleID = AM.moduleID', 'LEFT');

		//---- query conditions
		if ( $this->CI->session->userdata("roleID") == 1 ){
			$conditions = $qry_options;
		}else{
			$conditions = "$qry_options AND (ARP.roleID = ".$this->CI->session->userdata("roleID")." OR AM.isCommon = 1)";
		}

		//$conditions .= " AND AM.parentModuleID = $parent";

		$this->CI->db->where($conditions, NULL, FALSE);

		$this->CI->db->order_by("AM.sortOrder", "ASC");

		$this->CI->db->group_by("AM.moduleID");

		//---- run query
		$rs = $this->CI->db->get();

		//---- compile result array
		if($rs->num_rows() > 0){
			foreach($rs->result_array() as $arr){
				//---- URL
				$arr["url"] = '#';
				if ( !empty($arr["controller"]) ){
					$arr["url"] = SITE_URL.$arr["controller"];
					if ( !empty($arr["method"]) ){
						$arr["url"] .= "/".$arr["method"];
					}
				}
				
				$result[$arr['moduleID']] = $arr;

				//---- compile array
				/*if($arr["parentModuleID"] == 0){
					$result[$arr['moduleID']]['item'] = $arr;
				}else{
					if ( $arr['isHeadingOnly'] == 0 )
					$result[$arr['parentModuleID']]["sub_items"][$arr['moduleID']]['item'] = $arr;
					else
					$result[$arr['parentModuleID']]["sub_headings"][$arr['moduleID']]['item'] = $arr;

					if ( $arr['headingID'] != 0 )
					$result[$arr['parentModuleID']]["sub_headings"][$arr['headingID']]["sub_items"][$arr['moduleID']]['item'] = $arr;
				}*/
			}
		}

		return $result;
	}
}
