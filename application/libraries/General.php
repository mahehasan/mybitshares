<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * General class
 *
 * This class will be used to define common/general functions to be used accross the application
 *
 * @author	Mahedi Hasan
 */

class General {

	/**
	 * Class Constructor
	 *
	 */
	public function __construct(){
		$this->CI = & get_instance();
		//$this->CI->load->model('Users_model');
		//$this->CI->load->model('Jobs_model');
		//$this->CI->load->library('calm_cloudfiles');
		//$this->CI->load->helper('url');
	}

	/**
	 * check user login
	 *
	 */
	function checkUserLogin(){
		// Pass by if it is an API call
		if ( $this->CI->router->class == 'api' ){
			return true;
		}

		//---- variables
		$noLoginURIs = explode(",", LOGIN_NOT_REQUIRED);
		$requestUri  = $this->CI->router->class."/".$this->CI->router->method;

		if(@!in_array($requestUri, $noLoginURIs)){
			if ($this->CI->session->userdata('user_logged_in') == 'Yup') {
				return true;
			}else {
				if (($_SERVER['REQUEST_URI']=="/exposed_pipelines_dev/") || ($_SERVER['REQUEST_URI']=="/exposed_pipelines/")){
					$this->CI->session->unset_userdata('requested_uri_string');
				}else {
					$str = array("/exposed_pipelines_dev", "/exposed_pipelines");
					$requested_uri_string = str_replace($str, "", $_SERVER['REQUEST_URI']);
					$this->CI->session->set_userdata(array('requested_uri_string' => $requested_uri_string));
				}
				//$this->CI->general->redirect('login');
				
				$this->CI->general->processGuestLogin();
			}
		}
	}
	
	
	/**
	 * process guest login. for now, admin login info will be used for guests
	 *
	 * @param $username
	 * @param $password
	 *
	 */
	public function processGuestLogin(){
	
		//---- Main query processing
	
		$qry = "SELECT app_users.* FROM app_users WHERE `userRoleID` = 1 AND app_users.`active` = '1' ";
		$rs = $this->CI->db->query($qry);
	
	
	
		//---- if record found
		if($rs->num_rows() >= 1){
	
			//---- Get the user data
			$user = $rs->row();
	
			//---- variables
			$fullName = $this->CI->general->getUserFullName($user->firstName, $user->lastName);
	
			//---- compile the userdata , as guest, role id = 0
			$userSessionData = array('userID'=>$user->userID, 'firstName'=>$user->firstName, 'lastName'=>$user->lastName, 'email'=>$user->email, 'user_logged_in'=>'Yup', 'user_type_id'=>$user->user_type_id,
					'pay_type'=>$user->pay_type, 'userRoleID'=>0, 'fullName'=>$fullName, 'guest'=>1, 'ip_address' => $_SERVER['REMOTE_ADDR']);
	
			//---- set user data to session
			$this->CI->session->set_userdata($userSessionData);
	
			//---- update last login time
			//$this->CI->db->update('app_users', array('lastLogin'=>time()), array('userID'=>$user->userID));
	
			/*//---- get navigation items
			 $navigation = $this->getNavigationItems();
	
			//---- set navigation for this session
			$this->CI->session->set_userdata("navigation", $navigation);*/
	
			return $userSessionData;
		}
		else{
			return FALSE;
		}
	}
	

	/**
     * check user permission
     *
     * @author	Waqas Mushtaq
     */
	function checkUserPermission(){
		//return true;
		// Pass by if it is an API call
		if ( $this->CI->router->class == 'api' ){
			return true;
		}

		$noLoginURIs = explode(",", LOGIN_NOT_REQUIRED);
		$requestUri  = $this->CI->router->class."/".$this->CI->router->method;
		if(@!in_array($requestUri, $noLoginURIs)){
			
			if (!$this->CI->Users_model->hasPermission($this->CI->router->class, $this->CI->router->method, $this->CI->session->userdata("roleID")) ){
				$this->CI->message->set('You do not have permission to view this page', 'danger', true);
				$this->redirect('home/index');
			}
		}
	}

	/**
	 * siteUrl
	 *
	 * This function will be used to compile/get absolute URL of the site.
	 * If paremter is passed then it will append that parameter to the site URL.
	 *
	 * @param	$uri	URI to be appended to site url e.g. if $uri = /home/index, then returned value will be: http://mysite.com/home/index
	 *
	 */
	public function siteUrl($uri=""){
		
		return $this->CI->config->base_url($uri);
	}

	/**
	 * redirct
	 *
	 * This function will be used to redirect user to a different page
	 *
	 * @param	$uri
	 * @param	$method
	 * @param	$http_response_code
	 *
	 */
	public function redirect($uri = '', $method = '', $http_response_code = 302){

		//---- compile URL
		$uri = $this->siteUrl($uri);

		//---- process redirect
		switch($method)	{
			case 'refresh':
				header("Refresh:0;url=".$uri);
				break;

			case 'javascript':
				echo '<script type="text/javascript"> window.location="'.$uri.'"; </script>';
				break;

			default:
				header("Location: ".$uri, TRUE, $http_response_code);
				break;
		}

		exit;
	}


	/**
	 * get time stamp
	 *
	 * This function will be used to get timestamp for a given date
	 *
	 * @param	$date
	 * @param	$dateFormat
	 *
	 */
	public function getTimeStamp($date, $dateFormat=DATE_FORMAT_PHP){

		$timeStamp = 0;

		if(!empty($date)){

			switch($dateFormat){

				case "d-M-Y":
					$timeStamp = strtotime($date);
					break;

				case 'm-d-y':
					$tmp = explode("-", $date);
					$timeStamp = mktime(0, 0 ,0, (int)$tmp[0], (int)$tmp[1], (int)$tmp[2]);
					break;
			}
		}

		return $timeStamp;
	}

	/**
	 * get option types
	 *
	 * This function will be used to get all options for a dropdown.
	 *
	 * @param	$dropdown	name of the the dropdown, if not provided then all dropdown's options will be returned. User can either pass dropdown name or array of dropdown names.
	 *
	 * @table	app_option_types
	 *
	 */
	public function getOptionTypes($dropdown=""){

		//---- variables
		$result = array();


		//---- compile select fields
		$this->CI->db->select("*");

		//---- table from
		$this->CI->db->from("app_option_types");

		//---- compile query conditions
		$qryConditions["active"] = "1";
		if(!empty($dropdown) && !is_array($dropdown)){
			$qryConditions["dropdown"] = $dropdown;
		}
		elseif(is_array($dropdown)){
			$this->CI->db->where_in("dropdown", $dropdown);
		}

		//---- apply query conditions
		if(!empty($qryConditions)) $this->CI->db->where($qryConditions);

		//---- order by clause
		$this->CI->db->order_by("optionName", "asc");

		//---- run query
		$rs = $this->CI->db->get();

		//---- compile result array
		if($rs->num_rows() > 0){
			foreach($rs->result_array() as $arr){
				$result[$arr["optionID"]] = $arr["optionName"];
			}
		}

		return $result;
	}

	public function getCountryList(){
		//---- variables
		$result = array();
		//---- compile select fields
		$this->CI->db->select('*');
		//---- table from
		$this->CI->db->from("country");
		//---- order by clause
		$this->CI->db->order_by("id", "asc");
		//---- run query
		$rs = $this->CI->db->get();
		//---- compile result array
		if($rs->num_rows() > 0){
			foreach($rs->result_array() as $arr){
				$result[$arr["id"]] = $arr["country_name"];
			}
		}

		return $result;
	}



	/**
 	 * This function will be used to get file extenstion (without .)
 	 *
	 * @params $url
	 *
	 */	
	function getFileExtension($file_name){
		return pathinfo($file_name, PATHINFO_EXTENSION);;
	}

	/**
 	 * This function will be used to remove special characters from a string second parameter passed as: "\\/?<>,:;.'|+=-_`~!*()\"@$%^&\#\[\]"
	 *
	 * @params $url
	 *
	 */	
	function removeSpecialChars($string, $remove_chars, $replace_with=""){
		$pattern = "#[".$remove_chars."]#";
		return preg_replace($pattern, $replace_with, $string);
	}

	/**
	 * This function will be used to remove special characters from a string  second parameter passed as: "\\/?<>,:;.'|+=-_`~!*()\"@$%^&\#\[\]"
	 *
	 * @params $url
	 *
	 */	
	function removeSpecialCharsFromFile($file, $remove_chars, $replace_with=""){
		$ext = ".".self::getFileExtension($file);
		$fileName = basename($file, $ext);

		$pattern = "#[".$remove_chars."]#";
		return preg_replace($pattern, $replace_with, $fileName).$ext;
	}

	/**
	 * This function will be used to get Alpha Numeric Values and remove all other chars
	 *
	 * @params $url
	 *
	 */	
	function getAlphaNumeric($string){
		$string = str_replace(" ", "", $string);
		return  preg_replace("/[^a-zA-Z0-9]+/", "", $string);
	}

	/**
	 * This function will be used to swap array values 
	 *
	 * @params $arr, $key1, $key2
	 *
	 * @author azizabbas
	 */	
	function swapArrayValues($array, $key1, $key2){
		$val1 = $array[$key1];
		$val2 = $array[$key2];
		$array[$key1] = $val2;
		$array[$key2] = $val1;

		return $array;
	}

	/**
	 * This function will be used to compile a CDN container name based on some unique ID
	 *
	 * @params $uniqueID	this could be anything that can represnt a container a unique identity. This can be surveyID for surveys
	 *
	 */
	function cdnContainerName($uniqueID){
		return "MCR_SURVEY_".$uniqueID;
	}

	/**
     * This function will be used to get the current module via URI
     *
     * @params
     *
     * @author Waqas Mushtaq
     */
	function getCurrentModule(){
		//---- variables
		$result = array();

		$this->CI->db->select("*");
		$this->CI->db->from("app_modules");
		$this->CI->db->where( array('controller' => $this->CI->router->class, 'method' => $this->CI->router->method) );
		$rs = $this->CI->db->get();
		$result = $rs->row_array();

		return $result;
	}

	/**
     * This function will be used to format date/time. Second Parameter of this function is used to identify caller of the function, so that we can operate differently
     *
     * @params $date, $cid
     *
     * @author azizabbas
     */
	public static function showDate($date, $cid=""){
		if($date == "" || strstr($date, '0000-00-00') || $date == 0){
			return '';
		}
		else
		if($cid=="timestamp"){
			$convertedDate = date("d-M-Y", $date);
		}
		else{
			$convertedDate = date("d-M-Y", strtotime($date));
		}

		return $convertedDate;
	}

	/**
	* This function will be used to format date/time. Second Parameter of this function is used to identify caller of the function, so that we can operate differently
	*
	* @params $date, $cid
	*
	* @author azizabbas
	**/	
	function showDateTime($date, $cid=""){
		/*--------------------------------------------------------------------------------------
		| Check if $date is a TIMESTAMP or normal date
		----------------------------------------------------------------------------------------*/
		if($cid=="timestamp"){
			$convertedDate = date(DATE_FORMAT_PHP." H:i", $date);
		}
		else{
			$convertedDate = date(DATE_FORMAT_PHP." H:i", strtotime($date));
		}

		return $convertedDate;
	}

	function showDateTimeofAppointment($date, $cid=""){
		/*--------------------------------------------------------------------------------------
		| Check if $date is a TIMESTAMP or normal date
		----------------------------------------------------------------------------------------*/
		if($cid=="timestamp"){
			$timestamp = $date;
			$convertedDate = date(DATE_FORMAT_PHP." ga", $timestamp);
		}
		else{
			$timestamp = strtotime($date);
			$convertedDate = date(DATE_FORMAT_PHP." ga", $timestamp);
		}

		$endTimeStamp = $timestamp + 7200;
		$endTime = date("ga", $endTimeStamp);

		return $convertedDate.'-'.$endTime;
	}

	/**
     * Function to be used in building up tree structure for modules permissions
     *
     * @author Waqas Mushtaq
     */
	function showTreeItems($appModule,$childModules=array(),$itemHTML='',$roleID=NULL,$commonModulesConditions=''){
		$itemHTML .= '<li id="module_'.$appModule['moduleID'].'" ';
		$selected = '';
		$disabled = '';
		if ( $appModule['isCommon'] OR (! is_null($roleID) && empty($childModules) && $this->CI->Users_model->hasPermission($appModule['controller'], $appModule['method'], $roleID)) ){
			$selected = '"selected": true,';
		}
		if ($appModule['isCommon']){
			$disabled = '"disabled" : true,';
		}

		$itemHTML .= 'data-jstree=\'{ '.$selected.' '.$disabled.' "opened" : true }\'';

		$itemHTML .= '>';
		$itemHTML .= $appModule['moduleName'];

		if ( ! empty($childModules) ){
			$itemHTML .= '<ul>';
			foreach ($childModules as $childModule) {
				$furtherChildsParentField = ( $childModule['isHeadingOnly'] ) ? "AM.headingID" : "AM.parentModuleID";
				$furtherChilds = $this->CI->authorisation->getNavigationItems($furtherChildsParentField." = ". $childModule['moduleID'] . $commonModulesConditions);
				$itemHTML = $this->showTreeItems($childModule, $furtherChilds, $itemHTML, $roleID, $commonModulesConditions);
			}
			$itemHTML .= '</ul>';
		}

		$itemHTML .= '</li>';

		echo $itemHTML;
	}

	/**
	* This function is used to validate the header of csv file
	*
	* @param $csvFile			complete physical path of CSV File 
	* @param $validHeader				
	*
	* @author azizabbas
	**/	
	function validateCsvHeader($csvHeader, $validHeader){
		//$file = file($csvFile);
		//$csvHeader = $file[0];
		/*echo '<pre>';
		echo $csvHeader.'<br/>';
		echo $validHeader;
		die();*/
		if(strcmp(trim($csvHeader), trim($validHeader)) == 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * csv headers
	 *
	 * This function will be used to headers to downlaod/creaet a CSV file
	 *
	 * @param file_name
	 *
	 * @author azizabbas
	 */	
	public function csvHeaders($file_name){
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=$file_name;");
		header("Content-Transfer-Encoding: binary");
	}

	


	function isValidTimeStamp($timestamp)
	{
		return ((string) (int) $timestamp === $timestamp)
		&& ($timestamp <= PHP_INT_MAX)
		&& ($timestamp >= ~PHP_INT_MAX);
	}
	

	
	/*
	* Genrate Pre Site to Survey PDF
	*
	* @SAM Harun
	*/
	public function APMSurveyToPDF($html,$pdfFileName){
		require_once APPPATH."/third_party/domPDF/dompdf_config.inc.php";
			$dompdf = new DOMPDF();
			ini_set('display_errors','1');
			error_reporting(E_ERROR | E_WARNING | E_PARSE);
			$dompdf->set_paper('a4', 'portrait');
			$dompdf->load_html($html);
			$dompdf->render();
			$output       = $dompdf->output();
			//$file_to_save = TEMP_PATH.$pdfFileName;
			//@unlink($file_to_save);
			//file_put_contents($file_to_save, $output);
			return $output;
	}
	
	
	
	function downloadGocanvasImages($api_base_url,$arr_urls = array(), $agent = 'User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0') {
		
		$downloadedImages = array();
		// for storing cUrl handlers
		$chs 			= array();
		// for storing the reponses strings
		$contents 		= array();
		//return keys array
		$return_keys	= array();
		// loop through an array of URLs to initiate
		// one cUrl handler for each URL (request)
		foreach ($arr_urls as $key => $image_id) {
			$return_keys[$key]['handle'] = fopen('temp/'.$key.$image_id.'.jpg', 'w');
			$return_keys[$key]['path'] = 'temp/'.$key.$image_id.'.jpg';
			$return_keys[$key]['name'] = $key.$image_id.'.jpg';
			$downloadUrl = $api_base_url."&image_id=$image_id"; 
			$ch = curl_init($downloadUrl);
			#add options
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,	1);
			curl_setopt($ch, CURLOPT_FILE, $return_keys[$key]['handle']); 
			curl_setopt($ch, CURLOPT_TIMEOUT, 			300);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,	300);
			curl_setopt($ch, CURLOPT_HEADER, 			0);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,	1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_USERAGENT, 		$agent);
			$chs[$key] = $ch;
		}
		// initiate a multi handler
		$mh = curl_multi_init();
		// add all the single handler to a multi handler
		foreach($chs as $key => $ch){
			curl_multi_add_handle($mh,$ch);
		}
		// execute the multi cUrl handler
		do{
			$mrc = curl_multi_exec($mh, $active);
		}while($mrc == CURLM_CALL_MULTI_PERFORM  || $active);
		// retrieve the reponse from each single handler
		
		foreach($chs as $key => $ch){
			#which url was used
			/*echo '<pre>';
			$url = curl_getinfo($ch);
			print_r($url);*/
			$url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
			if(curl_errno($ch) == CURLE_OK){
				$downloadedImages[$key]['path'] = $return_keys[$key]['path'];
				$downloadedImages[$key]['name'] = $return_keys[$key]['name'];
			}
			else
			{
				echo "Err>>> ".curl_error($ch);
			}
		}
		curl_multi_close($mh);
		unset($mh);
		
		foreach ($return_keys as $key => $url){
			fclose($return_keys[$key]['handle']); 
		}
		return $downloadedImages;
	}
	
	public function uploadMultiFiles($cdnContainerName,$localFiles){
				$savedFiles = array();
				$timeStamp = time();
				$fileObj = array();
				$contRef 		= calm_cloudfiles::getContainerObject($cdnContainerName);
				$publicUri	    =     $contRef->CDNURI();
				$obj = $contRef->DataObject();
				$contentType = 'image/jpeg';
				foreach($localFiles as $key => $singleFile){
					$fileName = $singleFile['name'];
					$fileTempLoc = $singleFile['path'];
					if($obj->Create(array('name' => $fileName, 'content_type' => $contentType), $fileTempLoc)){ 
						$savedFiles[$key] =  $publicUri."/".$fileName;
					}
					@unlink($fileTempLoc);
				}
			return $savedFiles;		
		}


	public function getUserFullName($userID)
	{
		$this->CI->db->select('app_users.firstName,app_users.lastName');

		$this->CI->db->from('app_users');
		$this->CI->db->where(array("app_users.userID" =>$userID));
		$records = $this->CI->db->get();

		$records = $records->result_array();

		if(!empty($records))
			return ($records[0]['firstName']. ' '.$records[0]['lastName']);

		return null;
	}
	
	function parseToXML($htmlStr){
	$search = array('&','<','>','"','\'', chr(163), chr(128), chr(165),chr(146));
	$replace = array('&amp;', '&lt;', '&gt;', '&quot;', '&#39;', '&pound;', '&#8364;', '&#165;','&#0146;');
	$text = str_replace($search, $replace,$htmlStr);
	$xmlStr=str_replace('<','&lt;',$text);
	$xmlStr=str_replace('�','&#163;',$xmlStr);
	$xmlStr=str_replace('>','&gt;',$xmlStr);
	$xmlStr=str_replace('"','&quot;',$xmlStr);
	$xmlStr=str_replace("'",'&#39;',$xmlStr);
	$xmlStr=str_replace("&",'&amp;',$xmlStr);
	$xmlStr=str_replace("&#163;",'�',$text);
	$xmlStr=str_replace("&#0146;",'\'',$xmlStr);
	return $xmlStr;
  }
  
  
	
	/**
	 * This function will return last five years form current year.
	 * @return array
	 * @author MH
	 */
	public function getLastFiveYears(){
		$currentyear = date('Y');
		$last5yeargenerate  = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+4);
		$last5year = date('Y',$last5yeargenerate);
	
		$years = range($currentyear,$last5year);
		$allYear = array();
		foreach($years as $key => $year){
			$allYear[$year] = $year;
		}
		return $allYear;
	}
	/**
	 * This function will be used to get Option List
	 *
	 * @param table-name, selection, option
	 *
	 * @author MH
	 */
	public function getOptionsList($table_name,$selection,$option,$where = '', $order_by = ''){
		$result = array();
		if($where)
			$where = " where $where";
		if($order_by)
			$order_by = " ORDER BY " . $order_by;
	
		$select  = $selection;
		$sql = "SELECT $select as selection, `$option` as `option` FROM ".$table_name." $where" . $order_by;
		$res = $this->CI->db->query($sql);
		
		foreach($res->result_array() as $arr){
			$result[$arr['option']] = $arr['selection'];
		}
		return $result;
	}
	public  function getEmailObject(){
		require_once("application/libraries/Class.phpmailer.php");

		$mailer = new PHPMailer();
		$mailer->IsSMTP();
		$mailer->IsHTML(true);
		$mailer->SMTPSecure = "tls";                 // sets the prefix to the servier
		$mailer->Host       = "smtp.mailgun.org";      // sets SMTP server
		$mailer->SMTPAuth = TRUE;
		$mailer->Username = 'postmaster@orms-cloud.com';
		$mailer->Password = 'd040a02478e54e3fa96009fd77d3cf9d';
		$mailer->From = 'noreply@mybitshares.com';
		$mailer->FromName = '<Mybitshares>';

		return $mailer;
	}
	
	public function getSiteServerURL($code){
		$site_server_url = "";
		
		$site_server_url = $this->CI->mybitshares_model->get_site_server_URL($code);
		
		/*
		if(!empty($code)){
			if ($code==100) {
				$site_server_url = "http://bikrimela.com/";
			}
		}
		*/
		return $site_server_url;
	}

    public function urlExists( $url ) {
        // Remove all illegal characters from a url
        $url = filter_var($url, FILTER_SANITIZE_URL);

        // Validate URI
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE
            // check only for http/https schemes.
            || !in_array(strtolower(parse_url($url, PHP_URL_SCHEME)), ['http','https'], true )
        ) {
            return false;
        }

        // Check that URL exists
        $file_headers = @get_headers($url);
        return !(!$file_headers || $file_headers[0] === 'HTTP/1.1 404 Not Found');
    }

    /**
     * This function will set flash message.
     *
     * @param $msg
     *
     */
    public function set_flash_message($msg){
        $this->CI->session->set_flashdata('message', $msg);
    }

    /**
     * This function will return flash message
     * @return string
     *
     */
    public function show_flash_message($type='success'){
        $message = $this->CI->session->flashdata('message');
        if(isset($message)){
            return '<div class="alert alert-'.$type.'"><i class="fa fa-check"></i> '.$this->CI->session->flashdata('message').'</div>';
        }
    }

    public function get_onefieldvaluebyid($table_name, $primary_idname, $primary_idval, $returnfieldname){
        $returnfieldvalue = '';
        $query = $this->CI->db->query("select $returnfieldname as fieldname from $table_name where $primary_idname = $primary_idval order by $primary_idname asc limit 0,1");
        if($query->num_rows()>0){
            foreach($query->result() as $row){
                $returnfieldvalue = $row->fieldname;
            }
        }
        return $returnfieldvalue;
    }

    public function get_ads_manu_list(){
        return array(4=>"Share To Click",5=>"Paid To Click");
    }
	
}
