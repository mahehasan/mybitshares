<?php
class Admin_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    //--------------------------------------------------------------------------------------Price-----------------------------------------------------------------------
	public function getAllPriceList()
	{
		$qry = "SELECT * FROM price_lookup WHERE price_publish = 1 ORDER BY showing_order";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function getUserName($id)
	{
		$qry = "SELECT * FROM app_users WHERE userID = ".$id;
		$query = $this->db->query($qry);
		$data = $query->result();
		return $data[0]->firstName . ' ' . $data[0]->lastName;
	}
	public function getUsersIPList($id)
	{
		$qry = "SELECT * FROM user_ip WHERE userID = ".$id;
		$query = $this->db->query($qry);
		
		return $query->result();
	}
	public function getPackage($in_site_server_code, $in_site_advertise_id)
	{
        $qry = "SELECT  advertise_price_id FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
        $query = $this->db->query($qry);
        $rslt = $query->result();

        if($rslt)
        {
            $arr = $this->getPriceById($rslt[0]->advertise_price_id);
            if (isset($arr[0]->price_title)){
                return $arr[0]->price_title;
            }
        }
	}

	public function getMenu($id)
	{
		$qry = "SELECT * FROM category WHERE category_id = ".$id;
		$query = $this->db->query($qry);
		$data = $query->result();
		return $data[0]->category_title;
	}

	public function getPriceById($id)
	{
		$qry = "SELECT * FROM price_lookup WHERE price_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function InsertPrice($data)
	{
		$data['created_by']=$this->session->userData('userID');
		$data['created_date']= time();
		$data['price_publish']= 1;
		$this->db->insert('price_lookup',$data);
	}

	public function UpdatePrice($id, $data)
	{
		$data['modified_date']= time();
		$this->db->where('price_id',$id);
		$this->db->update('price_lookup',$data);
	}

	public function deletePrice($id)
	{
		$data['price_publish']= 0;
		$this->db->where('price_id',$id);
		$this->db->update('price_lookup',$data);
		// $this->db->where('price_id',$id);
		// $this->db->delete('price_lookup');
	}
	//--------------------------------------------------------------------------------------Product-----------------------------------------------------------------------
	public function getAllProductList()
	{
		$qry = "SELECT * FROM product WHERE product_publish = 1 ORDER BY product_id DESC";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function getProductById($id)
	{
		$qry = "SELECT * FROM product WHERE product_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function InsertProduct($data)
	{
		$data['created_date']= time();
		$data['created_by']= $this->session->userData('userID');
		$this->db->insert('product',$data);
	}

	public function add_advertise($data)
	{
		$this->db->insert('advertise',$data);
	}
	
    public function delete_advertise($id)
	{
		$data['advertise_publish']= 0;
		$this->db->where('advertise_id',$id);
		$this->db->update('advertise',$data);
	}

	public function UpdateProduct($id, $data)
	{
		$data['modified_date']= time();
		$this->db->where('product_id',$id);
		$this->db->update('product',$data);
	}

	public function deleteProduct($id)
	{
		$data['product_publish']= 0;
		$this->db->where('product_id',$id);
		$this->db->update('product',$data);
		// $this->db->where('price_id',$id);
		// $this->db->delete('price_lookup');
	}

	public function delete_product_image($product_id)
	{
		$qry = "SELECT product_image, product_thumbimage FROM product WHERE product_id = ".$product_id;
		$query = $this->db->query($qry);
		$old_image =  $query->result();
		$path = $old_image[0]->product_image ;
      if(is_file($path)){
        unlink($path);
      } else {
      	//could not delete, file not exists
    	}

		$path = $old_image[0]->product_thumbimage ;
      if(is_file($path)){
        unlink($path);
      } else {
      	//could not delete, file not exists
    	}
	}

	public function getAllCategoryList()
	{
		$qry = "SELECT * FROM category WHERE parent_category > 0 ORDER BY category_title";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_all_user_data_admin()
	{
		$qry = "SELECT * FROM app_users WHERE userID NOT IN (1) AND publish_flag = 1";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_all_user_data_by_id($id)
	{
		$qry = "SELECT * FROM app_users WHERE userID = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_all_users()
	{
		$qry = "SELECT * FROM app_users where publish_flag = 1 ";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_all_balance_history_by_id($id)
	{
		$qry = "SELECT * FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_all_share_history_by_id($id)
	{
		$qry = "SELECT * FROM client_shares WHERE client_shares_publish = 1 AND client_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}
	
	public function get_all_share_gift_by_admin()
	{
		//$qry = "SELECT sum(DISTINCT shares_amount) AS TotalShare  FROM client_shares WHERE site_server_code=0 AND site_server_advertise_id=0 AND (site_server_code is null or site_server_code='') ";
		
		$qry = "SELECT DISTINCT shares_amount AS TotalShare FROM client_shares WHERE site_server_code=0 AND site_server_advertise_id=0 AND (site_server_code is null or site_server_code='') ORDER by shares_id desc LIMIT 1 ";
		
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_all_product_category()
	{
		$qry = "SELECT * FROM product_category WHERE category_publish=1";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_product_category_by_id($category_id)
	{
		$qry = "SELECT * FROM product_category WHERE product_category_id = ".$category_id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function add_product_category($data)
	{
		$this->db->insert('product_category',$data);
		return $this->db->insert_id();
	}

	public function update_product_category($id, $data)
	{
		$this->db->where('product_category_id',$id);
		return $this->db->update('product_category',$data);
	}

	public function delete_product_category($id)
	{
		$data['category_publish']= 0;
		$this->db->where('product_category_id',$id);
		$this->db->update('product_category',$data);
	}


//--------------------------------------------------------------MANAGE CLIENT ADVERTISE------------------------
	public function get_all_manage_client_advertise()
	{
		$qry = "SELECT * FROM manage_client_advertise a 
				LEFT JOIN advertise b ON (a.advertise_id = b.advertise_id)
				LEFT JOIN app_users c ON (a.client_id = c.userID)
				WHERE manage_client_advertise_publish = 1";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_advertise_detail_by_id($id)
	{
		$qry = "SELECT * FROM advertise WHERE site_server_advertise_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}
	public function get_advertise_detail_by_advertise_id($id)
	{
		$qry = "SELECT * FROM advertise WHERE advertise_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}
	public function getFileName($site_server_advertise_id,$site_server_code)
	{
		$site_server_ads_data = array();
		$file_name="";
        $site_servers = $this->admin_model->get_all_site_servers();
        //echo "<pre>";print_r($site_servers);die;
        if(!empty($site_servers)){
            foreach ($site_servers as $site_server) {
                $site_server_url_exist = $this->general->urlExists($site_server->site_server_url."v1/");
                if(!$site_server_url_exist) continue;

                $site_server_url = $site_server->site_server_url;
                $handle = curl_init();
                curl_setopt_array($handle, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $site_server_url."v1/advertise/advertise"
                ));

                $res = curl_exec($handle);
                curl_close($handle);

                $site_server_ads_data = json_decode($res);
            }
        }
		
		if(!is_null($site_server_ads_data))
		{
			foreach($site_server_ads_data as $row1)
			{
				
				foreach ($row1 as $row) {				

				if($row->site_advertise_id == $site_server_advertise_id && $row->site_server_code == $site_server_code)
				   {
					    $file_name = $row->file_name;
				   }
				} 
			}
		}
		
		return $file_name;
		//return $site_server_ads_data;
	}

	public function enable_client_advertise($id)
	{
		$data = array(
			'manage_client_advertise_publish' => 0
			);
		$this->db->where('manage_client_advertise_id', $id);
		return $this->db->update('manage_client_advertise', $data);
		//return $previous_id;
	}

	public function update_advertise($id, $data)
	{
		$this->db->where('advertise_id',$id);
		return $this->db->update('advertise',$data);
	}
//--------------------------------------------------------------------------------------PRODUCT COMPANY-------------------------------------------------
	public function get_all_product_company_list()
	{
		$qry = "SELECT * FROM product_company WHERE company_publish = 1 ";
		$query = $this->db->query($qry);
		return $query->result();
	}
	
	public function get_product_company_by_id($id)
	{
		$qry = "SELECT * FROM product_company WHERE product_company_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function add_product_company($data)
	{
		$this->db->insert('product_company',$data);
		return $this->db->insert_id();
	}

	public function update_product_company($id, $data)
	{
		$this->db->where('product_company_id',$id);
		return $this->db->update('product_company',$data);
	}

	public function delete_product_company($id)
	{
		$data['company_publish']= 0;
		$this->db->where('product_company_id',$id);
		$this->db->update('product_company',$data);
	}

	public function add_referral($data)
	{
		$this->db->insert('referral_relations',$data);
	}
//-------------------------------------------------------------------------configuration
	public function get_all_configuration()
	{
		$qry = "SELECT * FROM configuration WHERE configuration_publish = 1";
		$query = $this->db->query($qry);
		return $query->result();
	}

	
	public function get_configuration_by_id($category_id)
	{
		$qry = "SELECT * FROM configuration WHERE configuration_id = ".$category_id;
		$query = $this->db->query($qry);
		return $query->result();
	}
	
	

	public function add_configuration($data)
	{
		$this->db->insert('configuration',$data);
		return $this->db->insert_id();
	}

	public function update_configuration($id, $data)
	{
		$this->db->where('configuration_id',$id);
		return $this->db->update('configuration',$data);
	}

	public function delete_configuration($id)
	{
		$data['configuration_publish']= 0;
		$this->db->where('configuration_id',$id);
		$this->db->update('configuration',$data);
	}
//------------------------------------------------------------------------bonus list
	public function get_all_bonus_list()
	{
		$qry = "SELECT * FROM share_bonus WHERE bonus_publish = 1";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_bonus_by_id($category_id)
	{
		$qry = "SELECT * FROM share_bonus WHERE share_bonus_id = ".$category_id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function add_bonus($data)
	{
		$this->db->insert('share_bonus',$data);
		return $this->db->insert_id();
	}

	public function update_bonus($id, $data)
	{
		$this->db->where('share_bonus_id',$id);
		return $this->db->update('share_bonus',$data);
	}

	public function delete_bonus($id)
	{
		$data['bonus_publish']= 0;
		$this->db->where('share_bonus_id',$id);
		$this->db->update('share_bonus',$data);
	}

	public function get_users_ip($userid)
	{
		$qry = "SELECT * FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$userid;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function enable_disable_user($id)
	{
		$qry = "SELECT * FROM app_users WHERE userID = ".$id;
		$query = $this->db->query($qry);
		$rslt= $query->result();
		if ($rslt[0]->active==1)
		{
			$active=0;
		}else{
			$active=1;
		}
		$this->db->where('userID',$id);
		$this->db->update('app_users', array('active' => $active ));
	}

	public function get_last_daily_earnings()
	{
		$qry = "SELECT * FROM system_daily_income";
		$query = $this->db->query($qry);
		return $query->result();
	}
	
	public function get_system_total_daily_earnings()
	{
		$qry = "SELECT SUM(amount) AS total_sum FROM system_daily_income";
		$query = $this->db->query($qry);
		return $query->result();
	}
	public function get_system_daily_transaction_history($userID)
	{
		$qry = "SELECT * FROM daily_transaction_history WHERE user_id = ".$userID." ORDER by  transaction_history_date desc";
		$query = $this->db->query($qry);
		return $query->result();
	}
	public function get_system_daily_transaction_history_by_pivot($userID)
	{
		$qry = "SELECT
		from_unixtime(transaction_history_date,'%Y-%m-%d') as transaction_history_date ,  
		SUM(CASE WHEN (transaction_message='Share To Click') THEN transaction_amount ELSE 0 END) AS 'Share_To_Click', SUM(CASE WHEN (transaction_message='Paid To Click') THEN transaction_amount ELSE 0 END) AS 'Paid_To_Click', SUM(CASE WHEN (transaction_message='Share Buy and Sell Amount') THEN transaction_amount ELSE 0 END) AS 'Share_Buy_and_Sell_Amount', SUM(CASE WHEN (transaction_message='Advertise Add') THEN transaction_amount ELSE 0 END) AS 'Advertise_Add' FROM  daily_transaction_history Where user_id = ".$userID." GROUP BY from_unixtime(transaction_history_date,'%Y-%m-%d') ORDER by  transaction_history_date desc LIMIT 15";
		$query = $this->db->query($qry);
		return $query->result();
	}
	public function get_max_transaction_history_date($userID)
	{
		$tempTitle = "";
		$qry = "SELECT MAX(from_unixtime(transaction_history_date,'%Y-%m-%d') ) as transaction_history_date FROM daily_transaction_history WHERE user_id = ".$userID."";
		$query = $this->db->query($qry);
		$rslt = $query->result();
		
			if($rslt)
			{
				$tempTitle = $rslt[0]->transaction_history_date;
			}
			
			return $tempTitle;
		
	}
	public function get_last_date_transaction_amount($userID,$date)
	{
		$tempTitle = "";
		$qry = "SELECT sum(transaction_amount) as transaction_amount FROM daily_transaction_history WHERE user_id = ".$userID." AND transaction_history_date>=".$date."";
		$query = $this->db->query($qry);
		$rslt = $query->result();
		
			if($rslt)
			{
				$tempTitle = $rslt[0]->transaction_amount;
			}
			
			return $tempTitle;
		
	}
	public function getMenu1Name($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  b.category_title FROM advertise a, category b where a.menu1_id = b.category_id AND a.site_server_code = ".$in_site_server_code." AND a.site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
		
			if($rslt)
			{
				$tempTitle = $rslt[0]->category_title;
			}
			
			return $tempTitle;
		
	}
	
	public function getMenu2Name($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  b.category_title FROM advertise a, category b where a.menu2_id = b.category_id AND a.site_server_code = ".$in_site_server_code." AND a.site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->category_title;
		}
			
		return $tempTitle;
	
	}
	
	public function getMenu1Value($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  menu1_click FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->menu1_click;
		}
			
		return $tempTitle;
	
	}
	
	public function getMenu2Value($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  menu2_click FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->menu2_click;
		}
			
		return $tempTitle;
	
	}
	
	public function getadvertise_id($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  advertise_id FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->advertise_id;
		}
			
		return $tempTitle;
	
	}
	

    public function getMainServerAdvetiseIdValue($in_site_server_code, $in_site_advertise_id)
    {
        $qry = "SELECT  advertise_id FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
        $query = $this->db->query($qry);
        $rslt = $query->result();

        if($rslt)
        {
            return $rslt[0]->advertise_id;
        }
    }
	
	public function getMenu1ID($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  menu1_id FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->menu1_id;
		}
			
		return $tempTitle;
	
	}
	public function getMenu2ID($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  menu2_id FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->menu2_id;
		}
			
		return $tempTitle;
	
	}
	public function get_advertise_publish($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  advertise_publish FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->advertise_publish;
		}
			
		return $tempTitle;
	
	}
	public function get_share_to_click_publish($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  share_to_click_publish FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->share_to_click_publish;
		}
			
		return $tempTitle;
	
	}
	public function get_paid_to_click_publish($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  paid_to_click_publish FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->paid_to_click_publish;
		}
			
		return $tempTitle;
	
	}
	public function get_server_total_click_share($id)
	{
		$qry = "SELECT menu1_click FROM advertise WHERE menu1_id =4 And advertise_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
				
		if( $query->num_rows()>0)
		{			
			return $rslt[0]->menu1_click;
		}else
		{
			$qry = "SELECT menu2_click FROM advertise WHERE menu2_id =4 And advertise_id = ".$id;
			$query = $this->db->query($qry);
			$rslt = $query->result();
			
			return $rslt[0]->menu2_click;
			
		}
				
	}	
	public function get_server_total_click_paid($id)
	{
		$qry = "SELECT IFNULL(menu1_click,0) as menu1_click FROM advertise WHERE menu1_id =5 And advertise_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
				
		if( $query->num_rows()>0)
		{			
			return $rslt[0]->menu1_click;
		}else
		{
			$qry = "SELECT IFNULL(menu2_click,0) as menu2_click FROM advertise WHERE menu2_id =5 And advertise_id = ".$id;
			$query = $this->db->query($qry);
			$rslt = $query->result();
			if( $query->num_rows()>0)
			{
				return $rslt[0]->menu2_click;
			}
			else 0;
			
		}
				
	}
	public function get_client_total_click_share($id)
	{
		$qry = "SELECT SUM(current_click_share) AS TotalClick FROM click_calculation WHERE advertise_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->TotalClick;
	}
	
	//Paid to share
	public function get_client_total_click_paid($id)
	{
		$qry = "SELECT SUM(current_click_paid) AS TotalClick FROM click_calculation WHERE advertise_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->TotalClick;
	}
	
	 public function getSiteServerUrl($in_site_server_code)
    {
        $qry = "SELECT  site_server_url FROM site_servers  where site_server_code = ".$in_site_server_code." ";
        $query = $this->db->query($qry);
        $rslt = $query->result();

        if($rslt)
        {
            return $rslt[0]->site_server_url;
        }
    }
	
	
	public function getAddpriceID($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  advertise_price_id FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->advertise_price_id;
		}
			
		return $tempTitle;
	
	}
	
	public function getSell_price($advertise_price_id)
	{
		$tempTitle = "";
		$qry = "SELECT  price_sell FROM price_lookup  where price_id = ".$advertise_price_id." ";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->price_sell;
		}
			
		return $tempTitle;
	
	}
	
	public function getadvertisePublish($in_site_server_code, $in_site_advertise_id)
	{
		$tempTitle = "";
		$qry = "SELECT  advertise_publish FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
		//$qry = "SELECT  COALESCE(advertise_publish,0)advertise_publish FROM advertise  where site_server_code = 120 AND site_server_advertise_id = 1";
		$query = $this->db->query($qry);
		$rslt = $query->result();
	
		if($rslt)
		{
			$tempTitle = $rslt[0]->advertise_publish;
		}
			
		return $tempTitle;
	
	}
	
	public function get_all_site_servers()
	{
		//$site_server_url = "";
	
		$qry = "SELECT site_server_id, site_server_code, site_server_url FROM site_servers WHERE publish_flag = 1 ";
		$query = $this->db->query($qry);
		$master_data = $query->result();
		$numrows = $query->num_rows();
		//if ($numrows > 0){
		//	$site_server_url = $master_data[0]->site_server_url;
		//}
	
		return $master_data;
	}
	
	
	public function add_site_server($data)
	{
		$this->db->insert('site_servers',$data);
		return $this->db->insert_id();
	}
	
	public function update_site_server($id, $data)
	{
		$this->db->where('site_server_id',$id);
		return $this->db->update('site_servers',$data);
	}
	
	public function delete_site_server($id)
	{
		$data['publish_flag']= 0;
		$this->db->where('site_server_id',$id);
		$this->db->update('site_servers',$data);
	}
	
	public function get_site_server_by_id($site_server_id)
	{
		$qry = "SELECT * FROM site_servers WHERE publish_flag = 1 AND site_server_id = ".$site_server_id;
		$query = $this->db->query($qry);
		//return $query->result_array();
         $row = $query->row_array();
         return $row;
	}
	
	public function process_check_ips($ip_list)
	{
		$data = array();
		
		foreach ($ip_list as $row)
		{
			// get user id, and name
			
			$qry = "SELECT a.userID, a.firstName, a.lastName, b.ip_address FROM app_users a, client_cash b WHERE a.userID = b.client_id AND b.client_cash_publish = 1 AND b.ip_address = '".$row."'";
			$query = $this->db->query($qry);
			$numrows = $query->num_rows();
			if($numrows > 0)
			{
				$resultset =  $query->result();				
				foreach ($resultset as $row1)
				{
					array_push($data, $row1);
				}				
			}	
			
			$qry = "SELECT a.userID, a.firstName, a.lastName, b.ip_address FROM app_users a, client_shares b WHERE a.userID = b.client_id AND b.client_shares_publish = 1 AND b.ip_address = '".$row."'";
			$query = $this->db->query($qry);
			$numrows = $query->num_rows();
			if($numrows > 0)
			{
				$resultset =  $query->result();			
				foreach ($resultset as $row1)
				{
					array_push($data, $row1);
				}			
			}
		
		}
		
		return $data;
	}
	
	public function update_system_daily_income($id, $amount)
	{
		$data['amount']= $amount;
		$this->db->where('income_id',$id);
		$this->db->update('system_daily_income',$data);
	}

    public function get_advertise_title_value($in_site_server_code, $in_site_advertise_id)
    {
        $tempTitle = "";
        $qry = "SELECT  advertise_title FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
        $query = $this->db->query($qry);
        $rslt = $query->result();

        if($rslt)
        {
            $tempTitle = $rslt[0]->advertise_title;
        }

        return $tempTitle;

    }

    public function get_advertise_description_value($in_site_server_code, $in_site_advertise_id)
    {
        $tempTitle = "";
        $qry = "SELECT  advertise_description FROM advertise  where site_server_code = ".$in_site_server_code." AND site_server_advertise_id = ".$in_site_advertise_id." ";
        $query = $this->db->query($qry);
        $rslt = $query->result();

        if($rslt)
        {
            $tempTitle = $rslt[0]->advertise_description;
        }

        return $tempTitle;

    }

}
?>