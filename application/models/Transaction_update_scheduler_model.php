<?php
class Transaction_update_scheduler_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    public function user_transaction_history_existence($userID)
	{
		$qry = "SELECT * FROM transaction_history WHERE user_id = ".$userID." order by transaction_history_id desc";
		$query = $this->db->query($qry);
		$tmp = $query->result();
		return $tmp;
	}
	public function user_transaction_history_existenceLimit($userID)
	{
		$qry = "SELECT * FROM transaction_history WHERE user_id = ".$userID." order by transaction_history_id desc Limit 20";
		$query = $this->db->query($qry);
		$tmp = $query->result();
		return $tmp;
	}
	
	public function user_transaction_main_existence($userID)
	{
		$qry = "SELECT * FROM transaction_main WHERE user_id = ".$userID." ";
		$query = $this->db->query($qry);
		$tmp = $query->result();
		return $tmp;
	}
	public function user_daily_transaction_history_existence($userID)
	{
		$qry = "SELECT * FROM daily_transaction_history WHERE user_id = ".$userID." ";
		$query = $this->db->query($qry);
		$tmp = $query->result();
		return $tmp;
	}
	
	// client_cash
	public function get_client_cash_latest_date($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MAX(click_date) as click_date FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->click_date;
		}
		return $tempTitle;
	}
	public function get_client_cash_first_date($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT IFNULL(MIN(click_date),0) as click_date FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->click_date;
		}
		return $tempTitle;

		//echo '<pre>';print_r($dates[0]->click_date);echo '</pre>';	die();

	}
	public function get_daily_transaction_history_latest_date_Paid_To_Click($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MAX(transaction_history_date) as transaction_history_date FROM daily_transaction_history WHERE transaction_message='Paid To Click' and user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->transaction_history_date;
		}
		return $tempTitle;

	}
	public function get_daily_transaction_history_latest_date_Share_To_Click($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MAX(transaction_history_date) as transaction_history_date FROM daily_transaction_history WHERE transaction_message='Share To Click' and user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->transaction_history_date;
		}
		return $tempTitle;

	}
	//Advertise Add 
	public function get_adverise_add_latest_date($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MAX(transaction_history_date) as transaction_history_date FROM transaction_history WHERE transaction_message='Advertise Add' AND user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->transaction_history_date;
		}
		return $tempTitle;
	}
	public function get_adverise_add_first_date($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MIN(transaction_history_date) as transaction_history_date FROM transaction_history WHERE transaction_message='Advertise Add' AND user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();

		if($rslt)
		{
			$tempTitle = $rslt[0]->transaction_history_date;
		}
		return $tempTitle;
	}
	public function get_adverise_add_Amount_by_date($userID,$dt1,$dt2)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS transaction_amount FROM transaction_history WHERE transaction_message='Advertise Add' AND transaction_history_date < ".$dt2." AND transaction_history_date >= ".$dt1." AND user_id = ".$userID." " ;

		
		$query = $this->db->query($qry);
		$rslt = $query->result();

		if($rslt)
		{
			$tempTitle = $rslt[0]->transaction_amount;
		}
		return $tempTitle;
	}
	public function get_daily_transaction_history_latest_date_Advertise_Add($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MAX(transaction_history_date) as transaction_history_date FROM daily_transaction_history WHERE transaction_message='Advertise Add' and user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->transaction_history_date;
		}
		return $tempTitle;

	}
	//Share Buy & sell
	public function get_transaction_history_latest_date($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MAX(transaction_history_date) as transaction_history_date FROM transaction_history WHERE transaction_message='Share_Buy_and_Sell_Amount' AND user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();

		//echo '<pre>';print_r($dates[0]->click_date);echo '</pre>';	die();
		
		if($rslt)
		{
			$tempTitle = $rslt[0]->transaction_history_date;
		}
		return $tempTitle;
	}
	public function get_transaction_history_first_date($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MIN(transaction_history_date) as transaction_history_date FROM transaction_history WHERE transaction_message='Share_Buy_and_Sell_Amount' AND user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();

		//echo '<pre>';print_r($dates[0]->click_date);echo '</pre>';	die();
		if($rslt)
		{
			$tempTitle = $rslt[0]->transaction_history_date;
		}
		return $tempTitle;
	}
	public function get_daily_transaction_history_latest_date_Share_Buy_and_Sell_Amount($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MAX(transaction_history_date) as transaction_history_date FROM daily_transaction_history WHERE transaction_message='Share Buy and Sell_Amount' and user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->transaction_history_date;
		}
		return $tempTitle;

	}
	
	public function get_admin_sharebuy_and_sell_Amount_by_date($userID,$dt1,$dt2)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS transaction_amount FROM transaction_history WHERE transaction_message='Share_Buy_and_Sell_Amount' AND transaction_history_date < ".$dt2." AND transaction_history_date >= ".$dt1." AND user_id = ".$userID." " ;

		
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->transaction_amount;
		}
		return $tempTitle;
	}
	
	//client_cash
	public function get_client_cash_Amount_transaction_history($userID,$dt)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS cash_amount FROM transaction_history WHERE transaction_message='Paid_To_Click'   AND transaction_history_date <= ".$dt." AND user_id = ".$userID." " ;
			
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->cash_amount;
		}
		return $tempTitle;
	}
	public function get_client_cash_Amount_First($userID,$dt)
	{
		
		//echo '<pre>';print_r($dt);echo '</pre>';	die();	
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(cash_amount),0) AS cash_amount FROM client_cash WHERE client_cash_publish = 1 AND click_date <= ".$dt." AND client_id = ".$userID." " ;
		
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->cash_amount;
		}
		return $tempTitle;
			
	}
	public function get_client_cash_history_date($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT cash_last_update_date FROM transaction_main WHERE publish_flag = 1 AND user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->cash_last_update_date;
		}
		return $tempTitle;

	}
	public function get_client_cash_Amount($userID,$dt1,$dt2)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(cash_amount),0) AS cash_amount FROM client_cash WHERE client_cash_publish = 1 AND click_date > ".$dt2." AND click_date <= ".$dt1." AND client_id = ".$userID." " ;

		//echo '<pre>';print_r($qry);echo '</pre>';	die();	
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->cash_amount;
		}
		return $tempTitle;
	}
	public function get_client_cash_Amount_by_date($userID,$dt1,$dt2)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(cash_amount),0) AS cash_amount FROM client_cash WHERE client_cash_publish = 1 AND click_date < ".$dt2." AND click_date >= ".$dt1." AND client_id = ".$userID." " ;

		//echo '<pre>';print_r($qry);echo '</pre>';	//die();	
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->cash_amount;
		}
		return $tempTitle;
	}
	
	// client_shares
	public function get_client_shares_latest_date($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MAX(click_date) as click_date FROM client_shares WHERE client_shares_publish = 1 AND client_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->click_date;
		}
		return $tempTitle;
	}
	public function get_client_shares_first_date($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT MIN(click_date) as click_date FROM client_shares WHERE client_shares_publish = 1 AND client_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->click_date;
		}
		return $tempTitle;
	}
	//client_shares
	public function get_client_shares_Amount_transaction_history($userID,$dt)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS shares_amount FROM transaction_history WHERE transaction_message='Share_To_Click' AND transaction_history_date <= ".$dt." AND user_id = ".$userID." " ;
			
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->shares_amount;
		}
		return $tempTitle;
			
	}
	public function get_cash_Amount_transaction_history_for_admin($userID,$dt)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS cash_amount FROM transaction_history WHERE (transaction_message='Paid_To_Click' || transaction_message='Share_Buy_and_Sell_Amount' || transaction_message='Advertise Add' ) AND transaction_history_date <= ".$dt." AND user_id = ".$userID." " ;
			
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->cash_amount;
		}
		return $tempTitle;
			
		
	}
	public function get_client_shares_Amount_First($userID,$dt)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(shares_amount),0) AS shares_amount FROM client_shares WHERE client_shares_publish = 1 AND click_date <= ".$dt." AND client_id = ".$userID." " ;
			
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->shares_amount;
		}
		return $tempTitle;
			
	}
	// public function get_gifted_shares_Amount($userID,$dt)
	// {
		// $qry = "SELECT IFNULL(SUM(transaction_amount),0) AS gifted_amount FROM transaction_history WHERE  user_id = ".$userID." AND transaction_message='Share_Gifted'" ;
		//transaction_history_date <= ".$dt." AND
		////echo '<pre>';print_r($qry);echo '</pre>';	die();
		// $query = $this->db->query($qry);
		// $data = $query->result();
			
		// return $data[0]->gifted_amount;
	// }
	
	public function get_client_shares_history_date($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT share_last_update_date FROM transaction_main WHERE publish_flag = 1 AND user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->share_last_update_date;
		}
		return $tempTitle;

	}
	public function get_client_share_Amount($userID,$dt1,$dt2)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(shares_amount),0) AS shares_amount FROM client_shares WHERE client_shares_publish = 1 AND click_date > ".$dt2." AND click_date <= ".$dt1." AND client_id = ".$userID." " ;

		
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->shares_amount;
		}
		return $tempTitle;
	}
	public function get_client_share_Amount_by_date($userID,$dt1,$dt2)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(shares_amount),0) AS shares_amount FROM client_shares WHERE client_shares_publish = 1 AND click_date < ".$dt2." AND click_date >= ".$dt1." AND client_id = ".$userID." " ;

		
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->shares_amount;
		}
		return $tempTitle;
	}
	public function get_cash_amount_transaction_main($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT IFNULL(SUM(cash_amount),0) AS  cash_amount FROM transaction_main WHERE publish_flag = 1 AND user_id = ".$userID."";
		//$qryOne="SELECT cash_amount FROM transaction_main WHERE publish_flag = 1 AND user_id = 4";
		$query = $this->db->query($qryOne);
		
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->cash_amount;
		}
		return $tempTitle;
			
	}
	public function get_shares_amount_transaction_main($userID)
	{
		$tempTitle = "";
		$qryOne="SELECT IFNULL(SUM(share_amount),0) AS share_amount FROM transaction_main WHERE publish_flag = 1 AND user_id = ".$userID."";
		//$qryOne="SELECT share_amount FROM transaction_main WHERE publish_flag = 1 AND user_id = 4";
		$query = $this->db->query($qryOne);
		
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->share_amount;
		}
		return $tempTitle;
						
	}
	
	public function get_gifted_shares_Amount($userID)
	{
		$tempTitle = "";
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS gifted_amount FROM transaction_history WHERE  user_id = ".$userID." AND transaction_message='Share_Gifted'" ;
		$query = $this->db->query($qry);
		$rslt = $query->result();
		if($rslt)
		{
			$tempTitle = $rslt[0]->gifted_amount;
		}
		return $tempTitle;
						
	}
	
}
?>