<?php
class User_transaction_update_scheduler_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	public function getAllUserID()
	{
		$qry = "SELECT userID FROM app_users WHERE userRoleID!=1 ";
		$query = $this->db->query($qry);
		return $query->result();
	}
	public function user_transaction_history_existence($userID)
	{
		$qry = "SELECT * FROM user_transaction_history WHERE user_id = ".$userID." order by transaction_history_id desc";
		$query = $this->db->query($qry);
		$tmp = $query->result();
		return $tmp;
	}
	
	public function user_transaction_main_existence($userID)
	{
		$qry = "SELECT * FROM user_transaction_main WHERE user_id = ".$userID." ";
		$query = $this->db->query($qry);
		$tmp = $query->result();
		return $tmp;
	}
	
	// client_cash
	public function get_client_cash_latest_date($userID)
	{
		$qryOne="SELECT MAX(click_date) as click_date FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$dates = $query->result();

		//echo '<pre>';print_r($dates[0]->click_date);echo '</pre>';	die();
		return $dates[0]->click_date;
	}
	//client_cash
	public function get_client_cash_Amount_First($userID,$dt)
	{
		
		//echo '<pre>';print_r($dt);echo '</pre>';	die();	
	
		$qry = "SELECT IFNULL(SUM(cash_amount),0) AS cash_amount FROM client_cash WHERE client_cash_publish = 1 AND click_date <= ".$dt." AND client_id = ".$userID." " ;
		
		$query = $this->db->query($qry);
		$data = $query->result();
			
		return $data[0]->cash_amount;
	}
	
	public function get_Total_cash_amount_of_user($userID)
	{

		$qry = "SELECT IFNULL(cash_amount,0) AS cash_amount FROM user_transaction_main WHERE  user_id = ".$userID." " ;
		$query = $this->db->query($qry);
		$data = $query->result();
			
		return $data[0]->cash_amount;
	}
	
	
	public function get_client_cash_Amount_transaction_history($userID,$dt)
	{
		
		//echo '<pre>';print_r($dt);echo '</pre>';	die();	
	
		//$qry = "SELECT IFNULL(SUM(cash_amount),0) AS cash_amount FROM client_cash WHERE client_cash_publish = 1 AND click_date <= ".$dt." AND client_id = ".$userID." " ;
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS cash_amount FROM user_transaction_history WHERE (transaction_message='Paid_To_Click' || transaction_message='Share_selled_Cash' )   AND transaction_history_date <= ".$dt." AND user_id = ".$userID." " ;
			
		$query = $this->db->query($qry);
		$data = $query->result();
			
		return $data[0]->cash_amount;
	}
	public function get_client_buy_share_deduct_cash_Amount($userID,$dt)
	{
		
		//echo '<pre>';print_r($dt);echo '</pre>';	die();	
	
		//$qry = "SELECT IFNULL(SUM(cash_amount),0) AS cash_amount FROM client_cash WHERE client_cash_publish = 1 AND click_date <= ".$dt." AND client_id = ".$userID." " ;
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS cash_amount FROM user_transaction_history WHERE (transaction_message='Share_Buy_Cash' || transaction_message='Admin_Percentage_from_buyer' || transaction_message='Admin_Percentage_from_seller')   AND transaction_history_date <= ".$dt." AND user_id = ".$userID." " ;
			
		$query = $this->db->query($qry);
		$data = $query->result();
			
		return $data[0]->cash_amount;
	}
	public function get_client_cash_history_date($userID)
	{
		$qryOne="SELECT cash_last_update_date FROM user_transaction_main WHERE publish_flag = 1 AND user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$dates = $query->result();

		return $dates[0]->cash_last_update_date;
	}
	public function get_client_cash_Amount($userID,$dt1,$dt2)
	{
		$qry = "SELECT IFNULL(SUM(cash_amount),0) AS cash_amount FROM client_cash WHERE client_cash_publish = 1 AND click_date > ".$dt2." AND click_date <= ".$dt1." AND client_id = ".$userID." " ;

		//echo '<pre>';print_r($qry);echo '</pre>';	die();	
		$query = $this->db->query($qry);
		$data = $query->result();
		
		
			
		return $data[0]->cash_amount;
	}
	
	// client_shares
	public function get_client_shares_latest_date($userID)
	{
		$qryOne="SELECT MAX(click_date) as click_date FROM client_shares WHERE client_shares_publish = 1 AND client_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$dates = $query->result();

		return $dates[0]->click_date;
	}
	//client_shares
	public function get_client_shares_Amount_transaction_history($userID,$dt)
	{
		

	
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS shares_amount FROM user_transaction_history WHERE (transaction_message='Share_To_Click' || transaction_message='Admin_Gifts' || transaction_message='Cancel_Selled_Share' || transaction_message='Buying_Share') AND transaction_history_date <= ".$dt." AND user_id = ".$userID." " ;
			
		$query = $this->db->query($qry);
		$data = $query->result();
			
		return $data[0]->shares_amount;
	}
	public function get_client_shares_Amount_First($userID,$dt)
	{
		

	
		$qry = "SELECT IFNULL(SUM(shares_amount),0) AS shares_amount FROM client_shares WHERE client_shares_publish = 1 AND click_date <= ".$dt." AND client_id = ".$userID." " ;
			
		$query = $this->db->query($qry);
		$data = $query->result();
			
		return $data[0]->shares_amount;
	}
	// public function get_gifted_shares_Amount($userID,$dt)
	// {
		// $qry = "SELECT IFNULL(SUM(transaction_amount),0) AS gifted_amount FROM transaction_history WHERE  user_id = ".$userID." AND transaction_message='Share_Gifted'" ;
		//transaction_history_date <= ".$dt." AND
		////echo '<pre>';print_r($qry);echo '</pre>';	die();
		// $query = $this->db->query($qry);
		// $data = $query->result();
			
		// return $data[0]->gifted_amount;
	// }
	
	public function get_client_shares_history_date($userID)
	{
		$qryOne="SELECT share_last_update_date FROM user_transaction_main WHERE publish_flag = 1 AND user_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$dates = $query->result();

		return $dates[0]->share_last_update_date;
	}
	public function get_client_share_Amount($userID,$dt1,$dt2)
	{
		$qry = "SELECT IFNULL(SUM(shares_amount),0) AS shares_amount FROM client_shares WHERE client_shares_publish = 1 AND click_date > ".$dt2." AND click_date <= ".$dt1." AND client_id = ".$userID." " ;

		
		$query = $this->db->query($qry);
		$data = $query->result();
			// echo '<pre>';print_r($data);echo '</pre>';	die();
		return $data[0]->shares_amount;
	}
	
	public function get_cash_amount_transaction_main($userID)
	{
		$qryOne="SELECT IFNULL(SUM(cash_amount),0) AS  cash_amount FROM user_transaction_main WHERE publish_flag = 1 AND user_id = ".$userID."";
		//$qryOne="SELECT cash_amount FROM transaction_main WHERE publish_flag = 1 AND user_id = 4";
		$query = $this->db->query($qryOne);
		
		$data = $query->result();
			
		return $data[0]->cash_amount;
	}
	public function get_shares_amount_transaction_main($userID)
	{
		$qryOne="SELECT IFNULL(SUM(share_amount),0) AS share_amount FROM user_transaction_main WHERE publish_flag = 1 AND user_id = ".$userID."";
		//$qryOne="SELECT share_amount FROM transaction_main WHERE publish_flag = 1 AND user_id = 4";
		$query = $this->db->query($qryOne);
		
		$data = $query->result();
			
		return $data[0]->share_amount;
	}
	
	public function get_gifted_shares_Amount($userID)
	{
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS gifted_amount FROM user_transaction_history WHERE  user_id = ".$userID." AND transaction_message='Share_Gifted'" ;
		$query = $this->db->query($qry);
		$data = $query->result();
			
		return $data[0]->gifted_amount;
	}
	public function get_selled_shares_Amount_by_time($userID,$time)
	{
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS Selled_Share_amount FROM user_transaction_history WHERE  user_id = ".$userID." AND transaction_message='Selled_Share' AND transaction_history_date < ".$time." " ;
		$query = $this->db->query($qry);
		$data = $query->result();
			
		return $data[0]->Selled_Share_amount;
	}
	public function get_selled_shares_Amount($userID)
	{
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS Selled_Share_amount FROM user_transaction_history WHERE  user_id = ".$userID." AND transaction_message='Selled_Share'" ;
		$query = $this->db->query($qry);
		$data = $query->result();
			
		return $data[0]->Selled_Share_amount;
	}
	public function get_cancel_selled_shares_Amount($userID)
	{
		$qry = "SELECT IFNULL(SUM(transaction_amount),0) AS Cancel_Selled_Share_amount FROM user_transaction_history WHERE  user_id = ".$userID." AND transaction_message='Cancel_Selled_Share'" ;
		$query = $this->db->query($qry);
		$data = $query->result();
			
		return $data[0]->Cancel_Selled_Share_amount;
	}
	
}
?>