<?php
class Referral_model extends CI_Model {

	private $m_calculate_total = 0;
	private $m_allowed_max_level = 0;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_childs($parent_id)
	{
		$qry = "SELECT * FROM referral_relations WHERE parent_id = ".$parent_id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_max_levels()
	{
		$qry = "SELECT MAX(price_lookup_id) AS n FROM referral_price_lookup ";
		$query = $this->db->query($qry);
		$result = $query->result();
		return $result[0]->n;
	}

	public function get_total_clicked_cash($user_id)
	{
		$qry = "SELECT SUM(cash_amount) as cash FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$user_id;
		$query = $this->db->query($qry);
		$result = $query->result();
		return $result[0]->cash;
	}

	public function get_total_clicked_cash_by_date($user_id, $date)
	{
		$qry = "SELECT SUM(cash_amount) as cash FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$user_id."
				AND click_date > ".$date;
		$query = $this->db->query($qry);
		$result = $query->result();
		return $result[0]->cash;
	}

	public function get_price_percentage_by_level($level_id)
	{
		$qry = "SELECT price_percentage FROM referral_price_lookup WHERE level_number = ".$level_id;
		$query = $this->db->query($qry);
		$result = $query->result();
		if(sizeof($result)>0){
			return $result[0]->price_percentage;
		}else{
			return 0;
		}
	}

	//recursive calculation part---------------------------------------------------------------------------------------------------------------------

	public function calculate_referral($user_id)
	{
		$this->m_calculate_total = 0;
		$this->m_allowed_max_level = $this->get_max_levels();
		// call method($user_id, 0)  -- 0 for root, no child as default
		$this->calculate_referral_recursive($user_id, 0);
		return $this->m_calculate_total;
	}

	public function calculate_referral_recursive($user_id, $in_track_level)
	{
		// $query = check how many child he has  -- $user_id
		$temp_max_level = $in_track_level+1;
		if($temp_max_level <= $this->m_allowed_max_level)
		{
			$temp_childs_arr = $this->get_childs($user_id);
			if (sizeof($temp_childs_arr)>0)
			{
				//has child
				$temp_Track_level = $in_track_level + 1;
				foreach ($temp_childs_arr as $row) 
				{
					$this->calculate_referral_recursive($row->child_id, $temp_Track_level);
				}
			}// else
		}
		$temp_total = $this->get_total_clicked_cash($user_id);
		$temp_price_percentage = $this->get_price_percentage_by_level($in_track_level);
		$temp_total_2 = ($temp_total*$temp_price_percentage)/100; 
		$this->m_calculate_total = $this->m_calculate_total + $temp_total_2;
	}





	//-----------------------------------------------------------------------------------------------------------------------------------------------
	public function calculate_referral_by_date($user_id, $date)
	{
		$this->m_calculate_total = 0;
		$this->m_allowed_max_level = $this->get_max_levels();
		// call method($user_id, 0)  -- 0 for root, no child as default
		$this->calculate_referral_recursive_by_date($user_id, 0, $date);
		return $this->m_calculate_total;
	}

	public function calculate_referral_recursive_by_date($user_id, $in_track_level, $date)
	{
		$temp_max_level = $in_track_level+1;
		if($temp_max_level <= $this->m_allowed_max_level)
		{
			$temp_childs_arr = $this->get_childs($user_id);
			if (sizeof($temp_childs_arr)>0)
			{
				//has child
				$temp_Track_level = $in_track_level + 1;
				foreach ($temp_childs_arr as $row) 
				{
					$this->calculate_referral_recursive_by_date($row->child_id, $temp_Track_level, $date);
				}
			}// else
		}
		$temp_total = $this->get_total_clicked_cash_by_date($user_id, $date);
		$temp_price_percentage = $this->get_price_percentage_by_level($in_track_level);
		$temp_total_2 = ($temp_total*$temp_price_percentage)/100; 
		$this->m_calculate_total = $this->m_calculate_total + $temp_total_2;
	}
}
?>