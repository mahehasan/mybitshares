<?php
class Mybitshares_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
	public function getAllCategoryByParent($parent = 0)
	{
		$qry = "SELECT * FROM category WHERE category_publish = 1 AND parent_category = ".$parent;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function getCategoryDetail($id)
	{
		$qry = "SELECT * FROM category WHERE category_publish = 1 AND category_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function getPriceOfAd($ad_id)
	{
		$qry = "SELECT * FROM category WHERE category_publish = 1 AND category_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function getProductListByCategory($id)
	{
		if ($id == 'All'){
			$qry = "SELECT * FROM product WHERE product_category_id";
		}else{
			$qry = "SELECT * FROM product WHERE product_category_id = ".$id;
		}
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function add_to_cart($product_id, $product_quantity, $unit_price)
	{
		//cleanup old garbage
		// $this->clear_old_carts();
		//check if the cart is already inserted in master
		$qry = "SELECT * FROM cart_master WHERE publish_flag = 1 AND secret = '".$this->session->userdata('cart_id')."'";
		$query = $this->db->query($qry);
		$master_data = $query->result();
		$numrows = $query->num_rows();
		if ($numrows==0){
			$this->db->insert('cart_master', array('secret' => $this->session->userdata('cart_id'),
													'created_on' => date('Y-m-d H:i:s')));
			//data inserted, find the id of hte row
			$qry = "SELECT * FROM cart_master WHERE publish_flag = 1 AND secret = '".$this->session->userdata('cart_id')."'";
			$query = $this->db->query($qry);
			$master_data = $query->result();
		}
		$cart_id = $master_data[0]->id;//got master id
		//now check if the product already exists
		$qry = "SELECT * FROM cart_detail WHERE publish_flag = 1 AND product_id = ".$product_id." 
				AND cart_master_id = ".$cart_id;
		$query = $this->db->query($qry);
		$numrows = $query->num_rows();
		if ($numrows>0){//that product exists
			$result = $query->result();
			$quantity_before = $result[0]->quantity;
			$data = array('quantity' => $product_quantity + $quantity_before);
			$this->db->where('cart_master_id', $cart_id);
			$this->db->where('product_id', $product_id);
			$this->db->update('cart_detail', $data);
		}else{//product dont exists
			$data = array('cart_master_id' => $cart_id,
						'product_id' => $product_id,
						'quantity' => $product_quantity,
						'unit_price' => $unit_price
						 );
			$this->db->insert('cart_detail', $data);
		}
		return $this->get_cart_summary();
	}

	public function clear_old_carts()
	{
		//tis is a garbage cleanup func
		$qry = "DELETE FROM cart_detail WHERE cart_master_id IN (SELECT id FROM cart_master WHERE checked_out = 0 AND DATE(created_on) < DATE_SUB(CURDATE(), INTERVAL 2 DAY)) ";
		$query = $this->db->query($qry);

		$qry = "DELETE FROM cart_master WHERE checked_out = 0 AND DATE(created_on) < DATE_SUB(CURDATE(), INTERVAL 2 DAY) ";
		$query = $this->db->query($qry);
	}

	public function cart_has_items()
	{
		$qry = "SELECT * FROM cart_master WHERE secret = '".$this->session->userdata('cart_id')."'";
		$query = $this->db->query($qry);
		$master_data = $query->result();
		if ($query->num_rows>0){
			$cart_id = $master_data[0]->id;
			$qry = "SELECT * FROM cart_detail WHERE cart_master_id = ".$cart_id;
			$query = $this->db->query($qry);
			$numrows = $query->num_rows();
			if ($numrows>0){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
		
	}
	public function get_cart_summary()
	{
		$qry = "SELECT * FROM cart_master WHERE secret = '".$this->session->userdata('cart_id')."'";
		$query = $this->db->query($qry);
		$master_data = $query->result();
		$cart_id = $master_data[0]->id;
		$qry = "SELECT SUM(quantity) AS item_count, SUM(quantity*unit_price) AS amount FROM cart_detail WHERE cart_master_id = ".$cart_id." GROUP BY cart_master_id";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_cart_contents($secret)
	{
		$qry = "SELECT a.product_id, a.quantity, a.unit_price, b.product_shares, b.product_name, b.product_price FROM cart_detail a, product b				
		 		WHERE a.publish_flag = 1 AND a.product_id = b.product_id AND cart_master_id = (SELECT id FROM cart_master WHERE publish_flag = 1 AND secret = '".$secret."' )";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_price_by_id($id)
	{
		$qry = "SELECT price_advertise FROM price_lookup WHERE price_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->price_advertise;
	}
	
	public function get_site_server_code_by_menuID($menu_id)
	{
		$result = array();
		$qry = "SELECT * FROM advertise WHERE menu1_id = $menu_id AND advertise_publish=1 GROUP BY site_server_code";
		$query = $this->db->query($qry);
		$result = $query->result_array();
		if($result){
			foreach($result as $arr){
				$results[$arr['site_server_code']] = $arr;
			}
			return $results;
		}else
			return false;
	}

	public function get_all_user_data_by_id($id)
	{
		$qry = "SELECT * FROM app_users WHERE userID = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_all_balance_history_by_id($id)
	{
		$qry = "SELECT * FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_all_balance_total($id)
	{
		//$qry = "SELECT SUM(cash_amount) AS balance_total FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$id;
		
		$qry = "SELECT cash_amount as balance_total FROM user_transaction_main WHERE user_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->balance_total;
	}

	public function get_all_share_history_by_id($id)
	{
		$qry = "SELECT * FROM client_shares WHERE client_shares_publish = 1 AND client_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_all_share_total($id)
	{
		//$qry = "SELECT SUM(shares_amount) AS share_total FROM client_shares WHERE client_shares_publish = 1 AND client_id = ".$id;
		$qry = "SELECT share_amount as share_total FROM user_transaction_main WHERE user_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->share_total;
	}

	public function get_clicked_ads($menu_id)
	{
		$id = $this->session->userdata('userID');
		$qry = "SELECT advertise_id FROM manage_client_advertise WHERE manage_client_advertise_publish = 1 AND client_id = ".$id. " AND menu_id =".$menu_id;
		$query = $this->db->query($qry);
		$result = $query->result();
		$data = '';
		foreach ($result as $row) {
			$data = $data.','.$row->advertise_id;
		}
		return explode(',', $data);
	}

	public function ad_clicked($data)
	{
		$param = explode('/',$this->input->post('url'));
		$menu_id = $this->input->post('menu');
		// Array(
		// 		    [0] => http:
		// 		    [1] => 
		// 		    [2] => lawyersbd.org
		//			[3]		mybitshares
		//			[4]		menu2_click
		// 		    [5] => siteserver
		// 		    [6] => $site_server_url
		// 		    [7] => $site_server_code
		// 		    [8] => site_advertise_id
		// 		    [9] => price
		// 		)
		//check if this user has been clicked the ad before
		$qry = "SELECT * FROM manage_client_advertise WHERE advertise_id = ".$param[8]." AND client_id = ".$this->session->userdata('userID');
		$query = $this->db->query($qry);
		$rslt = $query->num_rows();
		$iiiid = 0;
		if ($rslt>0){//data inserted before, lets update
			$rslt_data = $query->result();
			$previous_id = $rslt_data[0]->manage_client_advertise_id;
			$data = array(
			'manage_client_advertise_publish' => 1, //turn off that add
			'click_time' => time() 
			);
			$this->db->where('manage_client_advertise_id', $previous_id);
			$this->db->update('manage_client_advertise', $data);
			$iiiid = $previous_id;
		}else{//this user hasn't clicked on the ad before
			$data = array(
			'advertise_id' => $param[5], 
			'client_id' => $this->session->userdata('userID'), 
			'manage_client_advertise_publish' => 1, //turn off that add
			'click_time' => time() 
			);
			$this->db->insert('manage_client_advertise', $data);
			$iiiid = $this->db->insert_id();
		}

		$qry = "SELECT * FROM click_calculation WHERE advertise_id = ".$param[8]." AND user_id = ".$this->session->userdata('userID');
		$query = $this->db->query($qry);
		$temp_rslt = $query->num_rows();
		$rslt = $query->result();
		if($temp_rslt>0){//there exists rows that was inserted before so update
			$previous_share = $rslt[0]->current_click_share;
			$previous_click = $rslt[0]->current_click_paid;
			if ($menu_id==1){//share to click
				$previous_share++;
			}else if ($menu_id==2){//share to click
				$previous_click++;
			}

			$click_calculation_id = $rslt[0]->click_calculation_id;
			$data = array('current_click_share' => $previous_share,
						'current_click_paid' => $previous_click);
			$this->db->where('click_calculation_id', $click_calculation_id);
			$this->db->update('click_calculation', $data);
		}else{//no row exists, insert new
			$previous_share = 0;
			$previous_click = 0;
			if ($menu_id==1){//share to click
				$previous_share++;
			}else if ($menu_id==2){//share to click
				$previous_click++;
			}
			$temp_ad_id =  $this->get_ad_detail_by_id($param[8]);
			$temp_ad_price_id = $temp_ad_id[0]->advertise_price_id;
			$data = array('current_click_share' => $previous_share,
						'user_id' => $this->session->userdata('userID'),
						'price_id' => $temp_ad_price_id,
						'advertise_id' => $param[8],
						'current_click_paid' => $previous_click);
			$this->db->insert('click_calculation', $data);
		}
		
		$tempIP = "";
		
		if($this->CI->session->userdata('ip_address'))
		{
			$tempIP = $this->CI->session->userdata('ip_address');
		}
		else 
		{
			$tempIP = $_SERVER['REMOTE_ADDR'];
		}
		
		
		//insert data on client_cash
		$data = array(
			'site_server_code' => $param[7],
			'site_server_advertise_id' => $param[8], 
			'client_id' => $this->session->userdata('userID'), 
			'cash_amount' => $param[9], 
			'client_cash_publish' => 0, 
			'click_date' => time(), 
			//'ip_address' => $this->input->ip_address()
			'ip_address' => $tempIP
				
			);
		
		$this->db->insert('client_cash', $data);
		
		return $iiiid;
	}
	
	// for paid to click
	public function ad_clicked2($site_server_url,$site_server_code,$site_advertise_id, $price_advertise)
	{
		//$param = explode('/',$this->input->post('url'));
		//$menu_id = $this->input->post('menu');
		// Array(
		// 		    [0] => http:
		// 		    [1] =>
		// 		    [2] => lawyersbd.org
		//			[3]		mybitshares
		//			[4]		menu2_click
		// 		    [5] => siteserver
		// 		    [6] => $site_server_url
		// 		    [7] => $site_server_code
		// 		    [8] => site_advertise_id
		// 		    [9] => price
		// 		)
		//check if this user has been clicked the ad before
		$advertise_id = 0;
		
		$qry = "SELECT advertise_id FROM advertise WHERE site_server_code = ".$site_server_code." AND site_server_advertise_id = ".$site_advertise_id;
		$query = $this->db->query($qry);
		$rslt = $query->num_rows();
		
		if ($rslt > 0){
			$rslt_data = $query->result();
			$advertise_id = $rslt_data[0]->advertise_id;
		}
		
		
		$qry = "SELECT * FROM manage_client_advertise WHERE advertise_id = ".$advertise_id." AND menu_id = 5  AND client_id = ".$this->session->userdata('userID');
		$query = $this->db->query($qry);
		$rslt = $query->num_rows();
		$iiiid = 0;
		if ($rslt>0){//data inserted before, lets update
			$rslt_data = $query->result();
			$previous_id = $rslt_data[0]->manage_client_advertise_id;
			$data = array(
					'manage_client_advertise_publish' => 1, //turn off that add
					'click_time' => time()
			);
			$this->db->where('manage_client_advertise_id', $previous_id);
			$this->db->where('menu_id', 5);
			$this->db->update('manage_client_advertise', $data);
			$iiiid = $previous_id;
		}else{//this user hasn't clicked on the ad before
			$data = array(
					'advertise_id' => $advertise_id,
					'client_id' => $this->session->userdata('userID'),
					'menu_id'=> 5,
					'manage_client_advertise_publish' => 1, //turn off that add
					'click_time' => time()
			);
			$this->db->insert('manage_client_advertise', $data);
			$iiiid = $this->db->insert_id();
		}
	
		$qry = "SELECT * FROM click_calculation WHERE advertise_id = ".$advertise_id." AND user_id = ".$this->session->userdata('userID');
		$query = $this->db->query($qry);
		$temp_rslt = $query->num_rows();
		$rslt = $query->result();
		if($temp_rslt>0)
		{//there exists rows that was inserted before so update
			$previous_share = $rslt[0]->current_click_share;
			$previous_click = $rslt[0]->current_click_paid;
			//if ($menu_id==1){//share to click
			//	$previous_share++;
			//}else if ($menu_id==2){//share to click
				$previous_click++;
			//}
	
			$click_calculation_id = $rslt[0]->click_calculation_id;
			$data = array('current_click_share' => $previous_share,
					'current_click_paid' => $previous_click);
			$this->db->where('click_calculation_id', $click_calculation_id);
			$this->db->update('click_calculation', $data);
		}
		else
		{//no row exists, insert new
			//$previous_share = 0;
			$previous_click = 0;
		//	if ($menu_id==1){//share to click
		//		$previous_share++;
			//}else if ($menu_id==2){//share to click
				$previous_click++;
			//}
			$temp_ad_id =  $this->get_ad_detail_by_id($advertise_id);
			$temp_ad_price_id = $temp_ad_id[0]->advertise_price_id;
			$data = array('current_click_share' => $previous_share,
					'user_id' => $this->session->userdata('userID'),
					'price_id' => $temp_ad_price_id,
					'advertise_id' => $advertise_id,
					'current_click_paid' => $previous_click);
			$this->db->insert('click_calculation', $data);
		}
	
		/*
		$tempIP = "";
	
		if($this->CI->session->userdata('ip_address'))
		{
			$tempIP = $this->CI->session->userdata('ip_address');
		}
		else
		{
			$tempIP = $_SERVER['REMOTE_ADDR'];
		}
	
	
		//insert data on client_cash
		$data = array(
				'site_server_code' => $site_server_code,
				'site_server_advertise_id' => $site_advertise_id,
				'client_id' => $this->session->userdata('userID'),
				'cash_amount' => $price_advertise,
				'client_cash_publish' => 0,
				'click_date' => time(),
				//'ip_address' => $this->input->ip_address()
				'ip_address' => $tempIP
	
		);
	
		$this->db->insert('client_cash', $data);
	
		return $iiiid;
		*/
	}

	
	// for share to click  -- fix
	public function ad_clicked3($site_server_url,$site_server_code,$site_advertise_id, $price_advertise)
	{
		
		//$param = explode('/',$this->input->post('url'));
		//$menu_id = $this->input->post('menu');
		// Array(
		// 		    [0] => http:
		// 		    [1] =>
		// 		    [2] => lawyersbd.org
		//			[3]		mybitshares
		//			[4]		menu2_click
		// 		    [5] => siteserver
		// 		    [6] => $site_server_url
		// 		    [7] => $site_server_code
		// 		    [8] => site_advertise_id
		// 		    [9] => price
		// 		)
		//check if this user has been clicked the ad before
		$advertise_id = 0;
	
		$qry = "SELECT advertise_id FROM advertise WHERE site_server_code = ".$site_server_code." AND site_server_advertise_id = ".$site_advertise_id;
		$query = $this->db->query($qry);
		$rslt = $query->num_rows();
	
		if ($rslt > 0){
			$rslt_data = $query->result();
			$advertise_id = $rslt_data[0]->advertise_id;
		}
	
	
		$qry = "SELECT * FROM manage_client_advertise WHERE advertise_id = ".$advertise_id." AND menu_id = 4  AND client_id = ".$this->session->userdata('userID');
		$query = $this->db->query($qry);
		$rslt = $query->num_rows();
		$iiiid = 0;
		if ($rslt>0){//data inserted before, lets update
			$rslt_data = $query->result();
			$previous_id = $rslt_data[0]->manage_client_advertise_id;
			$data = array(
					'manage_client_advertise_publish' => 1, //turn off that add
					'click_time' => time()
			);
			$this->db->where('manage_client_advertise_id', $previous_id);
			$this->db->where('menu_id', 4);
			$this->db->update('manage_client_advertise', $data);
			$iiiid = $previous_id;
		}else{//this user hasn't clicked on the ad before
			$data = array(
					'advertise_id' => $advertise_id,
					'client_id' => $this->session->userdata('userID'),
					'menu_id'=> 4,
					'manage_client_advertise_publish' => 1, //turn off that add
					'click_time' => time()
			);
			$this->db->insert('manage_client_advertise', $data);
			$iiiid = $this->db->insert_id();
		}
	
		$qry = "SELECT * FROM click_calculation WHERE advertise_id = ".$advertise_id." AND user_id = ".$this->session->userdata('userID');
		$query = $this->db->query($qry);
		$temp_rslt = $query->num_rows();
		$rslt = $query->result();
		if($temp_rslt>0){//there exists rows that was inserted before so update
			$previous_share = $rslt[0]->current_click_share;
			$previous_click = $rslt[0]->current_click_paid;
			//if ($menu_id==1){//share to click
				$previous_share++;
			//}else if ($menu_id==2){//share to click
			//	$previous_click++;
			//}
	
			$click_calculation_id = $rslt[0]->click_calculation_id;
			$data = array('current_click_share' => $previous_share,
					'current_click_paid' => $previous_click);
			$this->db->where('click_calculation_id', $click_calculation_id);
			$this->db->update('click_calculation', $data);
		}else{//no row exists, insert new
			$previous_share = 0;
			$previous_click = 0;
			//if ($menu_id==1){//share to click
				$previous_share++;
			//}else if ($menu_id==2){//share to click
			//	$previous_click++;
			//}
			$temp_ad_id =  $this->get_ad_detail_by_id($advertise_id);
			$temp_ad_price_id = $temp_ad_id[0]->advertise_price_id;
			$data = array('current_click_share' => $previous_share,
					'user_id' => $this->session->userdata('userID'),
					'price_id' => $temp_ad_price_id,
					'advertise_id' => $advertise_id,
					'current_click_paid' => $previous_click);
			$this->db->insert('click_calculation', $data);
		}
	
		/*
			$tempIP = "";
	
			if($this->CI->session->userdata('ip_address'))
			{
			$tempIP = $this->CI->session->userdata('ip_address');
			}
			else
			{
			$tempIP = $_SERVER['REMOTE_ADDR'];
			}
	
	
			//insert data on client_cash
			$data = array(
			'site_server_code' => $site_server_code,
			'site_server_advertise_id' => $site_advertise_id,
			'client_id' => $this->session->userdata('userID'),
			'cash_amount' => $price_advertise,
			'client_cash_publish' => 0,
			'click_date' => time(),
			//'ip_address' => $this->input->ip_address()
			'ip_address' => $tempIP
	
			);
	
			$this->db->insert('client_cash', $data);
	
			return $iiiid;
			*/
	}
	
	public function get_ad_detail_by_id($id)
	{
		$qry = "SELECT * FROM advertise WHERE site_server_advertise_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_user_details_by_id($id)
	{
		$qry = "SELECT * FROM app_users a 
						INNER JOIN country b ON (a.country = b.id)
						WHERE userID = ". $id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_country_list()
	{
		$qry = "SELECT * FROM country WHERE 1";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function update_user_data($id)
	{
		$data = array('firstName' => $this->input->post('firstName'),
						'lastName' => $this->input->post('lastName'),
						'phone' => $this->input->post('phone'),
						'address1' => $this->input->post('address1'),
						'town' => $this->input->post('town'),
						'country' => $this->input->post('country'),
						'postCode' => $this->input->post('postCode')
		 						);
		$this->db->where('userID',$id);
		$this->db->update('app_users', $data);
	}

	public function get_sellable_shares($id)
	{
		$qry = "SELECT a.seller_id,a.share_sell_id, a.number_of_shares, b.firstName, b.lastName, a.amount FROM share_sell a, app_users b
				WHERE a.seller_id = b.userID AND a.share_sell_publish = 1 ";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_share_info_by_id($id)
	{
		$qry = "SELECT a.share_sell_id, a.amount, a.number_of_shares, b.userID, b.firstName, b.lastName FROM share_sell a, app_users b
				WHERE a.seller_id = b.userID AND a.share_sell_publish = 1 AND a.share_sell_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_share_sell_percentage()
	{
		$qry = "SELECT property_value FROM configuration WHERE property_name = 'share_sell_percentage'";
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->property_value;
	}

	public function get_share_buy_percentage()
	{
		$qry = "SELECT property_value FROM configuration WHERE property_name = 'share_buy_percentage'";
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->property_value;
	}

	public function get_per_share_price($id)
	{
		$qry = "SELECT per_share_price FROM current_share_price WHERE user_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->per_share_price;
	}

	public function get_share_buy_total_amount($amount)
	{
		$share_buy_percentage = $this->get_share_buy_percentage();
		$total_amount = $amount+(($amount*$share_buy_percentage)/100);
		return $total_amount;
	}

	public function process_buy($data)
	{
		$this->db->insert('share_buy', $data);
	}

	public function adjust_current_share_balance($buyer_id, $seller_id, $buyer_amount, $seller_amount, $used_system_income)
	{
		$this->adjust_current_share_balance_buyer($buyer_id, $buyer_amount, $used_system_income, $this->session->userdata('number_of_shares'));
		$this->adjust_current_share_balance_seller($seller_id, $seller_amount, $this->session->userdata('number_of_shares'));
	}
	
	public function adjust_current_share_balance_buyer($buyer_id, $buyer_amount, $used_system_income, $share_number)
	{
		$qry = "SELECT * FROM user_share_balance WHERE user_id = ".$buyer_id;
		$query = $this->db->query($qry);
		$result = $query->result();
		$rslt = $query->num_rows();
		if($rslt>0){//there was row inserted
			//$previous_balance = $result[0]->current_share_balance;
			$previous_share_number = $result[0]->current_share_number;
			
				//$final_balance = $previous_balance + $balance;
				$final_share_number = $previous_share_number + $share_number;
			
			$data = array('user_id' => $buyer_id,			
					
					'current_share_number' => $final_share_number,
					'date' => time()
			);
			$this->db->where('user_share_balance_id', $result[0]->user_share_balance_id);
			$this->db->update('user_share_balance', $data);
		}else{
			$data = array('user_id' => $buyer_id,					
					'current_share_number' => $share_number,
					'date' => time()
			);
			$this->db->insert('user_share_balance', $data);
		}
		
		if($used_system_income == 1)
		{
			//$current_income = 
			
			$data = $this->get_total_income($buyer_id);
			if(isset($data[0]))
			{
				$current_total = $data[0]->current_amount;
				
				$current_total = $current_total - $buyer_amount;
				
				$this->update_total_income($current_total,$buyer_id);
				
				
				
			}
		}
	}
	
	//$seller_amount = sell amount - system pay, after paying system pay
	public function adjust_current_share_balance_seller($seller_id, $seller_amount, $share_number)
	{
		$qry = "SELECT * FROM user_share_balance WHERE user_id = ".$seller_id;
		$query = $this->db->query($qry);
		$result = $query->result();
		$rslt = $query->num_rows();
		if($rslt>0)
		{//there was row inserted
			//$previous_balance = $result[0]->current_share_balance;
			$previous_share_number = $result[0]->current_share_number;
			
				//$final_balance = $previous_balance - $balance;
				$final_share_number = $previous_share_number - $share_number;
			
			$data = array('user_id' => $seller_id,					
					'current_share_number' => $final_share_number,
					'date' => time()
			);
			$this->db->where('user_share_balance_id', $result[0]->user_share_balance_id);
			$this->db->update('user_share_balance', $data);
			
			//increase total income
			
			$data = $this->get_total_income($seller_id);
			if(isset($data[0]))
			{
				$current_total = $data[0]->current_amount;
			
				$current_total = $current_total + $seller_amount;
			
				$this->update_total_income($current_total,$seller_id);			
			
			}
			else {
				//insert data in total_current_cash
				
				$data = array(
						'user_id' => $seller_id,
						'current_amount' => $seller_amount,
						'date' => time()				
				
				);
				
				$this->insert_total_income($data);
			}
		}
			/*
			 * //seller will have an entry in this table
		else{
			$data = array('user_id' => $seller_id,
					'current_share_balance' => $balance,
					'bonus_amount' => 0,
					'current_share_number' => $share_number,
					'date' => time()
			);
			$this->db->insert('user_share_balance', $data);
		}
		*/
	}
	
	public function process_current_share_balance($user_id, $balance, $buy_sell, $share_number)
	{
		$qry = "SELECT * FROM user_share_balance WHERE user_id = ".$user_id;
		$query = $this->db->query($qry);
		$result = $query->result();
		$rslt = $query->num_rows();
		if($rslt>0){//there was row inserted
			$previous_balance = $result[0]->current_share_balance;
			$previous_share_number = $result[0]->current_share_number;
			if($buy_sell == 'buy'){
				$final_balance = $previous_balance + $balance;
				$final_share_number = $previous_share_number + $share_number;
			}else if($buy_sell == 'sell'){
				$final_balance = $previous_balance - $balance;
				$final_share_number = $previous_share_number - $share_number;
			}
			$data = array('user_id' => $user_id,
						'current_share_balance' => $final_balance,
						'bonus_amount' => 0,
						'current_share_number' => $final_share_number,
						'date' => time()
						 );
			$this->db->where('user_share_balance_id', $result[0]->user_share_balance_id);
			$this->db->update('user_share_balance', $data);
		}else{
			$data = array('user_id' => $user_id,
						'current_share_balance' => $balance,
						'bonus_amount' => 0,
						'current_share_number' => $share_number,
						'date' => time()
						 );
			$this->db->insert('user_share_balance', $data);
		}
	}

	public function user_share_detail_by_id($id)
	{
		$qry = "SELECT * FROM user_share_balance 
				WHERE user_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}
	public function user_total_share_amount_by_id($id)
	{
		$qry = "SELECT * FROM user_transaction_main 
				WHERE user_id = ".$id;
		$query = $this->db->query($qry);
		return $query->result();
	}
	public function add_share_sell($data)
	{
		$this->db->insert('share_sell', $data); 
	}
	public function get_share_sell_count_by_seller_id($id)
	{
		//$qry = "SELECT IFNULL(package_quantity,0) as package_quantity FROM share_sell WHERE seller_id =".$id." order by seller_id desc ";
		$qry = "SELECT COUNT(share_sell_id) as package_quantity FROM share_sell WHERE seller_id =".$id." and share_sell_publish = 1 ";
		$query = $this->db->query($qry);
		//$rslt=$query->result();
		//return $rslt[0]->package_quantity;
		//return $query->first_row($qry)->package_quantity; 
		return $query->row('package_quantity');
	}
	public function change_user_current_share_price($previous_p,$current_p)
	{
		$data = array('per_share_price' => $previous_p,'date' =>time(),'user_id' => $this->session->userdata('userID') );
		$this->db->insert('share_price_history', $data);
		$data = array('per_share_price' => $current_p,'date' =>time() );
		$this->db->where('user_id',$this->session->userdata('userID'));
		$this->db->update('current_share_price', $data);
	}

	public function get_all_bonus_list()
	{
		$qry = "SELECT * FROM share_bonus WHERE bonus_publish = 1";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function get_bonus_by_id($category_id)
	{
		$qry = "SELECT * FROM share_bonus WHERE share_bonus_id = ".$category_id;
		$query = $this->db->query($qry);
		return $query->result();
	}
	
	

	public function add_fund($data)
	{
		$this->db->insert('user_fund', $data); 
	}

	public function get_referrals_count_by_id($id)
	{
		$qry = "SELECT COUNT(*) AS n FROM referral_relations WHERE parent_id =".$id;
		$query = $this->db->query($qry);
		$rslt=$query->result();
		return $rslt[0]->n;
	}

	public function get_referrals_by_id($id)
	{
		$qry = "SELECT * FROM app_users WHERE userID IN (SELECT child_id FROM referral_relations WHERE parent_id =".$id.")";
		$query = $this->db->query($qry);
		return $query->result();
	}
	
	public function add_cash_withdraw($data)
	{
		/*
		$data = array('user_id' => $user_id,
						'current_share_balance' => $balance,
						'bonus_amount' => 0,
						'current_share_number' => $share_number,
						'date' => time()
						 );
						 
						 */
		
		$this->db->insert('cash_withdraw_history', $data);
			//$this->db->insert('user_share_balance', $data);
	}
	//update total directly
	public function update_total_income($in_total_income, $in_user_id)
	{
		$data = array(
				'current_amount' => $in_total_income,
				'date' => time()
		);
		$this->db->where('user_id', $in_user_id);
		$this->db->update('total_current_cash', $data);
	}
	
	//gets the total and add with that
	public function update_total_income2($in_total_income, $in_user_id)
	{
				
		$data = $this->get_total_income($in_user_id);
		if(isset($data[0]))
		{
			$current_total = $data[0]->current_amount;
		
			$current_total = $current_total + $in_total_income;			
			
				
			$data = array(
						
					'current_amount' => $current_total,					
					'date' => time()
						
			);
				
			$this->db->where('user_id', $in_user_id);
			$this->db->update('total_current_cash', $data);
		
		}
		else {
			//insert data in total_current_cash
		
			$data = array(
					'user_id' => $in_user_id,
					'current_amount' => $in_total_income,
					'system_bonus' => 0,
					'date' => time()
		
			);
		
			$this->insert_total_income($data);
		}
	}
	
	public function get_total_income($in_user_id)
	{
		$qry = "SELECT * FROM total_current_cash WHERE user_id =".$in_user_id." ";
		$query = $this->db->query($qry);
		return $query->result();
	}
	
	public function insert_total_income($data){
		
		
		$this->db->insert('total_current_cash', $data);
	}
	
	

	public function add_shipping_address($data)
	{
		$qry = "SELECT * FROM user_shipping WHERE userID = ".$this->session->userdata('userID');
		$query = $this->db->query($qry);
		$temp_rslt = $query->num_rows();
		if($temp_rslt>0){//there exists rows that was inserted before so update
			$this->db->where('userID', $this->session->userdata('userID'));
			$this->db->update('user_shipping', $data);
		}else{//no row exists, insert new
			$data['userID'] = $this->session->userdata('userID');
			$this->db->insert('user_shipping', $data);
		}
	}

	public function get_shipping_address()
	{
		$qry = "SELECT * FROM user_shipping WHERE userID = ".$this->session->userdata('userID');
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function add_purchase_share_to_user($number_of_shares)
	{
		/*
		$qry = "SELECT * FROM current_share_price WHERE user_id = ".$this->session->userdata('userID');
		$query = $this->db->query($qry);
		$rslt = $query->result();
		$rate = 0;
		if($query->num_rows()>0){
			$rate = $rslt[0]->per_share_price;
		}
		*/
		
		
		$qry2 = "SELECT * FROM user_share_balance WHERE user_id = ".$this->session->userdata('userID');
		$query2 = $this->db->query($qry2);
		$rslt2 = $query2->result();
		$current_share_number = 0;
		if($query2->num_rows()>0){
			$current_share_number = $rslt2[0]->current_share_number;
		}
		$current_share_number=$current_share_number+$number_of_shares;
		//$balance = $current_share_number*$rate;
		if($query2->num_rows()>0){
			$data = array(
				'current_share_number'=>$current_share_number,				
				'date'=>time()
				);
			$this->db->where('user_id',$this->session->userdata('userID'));
			$this->db->update('user_share_balance',$data);
		}else{
			$data = array(
				'current_share_number'=>$current_share_number,				
				'user_id'=>$this->session->userdata('userID'),
				'date'=>time()
				);
			$this->db->insert('user_share_balance',$data);
		}
	}

	public function get_minimum_amount_to_see_sell()
	{
		$qry = "SELECT property_value FROM configuration WHERE property_name = 'minimum_amount_to_see_sell'";
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->property_value;
	}

	function get_daily_income_of_user($userID)
	{
		
		$qryOne="SELECT MAX(click_date) as click_date FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$userID."";
		$query = $this->db->query($qryOne);
		$dates = $query->result();
		
		$time=date("Y-m-d h:i:s A", $dates[0]->click_date);
 
		$result="";
		for ($i=0; $i<30; $i++){
			
			$dt = $dates[0]->click_date -(3600*24*$i);
			$dt2 =$dt -(3600*24*1);

			$key = date('Y-m-d h:i:s A',$dt);

			
			$qry = "SELECT SUM(cash_amount) AS cash_amount FROM client_cash WHERE client_cash_publish = 1 AND click_date >= ".$dt2." AND click_date <= ".$dt." AND client_id = ".$userID." GROUP BY client_id" ;
			
			// echo '<pre>';
		   // print_r($qry);
	     // echo '</pre>';die();
			
			$query = $this->db->query($qry);
			$data = $query->result();
			if(sizeof($data)>0){
				$result[$key] = $data[0]->cash_amount;
			}else{
				$result[$key] = 0;
			}
			
		}

		
		return $result;
	}
	
	public function remove_share_sell_post($in_share_sell_id)
	{
		$data = array(
				'share_sell_publish' => 0
				
		);
		$this->db->where('share_sell_id', $in_share_sell_id);
		$this->db->update('share_sell', $data);
	}
	
	function get_saved_cart($userID)
	{
		
			$qry = "SELECT d.secret, b.product_id, b.quantity, b.unit_price, c.product_name FROM saved_cart a, cart_detail b, product c, cart_master d WHERE a.cart_master_id = b.cart_master_id AND a.cart_master_id = d.id AND b.product_id = c.product_id AND a.publish_flag = 1 AND a.user_id = ".$userID." ";
			$query = $this->db->query($qry);
			$data = $query->result();
			
		return $data;
	}
	
	function save_cart($userID, $master_cart_id)
	{
		$qry = "SELECT * FROM cart_master WHERE publish_flag = 1 AND secret = '".$master_cart_id."'";
		$query = $this->db->query($qry);
		$master_data = $query->result();
		$numrows = $query->num_rows();
		if ($numrows==0){
			$this->db->insert('cart_master', array('secret' => $master_cart_id,
					'created_on' => date('Y-m-d H:i:s')));
			//data inserted, find the id of hte row
			$qry = "SELECT * FROM cart_master WHERE secret = '".$master_cart_id."'";
			$query = $this->db->query($qry);
			$master_data = $query->result();
		}
		$cart_id = $master_data[0]->id;//got master id
		
		$qry = "SELECT * FROM saved_cart WHERE publish_flag = 1 AND user_id = ".$userID."  ";
		$query = $this->db->query($qry);
		//$temp_data = $query->result();
		$numrows = $query->num_rows();
		if ($numrows==0){
			//not already inserted, we can insert now
			$data = array(
					'user_id' => $userID,
					'cart_master_id'=>$cart_id,
					'date'=>time()
			
			);
			$this->db->insert('saved_cart',$data);
		}
	}
	
	function delete_cart_details_with_master($master_cart_id)
	{
		$data = array(
				'publish_flag' => 0
		
		);
		$this->db->where('cart_master_id', $master_cart_id);
		$this->db->update('cart_detail', $data);
	}
	
	function delete__master_cart_id($master_cart_id)
	{
		$data = array(
				'publish_flag' => 0
		
		);
		$this->db->where('id', $master_cart_id);
		$this->db->update('cart_master', $data);
	}
	
	function delete_saved_cart($userID)
	{
		//get master card id, delete contents of this id in cart detail
		$qry = "SELECT * FROM saved_cart WHERE publish_flag = 1 AND user_id = '".$userID."'";
		$query = $this->db->query($qry);
		$master_data = $query->result();
		$numrows = $query->num_rows();
		if ($numrows > 0){
			$this->delete_cart_details_with_master($master_data[0]->cart_master_id);
			$this->delete__master_cart_id($master_data[0]->cart_master_id);
		}
		$data = array(
				'publish_flag' => 0
		
		);
		$this->db->where('user_id', $userID);
		$this->db->update('saved_cart', $data);
	}
	
	function delete_current_cart($secret)
	{
		
		$qry = "SELECT id FROM cart_master WHERE publish_flag = 1 AND secret = '".$secret."'";
		$query = $this->db->query($qry);
		$master_data = $query->result();
		$numrows = $query->num_rows();
		if ($numrows==0){
		      $master_cart_id = $master_data[0]->id;
		      
		      $data = array(
		      		'publish_flag' => 0
		      
		      );
		      $this->db->where('cart_master_id', $master_cart_id);
		      $this->db->update('cart_detail', $data);
		}
		
	}
	
	function has_saved_cart($userID)
	{
		$qry = "SELECT * FROM saved_cart WHERE publish_flag = 1 AND user_id = '".$userID."'";
		$query = $this->db->query($qry);
		$master_data = $query->result();
		$numrows = $query->num_rows();
		if ($numrows > 0){
			return true;
		}
		
		return false;
	}
	
	function save_purchase_info($ShippingInfo, $PaymentInfo)
	{
		//add data in product order table, user_id is 0 for guest
		
		$userID = 0;
		
		if($this->session->userData('guest'))
		{
			$userID = 0;
		}
		else {
			$userID = $this->session->userData('userID');
		}
		
		$data = array(
					'member_id' => $userID,
					'shipping_person_name'=>$ShippingInfo['name'],					
					'location'=>$ShippingInfo['address'],
					'delivery_date'=>$ShippingInfo['delivery_date'],
					'phone'=>$ShippingInfo['phone'],
					'city'=>$ShippingInfo['city'],
					'state'=>$ShippingInfo['state'],
					'zip'=>$ShippingInfo['zip_code'],
					'country'=>$ShippingInfo['country'],
					'email'=>$ShippingInfo['email'],
					'day_phone_a'=>$ShippingInfo['day_phone_a'],
					'services_name'=>$PaymentInfo['services_name'],
					'payment_getway'=>$PaymentInfo['payment_getway'],
					'credit_card_type'=>$PaymentInfo['card_type'],
					'cardholder_name'=>$PaymentInfo['cardholder_name'],
					'creadit_cardno'=>$PaymentInfo['card_number'],
					'expiration_date'=>$PaymentInfo['expire_date'],
					'card_security_code'=>$PaymentInfo['security_code'],
					'subtotal_amount'=>$PaymentInfo['subtotal_amount'],
					'tax_amount'=>$PaymentInfo['tax_amount'],
					'coupon_value'=>$PaymentInfo['coupon_value'],
					'deposit_value'=>$PaymentInfo['deposit_value'],
					'tip_value'=>$PaymentInfo['tip_value'],
					'delivery_value'=>$PaymentInfo['delivery_value'],
					'payment_getway_amount'=>$PaymentInfo['payment_getway_amount'],
					'total_amount'=>$PaymentInfo['total_amount'],
					'order_status'=>$PaymentInfo['order_status'],
					'shipping_status'=>$PaymentInfo['shipping_status'],
					'created_date'=>time(),
					'modified_date'=>time()
				
			
			);
			$this->db->insert('product_order',$data);
	}
	
	function get_site_server_URL($code)
	{
		$site_server_url = "";
		
		$qry = "SELECT site_server_url FROM site_servers WHERE publish_flag = 1 AND site_server_code = ".$code." ";
		$query = $this->db->query($qry);
		$master_data = $query->result();
		$numrows = $query->num_rows();
		if ($numrows > 0){
			$site_server_url = $master_data[0]->site_server_url;
		}
		
		return $site_server_url;
	}
	
	function get_all_site_server_URL()
	{
		$site_server_url = "";
	
		$qry = "SELECT site_server_code, site_server_url FROM site_servers WHERE publish_flag = 1 ";
		$query = $this->db->query($qry);
		$master_data = $query->result();
		//$numrows = $query->num_rows();
		//if ($numrows > 0){
		//	$site_server_url = $master_data[0]->site_server_url;
		//}
	
		return $master_data;
	}
	
	public function insert_in_client_cash($data)
	{
		$this->db->insert('client_cash', $data);
		return $this->db->insert_id();
	}
	
	public function insert_in_client_share($data)
	{
		$this->db->insert('client_shares', $data);
		return $this->db->insert_id();
	}

    public function updateData($table,$data,$itemDetail){
        $this->db->update($table, $data,$itemDetail);
        return $this->db->affected_rows();
    }
	public function cancel_added_share_sell($id)
	{
		$qry = "DELETE FROM share_sell WHERE share_sell_id = ".$id;
		$query = $this->db->query($qry);
		//return $query->result();
	}
	public function get_share_sell_amount($id)
	{
		$qry = "SELECT number_of_shares FROM share_sell WHERE share_sell_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->number_of_shares;
	}

}
?>