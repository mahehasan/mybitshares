<?php
class Category_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
    public function getAllCategoryData($qry_options=""){	
    	$results = array();
    	$qry_options = empty($qry_options) ? "WHERE category_publish = 1 " : $qry_options;
    	$qry = "SELECT * FROM category". $qry_options;
    	$res = $this->db->query($qry);
    	$result = $res->result_array();
    	if($result){
    		foreach($result as $arr){
    			$results[$arr['category_id']] = $arr;
    		}
    		return $results;
    	}else
    		return false;
    }
    
    function getcount_all_resultsrows($tablename, $conditionarray, $orderby){
    	if($conditionarray !=''){
    		$this->db->where($conditionarray);
    	}
    	if($orderby !=''){
    		$this->db->order_by($orderby,'asc');
    	}
    	$this->db->from($tablename);
    	$query = $this->db->count_all_results();
    	if($query)
    		return $query;
    	else
    		return false;
    }


	public function getallrow($tablename, $conditionarray){
		if($conditionarray !=''){
			$this->db->where($conditionarray);
		}
		$query = $this->db->get($tablename); 		
		if($query)
			return $query->result();
		else
			return false;
	} 
	public function getallrowbysqlquery($sqlquery){
		$query = $this->db->query($sqlquery);
		if($query)
			return $query->result();
		else
			return false;
	}

	public function get_onefieldnamebyid($table_name, $primary_idname, $primary_idval, $return_field_value){
		$returnfieldvalue = '';
		$query = $this->db->query("select $return_field_value as fieldvalue from $table_name where $primary_idname = $primary_idval order by $primary_idname asc limit 0,1");
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$returnfieldvalue = $row->fieldvalue;
			}
		}
		return $returnfieldvalue;
	}
	
	public function desendingoneintvalbycategoryid($tablename, $categoryidname, $categoryidvalue, $fieldname){
		$showing_order = 1;
	
		$query = $this->db->query("select $fieldname as showing_order from $tablename where $categoryidname = $categoryidvalue order by $fieldname desc limit 0,1");
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$showing_order = $row->showing_order+1;
			}
		}
		return $showing_order;
	}
	
	public function saveCategory($data){
		$this->db->insert('category', $data);
		return $this->db->insert_id();
	}
	
	public function updateCategory($data, $id){
		$this->db->where('category_id', $id);
		return $this->db->update('category', $data);
	}

}
?>