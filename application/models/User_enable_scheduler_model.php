<?php
class User_enable_scheduler_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    //--------------------------------------------------------------------------------------Price-----------------------------------------------------------------------
	public function get_enable_time()
	{
		$qry = "SELECT property_value FROM configuration WHERE property_name = 'enable_time'";
		$query = $this->db->query($qry);
		$tmp = $query->result();
		return $tmp[0]->property_value;
	}

	public function get_disabled_ads()
	{
		$qry = "SELECT * FROM manage_client_advertise WHERE manage_client_advertise_publish = 1 ";
		$query = $this->db->query($qry);
		return $query->result();
	}

	public function enable_ads($id)
	{
		if(strlen($id)>0){
			$qry = "UPDATE manage_client_advertise SET manage_client_advertise_publish = 0  WHERE manage_client_advertise_id IN (".$id.")";
			$query = $this->db->query($qry);
		}
	}
	
}
?>