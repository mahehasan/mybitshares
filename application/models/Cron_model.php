<?php 
class Cron_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('referral_model'));
	}

	public function get_last_click_cash($user_id)
	{
		$qry = "SELECT * FROM user_price_history WHERE user_id = ".$user_id."
				ORDER BY date DESC LIMIT 1";
		$query = $this->db->query($qry);
		$result = $query->result();
		$date = 0;
		$p_cash = 0;
		if (sizeof($result)>0){
			$date = $result[0]->date;
			$p_cash = $result[0]->price;
		}
		$qry = "SELECT SUM(cash_amount) as cash FROM client_cash WHERE client_cash_publish = 1 AND client_id = ".$user_id." AND click_date > ".$date;
		$query = $this->db->query($qry);
		$result = $query->result();
		$cash = 0;
		if (sizeof($result)>0){
			$cash = $result[0]->cash;
		}
		return $cash + $p_cash;
	}
	
	public function update_all_user_price_history()
	{
		$qry = "SELECT userID FROM app_users where active = 1 ";
		$query = $this->db->query($qry);
		$data = $query->result();
		$users_income="";
		foreach ($data as $row) {
			 $user_total_amount2 = $this->update_user_price_history($row->userID);
			 
			 $users_income = $users_income + $user_total_amount2;
		}
		
		//insert $users_income to system daily income
		
		$data1 = array(
				'amount' => $users_income,				
				'date' => time()
		);
		$this->db->insert('system_daily_income', $data1);
	}

	public function update_user_price_history($user_id)
	{
		$last_clicked_cash = $this->get_last_click_cash($user_id);
		$referral_price = $this->update_user_referral_price($user_id);
		$data = array(
			'user_id' => $user_id, 
			'price' => $last_clicked_cash + $referral_price, 
			'share' => 0, 
			'referral_price' => $referral_price, 
			'date' => time()
			);
		$this->db->insert('user_price_history', $data);
		$data1 = array(
			'user_id' => $user_id, 
			'price' => $referral_price, 
			'date' => time()
			);
		$this->db->insert('referral_price', $data1);
		
		// update total_current_cash amount
		
		$user_total_amount2 =  $last_clicked_cash + $referral_price;
		$this->mybitshares_model->update_total_income2($user_total_amount2, $user_id);
		return $user_total_amount2;
	}

	public function update_user_referral_price($user_id)
	{
		$qry = "SELECT * FROM referral_price WHERE user_id = ".$user_id."
			 ORDER BY date DESC LIMIT 1";
		$query = $this->db->query($qry);
		$result = $query->result();
		$date = 0;
		$r_cash = 0;
		if (sizeof($result)>0){
			$date = $result[0]->date;
			$r_cash = $result[0]->price;
		}
		$referral_price = $this->referral_model->calculate_referral_by_date($user_id, $date);
		return $referral_price + $r_cash;
	}

	public function update_user_cashout($user_id, $amount)
	{
		$last_clicked_cash = $this->get_last_click_cash($user_id);
		$referral_price = $this->update_user_referral_price($user_id);
		$temp_amount = (($last_clicked_cash + $referral_price) - $amount);
		if ($temp_amount <= $referral_price){
			$referral_price = 0;
		}
		$data = array(
			'user_id' => $user_id, 
			'price' => $temp_amount, 
			'share' => 0, 
			'referral_price' => $referral_price, 
			'date' => time()
			);
		$this->db->insert('user_price_history', $data);
		$data1 = array(
			'user_id' => $user_id, 
			'price' => $referral_price , 
			'date' => time()
			);
		$this->db->insert('referral_price', $data1);
		return $referral_price;
	}

	function last_24_hours_income_of_user($userID)
	{
		$qry = "SELECT SUM(cash_amount) AS cash_amount FROM client_cash WHERE client_cash_publish = 1 AND click_date > ".(time()-(3600*24))." AND client_id = ".$userID." GROUP BY client_id" ;
		$query = $this->db->query($qry);
		$data = $query->result();
		if(sizeof($data)>0){
			return $data[0]->cash_amount;
		}else{
			return 0;
		}
	}

	public function get_all_users_last_24_hours_income()
	{
		$qry = "SELECT userID FROM app_users";
		$query = $this->db->query($qry);
		$data = $query->result();
		$users_income="";
		foreach ($data as $row) {
			$users_income[$row->userID] = $this->last_24_hours_income_of_user($row->userID);
		}
		return $users_income;
	}

	public function get_daily_income_system_share()
	{
		$qry = "SELECT property_value FROM configuration WHERE configuration_publish = 1 AND property_name = 'daily_income_system_share'";
		$query = $this->db->query($qry);
		$numrows = $query->num_rows();
		$data = $query->result();
		
		if($numrows > 0)
		{
			return $data[0]->property_value;
		}
		else {
			return 0;
		}
		
	}

	public function get_total_shares_of_system()
	{
		$qry = "SELECT SUM(current_share_number) AS n FROM user_share_balance";
		$query = $this->db->query($qry);
		$data = $query->result();
		return $data[0]->n;
	}

	public function get_all_shares_of_user()
	{
		$qry = "SELECT * FROM user_share_balance";
		$query = $this->db->query($qry);
		return $query->result();
	}
	public function update_system_income_bonus_of_user($user_id,$system_income_bonus)
	{
		$data = $this->mybitshares_model->get_total_income($user_id);
		if(isset($data[0]))
		{
			$current_total = $data[0]->current_amount;
				
			$current_total = $current_total + $system_income_bonus;
			
			$system_bonus = $data[0]->system_bonus + $system_income_bonus;
				
			//$this->update_total_income($current_total,$user_id);
			
			$data = array(
					
					'current_amount' => $system_income_bonus,
					'system_bonus' => $system_bonus,
					'date' => time()
			
			);
			
			$this->db->where('user_id', $user_id);
			$this->db->update('total_current_cash', $data);
				
		}
		else {
			//insert data in total_current_cash
		
			$data = array(
					'user_id' => $user_id,
					'current_amount' => $system_income_bonus,
					'system_bonus' => $system_income_bonus,
					'date' => time()
		
			);
		
			$this->mybitshares_model->insert_total_income($data);
		}
		
		// insert data in system_user_grant for order this grant
		
		$data = array(
				'user_id' => $user_id,
				'amount' => $system_income_bonus,				
				'date' => time()
		
		);
		
		$this->db->insert('system_user_grant', $data);
		
	}

	public function update_system_daily_income($total_income_24hr)
	{
		$this->db->insert('system_daily_income', array('amount' => $total_income_24hr, 'date'=>time() ));
	}
	
	public function update_system_daily_income2($id, $amount)
	{
		$data['amount']= $amount;
		$this->db->where('income_id',$id);
		$this->db->update('system_daily_income',$data);
	}


}