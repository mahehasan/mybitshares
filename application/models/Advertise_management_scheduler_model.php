<?php
class Advertise_management_scheduler_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
	public function get_all_sahre_to_click_advertise()
	{
		//$result = array();
		$qry = "SELECT * FROM advertise WHERE advertise_publish = 1 AND share_to_click_publish = 1 ";
		$query = $this->db->query($qry);
		return $query->result();
		
	}
		
	public function get_all_paid_to_click_advertise()
	{
		//$result = array();
		$qry = "SELECT * FROM advertise WHERE advertise_publish = 1 AND paid_to_click_publish = 1 ";
		$query = $this->db->query($qry);
		return $query->result();
		
	}
	public function get_desable_advertise()
	{
		//$result = array();
		$qry = "SELECT * FROM advertise WHERE share_to_click_publish = 0 AND paid_to_click_publish = 0 ";
		$query = $this->db->query($qry);
		return $query->result();
		
	}
	
	public function get_server_total_click_share($id)
	{
		$qry = "SELECT menu1_click FROM advertise WHERE menu1_id =4 And advertise_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
				
		if( $query->num_rows()>0)
		{			
			return $rslt[0]->menu1_click;
		}else
		{
			$qry = "SELECT menu2_click FROM advertise WHERE menu2_id =4 And advertise_id = ".$id;
			$query = $this->db->query($qry);
			$rslt = $query->result();
			
			return $rslt[0]->menu2_click;
			
		}
				
	}	
	public function get_server_total_click_paid($id)
	{
		$qry = "SELECT menu1_click FROM advertise WHERE menu1_id =5 And advertise_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
				
		if( $query->num_rows()>0)
		{			
			return $rslt[0]->menu1_click;
		}else
		{
			$qry = "SELECT menu2_click FROM advertise WHERE menu2_id =5 And advertise_id = ".$id;
			$query = $this->db->query($qry);
			$rslt = $query->result();
			
			return $rslt[0]->menu2_click;
			
		}
				
	}	
	
	public function get_client_total_click_share($id)
	{
		$qry = "SELECT SUM(current_click_share) AS TotalClick FROM click_calculation WHERE advertise_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->TotalClick;
	}
	
	//Paid to share
	public function get_client_total_click_paid($id)
	{
		$qry = "SELECT SUM(current_click_paid) AS TotalClick FROM click_calculation WHERE advertise_id = ".$id;
		$query = $this->db->query($qry);
		$rslt = $query->result();
		return $rslt[0]->TotalClick;
	}
	
    public function desable_advertise($id,$data)
	{
		$this->db->where('advertise_id',$id);
		return $this->db->update('advertise',$data);
	}

}
?>