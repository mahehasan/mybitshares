<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * User Class
 *
 * This class includes functions related to users
 *
 * @author	Mahedi Hasan
 */
class User_model  extends CI_Model {
	var $CI;
	var $model_table;
	
	
	public function __construct(){
		parent::__construct();
      	$this->model_table = APP_USERS;
		$this->CI =& get_instance();
	
 	}
	
	/**
	 * login authentication
	 *
	 * This function will be used to authenticate login credentials.
	 *
	 * @param $userName
	 * @param $pass
	 *
	 * @author sohail abbas
	 */
	public function loginAuthentication($userName, $pass){
		$email = addslashes($userName);
		$password = md5($pass);
		$strSQL = "SELECT * from ".$this->model_table." inner join ".APP_USER_ROLES." on (".$this->model_table.".userRoleID = ".APP_USER_ROLES.".userRoleID) where (email = '$email' and password = '$password' )";
		$res = $this->db->query($strSQL);
		$result = parse_db_result($res);
		$res->free_result();
		if($result->result)
		return $result->result[0];
		else
		return false;		
	}
	
	/**
	 * get user details
	 *
	 * This function will be used to get information of the user.
	 *
	 * @param $userID
	 *
	 * @tables $this->model_table
	 *
	 * @author Naveed Mughal
	 */
    public function getUserDetails($userID){
        				
        $this->db->where(array("userID" => $userID));
		$query = $this->db->get($this->model_table);
		$row = $query->first_row();
        return $row;
    }
	
		
	/**
	 * get all user names
	 *
	 * This function will be used to get user names as per provided parameter
	 *
	 * @param userID
	 *
	 * @tables $this->model_table
	 *
	 * @author Naveed Mughal
	 */		
	public function getAllUserNames($userID=''){
		
				
		$result = array();
		$cond = array();
		if(!empty($userID))	{
			$cond["userID"] = $userID;
			$this->db->where($cond);
		}
		$res = $this->db -> get($this->model_table);
		$result_ar = parse_db_result($res);
		$res->free_result();
		if($result_ar->result){
			foreach($result_ar->result as $arr){
				$result[$arr['userID']] =$arr['firstName'].' '.$arr['LastName'];
			}
		}else
		return false;		
		
		return !empty($userID) ? $result[$userID] : $result;
	}
	
	/**
	 * get user by email
	 *
	 * This function will be used to user info based on email.
	 *
	 * @param $email
	 *
	 * @author Naveed Mughal
	 */
	public function getUserByEmail($email){
		$this->db ->where( "email = '".addslashes($email)."'");	
		$query = $this->db -> get($this->model_table);
		$res =  (array)$query->first_row();		
		return $res;
	}
	
	/**
	 * get user by code
	 *
	 * This function will be used to get user information from person table using user user id and email code
	 *
	 * @param $uid
	 * @param $cid
	 *
	 * @tables $this->model_table
	 *
	 * @author Naveed Mughal
	 */
	public function getUserByCode($uid, $cid){
		$this->db ->where("userID = '".$uid."'");
		$query = $this->db -> get($this->model_table);	
		$res =  (array)$query->first_row();	
		if(is_array($res)){
			if($cid == $res['password']) {
				return $res;
			} else {
				return array();
			}
		}
		else{
			return array();
		}
	} 
	
	/**
	 * get user by id
	 *
	 * This function will be used to get all user information against provided user id
	 *
	 * @param id
	 *
	 * @tables $this->model_table
	 *
	 * @author Naveed Mughal
	 */
	public function getUserById($id){
				
		if(empty($id)) {
			return array();
		}
		$this->db->where("userID = '".$id."'");
		$query = $this->db->get($this->model_table);
		$result =  (array)$query->first_row();
		return $result;
	}
	
	/**
	 * This function will be used to update password
	 *
	 * @param $uid
	 * @param $password
	 *
	 * @tables $this->model_table
	 *
	 * @author Naveed Mughal
	 */  
	public function updatePassword($uid, $password){
		
		
		$data["password"] = md5($password);
		$data["password_date"] = date("Y-m-d");	
			
		return $this->db -> update($this->model_table, $data, "userID = ".$uid);		
	}
	
	/**
	 * This function will be used to reset password
	 *
	 * @param $current_password
	 * @param $new_password
	 * @param $conform_new_password
	 *
	 * @tables $this->model_table
	 *
	 * @author Naveed Mughal
	 */  
	public function resetPassword($current_password, $new_password, $confirm_new_password) {
				
		$uid = $this->session->userData('userID');
		if(md5($current_password) !=  $this->session->userData('password')) {
			return 'curr_inv';
		} elseif($new_password != $confirm_new_password) {
			return 'different';
		} else {			
			
			$data["password"] = md5($new_password);
			$data["password_date"] = date("Y-m-d");		
			$this->db -> update($this->model_table, $data, "userID = ".$uid);			
			$this->session->set_userdata('password',$password)	;

			return 'reset';
		}		
	}
	
	
	/**
	 * get user levels
	 *
 	 * This function will be used to get user types
	 *
	 * @param qry_options
	 *
	 * @tables APP_USER_ROLES
	 *
	 * @author ikram
	 */	
	public function getUserLevels(){
		$result = array();
		// $this->db->where("userLevel >= ".$this->userLevel);
		$res = $this->db->get(APP_USER_ROLES);
		$result_ar = parse_db_result($res);
		$res->free_result();
		if($result_ar->result){
			foreach($result_ar->result as $arr){
				$result[$arr['userRoleID']] = $arr;
			}
			return $result;
		}else
		return false;		
		
		
		
	}
	/**
	 * get all users
	 *
	 * This function will be used to get all users as per qry options
	 *
	 * @params qry_options
	 *
	 * @tables $this->model_table
	 *
	 * @author azizabbas
	 */		
	public  function getAllUsers($qry_options=''){
		$result = array();
		$qry_options = empty($qry_options) ? "WHERE active = '1'" : $qry_options;
		$qry = "SELECT * FROM ".$this->model_table." $qry_options";
		$res = $this->db->query($qry);
		$result_ar = parse_db_result($res);
		$res->free_result();
		if($result_ar->result){
			foreach($result_ar->result as $arr){
				$result[$arr['userID']] = $arr;
			}
			return $result;
		}else
		return false;		
	}
	/**
	 * validate email address
	 *
	 * This function will be used to validate email address
	 *
	 * @param $email
	 * @param $qry_options=""
	 *
	 * @tables $this->model_table
	 *
	 * @author ikram
	 */
	public  function validateEmailAddress($email, $qry_options=""){
		$result = array();
		$qry = "email = '".$email."' $qry_options";
		$this->db ->where($qry );
		$query = $this->db ->get($this->model_table);
		$result =  (array)$query->first_row();
		if(!empty($result['email']))
			return TRUE;
		return FALSE;	
	}
	
	/**
	 * validate user ip address
	 *
	 * This function will be used to check whether ip address exists
	 *
	 * @param $ip_address
	 * @param $qry_options=""
	 *
	 * @tables $this->model_table
	 *
	 */
	public  function validateIPAddress($ip_address, $qry_options=""){
		$result = array();
		$qry = "ip_address = '".$ip_address."' $qry_options";
		$this->db ->where($qry );
		//$query = $this->db ->get('client_cash');
		$query = $this->db ->get('user_ip');
		$result =  (array)$query->first_row();
		if(!empty($result['ip_address']))
			return TRUE;
		return FALSE;	
	}
	
		public  function checkIPAddress($ip_address, $userID){
		$result = array();
		$qry = "ip_address = '".$ip_address."' and userID !='".$userID."'";
		$this->db ->where($qry );
		//$query = $this->db ->get('client_cash');
		$query = $this->db ->get('user_ip');
		$result =  (array)$query->first_row();
		if(!empty($result['ip_address']))
			return TRUE;
		return FALSE;	
	}
	
	
	public function addUserIP($data){
		$this->db->insert('user_ip', $data);
		return $this->db->affected_rows() > 0;
	}
	
	/**
	 * deleteUserById
	 *
	 * This function will be used to delete the user
	 *
	 * @param id
	 *
	 * @tables $this->model_table
	 *
	 * @author ikram
	 */
	public function deleteUserById($id){
		if(empty($id) || trim($id) == "") {
			return false;
		}
		$this->db ->delete($this->model_table, " userID = '".trim($id)."'");
		return true;
	}
	
	public function updateUser($uid, $data) {		
		return $this->db -> update($this->model_table, $data, "userID = ".$uid);	
	}
	/**
	 * Following function will be used to generate LOGIN statistics
	 *
	 * @tables `dm_login_stats`
	 *
	 * @author MH
	 **/
	public function generateLoginStats($login_status=""){
		if($this->session->userdata('userID')){
			if($login_status == 'login'){
				$entry = array(
						"request_time" => $_SERVER['REQUEST_TIME'],
						"userID" => $this->session->userdata('userID')
				);
				//---- INSERT INTO site_stats
				$this->db->insert(DM_LOGIN_STATS, $entry);
				//$this->db->affected_rows();
				
			}elseif ($login_status == 'logout'){
				$where = " userID = ".$this->session->userdata('userID');
				$this->db->select_max('login_stats_id');
				$this->db->where($where, NULL, FALSE);
				$query = $this->db->get(DM_LOGIN_STATS);
				$result =  (array)$query->first_row();
				$entry = array(
						"departure_time" => $_SERVER['REQUEST_TIME']
				);
				$this->db->update(DM_LOGIN_STATS, $entry, "login_stats_id = ".$result['login_stats_id']);
			}
		}
	}
	/**
	 * This function will be used to get data for the user login report
	 *
	 * @author MH
	 **/
	
	function userLoginReportData($where='', $limit='', $offset=0, $order_by_column = '', $order=''){

        $this->db->select("ls.login_stats_id, ls.userID, ls.request_time,
				u.userRoleID, u.email, u.firstName, u.LastName, ur.userTypeName AS user_type,
				DATE_FORMAT(FROM_UNIXTIME(ls.request_time), '%Y-%m-%d') AS date,
				DATE_FORMAT(FROM_UNIXTIME(ls.request_time), '%a') AS days",false);

        $this->db->from(DM_LOGIN_STATS.' ls');

        $this->db->join(APP_USERS.' u' , 'u.userID = ls.userID', 'left');
        $this->db->join(APP_USER_ROLES.' ur' , 'ur.userRoleID = u.userRoleID', 'left');


        if ( $where != '' )
        {
            if ( is_array($where) )
                $this->db->where($where);
            else
                $this->db->where($where, NULL, FALSE);
        }

        $this->db->group_by(array("userID", "date"));

        if ( $order_by_column != '' ){
            $this->db->order_by($order_by_column, $order);
        }
        if ( $limit != '' ){
            $this->db->limit($limit, $offset);
        }

        $result = $this-> db->get();
        $result = $result->result_array();

        return $result;
	}
	
	/**
	 * This function will be used to get data for the userTypeName from app_user_roles 
	 *
	 * @author MH
	 **/
	
	function getUserTypeName(){
		$result = array();
		$qry = "SELECT * FROM app_user_roles";
		$res = $this->db->query($qry);
		$result_ar = parse_db_result($res);
		$res->free_result();
		if($result_ar->result){
			foreach($result_ar->result as $arr){
				$result[$arr['userRoleID']] = $arr['userTypeName'];
			}
			return $result;
		}else
		return false;
	}
	
	public function getPermissionsByRoleID($userID, $userRoleID) {
		
		if($userRoleID == 1) {
			$strSQL = "SELECT perm.* 			
				FROM ".APP_PERMISSIONS." AS perm WHERE status = 1";			
		}		
		else {			
			$strSQL = "SELECT perm.*
					FROM ".APP_PERMISSIONS." AS perm
					
					WHERE permID IN (
					
						SELECT permID FROM ".APP_ROLE_PERMISSIONS." AS apr
					 	WHERE roleID = '".$userRoleID."'
						
						UNION
						
						SELECT permID FROM ".APP_USERS_PERMISSIONS."
						WHERE userID = '".$userID."'
						
						
				    )   AND status = 1";
			}
			
		$res = $this->general_model->query($strSQL);
		return $res;
	}
	
	public function add_referral_relation($id)
	{
		$data = array('parent_id' => $this->session->userdata('referral_id'), 
        		'child_id' =>$id);
        $this->db->insert('referral_relations', $data);
	}
}
?>