<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php
$this->usersecurity->destroyUserSession();
?>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?=TITLE?> | Reset Password</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<?=METRONIC_ASSETS_URL?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?=METRONIC_ASSETS_URL?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?=METRONIC_ASSETS_URL?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?=METRONIC_ASSETS_URL?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="<?=METRONIC_ASSETS_URL?>global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?=METRONIC_ASSETS_URL?>global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
    <link href="<?=METRONIC_ASSETS_URL?>admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?=METRONIC_ASSETS_URL?>global/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="<?=METRONIC_ASSETS_URL?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?=METRONIC_ASSETS_URL?>admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="<?=METRONIC_ASSETS_URL?>admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
    <link href="<?=METRONIC_ASSETS_URL?>admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>

<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="<?=SITE_URL?>">
        <img src="<?=SITE_URL?>images/logo_landing.png" alt=""/>
    </a>
</div>
<!-- END LOGO -->

<!-- BEGIN LOGIN -->
<div class="content">

<?php
if(@$_GET["conf_msg"]=="reset_mail_sent"){
	echo $this->html->showSuccessMsg("Your password has been successfully reset. Please check your registered email for more information.");
}
elseif(@$_GET["conf_msg"]=="reset_mail_not_sent" || @$_GET["conf_msg"]=="reset_unknown_issue"){
	echo $this->html->showErrorMsg("Sorry! we are unable to process your request this time. Please try again later or contact IT Support.");
}
?>

  <!-- Password reset form -->
  <form action="<?=SITE_URL?>login/process_reset_password" method="post" id="password_reset_frm" onSubmit="return verifyForm(this)">
    
    <h3 class="form-title">Reset your password</h3>
    
    <div class="alert alert-danger display-hide">
      <button class="close" data-close="alert"></button>
      <span>You have some errors.</span>
    </div>
    
    <div class="form-group">
      <label class="control-label">New Password</label>
      <input type="password" name="password" id="password" class="form-control" placeholder="Enter nwe password">
    </div>
    
    <div class="form-group">
      <label class="control-label">Confirm New Password</label>
      <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm new password">
    </div>
    
    <div class="form-actions">
      <div class="row">
        <div class="col-md-offset-6 col-md-6">
          <button type="submit" class="btn green" id="reset_password_btn" name="reset_password_btn">Reset Password</button>
        </div>
      </div>
    </div>
    
    <input type="hidden" name="cid" id="cid" value="<?=$_GET["cid"]?>" />
    <input type="hidden" name="uid" id="uid" value="<?=$_GET["uid"]?>" />
  </form>
  <!-- Password reset form ends -->

</div>
<!-- END LOGIN -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="<?=METRONIC_PLUGINS_URL?>jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="<?=METRONIC_PLUGINS_URL?>jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?=METRONIC_PLUGINS_URL?>jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?=METRONIC_PLUGINS_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=METRONIC_PLUGINS_URL?>jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?=METRONIC_PLUGINS_URL?>jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?=METRONIC_PLUGINS_URL?>jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?=METRONIC_PLUGINS_URL?>uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?=METRONIC_PLUGINS_URL?>bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?=METRONIC_PLUGINS_URL?>jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=METRONIC_PLUGINS_URL?>jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?=METRONIC_PLUGINS_URL?>bootbox/bootbox.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script language="javascript">
var SITE_URL = '<?=SITE_URL?>';
</script>

<script src="<?=METRONIC_ASSETS_URL?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?=METRONIC_ASSETS_URL?>admin/layout/scripts/layout.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script language="javascript">

jQuery(document).ready(function($) {
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
});

function verifyForm(frm){
	var password		 = trim_spaces(frm.password.value);
	var confirm_password = trim_spaces(frm.confirm_password.value);
	
	if(password == ""){
		bootbox.alert("Pleease provide password");
		return false;
	}
	
	if(confirm_password == ""){
		bootbox.alert("Pleease provide confirm password");
		return false;
	}
	
	if(confirm_password != password){
		bootbox.alert("Confirm password doesn't match with password above");
		return false;
	}
	
	return true;
}

function trim_spaces(s){
	s = s.replace(/(^\s*)|(\s*$)/gi,"");
	s = s.replace(/[ ]{2,}/gi," ");
	s = s.replace(/\n /,"\n");
	return s
}

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>