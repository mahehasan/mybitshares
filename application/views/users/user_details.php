<?php
if ( ! defined('SITE_URL')) exit('No direct script access allowed');
require_once("templates/default/header.php");

?>

<!-- <h3 class="page-title">Edit System User </h3> -->

    <!-- Results table -->
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue-madison">
      <div class="portlet-title">
        <div class="caption">
<!--           <i class="fa fa-support"></i>Edit System User Form -->
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse"></a>
        </div>
      </div>
      <div class="portlet-body form">
      <!-- BEGIN FORM-->
      <form action="<?=SITE_URL?>users/join_user" method="post" name="frmEditRegister" id="frmEditRegister" class="form-horizontal" onsubmit="return checkForm(this);">
    	<div class="form-body">
<!-- 	      <h5 class="form-section"> -->
<!--             Please complete the following fields and submit form by clicking the "Submit" button below. <b>Note: Required fields are marked with an asterisk.</b> -->
<!--           </h5> -->
<?php
if(isset($err)){
	if($err == 1){
		echo $this->html->showErrorMsg($MSG);
	}
	elseif($err == 0){
		echo $this->html->showSuccessMsg($MSG);
	}
}
?>
    	<?php //$this->html->showErrorMsg("You have some form errors. Please check below.", "", "display-hide")?>
	    <div class="form-group">
          <label class="col-md-3 control-label">Account Type</label>
          <div class="col-md-5">
            free
          </div>
          <div class="col-md-4"> </div>
        </div>
        
        <div class="form-group">
          <label class="col-md-3 control-label">Send emails to</label>
          <div class="col-md-5">
            <select name="send_emails_to" id="send_emails_to" class="form-control select2me">
              <option value="Site Inbox">Site Inbox</option>
              <option value="eMail Address">eMail Address</option>
              <option value="Site Inbox & eMail">Site Inbox & eMail</option>
            </select>
          </div>
          <div class="col-md-4"> </div>
        </div>
		<div class="form-group">
        	<label class="col-md-3 control-label">Password <span aria-required="true" class="required">*</span></label>
            <div class="col-md-5">
            	<input name="password" type="password" maxlength="255" class="form-control" id="password" />
            </div>
            <div class="col-md-4"> </div>
		</div>

        <div class="form-group">
        	<label class="col-md-3 control-label">Confirm Password <span aria-required="true" class="required">*</span></label>
            <div class="col-md-5">
            	<input type="password" maxlength="255" class="form-control" name="cpassword" id="cpassword" />
            </div>
            <div class="col-md-4"> </div>
      	</div>
        <div class="form-group">
          <label class="col-md-3 control-label">First Name</label>
           <div class="col-md-5">
             <input type="text" class="form-control" name="firstName" id="firstName" maxlength="255">
          </div>
          <div class="col-md-4"> </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label">Last Name</label>
          <div class="col-md-5">
            <input type="text" class="form-control" name="lastName" id="lastName" maxlength="255">
          </div>
          <div class="col-md-4"> </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label">Address</label>
          <div class="col-md-5">
            <input name="address1" maxlength="255" class="form-control" id="address" type="text" />
            </div>
            <div class="col-md-4">
            </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label">Town</label>
          <div class="col-md-5">
            <input name="town" maxlength="255" class="form-control" id="town" type="text" />
          </div>
          <div class="col-md-4"> </div>
        </div>
        
        <div class="form-group">
          <label class="col-md-3 control-label">Post Code</label>
          <div class="col-md-5">
            <input name="postCode" maxlength="255" class="form-control" id="postCode" type="text" />
            </div>
            <div class="col-md-4">
            </div>
        </div>

		<div class="form-group">
          <label class="col-md-3 control-label">Country</label>
          <div class="col-md-5">
          	<?php if (!empty($countryList)) {?>
          	<select name="country" class="form-control">
          			<?php foreach ($countryList as $key=>$value) {?>
    				<option value="<?=$key?>"><?=$value?></option>
    				<?php }?>
    		</select>
    		<?php }?>
          </div>
          <div class="col-md-4"> </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Cashout Profile</label>
          <div class="col-md-5">
            <b>No member yet of an payment processor. Join them by click on the button (open in new field).<br>
				<a target="_blank" href="http://www.sensipay.com/users/personalinfo/xpWUlpZ5">
						<img height="35" src="http://jillsclickcorner.com//images/small-pay1.jpg" width="62" border="0"></a>
				<a target="_blank" href="https://www.paypal.com/en/mrb/pal=UVYS2UR39QE4Q">
				<img height="31" src="http://www.donkeymails.com/pages/x-click-but01.gif" width="62" border="0"></a>
				</b>&nbsp;<a target="_blank" href="http://www.payza.com/?GgXoLizaKH5TFsm0NQ%2fbcw%3d%3d">
				<img height="28" src="https://secure.payza.com/PayNow/8695D27D6AA848BD8F68F9A0A9EFC562b0en.gif" width="88" border="0"></a>
				<a target="_blank" href="https://perfectmoney.com/?ref=501236">
				<img height="31" src="https://perfectmoney.com/img/88-31-10.png" width="88" border="0"></a><b>
				<a target="_blank" href="http://solidtrustpay.com/?r=81302">
				<img height="35" src="https://solidtrustpay.com/ImgDir/buttons/buynow1.gif" width="62" border="0"></a>
				</b><br>
				Payment method: 
				<select name="pay_type">
    				<option value="N/A">Not Selected</option>
    				<option value="Egopay">Egopay</option>
    				<option value="Okpay">Okpay</option>
    				<option value="PayPal">PayPal</option>
    				<option value="Payza" selected="">Payza</option>
    				<option value="PerfectMoney">PerfectMoney</option>
    				<option value="Solidtrustpay">Solidtrustpay</option>
    			</select>
          </div>
          <div class="col-md-4"> </div>
        </div>
      </div>
      <div class="form-actions">
        <div class="row">
          <div class="col-md-offset-3 col-md-9">
            <button type="submit" name="ssubmit" class="btn blue"><i class="fa fa-check"></i> Submit</button>
          </div>
        </div>
      </div>
      <input name="verified_email" type="hidden" id="verified_email" value="<?=$verified_email ?>">
	  </form>
  </div>
</div>
<script type="text/javascript">

  function checkPassword(str)
  {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
    return re.test(str);
  }

  function checkForm(form)
  {
//     if(form.username.value == "") {
//       alert("Error: Username cannot be blank!");
//       form.username.focus();
//       return false;
//     }
//     re = /^\w+$/;
//     if(!re.test(form.username.value)) {
//       alert("Error: Username must contain only letters, numbers and underscores!");
//       form.username.focus();
//       return false;
//     }
console.log(form.password.value);
console.log(form.cpassword.value);
    if(form.password.value != form.cpassword.value) {
//       if(!checkPassword(form.password.value)) {
        alert("The password you have entered is not valid!");
        form.password.focus();
        return false;
//       }
    }
    return true;
  }

</script>
<?php
//---- define page javascript
//$mtPageScripts = array("adminside/add_user.js");

require_once("templates/default/footer.php");
?>