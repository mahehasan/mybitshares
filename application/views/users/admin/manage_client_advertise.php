<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">List Of Clicked Ads
	            </div>
	            <div class="panel-body">
		                    <table class="table-striped table-hover table-bordered table">
		                    	<thead>
		                    		<tr>
		                    			<th>Advertise ID</th>
		                    			<th>Client</th>
		                    			<th>Click Time</th>
		                    			<th>Enable</th>
		                    		</tr>
		                    	</thead>
		                    	<tbody>
		                    	<?php foreach ($advertise_disabled_list as $row) {?>
		                    		<tr>
		                    			<td><?php echo $row->advertise_id; ?></td>
		                    			<td><?php echo $row->firstName.' '.$row->lastName; ?></td>
		                    			<td><?php echo date('Y-m-d h:i:s A',$row->click_time); ?></td>
		                    			<td><a href="#"><span class="glyphicon glyphicon-ok-circle" onclick="edit('<?php echo $row->manage_client_advertise_id;?>')"></span></a></td>
		                    		</tr>
		                    		<?php } ?> 
		                    	</tbody>
		                    </table>
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
function edit(category_id){
	window.location = "<?=SITE_URL?>admin/advertise_enable/"+category_id;
}
</script>
<?php
require_once("templates/default/admin_footer.php"); 
?>
