<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<p>&nbsp;</p>
	        <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                                    <thead>
                                        <tr role="row">
                                        	<th>Name</th>
                                        	<th>Address</th>
                                        	<th>Email</th>
                                        	<th>Last Login</th>
                                            <th>Active</th>
                                        	<th>Check IP</th>
                                        	<th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if (is_array($user_data_admin) || is_object($user_data_admin))
										{	foreach ($user_data_admin as $row) { ?>                                    
                                    	<tr class="gradeA" role="row">
                                         	<td><a  href="<?php echo base_url().'admin/user_details/'.$row->userID; ?>" ><?php echo $row->firstName. ' ' . $row->lastName ;?></td>
                                         	<td><?php echo $row->address1 . ', ' . $row->town. ' ' . $row->postCode ;?></td>
                                         	<td><?php echo $row->email ;?></td>
                                         	<!-- <td><?php echo $row->userRoleID ;?></td>
                                         	<td><?php echo $row->user_type_id ;?></td> -->
                                         	<td><?php echo date('Y-m-d h:i:s A', $row->lastLogin) ;?></td>
                                         	<td><?php if($row->active==1){
                                         		echo '<div class="btn btn-info btn-circle" onclick="toggle('.$row->userID.')"><i class="fa fa-check"></i></div>';
                                         	}else{
                                         		echo '<div class="btn btn-info btn-circle" onclick="toggle('.$row->userID.')"><i class="fa fa-crosshairs"></i></div>';
                                         		} ;?></td>
                                            <td><a class="btn btn-sm btn-success" href="<?php echo base_url().'admin/check_users_ip/'.$row->userID;?>">Check IP</a></td>
                                         	<td><a href="#"><span class="glyphicon glyphicon-edit" onclick="edit('<?php echo $row->userID ;?>')"></span>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-remove" onclick="del('<?php echo $row->userID ;?>')"></span>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-trash" onclick="del('<?php echo $row->userID ;?>')"></span></a></td>
                                        </tr>
                                        <?php } } ?>
                                    </tbody>
                                </table>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<script type="text/javascript">
function edit(category_id){
	window.location = "<?php echo base_url(); ?>users/edit_user/"+category_id;
}
function del(category_id){
	if (window.confirm('Are you sure that you want to delete?'))
	{
	    window.location = "<?php echo base_url(); ?>users/delete_user/"+category_id;
	}
	else
	{
	    // They clicked no
	}
}
function toggle(argument) {
    window.location = "<?php echo base_url(); ?>admin/enable_disable_user/"+argument;
}
</script>

<?php
require_once("templates/default/admin_footer.php"); 
?>
