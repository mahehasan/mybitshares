<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">List
	            	<div class="pull-right"><a href="<?=SITE_URL?>admin/add_site_server" class="btn btn-outline btn-success btn-sm" type="button">Add</a></div>
	            </div>
	            <div class="panel-body">
        	<p>&nbsp;</p>
	        <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
		
		    	<div class="col-lg-12">

                                    <thead>
                                        <tr role="row">
                                        	
                                        	<th>Site Server ID</th>
                                        	<th>Site Server Code</th>
                                        	<th>Site Server Url</th>
                                        	<th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if (is_array($site_servers) || is_object($site_servers))
										{	foreach ($site_servers as $row) { ?>                                    
                                    	<tr class="gradeA" role="row">

                                         	<td><?php echo $row->site_server_id ;?></td>
                                         	<td><?php echo $row->site_server_code ;?></td>
                                         	<td><?php echo $row->site_server_url ;?></td>
                                         	
                                         	<td><a href="#"><span class="glyphicon glyphicon-edit" onclick="edit('<?php echo $row->site_server_id ;?>')"></span>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-remove" onclick="del('<?php echo $row->site_server_id ;?>')"></span>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-trash" onclick="del('<?php echo $row->site_server_id ;?>')"></span></a></td>
                                        </tr>
                                        <?php } } ?>
                                    </tbody>
                                </table>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
		
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<script type="text/javascript">
function edit(site_server_id){
	window.location = "<?php echo base_url(); ?>admin/edit_site_server/"+site_server_id;
}
function del(site_server_id){
	if (window.confirm('Are you sure that you want to delete?'))
	{
	    window.location = "<?php echo base_url(); ?>admin/delete_site_server/"+site_server_id;
	}
	else
	{
	    // They clicked no
	}
}

</script>
<?php
require_once("templates/default/admin_footer.php"); 
?>
