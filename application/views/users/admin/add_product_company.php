<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
$category_id = $parent_category = 0;
if (!empty($category['category_id'])) $category_id = $category['category_id'];
if (!empty($category['parent_category'])) $parent_category = $category['parent_category'];
?>
<style type="text/css">
.optionGroup
{
    font-weight:bold;
    font-style:italic;
}

.optionChild
{
    padding-left:15px;
}
 </style>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	Form
	            </div>
	            <div class="panel-body">
	            	 <div class="row">
	            	 	<div class="col-lg-6">
                        	<form role="form" action="<?=SITE_URL?>admin/add_product_company" method="post">
                            	<div class="form-group">
                                	<label>Company Name</label>
                                    <input name="company_name" class="form-control" placeholder="Enter text" value="<?php if (isset($cpmpany_detail)) echo $cpmpany_detail[0]->company_name;?>">
                               	</div>
                                <div class="form-group">
                                	<label>Descriptions</label>
                                    <textarea name="company_description" class="form-control" rows="3"><?php if (isset($cpmpany_detail)) echo $cpmpany_detail[0]->company_description;?></textarea>
                              	</div>
                                <?php if (isset($cpmpany_detail)){echo '<input type="hidden" name="product_company_id" value ="'.$cpmpany_detail[0]->product_company_id.'">' ;}?>
                                  <?php echo validation_errors();?>       
                                <button type="submit" class="btn btn-default">Save</button>
                         	</form>
                     	</div>
               		</div>
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/admin_footer.php"); 
?>
