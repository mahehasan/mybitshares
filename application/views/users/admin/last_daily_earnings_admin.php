<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
		<p>&nbsp;</p>
            <p>System's total last daily earnings : <?php
		//echo '<pre>';print_r($daily_earnings_total);echo '</pre>';	die();
			echo number_format($daily_earnings_total, 2) ?> </p>
			<a class="btn btn-sm btn-info" href="<?php echo base_url().'transaction_update_scheduler/enable_daily_earnings';?>">Update last daily earnings</a>
            <p>&nbsp;</p>
            <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Share To Click</th>
					<th>Paid To Click</th>
					<th>Share Buy and Sell Amount</th>
					<th>Advertise Add</th>
                    
                </tr>
                </thead>
                <tbody>
                    <?php 
					$i=1;
					foreach ($daily_earnings as $row) { 
					if($i>10)  continue;
					?>
                    <tr>
                        <td><?php echo $row->transaction_history_date;?></td>
						<td><?php echo $row->Share_To_Click ;?></td>
						<td><?php echo $row->Paid_To_Click ;?></td>
						<td><?php echo $row->Share_Buy_and_Sell_Amount ;?></td>
						<td><?php echo $row->Advertise_Add ;?></td>
                        
                    </tr>
                    <?php 
					$i++;
					} ?>
                </tbody>
            </table>
        </div>
		<div class="col-lg-4">
                        	<form role="form" action="<?=SITE_URL?>admin/add_share_amount" method="post">

                            	<div class="form-group">
                                	<label>Share amount for all user</label>
                                     <input name="share_amount" class="form-control" placeholder="Enter Share amount" value="">
									 
                               	</div>

								<button type="submit" class="btn btn-default">Distribute Share</button>  
                                
                         	</form>
                     	</div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<script type="text/javascript">
function edit(category_id){
    window.location = "<?php echo base_url(); ?>admin/edit_user/"+category_id;
}
function del(category_id){
    if (window.confirm('Are you sure that you want to delete?'))
    {
        window.location = "<?php echo base_url(); ?>admin/delete_user/"+category_id;
    }
    else
    {
        // They clicked no
    }
}
function toggle(argument) {
    window.location = "<?php echo base_url(); ?>admin/enable_disable_user/"+argument;
}
</script>

<?php
require_once("templates/default/admin_footer.php"); 
?>
