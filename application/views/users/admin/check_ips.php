<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<p>&nbsp;</p>
	        <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                                    <thead>
                                        <tr role="row">
                                        	<th>Name</th>
                                        	<th colspan="4">IP Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if (is_array($user_data_admin) || is_object($user_data_admin))
										{	foreach ($user_data_admin as $row) { ?>                                    
                                    	<tr class="gradeA" role="row">
                                         	<td><?php echo $row->firstName. ' ' . $row->lastName ;?></td>
                                         	
                                         	<td>
											 <table>
												
											<?php $res = $this->admin_model->getUsersIPList($row->userID );
											
											
									if (is_array($res) || is_object($res))
										{	foreach ($res as $row) { ?> 
											
										<tr role="row">	  
										<td> <?php echo $row->ip_address;?> </td> </tr>
											
										<?php }
										}?>
											
										 
										</table>
											</td>
                                         	<!-- <td><?php echo $row->userRoleID ;?></td>
                                         	<td><?php echo $row->user_type_id ;?></td> -->
                                         	
                                        </tr>
                                        <?php } } ?>
                                    </tbody>
                                </table>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->


<?php
require_once("templates/default/admin_footer.php"); 
?>
