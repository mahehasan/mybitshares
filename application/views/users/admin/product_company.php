<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">List
	            	<div class="pull-right"><a href="<?=SITE_URL?>admin/add_product_company" class="btn btn-outline btn-success btn-sm" type="button">Add</a></div>
	            </div>
	            <div class="panel-body">
                    <table class="table-striped table-hover table-bordered table">
                    	<thead>
                    		<tr>
                    			<th>Company Name</th>
                    			<th>Description</th>
                    			<th>Operation</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    	<?php foreach ($product_company_list as $row) {?>
                    		<tr>
                    			<td><?php echo $row->company_name; ?></td>
                    			<td><?php echo $row->company_description; ?></td>
                    			<td><a href="#"><span class="glyphicon glyphicon-edit" onclick="edit('<?php echo $row->product_company_id;?>')"></span>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-remove" onclick="del('<?php echo $row->product_company_id;?>')"></span></a></td>
                    		</tr>
                    		<?php } ?> 
                    	</tbody>
                    </table>
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
function edit($id){
	window.location = "<?=SITE_URL?>admin/edit_product_company/"+$id;
}
function del($id){
if (window.confirm('Are you sure that you want to delete?'))
{
    window.location = "<?=SITE_URL?>admin/delete_product_company/"+$id;
}
else
{
    // They clicked no
}
}
</script>
<?php
require_once("templates/default/admin_footer.php"); 
?>
