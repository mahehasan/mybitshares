<?php
if ( ! defined('SITE_URL')) exit('No direct script access allowed');
require_once("templates/default/admin_header.php");

?>

<!-- <h3 class="page-title">Edit System User </h3> -->

    <!-- Results table -->
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue-madison">
      <div class="portlet-title">
        <div class="caption">
<!--           <i class="fa fa-support"></i>Edit System User Form -->
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse"></a>
        </div>
      </div>
      <div class="portlet-body form">
      <!-- BEGIN FORM-->
      <form action="<?=SITE_URL?>users/process_user_edit" method="post" name="frmEditRegister" id="frmEditRegister" class="form-horizontal" >
    	<div class="form-body">
<!-- 	      <h5 class="form-section"> -->
<!--             Please complete the following fields and submit form by clicking the "Submit" button below. <b>Note: Required fields are marked with an asterisk.</b> -->
<!--           </h5> -->
<?php
if(isset($err)){
	if($err == 1){
		echo $this->html->showErrorMsg($MSG);
	}
	elseif($err == 0){
		echo $this->html->showSuccessMsg($MSG);
	}
}
?>
    	<?php //$this->html->showErrorMsg("You have some form errors. Please check below.", "", "display-hide")?>
	   
        
       
		
        <div class="form-group">
          <label class="col-md-3 control-label">First Name</label>
           <div class="col-md-5">
             <input type="text" class="form-control" name="firstName" id="firstName" value="<?php echo $user_detail->firstName;?>" maxlength="255">
          </div>
          <div class="col-md-4"> </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label">Last Name</label>
          <div class="col-md-5">
            <input type="text" class="form-control" name="lastName" id="lastName" value="<?php echo $user_detail->lastName;?>" maxlength="255">
          </div>
          <div class="col-md-4"> </div>
        </div>
        
         <div class="form-group">
          <label class="col-md-3 control-label">Email</label>
          <div class="col-md-5">
            <input type="text" class="form-control" name="email" id="email" value="<?php echo $user_detail->email;?>" maxlength="255">
          </div>
          <div class="col-md-4"> </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label">Address</label>
          <div class="col-md-5">
            <input name="address1" maxlength="255" class="form-control" id="address" value="<?php echo $user_detail->address1;?>" type="text" />
            </div>
            <div class="col-md-4">
            </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label">Town</label>
          <div class="col-md-5">
            <input name="town" maxlength="255" class="form-control" id="town" value="<?php echo $user_detail->town;?>" type="text" />
          </div>
          <div class="col-md-4"> </div>
        </div>
        
        <div class="form-group">
          <label class="col-md-3 control-label">Post Code</label>
          <div class="col-md-5">
            <input name="postCode" maxlength="255" class="form-control" id="postCode" value="<?php echo $user_detail->postCode;?>" type="text" />
            </div>
            <div class="col-md-4">
            </div>
        </div>

		<div class="form-group">
          <label class="col-md-3 control-label">Country</label>
          <div class="col-md-5">
          	<?php if (!empty($countryList)) {?>
          	<select name="country" class="form-control">
          			<?php foreach ($countryList as $key=>$value) {?>
    				<option value="<?=$key?>"<?php if($user_detail->country == $key){echo "selected";};?>><?=$value?></option>
    				<?php }?>
    		</select>
    		<?php }?>
          </div>
          <div class="col-md-4"> </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Cashout Profile</label>
          <div class="col-md-5">
            <b>No member yet of an payment processor. Join them by click on the button (open in new field).<br>
				<a target="_blank" href="http://www.sensipay.com/users/personalinfo/xpWUlpZ5">
						<img height="35" src="http://jillsclickcorner.com//images/small-pay1.jpg" width="62" border="0"></a>
				<a target="_blank" href="https://www.paypal.com/en/mrb/pal=UVYS2UR39QE4Q">
				<img height="31" src="http://www.donkeymails.com/pages/x-click-but01.gif" width="62" border="0"></a>
				</b>&nbsp;<a target="_blank" href="http://www.payza.com/?GgXoLizaKH5TFsm0NQ%2fbcw%3d%3d">
				<img height="28" src="https://secure.payza.com/PayNow/8695D27D6AA848BD8F68F9A0A9EFC562b0en.gif" width="88" border="0"></a>
				<a target="_blank" href="https://perfectmoney.com/?ref=501236">
				<img height="31" src="https://perfectmoney.com/img/88-31-10.png" width="88" border="0"></a><b>
				<a target="_blank" href="http://solidtrustpay.com/?r=81302">
				<img height="35" src="https://solidtrustpay.com/ImgDir/buttons/buynow1.gif" width="62" border="0"></a>
				</b><br>
				Payment method: 
				<select name="pay_type">
    				<option value="N/A" <?php if($user_detail->pay_type == "N/A"){echo "selected";};?>>Not Selected</option>
    				<option value="Egopay" <?php if($user_detail->pay_type == "Egopay"){echo "selected";};?>>Egopay</option>
    				<option value="Okpay" <?php if($user_detail->pay_type == "Okpay"){echo "selected";};?>>Okpay</option>
    				<option value="PayPal" <?php if($user_detail->pay_type == "PayPal"){echo "selected";};?>>PayPal</option>
    				<option value="Payza" <?php if($user_detail->pay_type == "Payza"){echo "selected";};?>>Payza</option>
    				<option value="PerfectMoney" <?php if($user_detail->pay_type == "PerfectMoney"){echo "selected";};?>>PerfectMoney</option>
    				<option value="Solidtrustpay" <?php if($user_detail->pay_type == "Solidtrustpay"){echo "selected";};?>>Solidtrustpay</option>
    			</select>
          </div>
          <div class="col-md-4"> </div>
        </div>
      </div>
      <div class="form-actions">
        <div class="row">
          <div class="col-md-offset-3 col-md-9">
            <button type="submit" name="ssubmit" class="btn blue"><i class="fa fa-check"></i> Submit</button>
          </div>
        </div>
      </div>
      
	  </form>
  </div>
</div>
<script type="text/javascript">

  function checkPassword(str)
  {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
    return re.test(str);
  }

  function checkForm(form)
  {
//     if(form.username.value == "") {
//       alert("Error: Username cannot be blank!");
//       form.username.focus();
//       return false;
//     }
//     re = /^\w+$/;
//     if(!re.test(form.username.value)) {
//       alert("Error: Username must contain only letters, numbers and underscores!");
//       form.username.focus();
//       return false;
//     }
//console.log(form.password.value);
//console.log(form.cpassword.value);
//    if(form.password.value != form.cpassword.value) {
//       if(!checkPassword(form.password.value)) {
//        alert("The password you have entered is not valid!");
//        form.password.focus();
//        return false;
//       }
 //   }
    return true;
  }

</script>
<?php
//---- define page javascript
//$mtPageScripts = array("adminside/add_user.js");

require_once("templates/default/admin_footer.php");
?>