<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">List
	            	<div class="pull-right"><a href="<?=SITE_URL?>admin/add_category" class="btn btn-outline btn-success btn-sm" type="button">Add</a></div>
	            </div>
	            <div class="panel-body">
	            	 <div class="navbar-default" role="navigation">
		                <div class="sidebar-nav navbar-collapse">
		                    <ul class="nav" id="category-menu">
		                    	<?php
		                    	if ( ! empty($categories) ){
		                    		foreach ( $categories as $category ){
		                    			$qry_options = " WHERE `category_publish` = 1 AND parent_category = ".$category['category_id'];
		                    			//---- all records processing
		                    			$sub_categories = $this->category_model->getAllCategoryData($qry_options);
		                    			//print_r($sub_categories);
		                    			if (empty($sub_categories)){
		                    	?>
					                        <li>
					                            <a href="<?=SITE_URL?>admin/edit_category/<?=$category['category_id']?>"> <?=$category['category_title']?></a>
					                        </li>
		                        		<?php 
		                    			}else {
		                        		?>
					                        <li>
					                            <a href="#"> <?=$category['category_title']?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-edit" onclick="edit('<?=$category['category_id']?>')"></span>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-remove" onclick="del('<?=$category['category_id']?>')"></span><span class="fa arrow"></span></a>
					                            <ul class="nav nav-second-level">
					                            	<?php
					                            	foreach ( $sub_categories as $sub_category ){
					                            	?>
					                                <li>
					                                    <a href="<?=SITE_URL?>admin/edit_category/<?=$sub_category['category_id']?>"><?=$sub_category['category_title']?></a>
					                                </li>
					                                <?php }?>
					                            </ul>
					                            <!-- /.nav-second-level -->
					                        </li>
					                        <?php }?>
		                        <?php }?>
		                        <?php }?>
		                    </ul>
		                </div>
		                <!-- /.sidebar-collapse -->
		            </div>
		            <!-- /.navbar-static-side -->
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
function edit(category_id){
	window.location = "<?=SITE_URL?>admin/edit_category/"+category_id;
}
function del(category_id){
	alert("you want to delete! "+category_id);
}
</script>
<?php
require_once("templates/default/admin_footer.php"); 
?>
