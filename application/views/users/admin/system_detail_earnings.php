<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
    <!-- /.row -->
    <div class="row">
	

        <div class="col-lg-12">
            <p>&nbsp;</p>
			<a class="btn btn-sm btn-success" href="<?php echo base_url().'admin/system_earnings';?>">Back</a>
			<p>&nbsp;</p>
            <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                <thead>
                <tr>
                    <th colspan="5" style="text-align:center;" >Account Transaction History</th>
                </tr>
				<tr>
                    <th>&nbsp;&nbsp;</th>
					<th>Date</th>
					<th>Transaction Type</th>
					<th>Amount</th>
                </tr>
                </thead>
                <tbody>
						<?php $i=0 ;?>
                        <?php foreach ($transaction_history as $row) {
							
							if($i<365)
							{
                            echo '<tr>  <td>&nbsp;&nbsp;</td><td>'.date('Y-m-d h:i:s A', $row->transaction_history_date).'</td><td>'.$row->transaction_message.'</td><td>'.$row->transaction_amount.'</td>  </tr>';
							
							
							}
							$i ++;} ?>
                                            
                </tbody>
            </table>
        </div>
    </div> 
</div>
<!-- /.container-fluid earning stat -->

<script type="text/javascript">
function edit(category_id){
	window.location = "<?php echo base_url(); ?>admin/edit_user/"+category_id;
}
function del(category_id){
	if (window.confirm('Are you sure that you want to delete?'))
	{
	    window.location = "<?php echo base_url(); ?>admin/delete_user/"+category_id;
	}
	else
	{
	    // They clicked no
	}
}
</script>

<?php
require_once("templates/default/footer.php"); 
?>
