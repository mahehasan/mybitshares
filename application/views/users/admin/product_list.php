<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">List
	            	<div class="pull-right"><a href="<?=SITE_URL?>admin/add_product" class="btn btn-outline btn-success btn-sm" type="button">Add</a></div>
	            </div>
	            <div class="panel-body">
	            	 <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th colspan="8" style="text-align: center;">AD's in the server 100</th>
                        </tr>
                    </thead><thead>
                        <tr>
                            <th>Image</th>
                            <th>Product Name</th>
                            <th>Summary</th>
                            <th>Shares</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Discount</th>
                            <th>Operation</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($product_list as $row) { ?>
                            <tr>
                            <td style="text-align: center;"><?php 
							if(file_exists($row->product_thumbimage)){
							echo '<img src="'.base_url().$row->product_thumbimage.'" width="120" height="120">'; }else{
								echo 'No Image.';
							}?></td>
                            <?php 
                                echo '<td>'. $row->product_name.'</td>';
                                echo '<td>'. $row->short_description.'</td>';
                                echo '<td>'. $row->product_shares.'</td>';
                                echo '<td>'. $row->product_description.'</td>';
                                echo '<td>'. $row->product_price.'</td>';
                                echo '<td>'. $row->product_discount.'</td>'; ?>
                                <td><a href="#"><span class="glyphicon glyphicon-edit" onclick="edit('<?php echo $row->product_id ;?>')"></span>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-trash" onclick="del('<?php echo $row->product_id ;?>')"></span></a></td>
                            </tr>
                          <?php   }?>
                        </tbody>
                        </table>
                        </div>
              <!-- /.table-responsive -->
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
function edit(category_id){
	window.location = "<?=SITE_URL?>admin/edit_product/"+category_id;
}
function del(category_id){
if (window.confirm('Are you sure that you want to delete?'))
{
    window.location = "<?=SITE_URL?>admin/delete_product/"+category_id;
}
else
{
    // They clicked no
}
}
</script>
<?php
require_once("templates/default/admin_footer.php"); 
?>
