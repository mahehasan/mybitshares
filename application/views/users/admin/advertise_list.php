<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">List
	            	<div class="pull-right"><a href="<?=SITE_URL?>admin/add_advertise" class="btn btn-outline btn-success btn-sm" type="button">Add</a></div>
	            </div>
	            <div class="panel-body">
                <?php 
                // echo '<pre>';
                // print_r( $obj->advertise_list); // 12345
                // echo '</pre>';
                ?>

                <?php $i=0;
                if (!empty($obj->advertise_list)) {
                foreach ($obj as $row1) { //echo "<pre>";print_r($row1);die("here");?>
                <div class="table-responsive">   
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th colspan="8" style="text-align: center;">AD's in the server <?=$row1[$i]->site_server_code?></th>
                        </tr>
                    </thead><thead>
                        <tr>
                            <th>Ad Image</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Uploader</th>
                             <th>Package</th>
<!--                             <th>Menu</th> -->
                            <th>Client URL</th>
							<th>View Detail</th>
                            <th>Operation</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($row1 as $row) {
                                $main_advertise_id = $this->admin_model->getMainServerAdvetiseIdValue($row->site_server_code, $row->site_advertise_id);
								
								if($main_advertise_id ==0 || $main_advertise_id =="") continue;
									
                                $advertise_title = $this->admin_model->get_advertise_title_value($row->site_server_code, $row->site_advertise_id);
                                $advertise_description = $this->admin_model->get_advertise_description_value($row->site_server_code, $row->site_advertise_id);
								$advertise_id = $this->admin_model->getadvertise_id($row->site_server_code, $row->site_advertise_id);
								
                                $site_server_ads_url = '';
                                if ($row->file_name != ""){
                                    $site_server_ads_url = $this->general->getSiteServerURL($row->site_server_code) . 'v1/ads/' . $row->file_name;
                                }
                                ?>
                            <tr>
                            <td style="text-align: center;">
                                <?php
                                if($site_server_ads_url){
                                    echo '<img title="'.$advertise_title. ' ' .$advertise_description.'" src="'.$site_server_ads_url.'">';
                                }else{
                                    echo 'No image';
                                }
                                ?>
                            </td>
                            <?php 
                                echo '<td>'. $advertise_title.'</td>';
                                echo '<td>'. $advertise_description.'</td>';
                                echo '<td>'. $this->admin_model->getUserName($row->created_by).'</td>';
                                echo '<td>'. $this->admin_model->getPackage($row->site_server_code, $row->site_advertise_id).'</td>';
//                                echo '<td>'. $this->admin_model->getMenu1Name($row->site_server_code, $row->site_advertise_id).'</td>';
//                                echo '<td>'. $this->admin_model->getMenu1Value($row->site_server_code, $row->site_advertise_id).'</td>';
//                                echo '<td>'. $this->admin_model->getMenu2Name($row->site_server_code, $row->site_advertise_id).'</td>';
//                                echo '<td>'. $this->admin_model->getMenu2Value($row->site_server_code, $row->site_advertise_id).'</td>';
                                echo '<td>'. $row->client_url.'</td>'; ?>
								
								 <td><a class="btn btn-sm btn-success" href="<?php echo base_url().'admin/view_advertise/'.$advertise_id;?>">View</a></td>
								
                                <td><a href="#"><span class="glyphicon glyphicon-edit" onclick="edit('<?php echo $main_advertise_id?>')"></span>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-remove" onclick="del('<?php echo $main_advertise_id;?>')"></span></a></td>
                            </tr>
                          <?php   }?>
                        </tbody>
                        </table>
                        </div>
                        <?php } }else{?>
                        <p>No data</p>
                        <?php }?>
              <!-- /.table-responsive -->
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
function edit(main_advertise_id){
	window.location = "<?=SITE_URL?>admin/modify_advertise/"+main_advertise_id;
}
function del(main_advertise_id){
if (window.confirm('Are you sure that you want to delete?'))
{
    window.location = "<?=SITE_URL?>admin/delete_advertise/"+main_advertise_id;
}
else
{
    // They clicked no
}
}
</script>
<?php
require_once("templates/default/admin_footer.php"); 
?>
