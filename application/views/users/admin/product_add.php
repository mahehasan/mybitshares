<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
$category_id = $parent_category = 0;
if (!empty($category['category_id'])) $category_id = $category['category_id'];
if (!empty($category['parent_category'])) $parent_category = $category['parent_category'];
?>
<style type="text/css">
.optionGroup
{
    font-weight:bold;
    font-style:italic;
}

.optionChild
{
    padding-left:15px;
}
 </style>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	Add Product
	            </div>
	            <div class="panel-body">
	            	 <div class="row">
	            	 	<div class="col-lg-6">
                        	<form role="form" enctype="multipart/form-data" action="<?=SITE_URL?>admin/insert_product" method="post">
                            	<div class="form-group">
                                	<label>Name</label>
                                    <input name="product_name" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $product_detail[0]->product_name ;}?>">
                               	</div><div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control" name="product_category_id">
                                            <option value="NULL">Select</option>
                                            <?php foreach ($category_list as $row) {
                                              echo '<option value="'.$row->product_category_id.'"';
                                              if(isset($edit)){if($row->product_category_id == $product_detail[0]->product_category_id) {echo 'selected="selected"';}}
                                              echo '>'.$row->category_title.'</option>';
                                            }?>
                                            </select>
                                        </div><div class="form-group">
                                            <label>Company</label>
                                            <select class="form-control" name="product_company_id">
                                            <option value="NULL">Select</option>
                                            <?php foreach ($company_list as $row) {
                                              echo '<option value="'.$row->product_company_id.'"';
                                              if(isset($edit)){if($row->product_company_id == $product_detail[0]->product_company_id) {echo 'selected="selected"';}}
                                              echo '>'.$row->company_name.'</option>';
                                            }?>
                                            </select>
                                        </div>
                                <div class="form-group">
                                   <label>Quantity</label>
                                    <input name="product_quantity" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $product_detail[0]->product_quantity;}?>">
                                </div><div class="form-group">
                                            <label>Price</label>
                                            <select class="form-control" name="product_price">
                                            <option value="NULL">Select</option>
                                            <?php foreach ($price_list as $row) {
                                              echo '<option value="'.$row->price_id.'"';
                                              if(isset($edit)){if($row->price_id == $product_detail[0]->product_price) {echo 'selected="selected"';}}
                                              echo '>'.$row->price_title.'</option>';
                                            }?>
                                            </select>
                                        </div><div class="form-group">
                                  <label>Discount</label>
                                    <input name="product_discount" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $product_detail[0]->product_discount;}?>">
                                </div><div class="form-group">
                                  <label>Discount Percentage</label>
                                    <input name="product_discount_percentage" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $product_detail[0]->product_discount_percentage;}?>">
                                </div><div class="form-group">
                                  <label>Shares</label>
                                    <input name="product_shares" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $product_detail[0]->product_shares ;}?>">
                                </div><div class="form-group">
                                    <label class="checkbox-inline">
                                      <input type="checkbox" name="product_special" id="inlineCheckbox1" value="0"> Special
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="checkbox-inline">
                                      <input type="checkbox" name="new_arrivals" id="inlineCheckbox2" value="0"> New
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="checkbox-inline">
                                      <input type="checkbox" name="best_sellers" id="inlineCheckbox3" value="0"> Best Seller
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="checkbox-inline">
                                      <input type="checkbox" name="comming_soon" id="inlineCheckbox4" value="0"> Coming Soon
                                    </label>
                                </div><div class="form-group">
                                  <label>Summary</label>
                                    <input name="short_description" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $product_detail[0]->short_description ;}?>">
                                </div><div class="form-group">
                                  <label>Description</label>
                                    <input name="product_description" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $product_detail[0]->product_description ;}?>">
                                </div>
                               	<div class="form-group">
                                	<label>Image</label>
                                    <input type="file" name="userfile" size="20">
                               	</div>
                               	<?php if (isset($edit)){echo '<input type="hidden" name="product_id" value ="'.$product_detail[0]->product_id.'">' ;}?>
                                  <?php echo validation_errors();?>
                                <button type="submit" class="btn btn-default">Save</button>
                         	</form>
                     	</div>
               		</div>
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/admin_footer.php"); 
?>
