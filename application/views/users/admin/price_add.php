<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
$category_id = $parent_category = 0;
if (!empty($category['category_id'])) $category_id = $category['category_id'];
if (!empty($category['parent_category'])) $parent_category = $category['parent_category'];
?>
<style type="text/css">
.optionGroup
{
    font-weight:bold;
    font-style:italic;
}

.optionChild
{
    padding-left:15px;
}
 </style>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	Add Price
	            </div>
	            <div class="panel-body">
	            	 <div class="row">
	            	 	<div class="col-lg-6">
                        	<form role="form" action="<?=SITE_URL?>admin/insert_price" method="post">
                            	<div class="form-group">
                                	<label>Title</label>
                                    <input name="price_title" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $price_detail[0]->price_title ;}?>">
                               	</div>
                               	<div class="form-group">
                                	<label>Descriptions</label>
                                    <input name="price_description" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $price_detail[0]->price_description ;}?>">
                               	</div>	
                               	<div class="form-group">
                                	<label>Selling Price</label>
                                    <input name="price_sell" class="form-control" placeholder="Amount" value="<?php if (isset($edit)){echo $price_detail[0]->price_sell ;}?>">
                               	</div>
                               	<div class="form-group">
                                	<label>Advertise Price</label>
                                    <input name="price_advertise" class="form-control" placeholder="Amount" value="<?php if (isset($edit)){echo $price_detail[0]->price_advertise ;}?>">
                               	</div>
                               	<?php if (isset($edit)){echo '<input type="hidden" name="price_id" value ="'.$price_detail[0]->price_id.'">' ;}?>
                                  <?php echo validation_errors();?>
                                <button type="submit" class="btn btn-default">Save</button>
                         	</form>
                     	</div>
               		</div>
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/admin_footer.php"); 
?>
