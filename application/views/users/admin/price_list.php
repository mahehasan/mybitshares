<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">List
	            	<div class="pull-right"><a href="<?=SITE_URL?>admin/add_price" class="btn btn-outline btn-success btn-sm" type="button">Add</a></div>
	            </div>
	            <div class="panel-body">
	            	 <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover">
                     <thead>
                     	<tr>
                     		<th>Title</th>
                     		<th>Description</th>
                     		<th>Selling Price</th>
                     		<th>Advertise Price</th>
                     	<!-- 	<th>Publish Price</th>
                     		<th>Showing Order</th > -->
                     		<th>Operation</th>
                     	</tr>
                     </thead>
                     <tbody>
                     	<?php foreach ($price_list as $row) { ?>
                     		<tr>
                     			<td><?php echo $row->price_title;?></td>
                     			<td><?php echo $row->price_description;?></td>
                     			<td><?php echo $row->price_sell;?></td>
                     			<td><?php echo $row->price_advertise;?></td>
                     		<!-- 	<td><?php echo $row->showing_order;?></td>
                     			<td><?php echo $row->price_publish;?></td> -->
                     			<td><a href="#"><span class="glyphicon glyphicon-edit" onclick="edit('<?php echo $row->price_id;?>')"></span>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-remove" onclick="del('<?php echo $row->price_id;?>')"></span></a></td>
                     		</tr>
                     <?php	} ?>
                     </tbody>
                  </table>
              </div>
              <!-- /.table-responsive -->
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
function edit(category_id){
	window.location = "<?=SITE_URL?>admin/edit_price/"+category_id;
}
function del(category_id){
if (window.confirm('Are you sure that you want to delete?'))
{
    window.location = "<?=SITE_URL?>admin/delete_price/"+category_id;
}
else
{
    // They clicked no
}
}
</script>
<?php
require_once("templates/default/admin_footer.php"); 
?>
