<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">List
	            	<div class="pull-right"><a href="<?=SITE_URL?>admin/add_product_category" class="btn btn-outline btn-success btn-sm" type="button">Add</a></div>
	            </div>
	            <div class="panel-body">
		                    <table class="table-striped table-hover table-bordered table">
		                    	<thead>
		                    		<tr>
		                    			<th>Category Name</th>
		                    			<th>Operation</th>
		                    		</tr>
		                    	</thead>
		                    	<tbody>
		                    	<?php foreach ($category_list as $row) {?>
		                    		<tr>
		                    			<td><?php echo $row->category_title; ?></td>
		                    			<td><a href="#"><span class="glyphicon glyphicon-edit" onclick="edit('<?php echo $row->product_category_id;?>')"></span>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-remove" onclick="del('<?php echo $row->product_category_id;?>')"></span></a></td>
		                    		</tr>
		                    		<?php } ?> 
		                    	</tbody>
		                    </table>
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
function edit(category_id){
	window.location = "<?=SITE_URL?>admin/edit_product_category/"+category_id;
}
function del(category_id){
if (window.confirm('Are you sure that you want to delete?'))
{
    window.location = "<?=SITE_URL?>admin/delete_product_category/"+category_id;
}
else
{
    // They clicked no
}
}
</script>
<?php
require_once("templates/default/admin_footer.php"); 
?>
