<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">IP List</div>
	            <div class="panel-body">
	            	 <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover">
                     <thead>
                     	<tr>
                     		<th>Site Server Code</th>
                            <th>Site Server Advertise ID</th>
                            <th>Client ID</th>
                            <th>Cash Amount</th>
                     		<th>Date</th>
                     		<th>IP Address</th>
                     	</tr>
                     </thead>
                     <tbody>
                     	<?php foreach ($list as $row) { ?>
                     		<tr>
                     		<td><?php echo $row->site_server_code;?></td>
                                <td><?php echo $row->site_server_advertise_id;?></td>
                                <td><?php echo $row->client_id;?></td>
                                <td><?php echo $row->cash_amount;?></td>
                                <td><?php echo date('Y-m-d h:i:s A', $row->click_date);?></td>                               
                     			<td><?php echo $row->ip_address;?></td>
                     		</tr>
                     <?php	} ?>
                     </tbody>
                  </table>
              </div>
              <!-- /.table-responsive -->
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
function edit(category_id){
	window.location = "<?=SITE_URL?>admin/edit_bonus_list/"+category_id;
}
function del(category_id){
if (window.confirm('Are you sure that you want to delete?'))
{
    window.location = "<?=SITE_URL?>admin/delete_bonus_list/"+category_id;
}
else
{
    // They clicked no
}
}
</script>
<?php
require_once("templates/default/admin_footer.php"); 
?>
