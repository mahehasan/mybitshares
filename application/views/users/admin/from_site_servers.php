<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();

$site_server_id =0;
if (!empty($site_server['site_server_id'])) $site_server_id = $site_server['site_server_id'];

?>
<style type="text/css">
.optionGroup
{
    font-weight:bold;
    font-style:italic;
}

.optionChild
{
    padding-left:15px;
}
 </style>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	Form
	            </div>
	            <div class="panel-body">
				    <?php if ( isset($err) ){ if ( $err==1 ) echo $this->html->showErrorMsg($MSG); else if ( $err==0 ) echo $this->html->showSuccessMsg($MSG); } ?>
	            	 <div class="row">
	            	 	<div class="col-lg-6">
                        	<form role="form" action="<?=SITE_URL?>admin/add_site_server" method="post">

                            	<div class="form-group">
                                	<label>Site Server Code</label>
                                     <input name="site_server_code" class="form-control" placeholder="Enter Site Server Code" value="<?php if (!empty($site_server['site_server_code'])) echo $site_server['site_server_code'];?>">
                               	</div>
                                <div class="form-group">
                                	<label>Site Server Url</label>
									<input name="site_server_url" class="form-control" placeholder="Enter Site Server Url" value="<?php if (!empty($site_server['site_server_url'])) echo $site_server['site_server_url'];?>">
									
                                    
                              	</div>
								<input type="hidden" name="site_server_id" value="<?=$site_server_id?>">     
                                <button type="submit" class="btn btn-default">Save</button>
                         	</form>
                     	</div>
               		</div>
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>





<!-- /.container-fluid -->
<?php
require_once("templates/default/admin_footer.php"); 
?>
