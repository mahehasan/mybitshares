<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">Inactive Advertise List	            	
	            </div>
	            <div class="panel-body">
                <?php 
                // echo '<pre>';
                // print_r( $obj); // 12345
                // echo '</pre>';
                ?>

                <?php $i=0;
                if (!empty($obj) && !empty($site_server_ads_data->inactive_advertise_list)) {
                ?>
                <div class="table-responsive">   
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                           
                        </tr>
                    </thead><thead>
                        <tr>
                            <th>Ad Image</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Uploader</th>
                             <th>Package</th>
<!--                             <th>Menu</th> -->
                            <th>Client URL</th>
							<th>Active</th>
							
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($obj as $row) {
                                $main_advertise_id = $this->admin_model->getMainServerAdvetiseIdValue($row->site_server_code, $row->site_advertise_id);
                                $advertise_title = $this->admin_model->get_advertise_title_value($row->site_server_code, $row->site_advertise_id);
                                $advertise_description = $this->admin_model->get_advertise_description_value($row->site_server_code, $row->site_advertise_id);
								$advertise_id = $this->admin_model->getadvertise_id($row->site_server_code, $row->site_advertise_id);
								
                                $site_server_ads_url = '';
                                if ($row->file_name != ""){
                                    $site_server_ads_url = $this->general->getSiteServerURL($row->site_server_code) . 'v1/ads/' . $row->file_name;
                                }
                                ?>
                            <tr>
                            <td style="text-align: center;">
                                <?php
                                if($site_server_ads_url){
                                    echo '<img title="'.$advertise_title. ' ' .$advertise_description.'" src="'.$site_server_ads_url.'">';
                                }else{
                                    echo 'No image';
                                }
                                ?>
                            </td>
                            <?php 
                                echo '<td>'. $advertise_title.'</td>';
                                echo '<td>'. $advertise_description.'</td>';
                                echo '<td>'. $this->admin_model->getUserName($row->created_by).'</td>';
                                echo '<td>'. $this->admin_model->getPackage($row->site_server_code, $row->site_advertise_id).'</td>';
                                echo '<td>'. $row->client_url.'</td>'; ?>
								
							<td><?php  echo '<div class="btn btn-info btn-circle" onclick="enable('.$advertise_id.','.$row->site_advertise_id.','.$row->site_server_code.')"><i class="fa fa-crosshairs"></i></div>';
                                         		 ;?></td>
                            </tr>
                          <?php   }?>
                        </tbody>
                        </table>
                        </div>
                        <?php  }else{?>
                        <p>No data</p>
                        <?php }?>
              <!-- /.table-responsive -->
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<script type="text/javascript">
function enable(argument,site_advertise_id,site_server_code) {
    window.location = "<?php echo base_url(); ?>admin/enable_advertise/"+argument+"/"+site_advertise_id+"/"+site_server_code;
}
</script>

<?php
require_once("templates/default/admin_footer.php"); 
?>
