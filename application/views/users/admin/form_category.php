<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
$category_id = $parent_category = 0;
if (!empty($category['category_id'])) $category_id = $category['category_id'];
if (!empty($category['parent_category'])) $parent_category = $category['parent_category'];
?>
<style type="text/css">
.optionGroup
{
    font-weight:bold;
    font-style:italic;
}

.optionChild
{
    padding-left:15px;
}
 </style>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	Form
	            </div>
	            <div class="panel-body">
	            	 <div class="row">
	            	 	<div class="col-lg-6">
                        	<form role="form" action="<?=SITE_URL?>admin/add_category" method="post">
                        		<div class="form-group">
                                	<label>Category</label>
                                    <select name="parent_category" class="form-control">
							      		<option value="0">Parent</option>
							            <?php
							                 echo $this->html->get_parent_category($parent_category);               
							            ?>
							       	</select>
                               	</div>
                            	<div class="form-group">
                                	<label>Title</label>
                                    <input name="category_title" class="form-control" placeholder="Enter text" value="<?php if (!empty($category['category_title'])) echo $category['category_title'];?>">
                               	</div>
                                <div class="form-group">
                                	<label>Descriptyions</label>
                                    <textarea name="category_description" class="form-control" rows="3"><?php if (!empty($category['category_description'])) echo $category['category_description'];?></textarea>
                              	</div>
                              	<input type="hidden" name="category_id" value="<?=$category_id?>">        
                                <button type="submit" class="btn btn-default">Save</button>
                         	</form>
                     	</div>
               		</div>
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/admin_footer.php"); 
?>
