<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
    <div class="row">
<?php
 // echo'<pre>';print_r($client_share_history );echo'</pre>'; die();
?>
	<div class="panel-heading">
	<div class="pull-right"><a href="<?=SITE_URL?>admin/manage_users" class="btn btn-outline btn-success btn-sm" type="button">Back</a></div></div>
    	<div class="col-lg-4">
        	<p>&nbsp;</p>
	        <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                <thead>
                    <tr role="row">
                    	<td><b>Name</b></td><td><?php echo $user_data_detail[0]->firstName. ' ' . $user_data_detail[0]->lastName ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Address</b></td><td><?php echo $user_data_detail[0]->address1 . ', ' . $user_data_detail[0]->town. ' ' . $user_data_detail[0]->postCode ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Email</b></td><td><?php echo $user_data_detail[0]->email ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Last Login</b></td><td><?php echo date('Y-m-d h:i:s A', $user_data_detail[0]->lastLogin) ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Active</b></td><td><?php if($user_data_detail[0]->active==1){
                     		echo '<div class="btn btn-info btn-circle"><i class="fa fa-check"></i></div>';
                     	}else{
                     		echo '<div class="btn btn-info btn-circle"><i class="fa fa-cross"></i></div>';
                     		} ;?></td>
                    </tr>
                </thead>
            </table>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-4">
            <p>&nbsp;</p>
            <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                <thead>
                <tr>
                    <th colspan="2" style="text-align:center;" >Transaction Ledger</th>
                </tr>
                <tr>
                    <th>Account Balances</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Total Cash Balance:</td><td><?php $total = 0; foreach ($client_cash_history as $row)  {
                            $total += $row->cash_amount;
                        }
                        echo number_format($total,2) ?></td>
                    </tr><tr>
                        <td>Total Shares Balance:</td><td><?php $total = 0; foreach ($client_share_history as $row)  {
                            $total += $row->shares_amount;
                        }
                        echo number_format($total,2) ?></td>
                    </tr><tr>
                        <td>Admin Share gift amount:</td><td>
						 <?php foreach ($share_given_by_admin as $row) {
                            echo $row->TotalShare;
                        } ?>    
						
						
						</td>
                    </tr><tr>
                        <td>Total Cash Pending:</td><td>0.00</td>
                    </tr><tr>
                        <td>Total Shares Pending:</td><td>0.00</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-6">
            <p>&nbsp;</p>
            <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                <thead>
                <tr>
                    <th colspan="5" style="text-align:center;" >Account Transaction History</th>
                </tr>
                </thead>
                <tbody>
                        <tr><td colspan="5"><b>Cash Credits</b></td></tr>
                        <?php foreach ($client_cash_history as $row) {
                            echo '<tr>  <td>&nbsp;&nbsp;</td><td>'.date('Y-m-d h:i:s A', $row->click_date).'</td><td>Paid To Click Earnings</td><td>'.$row->cash_amount.'</td>  </tr>';
                        } ?>
                        <tr><td colspan="5"><b>Cash Debits</b></td></tr>
                        <tr><td colspan="5"><b>Shares Credits</b></td></tr>
                        <?php foreach ($client_share_history as $row) {
							
							$Message="Share Earnings";
							// if($row->site_server_code == 0 && $row->site_server_advertise_id == Null && $row->site_server_advertise_id == 0  && $row->site_server_code == "")
							if($row->site_server_code == Null && $row->site_server_advertise_id == 0)
							{
								$Message="Share Earnings from Admin";
							}
							
                            echo '<tr>  <td>&nbsp;&nbsp;</td><td>'.date('Y-m-d h:i:s A', $row->click_date).'</td><td>'.$Message.'</td><td>'.$row->shares_amount.'</td>  </tr>';
                        } ?>    
                        <tr><td colspan="5"><b>Shares Debits</b></td></tr>
                        <tr><td colspan="5"><b>Pending Cash Transactions</b></td></tr>
                        <tr><td colspan="5"><b>Pending Shares Transactions</b></td></tr>
                                            
                </tbody>
            </table>
        </div>
    </div> 
</div>
<!-- /.container-fluid earning stat -->

<script type="text/javascript">
function edit(category_id){
	window.location = "<?php echo base_url(); ?>admin/edit_user/"+category_id;
}
function del(category_id){
	if (window.confirm('Are you sure that you want to delete?'))
	{
	    window.location = "<?php echo base_url(); ?>admin/delete_user/"+category_id;
	}
	else
	{
	    // They clicked no
	}
}
</script>

<?php
require_once("templates/default/admin_footer.php"); 
?>
