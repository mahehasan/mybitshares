<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
				<div class="panel-heading">
	            	<div class="pull-left"><a href="<?=SITE_URL?>admin/list_advertise" class="btn btn-outline btn-success btn-sm" type="button">Back</a></div>
					<p  class="text-center" >Advertise View<p>
	            </div>
	        	
	            <div class="panel-body">
                <?php 
                // echo '<pre>';
                // print_r( $obj); // 12345
                // echo '</pre>';
                ?>

                <?php $i=0;
                if (!empty($obj)) {
                ?>

				  <div class="row">

					




                 


                        <?php
                            foreach ($obj as $row) {
                                $main_advertise_id = $this->admin_model->getMainServerAdvetiseIdValue($row->site_server_code, $row->site_server_advertise_id);
                                $advertise_title = $this->admin_model->get_advertise_title_value($row->site_server_code, $row->site_server_advertise_id);
                                $advertise_description = $this->admin_model->get_advertise_description_value($row->site_server_code, $row->site_server_advertise_id);
								$advertise_id = $this->admin_model->getadvertise_id($row->site_server_code, $row->site_server_advertise_id);
								//echo "<pre>";print_r($advertise_id);die (); echo "</pre>";
								$click_share = $this->admin_model->get_server_total_click_share($advertise_id);
								$click_paid = $this->admin_model->get_server_total_click_paid($advertise_id);
								
								$click_share_used = $this->admin_model->get_client_total_click_share($advertise_id);
								$click_paid_used = $this->admin_model->get_client_total_click_paid($advertise_id);
								
								$file_name=$this->admin_model->getFileName($row->site_server_advertise_id, $row->site_server_code);
								
                                $site_server_ads_url = '';
                                if ($file_name != ""){
                                    $site_server_ads_url = $this->general->getSiteServerURL($row->site_server_code) . 'v1/ads/' . $file_name;
                                }
                                ?>
                            <div class="col-lg-12">
							<div class="col-lg-4">
							<h5>Image</h5>
							</div>
							<div class="col-lg-8">
                                <?php
                                if($site_server_ads_url){
                                    echo '<img title="'.$advertise_title. ' ' .$advertise_description.'" src="'.$site_server_ads_url.'">';
                                }else{
                                    echo 'No image';
                                }
                                ?>
                            </div>
							</div>
							<div class="col-lg-12">
							<div class="col-lg-4">
							<h5>Title</h5></div>
							<div class="col-lg-8">
                                <?php
                                 echo '<span>'. $advertise_title.'</span>';
                                ?>
                            </div>
							</div>
							<div class="col-lg-12">
							<div class="col-lg-4">
							<h5>Site Server Code</h5></div>
							<div class="col-lg-8">
                                <?php
                                 echo '<span>'.  $row->site_server_code .'</span>';
                                ?>
                            </div>
							</div>
							
							<div class="col-lg-12">
							<div class="col-lg-4">
							<h5>Description</h5></div>
							<div class="col-lg-8">
                                <?php
                                 echo '<span>'. $advertise_description.'</span>';
                                ?>
                            </div>
							</div>
							<div class="col-lg-12">
							<div class="col-lg-4">
							<h5>Uploader</h5></div>
							<div class="col-lg-8">
                                <?php
								 if($row->created_by>0)
								 {
                                 echo 
								 '<span>'. $this->admin_model->getUserName($row->created_by) .'</span>';
								 }
								 
                                ?>
                            </div>
							</div>
							<div class="col-lg-12">
							<div class="col-lg-4">
							<h5>Package</h5></div>
							<div class="col-lg-8">
                                <?php
                                 echo '<span>'.  $this->admin_model->getPackage($row->site_server_code, $row->site_server_advertise_id) .'</span>';
                                ?>
                            </div>
							</div>
							
							<div class="col-lg-12">
							<div class="col-lg-4">
							<h5>Share to Click Amount</h5></div>
							<div class="col-lg-8">
                                <?php
                                 echo '<span>'.  $click_share .'</span>';
                                ?>
                            </div>
							</div>
							<div class="col-lg-12">
							<div class="col-lg-4">
							<h5>Share to Click Used</h5></div>
							<div class="col-lg-8">
                                <?php
                                 echo '<span>'.  $click_share_used .'</span>';
                                ?>
                            </div>
							</div>
							<div class="col-lg-12">
							<div class="col-lg-4">
							<h5>Paid to Click Amount</h5></div>
							<div class="col-lg-8">
                                <?php
                                 echo '<span>'.  $click_paid  .'</span>';
                                ?>
                            </div>
							</div>
							<div class="col-lg-12">
							<div class="col-lg-4">
							<h5>Paid to Click Used</h5></div>
							<div class="col-lg-8">
                                <?php
                                 echo '<span>'.  $click_paid_used  .'</span>';
                                ?>
                            </div>
							</div>
							

                                    <!--    <th>Ad Image</th>
                           
							<th>View Detail</th>
                            <th>Operation</th>-->
								


                          <?php   }?>


                        <?php  }else{?>
                        <p>No data</p>
                        <?php }?>
										  </div>
              <!-- /.table-responsive -->
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>

<?php
require_once("templates/default/admin_footer.php"); 
?>
