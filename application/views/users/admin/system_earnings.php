<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
    <!-- /.row -->
    <div class="row">
	
        <div class="col-lg-4">
		<p>&nbsp;</p>
		<a class="btn btn-sm btn-primary" href="<?php echo base_url().'admin/system_detail_earnings';?>">Detail Transaction History</a>
            <p>&nbsp;</p>
            <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                <thead>
                <tr>
                    <th colspan="2" style="text-align:center;" >Main Transaction Ledger</th>
                </tr>
                <tr>
                    <th>Account Balances</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Total Cash Balance:</td><td><?php echo $cash_amount; ?></td>
                    </tr><tr>
                        <td>Total Shares Balance:</td><td><?php echo $share_amount; ?></td>
                    </tr><tr>
                        <td>Total Gifted Shares Amount:</td><td><?php echo $gifted_shares_Amount; ?></td>
                    </tr>
                </tbody>
            </table>
			<div>&nbsp;&nbsp;</div>
			<a class="btn btn-sm btn-success" href="<?php echo base_url().'transaction_update_scheduler/enable_scheduler';?>">Update Today's History</a>
			<div>&nbsp;&nbsp;</div>
			<a class="btn btn-sm btn-success" href="<?php echo base_url().'transaction_update_scheduler/enable_scheduler_for_user';?>">Update All User Transaction History</a>
        </div>
        <div class="col-lg-8">
            <p>&nbsp;</p>
            <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                <thead>
                <tr>
                    <th colspan="5" style="text-align:center;" >Account Transaction History</th>
                </tr>
				<tr>
                    <th>&nbsp;&nbsp;</th>
					<th>Date</th>
					<th>Transaction Type</th>
					<th>Amount</th>
                </tr>
                </thead>
                <tbody>

                        <?php foreach ($transaction_history as $row) {
							
                            echo '<tr>  <td>&nbsp;&nbsp;</td><td>'.date('Y-m-d h:i:s A', $row->transaction_history_date).'</td><td>'.$row->transaction_message.'</td><td>'.$row->transaction_amount.'</td>  </tr>';
							
							
							} ?>
                                            
                </tbody>
            </table>
        </div>
    </div> 
</div>
<!-- /.container-fluid earning stat -->

<script type="text/javascript">
function edit(category_id){
	window.location = "<?php echo base_url(); ?>admin/edit_user/"+category_id;
}
function del(category_id){
	if (window.confirm('Are you sure that you want to delete?'))
	{
	    window.location = "<?php echo base_url(); ?>admin/delete_user/"+category_id;
	}
	else
	{
	    // They clicked no
	}
}
</script>

<?php
require_once("templates/default/footer.php"); 
?>
