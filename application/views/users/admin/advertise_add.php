<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
$category_id = $parent_category = 0;
if (!empty($category['category_id'])) $category_id = $category['category_id'];
if (!empty($category['parent_category'])) $parent_category = $category['parent_category'];
?>
<style type="text/css">
.optionGroup
{
    font-weight:bold;
    font-style:italic;
}

.optionChild
{
    padding-left:15px;
}
 </style>
<div class="container-fluid">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header"></h1>
          <div class="panel panel-success">
            <div class="panel-heading">
                <?php if(isset($edit)){echo "Edit Advertisement";}else{echo "Add Advertisement";}?>
              </div>
              <div class="panel-body">
                 <div class="row">
                  <div class="col-lg-6">
				  <?php
		//echo '<pre>';print_r($advertise_detail);echo '</pre>';	die();
		?>
                      <?=$this->general->show_flash_message()?>
                          <form role="form" enctype="multipart/form-data" action="<?=SITE_URL?>admin/<?php if (isset($edit)){echo 'edit_advertise/'.$ad_id ;}else{echo 'submit_advertise';}; ?>" method="post">
                              <div class="form-group">
                                  <label>Title</label>
                                    <input name="advertise_title" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $advertise_detail[0]->advertise_title ;}?>">
                                </div><div class="form-group">
                                  <label>Description</label>
                                    <input name="advertise_description" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $advertise_detail[0]->advertise_description ;}?>">
                                </div><div class="form-group">
                                            <label>Menu 1</label>
                                            <select class="form-control" name="menu1_id">
                                            <option value="NULL">Select</option>
                                            <?php
                                            foreach ($category_list as $row) {
                                                if(@!array_key_exists($row->category_id, $ads_menu_list))continue;
                                                echo '<option value="'.$row->category_id.'"';
                                                if (isset($edit)){if ($advertise_detail[0]->menu1_id == $row->category_id){echo 'selected="selected"';}}
                                                echo '>'.$row->category_title.'</option>';
                                            }?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                  <label>Menu 1 Click :</label>
                                    <input name="menu1_click" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $advertise_detail[0]->menu1_click ;}?>" >
                                </div>
                                        <div class="form-group">
                                            <label>Menu 2</label>
                                            <select class="form-control" name="menu2_id">
                                            <option value="NULL">Select</option>
                                            <?php foreach ($category_list as $row) {
                                                if(@!array_key_exists($row->category_id, $ads_menu_list))continue;
                                              echo '<option value="'.$row->category_id.'"';
                                              if (isset($edit)){if ($advertise_detail[0]->menu2_id == $row->category_id){echo 'selected="selected"';}}
                                              echo '>'.$row->category_title.'</option>';
                                            }?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                  <label>Menu 2 click :</label>
                                    <input name="menu2_click" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $advertise_detail[0]->menu2_click ;}?>" >
                                </div>
                                        <div class="form-group">
                                            <label>Price</label>
                                            <select class="form-control" name="advertise_price_id">
                                            <option value="NULL">Select</option>
                                            <?php foreach ($price_list as $row) {
                                              echo '<option value="'.$row->price_id.'"';
                                              if (isset($edit)){if ($advertise_detail[0]->advertise_price_id == $row->price_id){echo 'selected="selected"';}}
                                              echo '>'.$row->price_title.'</option>';
                                            }?>
                                            </select>
                                        </div>

                                <div class="form-group">
                                  <label>Image</label>
                                    <input type="hidden" name="created_by" value="<?php echo $this->session->userData('userID');?>">
                                    <input type="file" name="file_uploads[]" size="20">
                                </div>
                              <div class="form-group">
                                  <label>Client URL</label>
                                    <input name="client_url" class="form-control" placeholder="Enter text" value="<?php if(isset($edit)){echo $obj[0]->client_url;}?>">
                                </div>
                                <?php if (isset($edit)){echo '<input type="hidden" name="site_server_advertise_id" value ="'.$advertise_detail[0]->site_server_advertise_id.'">' ;}else{?>
                                  <div class="form-group">
                                  <label>Site Server ID</label>                                    
                                    <select name="site_server_code" class="form-control">
          			<?php foreach ($site_servers as $row) {?>
    				<option value="<?=$row->site_server_code?>"><?=$row->site_server_code?>, <?=$row->site_server_url?></option>
    				<?php }?>
    		</select>
                                </div>
                                  <?php } ?>
                                  <?php echo validation_errors();?>
                                <button type="submit" class="btn btn-default">Save</button>
                                <a href="<?=SITE_URL?>admin/list_advertise" id="cancel" name="cancel" class="btn btn-default">Cancel</a>
                          </form>
                      </div>
                  </div>
              </div>
        </div>
      </div>
        <!-- /.col-lg-12 -->
  </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/admin_footer.php"); 
?>
