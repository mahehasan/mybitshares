<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
$category_id = $parent_category = 0;
if (!empty($category['category_id'])) $category_id = $category['category_id'];
if (!empty($category['parent_category'])) $parent_category = $category['parent_category'];
?>
<style type="text/css">
.optionGroup
{
    font-weight:bold;
    font-style:italic;
}

.optionChild
{
    padding-left:15px;
}
 </style>
<div class="container-fluid">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header"></h1>
          <div class="panel panel-success">
            <div class="panel-heading">
                Add Configuration
              </div>
            <div class="panel-body">
               <div class="row">
                <div class="col-lg-6">
                        <form role="form" action="<?=SITE_URL?>admin/add_bonus_list" method="post">
                            <div class="form-group">
                                <label>Amount</label>
                                  <input name="amount" class="form-control" placeholder="Enter text" value="<?php if (isset($bonus_detail)) echo $bonus_detail[0]->amount;?>">
                              </div>
                              <div class="form-group">
                                <label>Fund Amount</label>
                                  <input name="fund_amount" class="form-control" placeholder="Enter text" value="<?php if (isset($bonus_detail)) echo $bonus_detail[0]->fund_amount;?>">
                              </div>
                              <?php if (isset($bonus_detail)){echo '<input type="hidden" name="share_bonus_id" value ="'.$bonus_detail[0]->share_bonus_id.'">' ;}?>
                                <?php echo validation_errors();?>       
                              <button type="submit" class="btn btn-default">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
        <!-- /.col-lg-12 -->
  </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/admin_footer.php"); 
?>
