<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/admin_header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <p>System's total daily earnings : <?php echo number_format($daily_earnings_total[0]->total_sum, 2) ?> </p>
            <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Grant</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($daily_earnings as $row) { ?>
                    <tr>
                        <td><?php echo date('Y-m-d h:i:s A', $row->date);?></td>
                        <td><?php echo number_format($row->amount,2);?></td>
                        <td><a class="btn btn-sm btn-success" href="<?php echo base_url().'admin/grant_users/'.$row->amount.'/'.$row->income_id;?>">Grant</a></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
		<div class="col-lg-4">
                        	<form role="form" action="<?=SITE_URL?>admin/add_share_amount" method="post">

                            	<div class="form-group">
                                	<label>Share amount for all user</label>
                                     <input name="share_amount" class="form-control" placeholder="Enter Share amount" value="">
									 
                               	</div>

								<button type="submit" class="btn btn-default">Distribute Share</button>  
                                
                         	</form>
                     	</div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<script type="text/javascript">
function edit(category_id){
    window.location = "<?php echo base_url(); ?>admin/edit_user/"+category_id;
}
function del(category_id){
    if (window.confirm('Are you sure that you want to delete?'))
    {
        window.location = "<?php echo base_url(); ?>admin/delete_user/"+category_id;
    }
    else
    {
        // They clicked no
    }
}
function toggle(argument) {
    window.location = "<?php echo base_url(); ?>admin/enable_disable_user/"+argument;
}
</script>

<?php
require_once("templates/default/admin_footer.php"); 
?>
