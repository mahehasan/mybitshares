<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php
$this->security->destroyUserSession();

$this->general->redirect("login");
exit;
?>