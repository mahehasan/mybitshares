<?php
require_once("templates/default/header.php");
?>

<h3 class="page-title">Become an affiliate </h3>

<!-- Results table -->
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue-madison">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa "></i>&nbsp;
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse"></a>
        </div>
      </div>
      <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="<?=SITE_URL?>users/join_user" method="post" name="frmRegister" id="frmRegister" class="form-horizontal">
          <div class="form-body">
            <h5 class="form-section">
              
            </h5>

            <?php if ( isset($err) ){ if ( $err==1 ) echo $this->html->showErrorMsg($MSG); else if ( $err==0 ) echo $this->html->showSuccessMsg($MSG); } ?>

            <?php //$this->html->showErrorMsg( "You have some form errors. Please check below.", "", "display-hide")?>
			<div class="form-group">
              <label class="col-md-3 control-label"></label>
              <div class="col-md-5">Please enter your email address to sign-up</div>
              <div class="col-md-4"> </div>
            </div> 
          </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Email <span aria-required="true" class="required">*</span></label>
              <div class="col-md-5">
                <input type="text" class="form-control" name="sign_up_email" id="emailAdd" maxlength="255">
              </div>
              <div class="col-md-4"> </div>
            </div> 
          </div>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" name="ssubmit" class="btn blue" ><i class="fa fa-check"></i> Continue</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php
//---- define page javascript
//$mtPageScripts = array("adminside/add_user.js");

require_once("templates/default/footer.php");
?>