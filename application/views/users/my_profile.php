<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php
require_once("templates/metronic/admin_header.php");
?>

<script src="<?=SITE_URL?>javascript/md5.js"></script>
<script src="<?=METRONIC_PLUGINS_URL?>jquery-1.11.2.min.js" type="text/javascript"></script>
<script language="javascript">

function back(){
	window.location='<?=SITE_URL?>dashboard/?mid=1';
}

var error=0;
var passStrength = '';

 $(document).ready(function() {
     $('#password').keyup(function (e) {
         var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
         var enoughRegex = new RegExp("(?=.{6,}).*", "g");
         $('#msgNewPass').show();
         if (false == enoughRegex.test($(this).val())) {
             $('#msgNewPass > .alert').html('Need 8 Characters minimum!');
             $('#msgNewPass> .alert').removeClass('alert-success');
             $('#msgNewPass> .alert').addClass('alert-danger');
             error = 1;
             passStrength = '';
         } else if (strongRegex.test($(this).val())) {
             $('#msgNewPass> .alert').removeClass('alert-danger');
             $('#msgNewPass> .alert').addClass('alert-success');
             $('#msgNewPass > .alert').html('Strong!');
             passStrength = 'strong';
             error = 0;
         } else {
             $('#msgNewPass> .alert').removeClass('alert-success');
             $('#msgNewPass> .alert').addClass('alert-danger');
             $('#msgNewPass > .alert').html('Weak!');
             error = 1
             passStrength = '';
           }
         return true;


     });
 });
function validateResetPass(){
<?php
	if($id==$this->session->userData('userID')){
?>
	var currPass 	    	= document.getElementById("currPass").value;
<?php
	}
?>
	var password   		    = document.getElementById("password").value;
	var password_confirm    = document.getElementById("password_confirm").value;


<?php
	if($id == $this->session->userData('userID')){
?>
	if(currPass==""){
		document.getElementById("msgCurr").style.display="block";
		error=1;
	}
	else
	{
		var passhash = CryptoJS.MD5(currPass);
		var dbPass = '<?php echo $this->session->userData('password'); ?>';

		if(passhash!=dbPass){
			document.getElementById("msgCurr").style.display="block";
			error = 1;
		}else{
			document.getElementById("msgCurr").style.display="none";
		}

	}
<?php
	}
?>

		if(password==""){
			document.getElementById("msgNewPass").style.display="block";
			error=1;
		}
		else
		{
			//document.getElementById("msgNewPass").style.display="none";
		}

        if(passStrength == 'strong'){
            document.getElementById("msgNewPass").style.display="none";
        }


	if(password_confirm==""){
		document.getElementById("msgNewConfirmPass").style.display="block";
		error=1;
	}
	else{
		document.getElementById("msgNewConfirmPass").style.display="none";
	}

	if(error==1){
		return false;
	}

	if(currPass == password){
		$('#msgNewPass > .alert').html("New password and Old password should be different.");
        $('#msgNewPass').show();
		return false;
	}else
	if(password!=password_confirm){
        $('#msgNewPass > .alert').html("New password and Confirm new password should match.");
        $('#msgNewPass').show();
		return false;
	}
	else{
        $('#msgNewPass > .alert').html("Please enter a new password.");
        $('#msgNewPass').hide();
	}

	document.frmPassReset.submit();
}



</script>

<h3 class="page-title">My Profile</h3>

<!--BEGIN TABS-->
<div class="tabbable tabbable-custom tabbable-full-width">
  <ul class="nav nav-tabs">
    <li <? if(!isset($_REQUEST[ 'tabname']) ||(isset($_REQUEST[ 'tabname']) && $_REQUEST[ 'tabname']=="overview")){echo 'class="active"';}?>>
      <a href="#tab_1_1" data-toggle="tab">Overview</a>
    </li>
    <li <? if( isset($_REQUEST[ 'tabname']) && $_REQUEST[ 'tabname']=="reset_pass" ){echo 'class="active"';}?>>
      <a href="#tab_1_2" data-toggle="tab">Password Reset</a>
    </li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane <? if(!isset($_REQUEST[ 'tabname']) ||(isset($_REQUEST['tabname']) && $_REQUEST[ 'tabname']=="overview")){echo 'active';}?>" id="tab_1_1">
      <div class="row">
        <div class="col-md-12">
          <div class="portlet box blue-madison">
            <div class="portlet-title">
              <div class="caption"> <i class="fa fa-support"></i>Update Information Form </div>
              <div class="tools"> <a href="javascript:;" class="collapse"></a> </div>
            </div>
            <div class="portlet-body form">
              <!-- BEGIN FORM-->
              <form action="" method="post" name="frmEditRegister" id="frmEditRegister" class="form-horizontal">
                <input type="hidden" name="tabname" value="overview" />
                <div class="form-body">
                  <h5 class="form-section">
                    Please complete the following fields and submit form by clicking the "Submit" button below. <b>Note: Required fields are marked with an asterisk.</b>
                  </h5>

                  <?php if (isset($err)) { if ($err==1 ) echo $this->html->showErrorMsg($MSG); else if ($err==0 ) echo $this->html->showSuccessMsg($MSG); } ?>

                  <?=$this->html->showErrorMsg( "You have some form errors. Please check below.", "", "display-hide")?>

                  <div class="form-group">
                    <label class="col-md-3 control-label">First Name <span aria-required="true" class="required">*</span></label>
                    <div class="col-md-5">
                      <input type="text" class="form-control" name="DB[firstName]" id="firstName" value="<?= $this->usersecurity->stripData($DB['firstName']) ?>" maxlength="255">
                    </div>
                    <div class="col-md-4"> </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Last Name <span aria-required="true" class="required">*</span></label>
                    <div class="col-md-5">
                      <input type="text" class="form-control" name="DB[LastName]" id="LastName" value="<?= $this->usersecurity->stripData($DB['LastName']) ?>" maxlength="255">
                    </div>
                    <div class="col-md-4"> </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Phone</label>
                    <div class="col-md-5">
                      <input name="DB[phone]" maxlength="255" class="form-control" id="phone" type="text" value="<?= $this->usersecurity->stripData($DB['phone']) ?>">
                    </div>
                    <div class="col-md-4"> </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Office address 1</label>
                    <div class="col-md-5">
                      <input name="DB[address1]" maxlength="255" class="form-control" id="address1" type="text" value="<?= $this->usersecurity->stripData($DB['address1']) ?>">
                    </div>
                    <div class="col-md-4"> </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Office address 2</label>
                    <div class="col-md-5">
                      <input name="DB[address2]" maxlength="255" class="form-control" id="address2" type="text" value="<?= $this->usersecurity->stripData($DB['address2']) ?>">
                    </div>
                    <div class="col-md-4"> </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Office address 3</label>
                    <div class="col-md-5">
                      <input name="DB[address3]" maxlength="255" class="form-control" id="address3" type="text" value="<?= $this->usersecurity->stripData($DB['address3']) ?>">
                    </div>
                    <div class="col-md-4"> </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Town</label>
                    <div class="col-md-5">
                      <input name="DB[town]" maxlength="255" class="form-control" id="town" type="text" value="<?= $this->usersecurity->stripData($DB['town']) ?>">
                    </div>
                    <div class="col-md-4"> </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Country</label>
                    <div class="col-md-5">
                      <input name="DB[country]" maxlength="255" class="form-control" id="country" type="text" value="<?= $this->usersecurity->stripData($DB['country']) ?>">
                    </div>
                    <div class="col-md-4"> </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Post Code</label>
                    <div class="col-md-5">
                      <input name="DB[postCode]" maxlength="255" class="form-control" id="postCode" type="text" value="<?= $this->usersecurity->stripData($DB['postCode']) ?>">
                    </div>
                    <div class="col-md-4"> </div>
                  </div>
                </div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                      <button type="submit" name="bsubmit" class="btn blue"><i class="fa fa-check"></i> Submit </button>
                    </div>
                  </div>
                </div>
                <input name="ssubmit" type="hidden" value="editInfo" />
                <input name="id" type="hidden" id="id" value="<?= $id ?>">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane <? if(isset($_REQUEST['tabname']) && $_REQUEST['tabname'] == "reset_pass"){echo 'active';}?>" id="tab_1_2">
      <div class="row">
        <div class="col-md-12">
          <div class="portlet box blue-madison">
            <div class="portlet-title">
              <div class="caption"> <i class="fa fa-support"></i>Reset Password Form </div>
              <div class="tools"> <a href="javascript:;" class="collapse"></a> </div>
            </div>
            <div class="portlet-body form">
              <form action="" method="post" name="frmPassReset" id="frmPassReset" class="form-horizontal">
                <input type="hidden" name="tabname" value="reset_pass" />
                <div class="form-body">
                  <h5 class="form-section">
                    Please complete the following fields and reset password by clicking the "Submit" button below. <b>Note: Required fields are marked with an asterisk.</b>
                  </h5>
                  
				  <?php 
				  if ( $this->input->get('ch') ) { echo $this->html->showErrorNote('Please change your password for security reasons before proceeding.'); } 
				  if (isset($MSG) && $MSG != '') { echo $this->html->showSuccessMsg($MSG); } 
				  ?>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Email <span aria-required="true" class="required">*</span></label>
                    <div class="col-md-5">
                      <input type="text" class="form-control" name="DB[email]" id="email" readonly="" value="<?=$this->usersecurity->stripData($resultUser['email'])?>" maxlength="255">
                    </div>
                    <div class="col-md-4" style="display:none;"> </div>
                  </div>

                  <?php if($id==$this->session->userData('userID')){ ?>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Current Password <span aria-required="true" class="required">*</span></label>
                    <div class="col-md-5">
                      <input type="password" class="form-control" name="currPass" id="currPass" value="" maxlength="255">
                    </div>
                    <div class="col-md-4" id="msgCurr" style=" <?php if($invalidPass==1){echo 'display:block;';}else{echo 'display:none;';}?> ">
                      <div class="alert alert-danger c-alert-narrow removeMargin"> Please enter a valid current password. </div>
                    </div>
                  </div>
                  <?php } ?>

                  <div class="form-group">
                    <label class="col-md-3 control-label">New Password <span aria-required="true" class="required">*</span></label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <input type="password" class="form-control" name="DB[password]" id="password" value="" maxlength="255">
                            <span class="help-block">
								Passwords must be at least 8 characters long and contain a character from each of the following categories:
                                	<ol>
                                    	<li>Alpha – Uppercase (A – Z)</li>
                                        <li>Alpha – Lowercase (a – z)</li>
                                        <li>Non-Alpha – Numeric (0 – 9)</li>
                                        <li>Non-Alpha – Special characters (! $ % ? …)</li>
                                  </ol> 
                            </span>
                            
                        </div>
                    </div>
                    <div class="col-md-4" id="msgNewPass" style=" <?php if($invalidPass==1){echo 'display:block;';}else{echo 'display:none;';}?> ">
                      <div class="alert alert-danger c-alert-narrow removeMargin"> Please enter a new password. </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Confirm New Password <span aria-required="true" class="required">*</span></label>
                    <div class="col-md-5">
                      <input type="password" class="form-control" name="DB[password_confirm]" id="password_confirm" value="" maxlength="255">
                    </div>
                    <div class="col-md-4" id="msgNewConfirmPass" style=" <?php if($invalidPass==1){echo 'display:block;';}else{echo 'display:none;';}?> ">
                      <div class="alert alert-danger c-alert-narrow removeMargin"> Please enter a new confirm password. </div>
                    </div>
                  </div>
                </div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                      <button type="button" onclick="return validateResetPass()" name="saveSubmit" class="btn blue"><i class="fa fa-check"></i> Submit </button>
                      <input name="generateTempPass" value="Generate Temporary Password" class="btn blue" style="cursor: pointer" type="submit"> 
                      <span style="font-size:11px">(Generate Temporary Password and Send to this user's Email)</span>
                    </div>
                  </div>
                </div>
                <input name="ssubmit" type="hidden" value="PostReplyResetPass" />
                <input name="mid" type="hidden" id="mid" value="3" />
                <input name="id" type="hidden" id="id" value="<?php echo @$_GET['id'];?>" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
//---- define page javascript
$mtPageScripts = array('my_profile.js');

require_once("templates/metronic/admin_footer.php");
?>