<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" ></script>
<script type="text/javascript">
function buy($id){
    //window.location = "<?=SITE_URL?>mybitshares/process_buy/"+$id;
	if (confirm('Are you sure to cancel?')) {
		window.location = "<?=SITE_URL?>mybitshares/process_buy/"+$id;
	} else {
		// Do nothing!
	}
}
</script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="page-header"></h1>
            <div class="panel panel-success">
                <div class="panel-heading">
                   Shares List
                </div>
                <div class="panel-body">
				
                <table class="table-striped table-hover table-bordered table">
                    <tbody>
                        <tr>
                            <td><b>Seller's Name</b></td><td><?php echo $share_info[0]->firstName.' '.$share_info[0]->lastName; ?></td>
                        </tr><tr>
                            <td><b>Number Of Shares</b></td><td><?php echo $share_info[0]->number_of_shares; ?></td>
                        </tr><tr>
                            <td><b>Price Of Shares</b></td><td><?php echo $share_info[0]->amount; ?></td>
                        </tr><tr>
                            <td><b>Total Amount</b>(After adding system fee)</td><td><?php echo $final_price = $this->mybitshares_model->get_share_buy_total_amount($share_info[0]->amount); 
                            $this->session->set_userdata('final_price', $final_price);
                            $this->session->set_userdata('share_sell_id', $share_info[0]->share_sell_id);
                            $this->session->set_userdata('total_price', $share_info[0]->amount);
							$this->session->set_userdata('users_cash_amount', $main_cash_amount);
                            $this->session->set_userdata('seller_id', $share_info[0]->userID);
                            $this->session->set_userdata('number_of_shares', $share_info[0]->number_of_shares);?></td>
                        </tr><tr>
						<?php //echo'<pre>';print_r($share_info[0]->amount);echo'</pre>';die();?>
                            <td colspan="2">&nbsp;</td>
                        </tr>
						<tr>
                            <td><b>My Cash Amount</b></td><td><?php echo $main_cash_amount; ?></td>
                        </tr><tr>
                            <td colspan="2"><button type="button" class="btn btn-primary" onclick="buy('<?php echo $share_info[0]->share_sell_id;?>')">Proceed</button></td>
                        </tr>
                    </tbody>
                </table>

               
                </div>
            </div>
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Ad Here
                </div>
                <div class="panel-body"></div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
