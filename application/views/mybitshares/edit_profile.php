<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
  <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
    	<div class="col-lg-6">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	Edit Profile
	            </div>
	            <div class="panel-body">
	            	 <div class="row">
	            	 	<div class="col-sm-12">
                        	<form role="form" enctype="multipart/form-data" action="<?=SITE_URL?>mybitshares/edit_profile" method="post">
                            	<div class="form-group">
                                  <label>First Name</label>
                                    <input name="firstName" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $user_detail[0]->firstName ;}?>">
                              </div><div class="form-group">
                                  <label>Last Name</label>
                                    <input name="lastName" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $user_detail[0]->lastName ;}?>">
                              </div><div class="form-group">
                                  <label>Phone</label>
                                    <input name="phone" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $user_detail[0]->phone ;}?>">
                              </div><div class="form-group">
                                  <label>Address</label>
                                    <input name="address1" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $user_detail[0]->address1 ;}?>">
                              </div><div class="form-group">
                                  <label>Town</label>
                                    <input name="town" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $user_detail[0]->town ;}?>"></div><div class="form-group">
                                            <label>Country</label>
                                            <select class="form-control" name="country">
                                            <option value="NULL">Select</option>
                                            <?php foreach ($country_list as $row) {
                                              echo '<option value="'.$row->id.'" ';
                                              if (isset($edit)){if($row->id == $user_detail[0]->country){echo 'selected = "selected"';}}
                                              echo '>'.$row->country_name.'</option>';
                                            }?>
                                            </select>
                                        </div><div class="form-group">
                                	<label>Post Code</label>
                                    <input name="postCode" class="form-control" placeholder="Enter text" value="<?php if (isset($edit)){echo $user_detail[0]->postCode ;}?>">
                              </div>
                               	<?php if (isset($edit)){echo '<input type="hidden" name="userID" value ="'.$user_detail[0]->userID.'">' ;}?>
                                  <?php echo validation_errors();?>
                                <button type="submit" class="btn btn-default">Save</button>
                         	</form>
                     	</div>
               		</div>
	            </div>
	     	</div>
    	</div>

      <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>