<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-4">
            <p>&nbsp;</p>
            <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                <thead>
                <tr>
                    <th colspan="2" style="text-align:center;" >Transaction Ledger</th>
                </tr>
                <tr>
                    <th>Account Balances</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Total Cash Balance:</td><td><?php $total = 0; foreach ($client_cash_history as $row)  {
                            $total += $row->cash_amount;
                        }
                        echo number_format($total,2) ?></td>
                    </tr><tr>
                        <td>Total Shares Balance:</td><td><?php $total = 0; foreach ($client_share_history as $row)  {
                            $total += $row->shares_amount;
                        }
                        echo number_format($total,2) ?></td>
                    </tr><tr>
                        <td>Total Cash Pending:</td><td>0.00</td>
                    </tr><tr>
                        <td>Total Shares Pending:</td><td>0.00</td>
                    </tr><tr>
                        <td>Total Referral Income:</td><td><?php echo $referral_income; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-6">
            <p>&nbsp;</p>
            <table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;">
                <thead>
                <tr>
                    <th colspan="5" style="text-align:center;" >Account Transaction History</th>
                </tr>
                </thead>
                <tbody>
                        <tr><td colspan="5"><b>Cash Credits</b></td></tr>
                        <?php foreach ($client_cash_history as $row) {
                            echo '<tr>  <td>&nbsp;&nbsp;</td><td>'.date('Y-m-d h:i:s A', $row->click_date).'</td><td>Paid To Click Earnings</td><td>'.$row->cash_amount.'</td>  </tr>';
                        } ?><tr>
                        <td></td><td><?php echo date('Y-m-d h:i:s A');?></td><td>Total Referral Balance:</td><td><?php echo $this->referral_model->calculate_referral($id);//id=current user id ?></td>
                            </tr>
                        <tr><td colspan="5"><b>Cash Debits</b></td></tr>
                        <tr><td colspan="5"><b>Shares Credits</b></td></tr>
                        <?php foreach ($client_share_history as $row) {
                            echo '<tr>  <td>&nbsp;&nbsp;</td><td>'.date('Y-m-d h:i:s A', $row->click_date).'</td><td>Share Earnings</td><td>'.$row->shares_amount.'</td>  </tr>';
                        } ?>    
                        <tr><td colspan="5"><b>Shares Debits</b></td></tr>
                        <tr><td colspan="5"><b>Pending Cash Transactions</b></td></tr>
                        <tr><td colspan="5"><b>Pending Shares Transactions</b></td></tr>
                                            
                </tbody>
            </table>
        </div>
    </div> 
</div>
<!-- /.container-fluid earning stat -->

<script type="text/javascript">
function edit(category_id){
	window.location = "<?php echo base_url(); ?>admin/edit_user/"+category_id;
}
function del(category_id){
	if (window.confirm('Are you sure that you want to delete?'))
	{
	    window.location = "<?php echo base_url(); ?>admin/delete_user/"+category_id;
	}
	else
	{
	    // They clicked no
	}
}
</script>

<?php
require_once("templates/default/footer.php"); 
?>
