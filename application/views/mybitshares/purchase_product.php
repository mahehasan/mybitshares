<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>

<script type="text/javascript">

  function add_to_cart(product_id, unit_price){
  var  product_quantity = 'Quantity_' + product_id;
  var quantity = document.getElementById(product_quantity).value; 
  $.ajax({
    url: '<?php echo base_url();?>mybitshares/add_to_cart/' + product_id + '/' + quantity + '/' + unit_price,
    context: document.body
  }).done(function (data) {
    document.getElementById('cartSummary').innerHTML = data; 
  })
  var modal_id = '#' + 'basicModal' + product_id;
  $(modal_id).modal('hide');
}

</script>


<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="page-header"></h1>
            <div class="panel panel-success">
                <div class="panel-heading">
                     <?php //echo $category_detail[0]->category_title;?>
                     Available Products
                </div>
                <div class="panel-body">
                           
<?php $rowcnt = 1 ; foreach ($product_list as $row) { ?>
<div class="col-sm-6 col-md-4">
  <div class="thumbnail" >
    <h4 class="text-center"><span class="label label-info"><?php echo  $row->product_name;?></span></h4>
    <?php echo '<img height="100" width="100" src="'.base_url().$row->product_thumbimage.'" class="img-responsive">'; ?>
    <div class="caption">
      <div class="row">
      <div class="col-md-12 col-xs-6 price" style="text-align: center;">
          <h4>
          <label>$<?php echo  $row->product_price;?></label></h4>
        </div>
      </div>
      <div class="col-md-12 col-xs-6 price" style="text-align: center;">
      <?php echo  $row->short_description;?><br>
      Discount : <?php echo  $row->product_discount;?>
      </div>
      <div class="row">
        <div class="col-md-12" style="text-align: center;">
          <a href="#" data-toggle="modal" data-target="#basicModal<?php echo $row->product_id;?>" class="btn btn-success btn-product"><span class="glyphicon glyphicon-shopping-cart"></span> Buy</a></div>
      </div>
      <div class="modal fade" id="basicModal<?php echo $row->product_id;?>" tabindex="-1" role="dialog" aria-labelledby="basicModal<?php echo $row->product_id;?>" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&amp;</button>
                      <h4 class="modal-title" id="myModalLabel">Purchase <?php echo  $row->product_name;?></h4>
                      </div>
                      <div class="modal-body">
                          Quantity : 
                          <input type="text" id="Quantity_<?php echo $row->product_id;?>" value="1" ></input>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                          <button type="button" onclick="add_to_cart(<?php echo $row->product_id;?>, <?php echo  $row->product_price;?>);" class="btn btn-primary">Add To Cart</button>
                  </div>
              </div>
            </div>
          </div>
    </div>
  </div>
</div>
<?php $rowcnt++; } ?>


                </div>
            </div>
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Cart Contents
                </div>
                <div class="panel-body"><div id="cartSummary">
                    <?php if ($this->mybitshares_model->cart_has_items()){
                    $cart_contents = $this->mybitshares_model->get_cart_summary();
                    echo "Items: ".$cart_contents[0]->item_count." Amount: ".$cart_contents[0]->amount.' in Cart ';
                    echo '<a href="'.base_url().'mybitshares/view_cart">Checkout</a>';
                    
                    		
                     }else{
                      echo "Items: 0 Amount: 0 in Cart ";
                    }
                    
                    if($this->mybitshares_model->has_saved_cart($this->session->userdata('userID')))
                    {
                    	//$this->session->set_userdata('show_saved_cart', 1);
                    	echo '<a href="'.base_url().'mybitshares/view_cart/1">Saved Cart</a>';
                    }
                    ?>
                </div></div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
