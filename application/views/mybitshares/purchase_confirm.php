<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="page-header"></h1>
            <div class="panel panel-success">
                <div class="panel-heading">
                    Cart Contents
                </div>
                <div class="panel-body">
                <?php if ($this->session->userdata('guest')) { ?>
                <b>If you want, you can join <a href="<?=SITE_URL?>users/join_user">here</a>, or you can also proceed without joining.</b>
                <?php  } ?>
                <form role="form" enctype="multipart/form-data" action="<?=SITE_URL?>mybitshares/product_buy" method="post">
                        
                        <div class="table-responsive">   
                <table class="table table-bordered table-striped table-hover">
                	<thead>
                		<tr>
                			<th>Product Name</th>
                			<th>Quantity</th>
                			<th>Unit Price</th>
                			<th>Total</th>
                		</tr>
                	</thead>
                	<tbody>
                		<?php if (isset($cart_contents)){ $tqty = 0; $tprice=0; foreach ($cart_contents as $row) { ?>
                			<tr>
                				<td><?php echo $row->product_name;?></td>
                				<td><?php echo $row->quantity;$tqty+=$row->quantity;?></td>
                				<td><?php echo $row->unit_price;?></td>
                				<td><?php echo $row->unit_price*$row->quantity;$tprice+=($row->unit_price*$row->quantity);?></td>
                			</tr>
                		<?php } } ?>
                 	</tbody>
                 	<tfoot>
                 		<tr>
                 			<td><b>Total</b></td>
                 			<td><b><?php echo $tqty;?></b></td>
                 			<td><b></b></td>
                 			<td><b><?php echo $tprice;?></b></td>
                 		</tr>
                 	</tfoot>
                </table>

<input type="hidden" name="total_amount" value="<?=$tprice?>">
                <table  class="table table-bordered table-striped table-hover">
                <tbody>
                 <tr role="row">
                    	<td><b>Shipping Address</td>
                    </tr>
                    <tr role="row">
                    	<td><b>Name</b></td><td><?php echo $shipping_info['name'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Address</b></td><td><?php echo $shipping_info['address'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Email</b></td><td><?php echo $shipping_info['email'] ;?></td>
                    </tr>
                     <tr role="row">
                    	<td><b>phone</b></td><td><?php echo $shipping_info['phone'] ;?></td>
                    </tr>
                     <tr role="row">
                    	<td><b>city</b></td><td><?php echo $shipping_info['city'] ;?></td>
                    </tr>
                     <tr role="row">
                    	<td><b>country</b></td><td><?php echo $shipping_info['country'] ;?></td>
                    </tr>
                     <tr role="row">
                    	<td><b>delivery_date</b></td><td><?php echo $shipping_info['delivery_date'] ;?></td>
                    </tr>
                     <tr role="row">
                    	<td><b>state</b></td><td><?php echo $shipping_info['state'] ;?></td>
                    </tr>
                     <tr role="row">
                    	<td><b>day_phone_a</b></td><td><?php echo $shipping_info['day_phone_a'] ;?></td>
                    </tr>
                    </tbody>
                    </table>

            <table  class="table table-bordered table-striped table-hover">
                <tbody>
                <tr role="row">
                    	<td><b>Payment Details</td>
                    </tr>
                    <tr role="row">
                    	<td><b>Payment Method</b></td><td><?php echo $payment_info['payment_getway'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Card Type</b></td><td><?php echo $payment_info['card_type'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Cardholder Name</b></td><td><?php echo $payment_info['cardholder_name'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Card Number</b></td><td><?php echo $payment_info['card_number'] ;?></td>
                    </tr>
                     <tr role="row">
                    	<td><b>Expire Date</b></td><td><?php echo $payment_info['expire_date'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Subtotal Amount</b></td><td><?php echo $payment_info['subtotal_amount'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Tax Amount</b></td><td><?php echo $payment_info['tax_amount'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Coupon Value</b></td><td><?php echo $payment_info['coupon_value'] ;?></td>
                    </tr>
                     <tr role="row">
                    	<td><b>Deposit Value</b></td><td><?php echo $payment_info['deposit_value'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Tip Value</b></td><td><?php echo $payment_info['tip_value'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Delivery Value</b></td><td><?php echo $payment_info['delivery_value'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Paymen Getway Amount</b></td><td><?php echo $payment_info['payment_getway_amount'] ;?></td>
                    </tr>
                     <tr role="row">
                    	<td><b>Total Amount</b></td><td><?php echo $payment_info['total_amount'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Order Status</b></td><td><?php echo $payment_info['order_status'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Shipping Status</b></td><td><?php echo $payment_info['shipping_status'] ;?></td>
                    </tr>
                    <tr role="row">
                    	<td><b>Services Name</b></td><td><?php echo $payment_info['services_name'] ;?></td>
                    </tr>
                     <tr role="row">
                    	<td><b>Security Code</b></td><td><?php echo $payment_info['security_code'] ;?></td>
                    </tr>
                        
                    </tbody>
                </table>
                </form>
              
                
                	</div>
                </div>
            </div>
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Advertisement
                </div>
                <div class="panel-body"><div id="cartSummary">
                    bla bla bla
                </div></div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
