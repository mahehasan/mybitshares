<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="page-header"></h1>
            <div class="panel panel-success">
                <div class="panel-heading">
                    Cart Contents
                </div>
                <div class="panel-body">
                <?php if ($this->session->userdata('guest')) { ?>
                <b>If you want, you can join <a href="<?=SITE_URL?>users/join_user">here</a>, or you can also proceed without joining.</b>
                <?php  } ?>
                <form role="form" enctype="multipart/form-data" action="<?=SITE_URL?>mybitshares/product_buy" method="post">
                        
                        <div class="table-responsive">   
                <table class="table table-bordered table-striped table-hover">
                	<thead>
                		<tr>
                			<th>Product Name</th>
                			<th>Quantity</th>
                			<th>Unit Price</th>
                			<th>Total</th>
                		</tr>
                	</thead>
                	<tbody>
                		<?php if (isset($cart_contents)){ $tqty = 0; $tprice=0; foreach ($cart_contents as $row) { ?>
                			<tr>
                				<td><?php echo $row->product_name;?></td>
                				<td><?php echo $row->quantity;$tqty+=$row->quantity;?></td>
                				<td><?php echo $row->unit_price;?></td>
                				<td><?php echo $row->unit_price*$row->quantity;$tprice+=($row->unit_price*$row->quantity);?></td>
                			</tr>
                		<?php } } ?>
                 	</tbody>
                 	<tfoot>
                 		<tr>
                 			<td><b>Total</b></td>
                 			<td><b><?php echo $tqty;?></b></td>
                 			<td><b></b></td>
                 			<td><b><?php echo $tprice;?></b></td>
                 		</tr>
                 	</tfoot>
                </table>

<input type="hidden" name="total_amount" value="<?=$tprice?>">
                <table  class="table table-bordered table-striped table-hover">
                <tbody>
                        <tr>
                            <td colspan="2"><b>Shipping Information</b></td>
                        </tr><tr>
                            <td><b>Name</b></td><td><input type="text" name="name" value="<?php if ($show==true){echo $address[0]->name ;}?>"></td>
                        </tr><tr>
                            <td><b>Address</b></td><td><input type="text" name="address" value="<?php if ($show==true){echo $address[0]->address ;}?>"></td>
                        </tr><tr>
                            <td><b>Phone</b></td><td><input type="text" name="phone" value="<?php if ($show==true){echo $address[0]->phone ;}?>"></td>
                        </tr><tr>
                            <td><b>City</b></td><td><input type="text" name="city" value="<?php if ($show==true){echo $address[0]->city ;}?>"></td>
                        </tr><tr>
                            <td><b>Country</b></td><td><select class="form-control" name="country">
                                            <option value="NULL">Select</option>
                                            <?php foreach ($country_list as $row) {
                                              echo '<option value="'.$row->id.'" ';
                                              if ($show==true){if($row->id == $address[0]->country){echo 'selected = "selected"';}}
                                              echo '>'.$row->country_name.'</option>';
                                            }?>
                                            </select></td>
                        </tr><tr>
                            <td><b>Zip Code</b></td><td><input type="text" name="zip_code" value="<?php if ($show==true){echo $address[0]->zip_code ;}?>"></td>
                        </tr>
                    </tbody>
                </table>

            <table  class="table table-bordered table-striped table-hover">
                <tbody>
               <tr>
                            <td><b>Card Type</b></td><td> <select name="card_type">
                                                                  <option value="1">VISA</option>
                                                                  <option value="2">Master Card</option>
                                                                  <option value="3">American Express</option>
                                                                  <option value="4">Discover</option>
                                                                </select> </td>
                        </tr><tr>
                            <td><b>Cardholder's Name</b></td><td><input type="text" name="cardholder_name" value=""></td>
                        </tr><tr>
                            <td><b>Card Number</b></td><td><input type="text" name="card_number" value=""></td>
                        </tr><tr>
                            <td><b>Security Code</b></td><td><input type="text" name="security_code" value=""></td>
                        </tr><tr>
                            <td><b>Expire Date</b></td><td><input type="text" name="expire_date" value=""></td>
                        </tr><tr>
                            <td colspan="2"><button type="submit" class="btn btn-default">Proceed</button></td>
                             
                        </tr>
                        <tr>
                        <td><b><a href="<?=SITE_URL?>mybitshares/save_cart">Save Cart</a></b></td>
                        <td><b><a href="<?=SITE_URL?>mybitshares/delete_save_cart">Remove Cart</a></b></td>
                        </tr>
                        
                    </tbody>
                </table>
                </form>
                <div class="panel panel-default">
                <div class="panel-heading">
                    Payment Information
                </div>
                <div class="panel-body">
                <a target="_blank" href="http://www.sensipay.com/users/personalinfo/xpWUlpZ5">
                        <img height="35" src="http://jillsclickcorner.com//images/small-pay1.jpg" width="62" border="0"></a>
                <a target="_blank" href="https://www.paypal.com/en/mrb/pal=UVYS2UR39QE4Q">
                <img height="31" src="http://www.donkeymails.com/pages/x-click-but01.gif" width="62" border="0"></a>
                </b>&nbsp;<a target="_blank" href="http://www.payza.com/?GgXoLizaKH5TFsm0NQ%2fbcw%3d%3d">
                <img height="28" src="https://secure.payza.com/PayNow/8695D27D6AA848BD8F68F9A0A9EFC562b0en.gif" width="88" border="0"></a>
                <a target="_blank" href="https://perfectmoney.com/?ref=501236">
                <img height="31" src="https://perfectmoney.com/img/88-31-10.png" width="88" border="0"></a><b>
                <a target="_blank" href="http://solidtrustpay.com/?r=81302">
                <img height="35" src="https://solidtrustpay.com/ImgDir/buttons/buynow1.gif" width="62" border="0"></a>
                </div>
                </b>
                </div>
                	</div>
                </div>
            </div>
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Advertisement
                </div>
                <div class="panel-body"><div id="cartSummary">
                    bla bla bla
                </div></div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
