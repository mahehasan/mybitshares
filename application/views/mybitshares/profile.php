<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<style type="text/css">
    .profile 
{
    min-height: 355px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }
.dropdown-menu 
{
    background-color: #34495e;    
    box-shadow: none;
    -webkit-box-shadow: none;
    width: 250px;
    margin-left: -125px;
    left: 50%;
    }
.dropdown-menu .divider 
{
    background:none;    
    }
.dropdown-menu>li>a
{
    color:#f5f5f5;
    }
.dropup .dropdown-menu 
{
    margin-bottom:10px;
    }
.dropup .dropdown-menu:before 
{
    content: "";
    border-top: 10px solid #34495e;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    position: absolute;
    bottom: -10px;
    left: 50%;
    margin-left: -10px;
    z-index: 10;
    }
</style>
<div class="container-fluid">
	<div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
		<div class="col-lg-6">
			<h1 class="page-header"></h1>
	        <div class="panel panel-default">
	        	<div class="panel-heading">
	            	Profile                <div class="pull-right"><a href="<?php echo base_url();?>mybitshares/edit_profile" class="btn btn-success btn-sm" type="button">Edit</a></div>
                    </div>
	            <div class="panel-body">
	            <div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                    <h2><?php echo $user_details[0]->firstName.' '.$user_details[0]->lastName;?></h2>
                    <p><strong>Email: </strong> <?php echo $user_details[0]->email;?> </p>
                    <p><strong>Last Login: </strong> <?php echo date('Y-M-D h:i:s A', $user_details[0]->lastLogin);?> </p>
                    <p><strong>Address: </strong>
                    <?php echo $user_details[0]->address1.'<br>'.$user_details[0]->town.'<br>'.$user_details[0]->country_name;?>
                        <!-- <span class="tags">html5</span> 
                        <span class="tags">css3</span>
                        <span class="tags">jquery</span>
                        <span class="tags">bootstrap3</span> -->
                    </p>
                </div>             
                <div class="col-xs-12 col-sm-4 text-center">
                    <figure>
<!--                        <img src="http://api.ning.com/files/3xh5OKLELdmunxUkiaqtIRfQ5ExlXCrJ85pKr3jGnbtvlRubARY5VrFNK8TmRXit182qWa1q1WZg-IU*cwBYaFQ-74ff0zW6/Passportsizephoto.JPG?width=390&height=508" alt="" class="img-square img-responsive">-->
                        <figcaption class="ratings">
                            <!-- <p>Ratings
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                 <span class="fa fa-star-o"></span>
                            </a> 
                            </p> -->
                        </figcaption>
                    </figure>
                </div>
            </div>            
            <div class="col-xs-12 divider text-center">
                <div class="col-xs-12 col-sm-4 emphasis">
                    <h2><strong><?php echo $balance_total; ?></strong></h2>                    
                    <p><small>Cash</small></p>
                    <button  class="btn btn-success btn-block" onclick='Cash()'><span class="fa fa-plus-circle"></span> Cash </button>
                </div>
                <div class="col-xs-12 col-sm-4 emphasis">
                    <h2><strong><?php echo $share_total; ?></strong></h2>                    
                    <p><small>Shares</small></p>
                    <button class="btn btn-info btn-block" onclick='Shares()'><span class="fa fa-plus-circle"></span> Shares </button>
                </div>
            </div>
         </div>
	            </div>
	     	</div>
		</div>

        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here 
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
        </div>
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<script type="text/javascript">
function Cash(){
	window.location = "<?php echo base_url(); ?>mybitshares/fund_add";
}
function Shares(){
	window.location = "<?php echo base_url(); ?>mybitshares/buy_sell";
}
</script>

<?php
require_once("templates/default/footer.php"); 
?>
