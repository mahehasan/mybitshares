<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" ></script>
<script type="text/javascript">
function buy($id){
    window.location = "<?=SITE_URL?>mybitshares/confirm_share_to_sell/"+$id;
}
</script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="page-header"></h1>
            <div class="panel panel-success">
                <div class="panel-heading">
                   Shares List
                </div>
                <div class="panel-body">
                <form name="test" action="<?php echo base_url('mybitshares/confirm_share_to_sell');?>"  method="post">
                <table class="table-striped table-hover table-bordered table">
                
                        <tbody>
                            <tr><td><b>Number Of Shares</b></td><td><?php if(sizeof($share_detail)>0){echo $share_detail[0]->current_share_number;}else{echo 0;}?></td></tr>
                            <tr><td><b>Per Share Value</b></td><td><input type="text" value="<?php if(sizeof($share_detail)>0){echo $share_detail[0]->per_share_price;}else{echo 0;}?>" name="per_share_value"></td></tr>
                            <tr><td><b>Share To Add For Sale</b></td><td><input type="text" name="share_to_add_for_sell"></td></tr>
                            <tr><td colspan="2"><input type="submit" class="btn btn-primary" value="Submit"></td></tr>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Ad Here
                </div>
                <div class="panel-body"></div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
