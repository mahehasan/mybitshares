<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" ></script>
<script type="text/javascript">
function buy($id){
    window.location = "<?=SITE_URL?>mybitshares/share_buy/"+$id;
}
function cancel($id){
    //window.location = "<?=SITE_URL?>mybitshares/share_cancel/"+$id;
	if (confirm('Are you sure to cancel?')) {
		window.location = "<?=SITE_URL?>mybitshares/share_cancel/"+$id;
	} else {
		// Do nothing!
	}
}
</script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="page-header"></h1>
            <div class="panel panel-success">
                <div class="panel-heading">
                   Shares List
                </div>
                <div class="panel-body">Do you need more cash balance to buy shares fund your account. (notice account fund only can be used for advertising and to buy shares you can not use it for payouts<br><br>
                <?php $bonus_list = $this->mybitshares_model->get_all_bonus_list();
                    $rslt = '';
                    $i=1;
                    foreach ($bonus_list as $row) {
                       if(sizeof($bonus_list)==$i){
                        $rslt .= '<a target="_blank" href="'.base_url('mybitshares/add_fund').'/'.$row->share_bonus_id.'">Fund $'.$row->fund_amount.' (+'.$row->amount.'% bonus)</a>.';
                       }else{
                        $rslt .= '<a target="_blank" href="'.base_url('mybitshares/add_fund').'/'.$row->share_bonus_id.'">Fund $'.$row->fund_amount.' (+'.$row->amount.'% bonus)</a> | ';
                       }
                       $i++;
                    }
                    echo $rslt;
                ?><br><br>
                Automatically on each successful transaction: Seller pays <?php echo $share_sell_percentage; ?>% site fee, and buyer pays <?php echo $share_buy_percentage; ?>%.
                <table class="table-striped table-hover table-bordered table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Shares</th>
                                <th>Price</th>
                                <th>Buy</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php //echo'<pre>'; print_r($sellable_shares); echo'<pre>'; die();

						if(sizeof($sellable_shares)>0)
						{
						foreach ($sellable_shares as $row) {?>
                            <tr>
                                <td><?php echo $row->firstName.' '.$row->lastName; ?></td>
                                <td><?php echo $row->number_of_shares; ?></td>
                                <td><?php echo $row->amount; ?></td>
                                <td>
								
								<?php if($row->seller_id == $this->session->userdata('userID')){ ?>
									<button type="button" class="btn btn-primary" onclick="cancel('<?php echo $row->share_sell_id;?>')">Cancel</button>
								<?php } else{?>
								
								<button type="button" class="btn btn-primary" onclick="buy('<?php echo $row->share_sell_id;?>')">Buy</button>
								
								
								 <?php } ?>
                                
                                </td>
                            </tr>
						<?php }} ?> 
                        </tbody>
                    </table>
                     <form name="test" action="<?php echo base_url('mybitshares/confirm_share_to_sell');?>"  method="post">
                <table class="table-striped table-hover table-bordered table">
                
                        <tbody>
                            <tr><td><b>Number Of Shares</b></td><td><?php if(sizeof($share_detail)>0){echo $share_detail[0]->share_amount;}else{echo 0;}?></td>
							<td><?php  
							//echo'<pre>'; print_r($package_quantity); echo'<pre>'; die();
							if($package_quantity>2)
							{
								echo "your package quota limit reached.";								
							}														
							?></td></tr>
                            <tr><td><b>Share To Add For Sale</b></td><td colspan='2'><input type="text" name="share_to_add_for_sell"></td></tr>
                            <tr><td><b>Amount</b></td><td colspan='2'><input type="text" value="" name="amount"></td></tr>
                            
                            <tr><td colspan="3"><input type="submit" class="btn btn-primary" value="Submit"></td></tr>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Ad Here
                </div>
                <div class="panel-body"></div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
