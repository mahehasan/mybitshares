<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<script type="text/javascript">
  function get_models_of_brand(cart_id, product_id, product_quantity){
  var model_id = id + "_model";
  $.ajax({
    url: '<?php echo base_url();?>inventory/combo_models_of_brand/' + brand_id + '/' + id.match(/\d+$/)[0],
    context: document.body
  }).done(function (data) {
    document.getElementById(model_id).innerHTML = data; 
  })
}

</script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="page-header"></h1>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <?php echo $category_detail[0]->category_title;?>
                </div>
                <div class="panel-body">
                
                

<?php echo form_open('path/to/controller/update/function'); ?>

<table cellpadding="6" cellspacing="1" style="width:100%" border="0">

<tr>
  <th>QTY</th>
  <th>Item Description</th>
  <th style="text-align:right">Item Price</th>
  <th style="text-align:right">Sub-Total</th>
</tr>

<table width="100%" border="1">
  
  <tr>
    <td width="37%">ID</td>
    <td width="30%">Name</td>
    <td width="16%">Price</td>
    <td width="16%">&nbsp;</td>
  </tr>
  <?php $key = 0;?>
  <?php foreach($products as $product):?>
  <tr>
    <td><?php echo $product['id'];?></td>
    <td><?php echo $product['name'];?></td>
    <td><?php echo $product['price'];?></td>
    <td><a href="?id=<?php echo $key;?>">Add to Cart</a></td>
  </tr>
  <?php $key ++;?>
  <?php endforeach;?>
</table>

<hr>


<b>Your Cart</b>

<?php echo form_open(base_url()); ?>

<table cellpadding="6" cellspacing="1" style="width:100%" border="1">

<tr>
  <th>QTY</th>
  <th>Item Description</th>
  <th style="text-align:right">Item Price</th>
  <th style="text-align:right">Sub-Total</th>
</tr>

<?php $i = 1; ?>

<?php foreach ($this->cart->contents() as $items): ?>

  <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>

  <tr>
      <td><?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5')); ?></td>
      <td>
        <?php echo $items['name']; ?>

            <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

                <p>
                    <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>

                        <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />

                    <?php endforeach; ?>
                </p>

            <?php endif; ?>

      </td>
      <td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?></td>
      <td style="text-align:right">$<?php echo $this->cart->format_number($items['subtotal']); ?></td>
    </tr>

<?php $i++; ?>

<?php endforeach; ?>

<tr>
  <td colspan="2"></td>
  <td class="right"><strong>Total</strong></td>
  <td class="right" align="right">$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
</tr>


</table>

<p><?php echo form_submit('update_cart', 'Update your Cart'); ?></p>
       </div>
            </div>
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Cart Contents
                </div>
                <div class="panel-body">
                Items: 0 Amount: 0 in Cart
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
