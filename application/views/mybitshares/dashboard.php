<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="page-header"></h1>
            <div class="panel panel-success">
            <?php if (strlen($MSG)>0) echo $this->html->showSuccessMsg($MSG);  ?>
                <div class="panel-heading">
                    Members Earnings Area
                </div>
                <div class="panel-body">
                <?php $parent_category = $this->mybitshares_model->getAllCategoryByParent(0);
                        foreach ($parent_category as $row_parent) { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo $row_parent->category_title; ?>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                    <?php $child_category = $this->mybitshares_model->getAllCategoryByParent($row_parent->category_id); ?>
                                    <tr>
                                    <?php $rowcnt = 1 ; foreach ($child_category as $row_child) { 
                                        $target="";
                                        $link="";
										$class= "";
                                            if($row_child->category_id==4 || $row_child->category_id==5){
                                                $target="_blank";
                                            }else{
                                                $target="";
                                            }
                                            if($row_child->category_id==12){
                                                $class= "confirmation" ;
                                            }
                                            
                                            if (strlen($MSG)>0)
                                            {
                                                if($row_child->category_id==4 || $row_child->category_id==5)
                                                { 
                                                  // $link="javascript:void(0);";
												   $link=" ";
												   $class= "btn disabled" ;
                                                }
												else{
                                               $link = base_url("mybitshares/".$row_child->category_description);
                                            }
                                            }
                                            else{
                                               $link = base_url("mybitshares/".$row_child->category_description);
                                            }
                                        ?>
                                            <td style="text-align: center;"><?php echo '<a target="'.$target.'"  href="'.$link.'" class="'.$class.'">'.$row_child->category_title.'</a>'; ?></td>
											
											
                                            <?php if ($rowcnt%4==0){
                                                echo '</tr><tr>';
                                            } 
                                       $rowcnt++; } ?>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                <?php   ;   }          ?>
                </div>
            </div>
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
</script>
<?php
require_once("templates/default/footer.php"); 
?>
