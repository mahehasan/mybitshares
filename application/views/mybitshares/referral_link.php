<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" ></script>
<script type="text/javascript">
$( document ).ready(function() {
    $('.ad_img').click(function(e) {
        // e.preventDefault();
        var m_menu = 1;
        var row = $(this).closest('tr');
        row.hide();
        var location = $(this).attr('href');
        $.ajax({
            method: 'post',
            url: '<?php echo base_url();?>mybitshares/click_ad',
            data: {url: location, menu: m_menu} 
        }).done(function(response){
            console.log(response);
            // window.location.href = location;
            // var win = window.open(location, '_blank');
            // win.focus();
        });
        // return false;
    });
});
</script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="page-header"></h1>
            <div class="panel panel-success">
                <div class="panel-heading">
                    Referral Link
                </div>
                <div class="panel-body">
                Please share the following link to all the people you refer to our site.<br>
                <input type="text" size="80" name="ref-link" value="<?php echo base_url('info/referred_by/'.$user_details[0]->userID); ?>">
                </div>
            </div>
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Ad Here
                </div>
                <div class="panel-body"></div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
