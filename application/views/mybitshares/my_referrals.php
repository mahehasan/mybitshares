<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" ></script>
<script type="text/javascript">
function getreferrals(id, lvl){
     $.get("<?=SITE_URL?>mybitshares/get_referrals/"+id+'/'+lvl, function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        lvl++;
        var levelname = 'level'+ lvl;
        // alert(levelname);
        document.getElementById(levelname).innerHTML = data;
    });
}
</script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Add here
                </div>
                <div class="panel-body">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="page-header"></h1>
            <div class="panel panel-success">
                <div class="panel-heading">
                   Referral Information
                </div>
                <div class="panel-body">
                <form role="form" action="<?=SITE_URL?>mybitshares/my_referrals" method="post">
                <select name="level1" class="form-control" onchange="getreferrals(this.value,1)">
                    <option value="0">Level: 1 (<?php echo $level1_count; ?>)</option>
                    <?php foreach ($level1_users as $row) {?>
                    <option value="<?php echo $row->userID;?>"><?php echo $row->firstName.' '.$row->lastName.' ('.$this->mybitshares_model->get_referrals_count_by_id($row->userID).')';?></option>
                    <?php } ?>
                </select> 
                <div id="level2">
                    <select name="level2" class="form-control">
                        <option value="0">Level: 2 (0)</option>
                    </select> 
                </div>
                <div id="level3">
                    <select name="level3" class="form-control">
                        <option value="0">Level: 3 (0)</option>
                    </select> 
                </div>
                <div id="level4">
                    <select name="level4" class="form-control">
                        <option value="0">Level: 4 (0)</option>
                    </select> 
                </div>
                                        
                </form>

                </div>
            </div>
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
            <h1 class="page-header"></h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Ad Here
                </div>
                <div class="panel-body"></div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
