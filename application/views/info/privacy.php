<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
		</div>
    	<div class="col-lg-6">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	We Respect Your Privacy!
	            </div>
	            <div class="panel-body">

<h3>Information Collected</h3>
<p>MyBitShares.com has created this privacy statement in order to
    demonstrate our firm commitment to privacy. MyBitShares.com    uses your IP address to help diagnose problems with our server, and to
    administer our Web site. Your IP address is used to gather broad
    demographic information. MyBitShares.com uses cookies to deliver
    content specific to your interests, to save your password so you don't
    have to reenter it each time you visit our site, and for other purposes. 
    MyBitShares.com's registration form requires users to give us
    contact information (like their name, address, and e-mail address). 
    MyBitShares.com uses member contact information from the
    registration form to send the member information about our company
    and e-mail advertisements.</p>

<p>Affiliates may opt-out of receiving future mailings; see the choice/opt-out section 
    below. Demographic and profile data is also collected at our site. We use this 
    data to tailor the visitor's experience at our site, showing them content that 
    we think they might be interested in, and displaying the content according to 
    their preferences. We collect personal off-line information for the sole purpose 
    of paying affiliates for earnings from our program. This site contains links to 
    other sites. MyBitShares.com is not responsible for the privacy practices
    or the content of such Web sites. MyBitShares.com Uses a sign-up form
    for new affiliates and advertisers. We collect visitor's contact 
    information (like their e-mail address, address, and name). The member's contact 
    information is used to get in touch with the member when necessary. Users may 
    opt-out of receiving future mailings; see the choice/opt-out section below.</p>

<h3>Public Forums</h3>

<p>MyBitShares.com, in the future, may make chat rooms, forums, message boards, and/or news groups 
available to its users. Please remember that any information that is disclosed 
in these areas becomes public information and you should exercise caution when 
deciding to disclose your personal information.</p>

<h3>Security</h3>
<p>MyBitShares.com has security measures in place to protect the loss,
    misuse and alteration of the information under our control. </p>

<h3>Children's Guidelines</h3>
<p>We do not collect online contact information without prior parental consent or 
parental notification, which will include an opportunity for the parent to prevent 
use of the information and participation in the activity. Without prior parental 
consent, online information will only be used to respond directly to the child's 
request and will not be used for other purposes without prior parental consent. 
We do not collect personally identifiable off-line contact information without 
prior parental consent. We do not distribute to third parties any personally identifiable 
information. We do not give the ability to publicly post or otherwise distribute 
personally identifiable contact information. We do not entice by the prospect 
of a special game, prize or other activity, to divulge more information than is 
needed to participate in the activity.</p>

<h3>Choice/Opt-Out</h3>

<p>MyBitShares.com provides users the opportunity to opt-out of receiving
    communications from us at the point where we request information about the visitor. 
    MyBitShares.com gives users the following options for removing their
    information from our database to not receive future communications or to no
    longer receive our service. Log in to your account and "Cancel Account".
    You may also <a href="#">Contact Us</a> and
    request your affiliateship to be canceled at anytime.</p>

<h3>Correct/Update</h3>

<p>MyBitShares.com gives users the following options for changing and
    modifying information previously provided. Affiliates Area - log-in
    and change your personal information. Contacting the Web Site If you have
    any questions about this privacy statement, the practices of this site,
    or your dealings with MyBitShares.com, you can 
    <a href="#">Contact Us</a>.</p>

	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
        </div>
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
