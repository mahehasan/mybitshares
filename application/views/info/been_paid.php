<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
		</div>
    	<div class="col-lg-6">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	Been Paid
	            </div>
	            <div class="panel-body">

<h3>List Since Relaunch 1 May 2016</h3>

<p>We have paid out total of $106,247.02 to our members!</p>

<table style="width:600px; margin-left: auto; margin-right: auto;" border="0">
            <tbody><tr><th>Username</th><th>Requested</th><th>Paid</th><th>Amount</th><th>Fee</th><th>By</th></tr><tr><td>****</td><td>Apr 28, 2016</td><td>Apr 28, 2016</td><td>$50.00</td><td>$5.00</td><td>Okpay</td></tr><tr><td>autao</td><td>Apr 27, 2016</td><td>Apr 28, 2016</td><td>$30.00</td><td>$3.00</td><td>Okpay</td></tr><tr><td>mamo95</td><td>Apr 26, 2016</td><td>Apr 28, 2016</td><td>$16.45</td><td>$1.64</td><td>PerfectMoney</td></tr><tr><td>mrliu</td><td>Apr 18, 2016</td><td>Apr 28, 2016</td><td>$10.00</td><td>$1.00</td><td>PerfectMoney</td></tr><tr><td>wqb5001</td><td>Apr 15, 2016</td><td>Apr 28, 2016</td><td>$12.97</td><td>$1.30</td><td>PerfectMoney</td></tr><tr><td>chmagp</td><td>Apr 25, 2016</td><td>Apr 28, 2016</td><td>$14.13</td><td>$1.41</td><td>PerfectMoney</td></tr><tr><td>ufone</td><td>Apr 14, 2016</td><td>Apr 28, 2016</td><td>$38.07</td><td>$3.80</td><td>PerfectMoney</td></tr><tr><td>qazxmaja</td><td>Apr 16, 2016</td><td>Apr 28, 2016</td><td>$39.03</td><td>$3.90</td><td>PerfectMoney</td></tr><tr><td>dsbe</td><td>Apr 22, 2016</td><td>Apr 28, 2016</td><td>$40.00</td><td>$4.00</td><td>Payza</td></tr><tr><td>ragufree</td><td>Apr 19, 2016</td><td>Apr 28, 2016</td><td>$10.52</td><td>$1.05</td><td>Payza</td></tr><tr><td>mfswood24</td><td>Apr 27, 2016</td><td>Apr 28, 2016</td><td>$10.00</td><td>$1.00</td><td>Payza</td></tr><tr><td>mvdh1894</td><td>Apr 20, 2016</td><td>Apr 28, 2016</td><td>$10.00</td><td>$1.00</td><td>Payza</td></tr><tr><td>patsong</td><td>Apr 21, 2016</td><td>Apr 28, 2016</td><td>$16.97</td><td>$1.70</td><td>Payza</td></tr><tr><td>profitclick</td><td>Apr 19, 2016</td><td>Apr 28, 2016</td><td>$10.00</td><td>$0.00</td><td>PayPal</td></tr><tr><td>smw1962</td><td>Apr 24, 2016</td><td>Apr 28, 2016</td><td>$10.00</td><td>$0.00</td><td>PayPal</td></tr><tr><td>psinovas</td><td>Apr 17, 2016</td><td>Apr 28, 2016</td><td>$10.00</td><td>$0.00</td><td>PayPal</td></tr><tr><td>jwgaither6</td><td>Apr 17, 2016</td><td>Apr 28, 2016</td><td>$10.00</td><td>$0.00</td><td>PayPal</td></tr><tr><td>refanna</td><td>Apr 17, 2016</td><td>Apr 28, 2016</td><td>$10.00</td><td>$0.00</td><td>PayPal</td></tr><tr><td>randog77</td><td>Apr 17, 2016</td><td>Apr 28, 2016</td><td>$10.00</td><td>$0.00</td><td>PayPal</td></tr><tr><td>moneyfabri</td><td>Apr 14, 2016</td><td>Apr 28, 2016</td><td>$10.00</td><td>$0.00</td><td>PayPal</td></tr></tbody></table><center><a href="#"> Next &gt;&gt;</a></center></td>
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
        </div>
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
