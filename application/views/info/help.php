<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
		</div>
    	<div class="col-lg-6">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	Frequently Asked Questions
	            </div>
	            <div class="panel-body">
	            <td width="610" align="center" valign="top">
<h3>Account Questions:</h3>

<h4>Am I allowed to have more than one account?</h4>
<p>We allow one account per individual. There may be more than
   one account per household, however each person registering an
   account must have a separate computer and separate ISP to
   participate in our program. It is as well not allowed to use proxy to login at your account and to click our advertising.</p>

<h4>How can I log into my account?</h4>
<p>Log in at http://www.mybitshares.com</p>

<h4>How can I cancel my account?</h4>
<p>Go to your user info page and at the bottom of the page you
   enter your user name and password, then click cancel.</p>


<h4>How can I cancel receiving your emails?</h4>
<p>Your affiliateship in our program allows MyBitShares.com   to send you email messages and advertisements. You may not cancel
   receiving email ads from MyBitShares.com without terminating
   your affiliateship in our program and forfeiting any and all 
   earnings. Log in to your account and click the "User Account Info." Link. At the 
   bottom of the page you will find the option to "Cancel Account".</p>


<h4>How can I change my personal information?</h4>
<p>Log in to your account and click the "User Account Info." link.
   You will be able to edit any of your information except your Affiliate ID.</p>


<h4>I live outside the US, can I join?</h4>
<p>Yes, we welcome all international affiliates.</p>


<h4>I have Web-TV, can I join too?</h4>
<p>Yes, though some programs will not allow WEB-TV users to join their program, we 
   welcome WEB-TV users. Our efficient tracking method allows WEB-TV users full earning 
   privileges.</p>


<h3>Earnings Questions</h3>

<h4>How much do I earn by participating in paid-emails?</h4>
<p>Each advertiser pays a different amount per unique visit. These amounts change 
   frequently so we cannot give an exact amount for each visit, however we do give 
   you a large percentage of the amount we earn.</p>


<h4>When can I request payment?</h4>
<p>You may request payment after you have earned 
   $10.00. </p>


<h4>How can I request payment?</h4>
<p>As soon as you reach payout, you will see the redemption option appear in your 
   account area, after submitting your redemption request, we will validate your 
   account, and then begin the payment process. You will also have the option to 
   redeem your earnings for discounted advertising with us. When you qualify, you 
   will see what options are available to you in your account area.</p>


<h4>When am I paid?</h4>
<p>After your payment request is made and your account is in good-standing we
   will issue payment to you within 30 days.</p>


<h4>How am I paid?</h4>
<p>Payments are made via the following payment processors:
    Egopay, Okpay, PayPal, Payza, PerfectMoney, Solidtrustpay

</p><h3>Referral Questions</h3>


<h4>How can I refer other affiliates?</h4>
<p>You may place a link with your referral url to MyBitShares.com   on your website. You may add your referral url to your email signature.
   You can give out flyers or advertise in any way you wish except
   for Spamming.</p>


<h4>What is my referral link?</h4>
<p>Your personal referral link can be found on your personal
    "Referral links" page.</p>


<h4>What can I earn through referrals?</h4>
<p>Affiliates will earn from 5 levels
   of referrals.</p>
   Level 1 = 25%, Level 2 = 10%, Level 3 = 5%, Level 4 = 3%, Level 5 = 1%
<br>
<br>
<h4>Are my referral earnings working?</h4>
<p>Yes, referral earnings are updated once a day. You must be active
   on our site to earn from your referrals To qualify to receive
   earnings from your downline, you must login at least once every
   100 days and
   click on at least 5%
   of the ads your downline click.</p>

<h3>Advertising Questions</h3>

<h4>I have a website I would like to advertise, where can I find information on your 
different programs and costs?</h4>
<p>Visit our <a href="#">advertisers</a>
   section of the website for complete details.</p>


<h4>I have questions not answered on this FAQ page, what can I do?</h4>
<p><a href="#">Contact Us</a> and
   supply as much information as possible so we can solve the
   problem in a timely manner.</p>
   
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
        </div>
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
