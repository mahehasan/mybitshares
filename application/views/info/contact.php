<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
		</div>
    	<div class="col-lg-6">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	Contact Us
	            </div>
	            <div class="panel-body">
	            <td width="610" align="center" valign="top">

<p>Feel free to contact us. Please take a look at our
   <a href="#">FAQ</a> section of the site. Should
   you not find the answer your looking for, please complete the form below
   and submit your question to our support department.</p>

<hr>

<form method="POST" action="/pages/contact.php">
    
<input type="hidden" name="user_form" value="email">
<input type="hidden" name="email_to" value="support_email">
<input type="hidden" name="required" value="message_type,bquestion_type,subject,email_from">


<table cellpadding="3" cellspacing="3" border="0">
<tbody><tr> 
    <td nowrap="">Your Name:</td>
    <td width="100%"><input type="text" name="userform[name]" value=""></td>
</tr>
<tr> 
    <td nowrap="">Your Email:</td>
    <td width="100%"><input type="text" name="userform[email_from]" value=""></td>
</tr>
<tr> 
    <td nowrap="">Your Username:</td>
    <td width="100%"> <input type="text" name="userform[username]" value=""></td>
</tr>
<tr> 
    <td nowrap="">Question Type:</td>
    <td width="100%"> 
        <select name="userform[subject]">
            <option value="" selected="">Choose Your Subject Type:</option>
            <option value="Paid E-Mail Question">Paid E-Mail Question</option>
    <option value="Change payment Question">Change payment Question</option>
          <option value="Signup/Confirmation Question">Signup/Confirmation Question</option>
            <option value="Earnings/Points Question">Earnings/Points Question</option>
            <option value="Personal Information Question">Personal Information Question</option>
            <option value="Advertising Question">Advertising Question</option>
            <option value="Other">Other</option>
        </select>
    </td>
</tr>
<tr> 
    <td nowrap="">Description of<br>your question:</td>
    <td width="100%"><input type="text" name="userform[bquestion_type]" value=""></td>
</tr>
<tr> 
    <td colspan="2" nowrap="">Describe Your Question in Full:<br>
    <textarea cols="40" name="userform[message_type]" rows="7"></textarea>
    <br>
    <input type="submit" value="Contact Us" name="submit">
    <input type="reset" value="Clear Form" name="reset">
    </td>
</tr>
</tbody></table>
</form>
	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
        </div>
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
