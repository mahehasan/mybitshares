<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("templates/default/header.php"); 
//$this->message->display();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
		</div>
    	<div class="col-lg-6">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-success">
	        	<div class="panel-heading">
	            	Terms Of Service
	            </div>
	            <div class="panel-body">
	            <td width="610" align="center" valign="top">


<h3>MyBitShares.com Terms Of Service</h3>

<p>By signing up and actively using the MyBitShares.com program you agree
   to all terms and conditions set forth in this agreement. MyBitShares.com   may, at any time, choose to edit, add and/or delete portions of this agreement 
   and impose changes without prior notification of its affiliates.</p>
   
<p>Affiliates will 
   be informed of any and all changes to this policy via an e-mail to their primary 
   contact e-mail address provided upon signing up with MyBitShares.com. If
   any modification is unacceptable to you, your only recourse is to terminate 
   this agreement. Your continued participation in the MyBitShares.com   program following our posting of a change in policy notice or new agreement on 
   our site will constitute binding acceptance to the change.</p>
   
<h3>Responsibility of Affiliates</h3>

<p>You must be 13 years of age or older to participate in this program. Our program 
  is open to both US and International affiliates and is void where prohibited by 
  local governing laws.</p>
  
<p>You agree to and must use your own name and e-mail address in the sign-up process 
   and may not assume a false identity. You may not sign-up multiple times with different 
   e-mail addresses. Your computer must not be shared with another individual that 
   has an existing MyBitShares.com account.</p>

<p>You had to be active in the last 100 days to earn daily share earnings. So after 100 days inactive your earnings will stop till you become active again.
Each day you lose 0.15% of your earned shares. The maximum amount you can request in 1 payout is $75. If your balance is higher on moment of request the extra earnings can be used for advertising only. This amount will then be put on hold till you used this for advertising only.</p>

<p>You will use your e-mail address / password combination to access your account, 
   it is your responsibility to keep this information confidential, you may change 
   this information by logging into your account and following the "User Account Info."
   link.</p>
   
<p>You are solely responsible for any and all use of your MyBitShares.com   account including authorization by you to any third party individual who may use 
   your account.</p>

<p>Should any instances of fraud, system abuse, or any type of activity deemed to 
   be inappropriate or illegal by MyBitShares.com be detected it may result
   in member termination and possible legal action. Any solicitation of advertisers
   for a confirmation e-mail for purposes of receiving credit in your account will
   be grounds for immediate termination.</p>


<p>The upgraded membership benefits always can be changes if this happend it start for payments after the changes are made. Older payments ofcourse will be receive still old benefits (only when they become better older payments get as well the same better benefits).</p>


<p>You must maintain your e-mail account that you used when signing up with 
   MyBitShares.com. You agree to receive e-mail advertising from MyBitShares.com. 
   Limited to no more than (10) per day.</p>

<p>In the event your e-mail account on file becomes closed or blocked to messages 
   from MyBitShares.com, your account will be terminated. This especially applies
   to e-mail accounts returned for being Full. Any member sending us gibberish will result
   in automatic termination of account and Forfeiture of All earnings to date of termination. 
    Auto-responses will also be deleted immediately.</p>

<h3>Relationship of the Parties</h3>

<p>You and MyBitShares.com are independent contractors, and nothing in this Agreement
   creates any partnership, joint venture, agency, franchise, sales representative
   or employment relationship between you and MyBitShares.com. You understand
   that you do not have authority to make or accept any offers or make any representations
   on behalf of MyBitShares.com. You may not make any statement, whether on your
   site or otherwise, that would contradict anything in this section. You are solely
   responsible for the reporting and payment of any taxes for money earned while using
   the program.</p>

<h3>Limitation of Liability</h3>

<p>MyBitShares.com  will not be liable for lost profits, lost business opportunities,
   or any other indirect, special, punitive, incidental or consequential damages arising out of 
   or related to this Agreement or MyBitShares.com program, even if MyBitShares.com 
   has been advised of the possibility of such damages. Furthermore, MyBitShares.com 
   aggregate liability arising under this Agreement will not exceed the amount of 
   the total fees paid or payable to you under this Agreement. The provisions of 
   this Section survive termination or expiration of the Agreement.</p>

<h3>Payments</h3>

<p>Affiliates may earn by receiving paid-emails and visiting the advertiser websites 
  after they have entered their member ID in the area on the website provided by 
  MyBitShares.com. Affiliates may earn by referring new affiliates to the 
  MyBitShares.com program. Affiliates will earn a percentage of advertising revenues
  collected by MyBitShares.com. MyBitShares.com  will pay affiliates when
  their balance reaches the minimum payout rate set by MyBitShares.com. Once
  the member has reached payout they may request payment. Payments will be made within
  30 days or a reasonable time after.</p>

<h3>Spam</h3>

<p>You may not promote your referral links through unsolicited emailing (i.e. SPAMMING), 
   newsgroup postings, or any other method of mass communication. Failure to comply 
   will result in immediate termination of your affiliateship with MyBitShares.com 
   and may result in legal prosecution. MyBitShares.com  strictly enforces
   anti-Spamming laws. Spamming is a federal crime.</p>
   
<p>Any member caught Spamming will
   not only have their account terminated immediately and lose any past, present and
   future earnings, but shall also be held liable for Spamming as we shall cooperate
   with any authorities and investigations that may arise from the Spamming incident. 
   MyBitShares.com may charge up to $5 per spam e-mail sent. Multiple sign-ups
   shall be grounds for immediate termination of all involved accounts. Signing up
   multiple times from the same computer also constitutes fraud. MyBitShares.com 
   will file charges for recovery of any earnings received from multiple sign-ups.</p>


	            </div>
	     	</div>
    	</div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-3">
        	<h1 class="page-header"></h1>
	        <div class="panel panel-primary">
	        	<div class="panel-heading">
	            	Add here
	            </div>
	            <div class="panel-body">
	            
	            </div>
	     	</div>
        </div>
	</div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
require_once("templates/default/footer.php"); 
?>
