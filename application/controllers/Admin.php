<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller {
   private $timestamp;
   private $adminID;
	function __construct()
	{		
		parent::__construct();
		$this->load->library('form_validation');
     	$this->load->helper('form');
		$this->load->model(array('category_model'));
		$this->load->model(array('admin_model', 'cron_model', 'mybitshares_model','transaction_update_scheduler_model','user_transaction_update_scheduler_model'));
		$this->timestamp = time();
		$this->adminID = $this->session->userdata('userID');
		if ($this->adminID!=1) {
			$this->general->redirect( 'logout' );
		}
	}
	
	public function resize_image($uploadedFilePath, $resizedFilePath, $width, $height)
	{// /uploads/avatar/tmp/   /uploads/avatar/
		$config['image_library'] = 'gd2';
	    $filename = $this->input->post('new_val');
	    // $source_path = $_SERVER['DOCUMENT_ROOT'] . $uploadedFilePath;
	    // $target_path = $_SERVER['DOCUMENT_ROOT'] . $resizedFilePath;
	    $source_path =  '.'. $uploadedFilePath;
	    $target_path =  '.'. $resizedFilePath;
	    $config_manip = array(
	        'image_library' => 'gd2',
	        'source_image' => $source_path,
	        'new_image' => $target_path,
	        'maintain_ratio' => FALSE,
	        'create_thumb' => TRUE,
	        'thumb_marker' => '_thumb',
	        'width' => $width,
	        'height' => $height
	    );
	    $this->load->library('image_lib', $config_manip);
	    // $this->image_lib->initialize($config);
	    if (!$this->image_lib->resize()) {
	        echo $this->image_lib->display_errors();
	    }
	    // clear //
	    $this->image_lib->clear();
	}

	public function index(){
		$data = array();
		$this->load->view('users/login',$data);
	}
	
	public function dashboard(){
		$data = array();
		$data['welcomeText'] = 'Welcome Admin Panel';
		$this->load->view('users/admin/dashboard',$data);
	}

	public function manage_users(){
		$data['user_data_admin'] = $this->admin_model->get_all_user_data_admin();
		$this->load->view('users/admin/manage_users',$data);
	}

	public function user_details($id)
	{
		$data['user_data_detail'] = $this->admin_model->get_all_user_data_by_id($id);
		$data['client_cash_history'] = $this->admin_model->get_all_balance_history_by_id($id);
		$data['client_share_history'] = $this->admin_model->get_all_share_history_by_id($id);
		$data['share_given_by_admin'] = $this->admin_model->get_all_share_gift_by_admin();
		
		
		$this->load->view('users/admin/user_detail',$data);
	}
	
	public function list_category(){
		$data = array();
		$qry_options = " WHERE `category_publish` = 1 AND parent_category = 0 ";
		//---- all records processing
		$data['categories'] = $this->category_model->getAllCategoryData($qry_options);
		//---- default date filters
		$this->load->view('users/admin/categories', $data);
	
	}
	public function add_category(){
		$data = array();
		$postData = $this->input->post(NULL, TRUE);
		if (isset($postData['category_id'])) {
			if ($postData['category_id']==0) {
				$data = array(
						'parent_category' => $postData['parent_category'],
						'category_title' => $postData['category_title'],
						'category_description' => $postData['category_description'],
						'category_publish' => 1,
						'created_date' => $this->timestamp,
						'modified_date' => $this->timestamp,
						'created_by' => $this->adminID
				);
					
				$category_id = $this->category_model->saveCategory($data);
				if($category_id>0){
					$this->general->redirect('admin/list_category');
				}
			}else{
				$data = array(
						'category_title' => $postData['category_title'],
						'category_description' => $postData['category_description'],
						'modified_date' => $this->timestamp,
						'created_by' => $this->adminID
				);
				$update_category_form = $this->category_model->updateCategory($data, $postData['category_id']);
				if($update_category_form){
					$this->general->redirect('admin/list_category');
				}
			}
		}
		//---- default date filters
		$this->load->view('users/admin/form_category', $data);
	
	}
	public function edit_category($category_id){
		$data = array();
		$qry_options = " WHERE `category_publish` = 1 AND category_id = $category_id ";
		//---- all records processing
		$category = $this->category_model->getAllCategoryData($qry_options);
		$data['category'] = $category[$category_id];
		//---- default date filters
		$this->load->view('users/admin/form_category', $data);
	
	}
	
	public function add_advertise()
	{
        $data['ads_menu_list'] = $this->general->get_ads_manu_list();
		$data['price_list'] = $this->admin_model->getAllPriceList();
		$data['category_list'] = $this->admin_model->getAllCategoryList();
		$data['site_servers'] = $this->admin_model->get_all_site_servers();
		$this->load->view('users/admin/advertise_add',$data);
	}

    public function submit_advertise(){
        $site_server_url = $this->general->getSiteServerURL($this->input->post('site_server_code'));
        $site_server_url_exist = $this->general->urlExists($site_server_url."v1/");
//        var_dump ($site_server_url_exist);die;
        if(!$site_server_url_exist){
            $this->general->set_flash_message("'{$site_server_url}' does not exist");
            $this->general->redirect('admin/add_advertise');
            exit;
        }

        $site_server_add_data = array(
            "site_server_code"=>$this->input->post('site_server_code'),
            "client_url"=>$this->input->post('client_url'),
            "created_by"=>$this->input->post('created_by')
        );

        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, $site_server_url."v1/advertise/save");
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($site_server_add_data));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($handle);
//        print_r($res);
        curl_close($handle);
        $resData = json_decode($res);

        $advertiseID = $resData->advertiseID;
        if ($_FILES['file_uploads']['name'][0]!='') {
            //----- The $_FILES param can not be sent directly, so let's process it first
            $filesData = $this->_prepareFileUploadArray($_FILES['file_uploads']);
            foreach($filesData as $fileData) {
                $fileTempLoc = $fileData["tmp_name"];
                $fileHandle = fopen($fileTempLoc, "rb");
                $fileContents =  base64_encode(stream_get_contents($fileHandle));
                $postfields = array(
                    "advertiseID" => $advertiseID,
                    "fileName" => $fileData['name'],
                    "fileExt" => pathinfo($fileData['name'], PATHINFO_EXTENSION),
                    "fileData" => $fileContents,
                    "size" => $fileData['size']
                );
                $handle = curl_init();
                curl_setopt($handle, CURLOPT_URL, $site_server_url."v1/advertise/upload_binary");
                curl_setopt($handle, CURLOPT_POST, true);
                curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($postfields));
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                $res = curl_exec($handle);
                // print_r($res);
                curl_close($handle);
                $resData2 = json_decode($res);
                //var_dump($resData);
            }
            $RD2 = (array) $resData2;
            $message1 = $RD2['message'];
            $sizeInBytes = $RD2['sizeInBytes'];
            $fileName = $RD2['fileName'];
        }
        $data = array();
        $advertise_title = $this->input->post('advertise_title');
        $advertise_description = $this->input->post('advertise_description');
        $menu1_id = $this->input->post('menu1_id');
        $menu2_id = $this->input->post('menu2_id');
        $menu1_click = $this->input->post('menu1_click');
        $menu2_click = $this->input->post('menu2_click');
        $advertise_price_id = $this->input->post('advertise_price_id');
        $created_by = $this->input->post('created_by');
        //$client_url = $this->input->post('client_url');
        $site_server_code = $this->input->post('site_server_code');

        $RD1 = (array) $resData;
        $advertiseID = $RD1['advertiseID'];
        $message2 = $RD1['message'];
        $data = array('site_server_code' => $site_server_code,
            'site_server_advertise_id' => $advertiseID,
            'advertise_title' => $advertise_title,
            'advertise_description' => $advertise_description,
            'advertise_price_id' => $advertise_price_id,
            'menu1_id' => $menu1_id,
            'menu2_id' => $menu2_id,
            'menu1_click' => $menu1_click,
            'menu2_click' => $menu2_click,
            'showing_order' => 0,
            'created_date' => $this->timestamp,
            'created_by' => $created_by          
        );
		
		//Update client_share
		$price_sell = $this->admin_model->getSell_price($advertise_price_id);
		$this->update_cash_transaction($price_sell);
		
        // print_r($data);
        $this->admin_model->add_advertise($data);
        $this->general->redirect('admin/list_advertise');
    }

	public function modify_advertise($id)
	{
		$data['edit'] = true;
		$data['ad_id'] = $id;
        $site_server_code = $this->general->get_onefieldvaluebyid('advertise', 'advertise_id', $id, 'site_server_code');
        $site_server_url = $this->general->getSiteServerURL($site_server_code);
        $site_server_advertise_id = $this->general->get_onefieldvaluebyid('advertise', 'advertise_id', $id, 'site_server_advertise_id');

        $handle = curl_init();
        curl_setopt_array($handle, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $site_server_url."v1/advertise/advertise/".$site_server_advertise_id
        ));
        $res = curl_exec($handle);
        curl_close($handle);
        $resData = json_decode($res);

        $data['obj'] = $resData->advertise_list;
        $data['ads_menu_list'] = $this->general->get_ads_manu_list();
		$data['price_list'] = $this->admin_model->getAllPriceList();
		$data['category_list'] = $this->admin_model->getAllCategoryList();
		//$data['advertise_detail'] = $this->admin_model->get_advertise_detail_by_id($id);
		$data['advertise_detail'] = $this->admin_model->get_advertise_detail_by_advertise_id($id);

		$this->load->view('users/admin/advertise_add',$data);
	}

    public function edit_advertise($main_ad_id){
        $site_server_code = $this->general->get_onefieldvaluebyid('advertise', 'advertise_id', $main_ad_id, 'site_server_code');
        $site_server_url = $this->general->getSiteServerURL($site_server_code);
        $site_server_advertise_id = $this->input->post('site_server_advertise_id');
        $site_server_data = array(
            "site_server_advertise_id"=>$site_server_advertise_id,
            "client_url"=>$this->input->post('client_url'),
            "created_by"=>$this->input->post('created_by')
        );
        $data = array();
        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, $site_server_url."v1/advertise/edit_save");
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($site_server_data));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($handle);
        curl_close($handle);
        $resData = json_decode($res);

        if ($_FILES['file_uploads']['name'][0]!='' && $site_server_advertise_id) {
            //----- The $_FILES param can not be sent directly, so let's process it first
            $filesData = $this->_prepareFileUploadArray($_FILES['file_uploads']);
            foreach($filesData as $fileData) {
                $fileTempLoc = $fileData["tmp_name"];
                $fileHandle = fopen($fileTempLoc, "rb");
                $fileContents =  base64_encode(stream_get_contents($fileHandle));
                $postfields = array(
                    "advertiseID" => $site_server_advertise_id,
                    "fileName" => $fileData['name'],
                    "fileExt" => pathinfo($fileData['name'], PATHINFO_EXTENSION),
                    "fileData" => $fileContents,
                    "size" => $fileData['size']
                );
                $handle = curl_init();
                curl_setopt($handle, CURLOPT_URL, $site_server_url."v1/advertise/upload_binary");
                curl_setopt($handle, CURLOPT_POST, true);
                curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($postfields));
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                $res = curl_exec($handle);
                // print_r($res);
                curl_close($handle);
                $resData2 = json_decode($res);
                //var_dump($resData);
            }
            $RD2 = (array) $resData2;
            $message1 = $RD2['message'];
            $sizeInBytes = $RD2['sizeInBytes'];
            $fileName = $RD2['fileName'];
        }

        $advertise_title = $this->input->post('advertise_title');
        $advertise_description = $this->input->post('advertise_description');
        $menu1_id = $this->input->post('menu1_id');
        $menu2_id = $this->input->post('menu2_id');
        $menu1_click = $this->input->post('menu1_click');
        $menu2_click = $this->input->post('menu2_click');
        $advertise_price_id = $this->input->post('advertise_price_id');
        $created_by = $this->input->post('created_by');
        $client_url = $this->input->post('client_url');


        $updateData = array(
            'advertise_title' => $advertise_title,
            'advertise_description' => $advertise_description,
            'advertise_price_id' => $advertise_price_id,
            'menu1_id' => $menu1_id,
            'menu2_id' => $menu2_id,
            'menu1_click' => $menu1_click,
            'menu2_click' => $menu2_click,
            'showing_order' => 0,
            'modified_date' => $this->timestamp,
            'created_by' => $created_by,
            "client_url"=>$client_url
        );
        $this->admin_model->update_advertise($main_ad_id,$updateData);
        //---- default date filters
        redirect('admin/list_advertise');

    }

	public function view_advertise($id){

		//echo "<pre>";print_r($this->admin_model->get_advertise_detail_by_advertise_id($id));die (); echo "</pre>";
		
       $data['obj'] =$this->admin_model->get_advertise_detail_by_advertise_id($id);
		$this->load->view('users/admin/advertise_view', $data);

	}
	
	public function list_inactive_advertise(){
        $site_server_ads_data = array();
        $site_servers = $this->admin_model->get_all_site_servers();
//        echo "<pre>";print_r($site_servers);die;



        if(!empty($site_servers)){
            foreach ($site_servers as $site_server) {
                $site_server_url_exist = $this->general->urlExists($site_server->site_server_url."v1/");
                if(!$site_server_url_exist) continue;

                $site_server_url = $site_server->site_server_url;								
                $handle = curl_init();
                curl_setopt_array($handle, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $site_server_url."v1/advertise/get_inactive_advertise"
                ));

                $res = curl_exec($handle);
                curl_close($handle);

                $site_server_ads_data = json_decode($res);
            }
        }
		         // echo '<pre>';
                    // print_r($site_server_ads_data);
					// die();
                    // echo '</pre>';

		
		$newSite_server_ads=array();
		//$advertise_publish = "";
		$rVal = 0;
		$index = 0;
		if(!is_null($site_server_ads_data) && !empty($site_server_ads_data->inactive_advertise_list))
		{
			foreach($site_server_ads_data as $row1)
			{
				
				foreach ($row1 as $row) {
				
				$advertise_publish_main = $this->admin_model->get_advertise_publish($row->site_server_code, $row->site_advertise_id);
				//$share_to_click_publish = $this->admin_model->get_share_to_click_publish($row->site_server_code, $row->site_advertise_id);
				//array_push($newSite_server_ads,$menu2_id);

				if( $advertise_publish_main == 0)
				   {
					   $rVal = 0;
				   }else{
					   $rVal = 1;
					   unset($row1[$index]);
				   }
				  $index =$index+1;
				}

				 $site_server_ads_data=$row1;
			}
		}
		
				         // echo '<pre>';
                    // print_r($site_server_ads_data);
					// die();
                    // echo '</pre>';
		
		
        $data['obj'] = $site_server_ads_data;
        //echo "<pre>";print_r($data);die("here");
		$this->load->view('users/admin/inactive_advertise_list', $data);

	}
	public function enable_advertise($advertise_id,$site_server_advertise_id,$site_server_code)
	{
		$main_ad_id=$advertise_id;
		$created_by=$this->session->userdata('userID');

		   // echo '<pre>';
                    // print_r($site_server_code);
					// die();
                    // echo '</pre>';
		$site_server_url = $this->general->getSiteServerURL($site_server_code);
		$handle = curl_init();
        curl_setopt_array($handle, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $site_server_url."v1/advertise/active_advertise/".$site_server_advertise_id
        ));
        $res = curl_exec($handle);
        curl_close($handle);
        $resData = json_decode($res);

		
		
		
        $updateData = array(
            'advertise_publish' => 1,
            'share_to_click_publish' => 1,
            'paid_to_click_publish' => 1,
            'modified_date' => $this->timestamp,
            'created_by' => $created_by
        );
		
		
		//Update client_share
		$advertise_price_id = $this->admin_model->getAddpriceID($site_server_code,$site_server_advertise_id);
		$price_sell = $this->admin_model->getSell_price($advertise_price_id);
		$this->update_cash_transaction($price_sell);
		

        $this->admin_model->update_advertise($main_ad_id,$updateData);
        redirect('admin/list_inactive_advertise');
		
	}
	
	public function list_advertise(){
        $site_server_ads_data = array();
        $site_servers = $this->admin_model->get_all_site_servers();
//        echo "<pre>";print_r($site_servers);die;
        if(!empty($site_servers)){
            foreach ($site_servers as $site_server) {
                $site_server_url_exist = $this->general->urlExists($site_server->site_server_url."v1/");
                if(!$site_server_url_exist) continue;

                $site_server_url = $site_server->site_server_url;
                $handle = curl_init();
                curl_setopt_array($handle, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $site_server_url."v1/advertise/advertise"
                ));

                $res = curl_exec($handle);
                curl_close($handle);

                $site_server_ads_data = json_decode($res);
            }
        }
        $data['obj'] = $site_server_ads_data;
       // echo "<pre>";print_r($data);echo "</pre>";die();
		$this->load->view('users/admin/advertise_list', $data);

	}

    public function delete_advertise($main_advertise_id)
    {
        $site_server_code = $this->general->get_onefieldvaluebyid('advertise', 'advertise_id', $main_advertise_id, 'site_server_code');
        $site_server_url = $this->general->getSiteServerURL($site_server_code);
        $site_server_advertise_id = $this->general->get_onefieldvaluebyid('advertise', 'advertise_id', $main_advertise_id, 'site_server_advertise_id');
		
        $handle = curl_init();
        curl_setopt_array($handle, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $site_server_url."v1/advertise/del_advertise/".$site_server_advertise_id
        ));
        $res = curl_exec($handle);
        curl_close($handle);
        $resData = json_decode($res);

		// echo '<pre>';
		////print_r("site_server_code=".$site_server_code." site_server_url=".$site_server_url." site_server_advertise_id= ".$site_server_advertise_id);
		
		// print_r($site_server_url."v1/advertise/del_advertise/".$site_server_advertise_id );
		// die();
		// echo '</pre>';
		
        $updateData = array('advertise_publish' => 0);
        $this->admin_model->update_advertise($main_advertise_id,$updateData);
        redirect('admin/list_advertise');
    }
	
	/*
	 * This function helps preparing $_FILE var for upload
	 */
	
	private function _prepareFileUploadArray($filesArr) {
		$processesFileArray = array();
		for ($c = 0; $c < count($filesArr['name']); $c++) {
			if($filesArr['name'][$c]!="") {
				$processesFileArray[$c]['name'] = $filesArr['name'][$c];
				$processesFileArray[$c]['type'] = $filesArr['type'][$c];
				$processesFileArray[$c]['tmp_name'] = $filesArr['tmp_name'][$c];
				$processesFileArray[$c]['error'] = $filesArr['error'][$c];
				$processesFileArray[$c]['size'] = $filesArr['size'][$c];
			}
		}
		return $processesFileArray;
	}
	
	// ====================================================== For Common Function =======================================================//
	public function uploadimageadncreateThumbnail($fileName, $foldername, $thumbwidth, $thumbheight) {
		$config['upload_path'] = $foldername; // NB! create this dir! 
		$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
		$config['max_size']  = '0';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
      	// Load the upload library 
		$this->load->library('upload', $config);

		// Create the config for image library (pretty self-explanatory)
      	$configThumb = array();
      	$configThumb['image_library'] = 'gd2';
      	$configThumb['source_image'] = '';
      	$configThumb['create_thumb'] = TRUE;
      	$configThumb['maintain_ratio'] = TRUE;
		// Set the height and width or thumbs. Do not worry - CI is pretty smart in resizing.
		// Thumbs will be saved in same upload dir but diff name e.g. 'image.jpg' thumb would be called 'image_thumb.jpg' 
		
     	$configThumb['width'] = $thumbwidth;
      	$configThumb['height'] = $thumbheight;
      	
		// Load the image library 
      	$this->load->library('image_lib');
     
		$upload_image = $this->upload->do_upload($fileName); 
	  	if($upload_image === FALSE){
			return false;
		}
	  	else{
	  		$data = $this->upload->data();
			// If the file is an image - create a thumbnail 
        	if($data['is_image'] == 1) {
          		$configThumb['source_image'] = $data['full_path'];
          		$this->image_lib->initialize($configThumb);
          		$this->image_lib->resize();			
        	}
			return $data;
	  	}
	}
	
	public function tablerow_remove($tablename, $tableidname, $tableidvalue,$classname, $returnpage){
		
		$delete_tablerow = $this->Common_model->deletetable_row($tablename, $tableidname, $tableidvalue);
			
		if($delete_tablerow){
			redirect("/$classname/$returnpage/delete_success/");
		}
		else{
			redirect("/$classname/$returnpage/couldnot_delete/");
		}
	}
	
	public function update_onetablefield($table_name, $table_id, $table_idvalue, $update_field_name, $update_field_value, $classnname, $redirectfunction){
		$data = array($update_field_name => $update_field_value);				
		$updatetableonefield = $this->Common_model->update_table($table_name, $table_id, $table_idvalue, $data);
		
		if($updatetableonefield){
			redirect('/'.$classnname.'/'.$redirectfunction.'/update_success');
		}
		else{
			redirect('/admin/');
		}		
	}
	
	public function update_tableshow_orderfield($table_name, $table_id, $table_idvalue, $update_field_name, $currentorderval,  $update_ordervalue, $classnname, $redirectfunction){
		$updatetableonefield = $this->Common_model->update_ordertable($table_name, $table_id, $table_idvalue, $update_field_name, $currentorderval, $update_ordervalue);
		
		if($updatetableonefield>0){
			redirect('/'.$classnname.'/'.$redirectfunction.'/update_success');
		}
		else{
			redirect('/admin/');
		}		
	}
	
	public function update_showing_orderbycategory($table_name, $category_idname, $category_idvalue, $table_id, $table_idvalue, $update_field_name, $currentorderval,  $update_ordervalue, $classnname, $redirectfunction){
		$updatetableonefield = $this->Common_model->update_ordertablebycategory($table_name, $category_idname, $category_idvalue, $table_id, $table_idvalue, $update_field_name, $currentorderval, $update_ordervalue);
		
		if($updatetableonefield>0){
			redirect('/'.$classnname.'/'.$redirectfunction.'/update_success');
		}
		else{
			redirect('/admin/');
		}		
	}
	//======================================================For Global Config =========================================================//
	public function site_global_config(){
		if(!$this->session->userdata('user_id')){
			redirect('/admin/');
		}
		
		$admin_data['commonmodel'] = $this->Common_model;
		$admin_data['title'] = 'Admin Panel Global Config Page';
		if($this->session->userdata('global_config_id')){
			$admin_data['admin_pagemiddle'] = 'admin/global_config';
			$this->load->view('admin/admin_template',$admin_data);			
		}
		else{
			if($this->Common_model->get_global_config()){
				$admin_data['admin_pagemiddle'] = 'admin/global_config';
				$this->load->view('admin/admin_template',$admin_data);	
			}
			else{
				redirect('/admin/');
			}						
		}
	}
	public function save_global_config_form(){
		$admin_data['commonmodel'] = $this->Common_model;
		$this->form_validation->set_rules('site_name','Site Name','trim|required|min_length[2]');
		$this->form_validation->set_rules('site_email','Site Email','trim|required||valid_email');
		$this->form_validation->set_rules('site_address','Site Address','trim|required');
		$this->form_validation->set_rules('site_telephone','Site Telephone','trim|required');
		$this->form_validation->set_rules('site_mobile','Site Mobile','trim|required');
		$this->form_validation->set_rules('site_fax','Site Fax','trim|required');
		$this->form_validation->set_rules('site_paypal_email','Site Paypal Email','trim|required');
		$this->form_validation->set_rules('site_bank_name','Site Bank Name','trim|required');
		$this->form_validation->set_rules('site_branch_name','Site Branch Name','trim|required');
		$this->form_validation->set_rules('site_account_no','Site Account No','trim|required');
		$this->form_validation->set_rules('site_seo_title','Site SEO Title','trim|required');
		$this->form_validation->set_rules('site_seo_description','Site SEO Description','trim|required');
		$this->form_validation->set_rules('site_seo_keywords','Site SEO Keywords','trim|required');
		
			if($this->form_validation->run()==FALSE){
				$admin_data['title'] = 'Admin Panel Global Config Page';
				$admin_data['admin_pagemiddle'] = 'admin/global_config';
				$this->load->view('admin/admin_template',$admin_data);	
			}
			else{
				$site_name = $this->input->post('site_name');			
				$site_email = $this->input->post('site_email');
				$site_address = $this->input->post('site_address');
				$latlongaddress = str_replace(' ','+',$site_address);
				$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$latlongaddress.'&sensor=false');

				$output= json_decode($geocode);
				$site_latitude = $output->results[0]->geometry->location->lat;
				$site_longitude = $output->results[0]->geometry->location->lng;

				$site_telephone = $this->input->post('site_telephone');
				$site_mobile = $this->input->post('site_mobile');
				$site_fax = $this->input->post('site_fax');			
				$site_paypal_email = $this->input->post('site_paypal_email');			
				$site_bank_name = $this->input->post('site_bank_name');			
				$site_branch_name = $this->input->post('site_branch_name');			
				$site_account_no = $this->input->post('site_account_no');			
				$site_seo_title = $this->input->post('site_seo_title');
				$site_seo_description = $this->input->post('site_seo_description');
				$site_seo_keywords = $this->input->post('site_seo_keywords');
				
				$data = array( 	'site_name ' => $site_name ,
								'site_email ' => $site_email,
								'site_address' => $site_address,
								'site_latitude' => $site_latitude,
								'site_longitude' => $site_longitude,
								'site_telephone' => $site_telephone,
								'site_mobile' => $site_mobile,
								'site_fax' => $site_fax,
								'site_paypal_email' => $site_paypal_email,
								'site_bank_name' => $site_bank_name,
								'site_branch_name' => $site_branch_name,
								'site_account_no' => $site_account_no,
								'site_seo_title' => $site_seo_title,
								'site_seo_description' => $site_seo_description,
								'site_seo_keywords' => $site_seo_keywords);
				
				$global_config_id = $this->Common_model->update_table('global_config','global_config_id', 1 , $data);
				if($global_config_id){
					
					$site_email_logoarray = $this->Common_model->uploadimageadncreateThumbnail('site_email_logo', './application/views/admin/global_images/',250,100);
					$site_email_logo = $site_email_logoarray['file_name'];
					$site_email_logo_thumbimage = $site_email_logoarray['raw_name'].'_thumb'.$site_email_logoarray['file_ext'];
					if($site_email_logo !=''){
						
						$global_config_row = $this->Common_model->getonerowallvaluebycondition('global_config', array('global_config_id'=>$global_config_id));
						if($global_config_row){							
							$old_site_email_logo = $global_config_row->site_email_logo;
							$old_site_email_logopath = './application/views/admin/global_images/'.$old_site_email_logo;
							$old_site_email_logo_thumbimage = $global_config_row->site_email_logo_thumbimage;
							$old_site_email_logo_thumbimagepath = './application/views/admin/global_images/'.$old_site_email_logo_thumbimage;
							
							$removeimage = 'Not OK';
							if($old_site_email_logo !='' && $old_site_email_logo_thumbimage !='')
								if(unlink($old_site_email_logopath) && unlink($old_site_email_logo_thumbimagepath))
									$removeimage = 'OK';
						}
						
						$data = array('site_email_logo' => $site_email_logo,
						   			'site_email_logo_thumbimage' => $site_email_logo_thumbimage
									);						
						$update_photo_gallery_form = $this->Common_model->update_table('global_config', 'global_config_id',$global_config_id, $data);
					}					
					redirect('/admin/site_global_config/update_success');
				}
			}

		}
	// ====================================================== For User Component =======================================================//
	
	
	
	public function forgotpass(){
		$admin_data['commonmodel'] = $this->Common_model;
		$this->form_validation->set_rules('fuser_email','Email','trim|required|valid_email|min_length[6]');
		
		if($this->form_validation->run()==FALSE){
			$admin_data['title'] = 'Admin Panel Home Page';
			$admin_data['err_message'] = 'Your email address is invalid. Please type right email address.';

			$admin_data['admin_pagemiddle'] = 'admin/home';
			$this->load->view('admin/admin_template',$admin_data);	
		}
		else{
			
			$user_email = $this->input->post('fuser_email');
			
			$conditionarray = array('user_email' => $user_email, 'user_published' => 1);
			$user_login = $this->Common_model->getallrow('user', $conditionarray);
			if($user_login){
				$newdata = '';
				$user_email = '';
				foreach($user_login as $row){						
					$user_email = $row->user_email;
					$user_name = $row->user_name;
					$user_password = $row->user_password;
					$user_fullname = $row->user_fullname;					
				}
				
				$this->load->library('email');
				
				if($user_email !=''){
					if($this->session->userdata('site_email_logo')){
						$sitelogo = site_url('/').'application/views/admin/global_images/'.$this->session->userdata('site_email_logo');
						$sitename = $this->session->userdata('site_name');
						$siteaddress = $this->session->userdata('site_address');
						$sitemobile = $this->session->userdata('site_telephone');
						$sitetelephone = $this->session->userdata('site_mobile');
						$sitefax = $this->session->userdata('site_fax');
						$siteemail = $this->session->userdata('site_email');
						
					}
					else{
						$global_data = $this->Common_model->get_global_configdata();
						$site_name = $global_data['site_name'];
						$site_email = $global_data['site_email'];
						$site_address = $global_data['site_address'];
						$site_telephone = $global_data['site_telephone'];
						$site_mobile = $global_data['site_mobile'];	
						$site_fax = $global_data['site_fax'];		
						$site_email_logo_thumbimage = $global_data['site_email_logo_thumbimage'];
					}
					$admin_user_email = $this->Common_model->get_onefieldnamebyid('user', 'user_id', 1, 'user_email');					
					
					$mail_body = "<html>
					<body>
					<table align=\"center\" style=\"border:1px solid #CCCCCC; padding:2px; background:#f3f3f3\" width=\"700\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td>
							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
							  <tr>
								<td width=\"300\"><img src=\"$sitelogo\" /></td>
								<td>
									&nbsp;
								</td>
								<td width=\"200\">
									<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#003300\">
										$sitename
									</span><br />
									<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#003300\">
										$siteaddress
									</span><br />
									<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#003300\">
										Mobile : $sitemobile
									</span><br />
									<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:normal; color:#003300\">
										Tel: $sitetelephone
									</span><br />
									<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:normal; color:#003300\">
										Fax: $sitefax
									</span><br />
									<a href=\"mailto:demo@mail.com\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:normal; color:#003300\">
										Email: $siteemail
									</a><br />				
								</td>
							  </tr>
							</table>
						</td>
					  </tr>
					  <tr>
						<td align=\"center\">
							<table style=\"padding:15px; background:#ffffff\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
								<tr>
									<td style=\"text-align:left; color:#000000; font-size:12px; font-weight:bold;padding-left:10px;\">
										  Dear $user_fullname,
									</td>
								</tr>
								<tr>
									<td style=\"font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:#000000; line-height:20px;\" align=\"left\">
										<strong>Welcome to $sitename -Admin panel forgot password information</strong><br />
										<br />
										Please try to login with the following User name and password:
									</td>
								</tr>
								<tr>
									<td style=\"background:#E9F0F5;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:#000000; line-height:20px; padding:15px;\" align=\"left\">
										Your user name and password is <br />
										User name: $user_name
										<br />
										Password: $user_password
									</td>
								</tr>
								<tr>
									<td style=\"font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:#000000; line-height:20px;\" align=\"left\">
										Please click on the link below to login.<br />
										<br />
										<a href=\"".site_url('/')."admin/\">Click Here</a>
									</td>
								</tr>
								<tr>
									<td style=\"font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:#000000; line-height:20px;\" align=\"left\">
										<br />
										Sincerely,<br />
										The $sitename Team
									</td>
								</tr>
							</table>
						</td>	
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					</table>
					</body>
					</html>
					";
					
					$this->email->clear();
					$config['mailtype'] = "html";
					$this->email->initialize($config);
					$this->email->set_newline("\r\n");
					
					$this->email->from($admin_user_email, 'Administrator');
					$this->email->to($user_email);
					$this->email->cc('mahehasan@gmail.com');
					
					$this->email->subject("Forgot password mail for $customer_name");
					$this->email->message($mail_body);				
					$this->email->send();
					redirect('/admin/index/forgot_successmail');
				}
				else{
					$admin_data['err_message'] = 'Your email didnot found. Please type right email address.';
					$admin_data['title'] = 'Admin User Login Page';
					$admin_data['admin_pagemiddle'] = 'admin/home';
					$this->load->view('admin/admin_template',$admin_data);
				}
			}
			else{
				$admin_data['err_message'] = 'Your email didnot found. Please type right email address.';
				$admin_data['title'] = 'Admin User Login Page';
				$admin_data['admin_pagemiddle'] = 'admin/home';
				$this->load->view('admin/admin_template',$admin_data);				
			}
		}
	}
	//======================================================For Price =========================================================//		
	public function price_list()
	{
		$data['price_list'] = $this->admin_model->getAllPriceList();
		$this->load->view('users/admin/price_list',$data);
	}

	public function add_price()
	{
		$this->load->view('users/admin/price_add');
	}

	public function edit_price($id)
	{
		$data['price_detail']  = $this->admin_model->getPriceById($id);
		$data['edit']  = true;
		$this->load->view('users/admin/price_add',$data);
	}

	public function delete_price($id)
	{
		$data['price_detail']  = $this->admin_model->deletePrice($id);
		redirect('admin/price_list');
	}

	public function insert_price()
	{

		$this->form_validation->set_rules('price_title','Title','trim|required');
		$this->form_validation->set_rules('price_description','Description','trim|required');
		$this->form_validation->set_rules('price_sell','Selling Price','trim|required|numeric');
		$this->form_validation->set_rules('price_advertise','Advertise Price','trim|required|numeric');
		if($this->form_validation->run()==FALSE){
			$this->load->view('users/admin/price_add');
		}else{
			$data = array(
				'price_title' =>  $this->input->post('price_title'),
				'price_description' =>  $this->input->post('price_description'),
				'price_sell' =>  $this->input->post('price_sell'),
				'price_advertise' =>  $this->input->post('price_advertise')
			 );
			if($this->input->post('price_id')){
				$this->admin_model->UpdatePrice($this->input->post('price_id'), $data);
			}else{
				$this->admin_model->InsertPrice($data);
			}
			redirect('admin/price_list');
		}
	}

	//======================================================For Product =========================================================//		
	public function product_list()
	{
		$data['product_list'] = $this->admin_model->getAllProductList();
		$this->load->view('users/admin/product_list',$data);
	}

	public function add_product()
	{
		$data['price_list'] = $this->admin_model->getAllPriceList();
		$data['category_list'] = $this->admin_model->get_all_product_category();
		$data['company_list'] = $this->admin_model->get_all_product_company_list();
		$this->load->view('users/admin/product_add',$data);
	}

	public function edit_product($id)
	{
		$data['product_detail']  = $this->admin_model->getProductById($id);
		$data['category_list'] = $this->admin_model->get_all_product_category();
		$data['company_list'] = $this->admin_model->get_all_product_company_list();
		$data['edit']  = true;
		$this->load->view('users/admin/product_add',$data);
	}

	public function delete_product($id)
	{
		$this->admin_model->deleteProduct($id);
		redirect('admin/product_list');
	}

	public function insert_product()
	{//'upload/images/products/thumbs'
	// var_dump($_POST);
	// print_r($_FILES);
		$this->form_validation->set_rules('product_name','Name','trim|required');
		$this->form_validation->set_rules('product_quantity','Quantity','trim|required');
		$this->form_validation->set_rules('product_discount','Discount','trim|required');
		$this->form_validation->set_rules('product_discount_percentage','Discount Percentage','trim|required');
		$this->form_validation->set_rules('short_description','Description','trim|required');
		if($this->form_validation->run()==FALSE){
			// $this->load->view('users/admin/price_add');
			echo validation_errors();
		}else{
			$config['upload_path'] = './upload/images/products';
			$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config['max_size']	= '2048';
			$config['max_width']  = '4000';
			$config['max_height']  = '4000';
			$config['encrypt_name']  = true;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());
				print_r($error);
			}
			else
			{ //upload success
				$data =  $this->upload->data();
				// print_r($data);
				$this->resize_image('/upload/images/products/'.$data['file_name'], '/upload/images/products/thumbs/', 250, 250);
				//make these file names
				$filename = 'upload/images/products/'.$data['file_name'];
				$thumbname = 'upload/images/products/thumbs/'.$data['raw_name'].'_thumb'.$data['file_ext'];

				$data = array(
					'product_name' =>  $this->input->post('product_name'),
					'product_itemid' =>  0,
					'product_category_id' =>  $this->input->post('product_category_id'),
					'product_company_id' =>  $this->input->post('product_category_id'),
					'product_brand_id' =>  0,
					'product_quantity' =>  $this->input->post('product_quantity'),
					'product_price' =>  $this->input->post('product_price'),
					'product_shares' =>  $this->input->post('product_shares'),
					'product_discount' =>  $this->input->post('product_discount'),
					'product_discount_percentage' =>  $this->input->post('product_discount_percentage'),
					'product_special' =>  $this->input->post('product_special'),
					'best_sellers' =>  $this->input->post('best_sellers'),
					'comming_soon' =>  $this->input->post('comming_soon'),
					'product_image' =>  $filename,
					'product_thumbimage' =>  $thumbname,
					'product_thumbimage1' =>  $thumbname,
					'short_description' =>  $this->input->post('short_description'),
					'product_description' =>  $this->input->post('product_description'),
					'product_sellscount' =>  0,
					'showing_order' =>  0					
				 );
				if($this->input->post('product_id')){
					//delete existing image
					$this->admin_model->delete_product_image($this->input->post('product_id'));
					$this->admin_model->UpdateProduct($this->input->post('product_id'), $data);
				}else{
					$this->admin_model->InsertProduct($data);
				}
				redirect('admin/product_list');
			}

		}
	}

	public function list_product_category(){
		$data['category_list'] = $this->admin_model->get_all_product_category();
		//---- default date filters
		$this->load->view('users/admin/product_categories', $data);
	}

	public function add_product_category(){
		$data['n'] = false;
		if (!empty($_POST)){
			$this->form_validation->set_rules('category_title','Title','trim|required');
			$postData = $this->input->post(NULL, TRUE);
			if (isset($postData['product_category_id'])) {
				$data = array(
						'category_title' => $postData['category_title'],
						'category_description' => $postData['category_description']
				);
				$update_category_form = $this->admin_model->update_product_category($postData['product_category_id'],$data);
				if($update_category_form){
					$this->general->redirect('admin/list_product_category');
				}
			}else{
				$data = array(
						'category_title' => $postData['category_title'],
						'category_description' => $postData['category_description'],
						'category_publish' => 1,
						'created_date' => $this->timestamp,
						'modified_date' => $this->timestamp,
						'created_by' => $this->adminID
				);
					
				$product_category_id = $this->admin_model->add_product_category($data);
				if($product_category_id>0){
					$this->general->redirect('admin/list_product_category');
				}
			}
		}
		//---- default date filters
		$this->load->view('users/admin/add_product_category', $data);
	}

	public function edit_product_category($category_id){
		$data['product_category_id'] = $category_id;
		$data['category_detail'] = $this->admin_model->get_product_category_by_id($category_id);
		$this->load->view('users/admin/add_product_category', $data);
	}

	public function delete_product_category($category_id){
		$this->admin_model->delete_product_category($category_id);
		redirect('admin/list_product_category');
	}

	public function manage_client_advertise(){
		$data['advertise_disabled_list'] = $this->admin_model->get_all_manage_client_advertise();
		$this->load->view('users/admin/manage_client_advertise', $data);
	}

	public function advertise_enable($ad_id){
		$data['advertise_disabled_list'] = $this->admin_model->enable_client_advertise($ad_id);
		redirect('admin/manage_client_advertise');
	}

	public function product_company(){
		$data['product_company_list'] = $this->admin_model->get_all_product_company_list();
		$this->load->view('users/admin/product_company', $data);
	}

	public function add_product_company(){
		$data = '';
		if (!empty($_POST)){
			$this->form_validation->set_rules('company_name','Company Name','trim|required');
			if($this->form_validation->run()==TRUE){
				$postData = $this->input->post(NULL, TRUE);
				if (isset($postData['product_company_id'])) {
					$data = array(
							'company_name' => $postData['company_name'],
							'company_description' => $postData['company_description']
					);
					$update_company_form = $this->admin_model->update_product_company( $postData['product_company_id'],$data);
					if($update_company_form){
						$this->general->redirect('admin/product_company');
					}
				}else{
					$data = array(
							'company_name' => $postData['company_name'],
							'company_description' => $postData['company_description'],
							'created_date' => $this->timestamp
					);
						
					$product_company_id = $this->admin_model->add_product_company($data);
					if($product_company_id>0){
						$this->general->redirect('admin/product_company');
					}
				}
			}//validation
		}
		$this->load->view('users/admin/add_product_company', $data);
	}

	public function edit_product_company($id){
		$data['product_company_id'] = $id;
		$data['cpmpany_detail'] = $this->admin_model->get_product_company_by_id($id);
		$this->load->view('users/admin/add_product_company', $data);
	}

	public function delete_product_company($id){
		$this->admin_model->delete_product_company($id);
		redirect('admin/product_company');
	}

	public function referral_add(){
		if(isset($_POST)){
			$this->form_validation->set_rules('parent_id','Referred By(Owner)','trim|required|greater_than[0]');
			$this->form_validation->set_rules('child_id','Referral','trim|required|greater_than[0]');
			if($this->form_validation->run()==TRUE){
				$data = array('parent_id' => $this->input->post('parent_id'),
					'child_id' =>  $this->input->post('child_id'));
				if($this->input->post('parent_id') < $this->input->post('child_id'))
				{
				$this->admin_model->add_referral($data);
				redirect('admin/referral_add');
				}
				else {
					echo "Could not add referral, child id cannot smaller than parent id.";
				}
			}
		}
		$data['user_list'] = $this->admin_model->get_all_users();
		$this->load->view('users/admin/referral_add', $data);
	}

	public function configuration(){
		$data['configuration_list'] = $this->admin_model->get_all_configuration();
		$this->load->view('users/admin/configuration_list', $data);
	}

	public function add_configuration(){
		$data['nothing'] = false;
		if (!empty($_POST)){
			$this->form_validation->set_rules('property_name','Name','trim|required');
			$this->form_validation->set_rules('property_value','Value','trim|required');
			$postData = $this->input->post(NULL, TRUE);
			if (isset($postData['configuration_id'])) {
				$data = array(
						'property_name' => $postData['property_name'],
						'property_value' => $postData['property_value']
				);
				$update_category_form = $this->admin_model->update_configuration( $postData['configuration_id'], $data);
				if($update_category_form){
					$this->general->redirect('admin/configuration');
				}
			}else{
				$data = array(
						'property_name' => $postData['property_name'],
						'property_value' => $postData['property_value']
				);
					
				$product_category_id = $this->admin_model->add_configuration($data);
				if($product_category_id>0){
					$this->general->redirect('admin/configuration');
				}
			}
		}
		$this->load->view('users/admin/add_configuration', $data);
	}

	public function edit_configuration($configuration_id){
		$data['configuration_detail'] = $this->admin_model->get_configuration_by_id($configuration_id);
		$this->load->view('users/admin/add_configuration', $data);
	}

	public function delete_configuration($configuration_id){
		$this->admin_model->delete_configuration($configuration_id);
		redirect('admin/configuration');
	}
	
	public function bonus_list(){
		$data['bonus_list'] = $this->admin_model->get_all_bonus_list();
		$this->load->view('users/admin/bonus_list', $data);
	}

	public function add_bonus_list(){
		$data['nothing'] = false;
		if (!empty($_POST)){
			$this->form_validation->set_rules('amount','Amount','trim|required');
			$this->form_validation->set_rules('fund_amount','Fund Amount','trim|required');
			$postData = $this->input->post(NULL, TRUE);
			if (isset($postData['share_bonus_id'])) {
				$data = array(
						'amount' => $postData['amount'],
						'fund_amount' => $postData['fund_amount']
				);
				$update_category_form = $this->admin_model->update_bonus( $postData['share_bonus_id'], $data);
				if($update_category_form){
					$this->general->redirect('admin/bonus_list');
				}
			}else{
				$data = array(
						'amount' => $postData['amount'],
						'fund_amount' => $postData['fund_amount']
				);
					
				$product_category_id = $this->admin_model->add_bonus($data);
				if($product_category_id>0){
					$this->general->redirect('admin/bonus_list');
				}
			}
		}
		$this->load->view('users/admin/add_bonus_list', $data);
	}

	public function edit_bonus_list($id){
		$data['bonus_detail'] = $this->admin_model->get_bonus_by_id($id);
		$this->load->view('users/admin/add_bonus_list', $data);
	}

	public function delete_bonus_list($id){
		$this->admin_model->delete_bonus($id);
		redirect('admin/bonus_list');
	}

	public function check_users_ip($userid)
	{
		$data['list']=$this->admin_model->get_users_ip($userid);
		$this->load->view('users/admin/ip_list', $data);
	}

	public function enable_disable_user($id)
	{
		$this->admin_model->enable_disable_user($id);
		redirect('admin/manage_users');
	}

	public function last_daily_earnings()
	{
		$id = $this->session->userData('userID');
		$max_transaction_history_date= $this->admin_model->get_max_transaction_history_date($id);
		$data['daily_earnings_total'] = $this->admin_model->get_last_date_transaction_amount($id,strtotime($max_transaction_history_date));
		
		//Previous code
		//$data['daily_earnings'] = $this->admin_model->get_last_daily_earnings();
		//$data['daily_earnings_total'] = $this->admin_model->get_system_total_daily_earnings();
		
		$data['daily_earnings'] = $this->admin_model->get_system_daily_transaction_history_by_pivot($id);
		$this->load->view('users/admin/last_daily_earnings_admin',$data);
	}
	
	public function system_earnings()
	{
		$data['id'] = $id = $this->session->userData('userID');
		//$data['id']=$id =4;

		$data['cash_amount'] = $this->transaction_update_scheduler_model->get_cash_amount_transaction_main($id);
		$data['share_amount'] = $this->transaction_update_scheduler_model->get_shares_amount_transaction_main($id);
		$data['gifted_shares_Amount'] = $this->transaction_update_scheduler_model->get_gifted_shares_Amount($id);
		
		$data['transaction_history'] = $this->transaction_update_scheduler_model->user_transaction_history_existenceLimit($id);
		
		
			
			$this->load->view('users/admin/system_earnings',$data);
	}
	public function system_detail_earnings()
	{
		$data['id'] = $id = $this->session->userData('userID');
		//$data['id']=$id =4;

		$data['cash_amount'] = $this->transaction_update_scheduler_model->get_cash_amount_transaction_main($id);
		$data['share_amount'] = $this->transaction_update_scheduler_model->get_shares_amount_transaction_main($id);
		$data['gifted_shares_Amount'] = $this->transaction_update_scheduler_model->get_gifted_shares_Amount($id);
		
		$data['transaction_history'] = $this->transaction_update_scheduler_model->user_transaction_history_existence($id);
		
		
			
			$this->load->view('users/admin/system_detail_earnings',$data);
	}
	public function site_servers(){
		$data['site_servers'] = $this->admin_model->get_all_site_servers();
		$this->load->view('users/admin/site_servers', $data);
	}
				
	//Update client cash after creating advertise
	public function update_cash_transaction($cash_amount){

	    $userID=$this->session->userData('userID');
	    
			//client_shares Paid_To_Click		
			$data2 = array(
				'user_id' => $userID,
				'transaction_message' => "Advertise Add",
				'transaction_amount' => $cash_amount,
				'transaction_history_date' => time()				
			
			);
			$this->db->insert('transaction_history', $data2);

			$share_last_update_date= $this->transaction_update_scheduler_model->get_client_cash_history_date($userID);
			//$gifted_amount= $this->transaction_update_scheduler_model->get_gifted_shares_Amount($userID);
			$main_cash_amount= $this->transaction_update_scheduler_model->get_cash_Amount_transaction_history_for_admin($userID,time());
			
			//echo '<pre>';print_r($gifted_amount);echo '</pre>';	die();
			//transaction_main

			$data3 = array(
					'cash_amount' => $main_cash_amount,
					'share_last_update_date' => time()
			);

			 $this->db->where('user_id', $userID);
			 $this->db->update('transaction_main', $data3);

	}
	
	
	//admin shareing his share to all user
	public function add_share_amount(){
		
		$ip_address = "";
		
		$userID=$this->session->userData('userID');
		//$userID=4;
		
		if($this->session->userdata('ip_address'))
		{
			$ip_address = $this->session->userdata('ip_address');
		}
		else
		{
			$ip_address = $_SERVER['REMOTE_ADDR'];
		}
		$all_user = $this->admin_model->get_all_user_data_admin();
		$share_last_update_date= $this->transaction_update_scheduler_model->get_client_shares_latest_date($userID);
		$main_share_amount= $this->transaction_update_scheduler_model->get_client_shares_Amount_First($userID,$share_last_update_date);
		
		$numberofuser=count($all_user);
		$givenamount = $this->input->post('share_amount');
		$amountforAll=$givenamount/$numberofuser ;
		
		if( $main_share_amount>$givenamount&&  $givenamount>0 && $givenamount!="" && is_numeric($givenamount))
		{
		  if(!empty($all_user)){
			foreach ($all_user as $user){
				
				$data = array(
				//'site_server_code' => $site_server_code,
				//'site_server_advertise_id' => $site_advertise_id,
				'client_id' => $user->userID,
				'shares_amount' => round($amountforAll, 2),
				'client_shares_publish' => 1,
				'click_date' => time(),
				'ip_address' => $ip_address
				);
				$inserted_row_id = $this->mybitshares_model->insert_in_client_share($data);
				
				
				//Users History Table
				$data_users = array(
				'user_id' => $user->userID,
				'transaction_message' => "Admin_Gifts",
				'transaction_amount' => round($amountforAll, 2),
				'transaction_history_date' => time()				
			    );
				$this->db->insert('user_transaction_history', $data_users);
				//Main table
				$main_share_amount= $this->user_transaction_update_scheduler_model->get_client_shares_Amount_transaction_history($user->userID,time());
				$data_users_main = array(
					'share_amount' => ($main_share_amount),
					'cash_last_update_date' => time()
				);
				$this->db->where('user_id', $user->userID);
				$this->db->update('user_transaction_main', $data_users_main);
		
		
		  
				
			}
		//All Users History & Main Part	Update
			
			
		//Admin History & Main Part	
		$data2 = array(
			'user_id' => $userID,
			'transaction_message' => "Share_Gifted",
			'transaction_amount' => $givenamount,
			'transaction_history_date' => time()						
			);
		$this->db->insert('transaction_history', $data2);
		$share_last_update_date= $this->transaction_update_scheduler_model->get_client_shares_latest_date($userID);
		$main_share_amount= $this->transaction_update_scheduler_model->get_client_shares_Amount_First($userID,$share_last_update_date);

		$data3 = array(
					'share_amount' => ($main_share_amount-$givenamount),
					'share_last_update_date' => time()
					);
		 $this->db->where('user_id', $userID);
		 $this->db->update('transaction_main', $data3);
		}
		redirect('/admin/system_earnings');		
	  }
		$this->last_daily_earnings();
		
	}
	
	
	

	public function add_site_server(){
		$data = array();
			$this->form_validation->set_rules('site_server_code','Site Server Code','trim|required');
			$this->form_validation->set_rules('site_server_url','Site Server URL','trim|required');
			$postData = $this->input->post(NULL, TRUE);
		if (isset($postData['site_server_id'])) {
			if ($postData['site_server_id']==0) {
				$data = array(
						'site_server_code' => $postData['site_server_code'],
						'site_server_url' => $postData['site_server_url'],
						'publish_flag' => 1
				);
					
				$site_server_id = $this->admin_model->add_site_server($data);
				if($site_server_id>0){
					$this->general->redirect('admin/site_servers');
				}
			}else{
				$data = array(
						'site_server_code' => $postData['site_server_code'],
						'site_server_url' => $postData['site_server_url'],
						'publish_flag' => 1
				);
				$update_site_servers = $this->admin_model->update_site_server($postData['site_server_id'],$data);
				if($update_site_servers){
					$this->general->redirect('admin/site_servers');
				}
			}
		}
		//---- default date filters
		$this->load->view('users/admin/from_site_servers', $data);
	
	}
	public function edit_site_server($site_server_id){
		$data['site_server'] = $this->admin_model->get_site_server_by_id($site_server_id);
		$this->load->view('users/admin/from_site_servers', $data);
	}
	

	
	public function delete_site_server($site_server_id){
		$this->admin_model->delete_site_server($site_server_id);
		redirect('admin/site_servers');
	}
	
	public function check_ips(){
		$data = array();
		$user_ip='';
		//$data['site_servers'] = $this->admin_model->get_all_site_servers();
		$data['user_data_admin'] = $this->admin_model->get_all_user_data_admin();
		// if (is_array($data['user_data_admin']) || is_object($data['user_data_admin']))
		// {	foreach ($data['user_data_admin'] as $row) {
		
		// $user_ip = $user_ip + $this->admin_model->getUsersIPList($row->userID );
		// }
		// }
		//$data['user_ip'] = $this->admin_model->getUsersIPList($id);

		// echo'<pre>'; print_r($user_ip);echo'</pre>'; die();
		
		$this->load->view('users/admin/check_ips', $data);
	}
	
	public function process_check_ips()
	{
		
		if($this->input->post('ip_address'))
		{
			//echo "hi";
			$ip_list = $this->input->post('ip_address'); // this is a comma seperated list
			
			$ip_list2 = explode(",", $ip_list);
			
			$data = $this->admin_model->process_check_ips($ip_list2);
			$data2 = array();
			
			$data2["check_ips"] = $data;
			
			//echo $data[0]->firstName;
			
			$this->load->view('users/admin/view_ip_check', $data2);
			
			
		}
	}
	
	public function grant_users($total_income_24hr = 0, $income_id = 0)
	{
		//$all_users_income = $this->cron_model->get_all_users_last_24_hours_income();
		//$total_income_24hr=0;
		//foreach ($all_users_income as $key => $value) {
		//	$total_income_24hr+=$value;
		//}
		
		
		$system_share_percentage = $this->cron_model->get_daily_income_system_share();
		
		//echo $system_share_percentage;
		//$this->cron_model->update_system_daily_income($total_income_24hr);
		$system_share = ($total_income_24hr*$system_share_percentage)/100;
		$total_shares_of_system = $this->cron_model->get_total_shares_of_system();
		$per_share_value = $system_share / $total_shares_of_system;
		$all_shares = $this->cron_model->get_all_shares_of_user();
		foreach ($all_shares as $row) {
			$system_income_bonus = ($row->current_share_number*$per_share_value);
			$this->cron_model->update_system_income_bonus_of_user($row->user_id,$system_income_bonus);
		}
		
		//echo $income_id.'/'.$system_share;
		
		$adjusted_system_income = $total_income_24hr - $system_share;
		$this->cron_model->update_system_daily_income2update_system_daily_income2($income_id, $adjusted_system_income);
		
		$data2 = array();		
					
		$this->load->view('users/admin/view_grant', $data2);
	}

}
?>