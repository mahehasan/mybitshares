<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Referral extends CI_Controller {
	private $timestamp;
	private $adminID;
	private $m_calculate_total = 0;
	private $m_allowed_max_level = 0;
	function __construct()
	{		
		parent::__construct();
		$this->load->library('form_validation');
     	$this->load->helper('form');
		$this->load->model(array('category_model'));
		$this->load->model(array('referral_model'));
		$this->timestamp = time();
		$this->adminID = $this->session->userdata('userID');
		if ($this->adminID!=1) {
			$this->general->redirect( 'logout' );
		}
	}

	public function index(){
		$data = array();
		$this->load->view('users/login',$data);
	}

	public function calculate_referral($user_id)
	{
		$this->m_calculate_total = 0;
		$this->m_allowed_max_level = $this->referral_model->get_max_levels();
		// call method($user_id, 0)  -- 0 for root, no child as default
		$this->calculate_referral_recursive($user_id, 0);
		echo $this->m_calculate_total+$this->referral_model->get_total_clicked_cash($user_id);
	}

	public function calculate_referral_recursive($user_id, $in_track_level)
	{
		// $query = check how many child he has  -- $user_id
		$temp_max_level = $in_track_level+1;
		if($temp_max_level <= $this->m_allowed_max_level)
		{
			$temp_childs_arr = $this->referral_model->get_childs($user_id);
			if (sizeof($temp_childs_arr)>0)
			{
				//has child
				$temp_Track_level = $in_track_level + 1;
				foreach ($temp_childs_arr as $row) 
				{
					$this->calculate_referral_recursive($row->child_id, $temp_Track_level);
				}
			}// else
		}
		// leaf node or done processing all children
		// $temp_total = calculate this user_id price since last date (one day)
		$temp_total = $this->referral_model->get_total_clicked_cash($user_id);
		$temp_price_percentage = $this->referral_model->get_price_percentage_by_level($in_track_level);
		// $temp_total 2 = calculate referral benefit with child level   $in_track_level
		$temp_total_2 = ($temp_total*$temp_price_percentage)/100; 
		// $calculate_total = $calculate_total + $temp_total2
		$this->m_calculate_total = $this->m_calculate_total + $temp_total_2;
		//update this user_id current total by this date  $temp_total
		echo $this->m_calculate_total.'-'.$temp_price_percentage.'%, ';
	}



}
?>