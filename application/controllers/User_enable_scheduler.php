<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_enable_scheduler extends CI_Controller {
   private $timestamp;
   private $adminID;
	function __construct()
	{		
		parent::__construct();
		$this->load->model(array('user_enable_scheduler_model'));
	}
	
	public function enable_scheduler(){

		$enable_time = $this->user_enable_scheduler_model->get_enable_time();
		$time_hour = intval(substr($enable_time,0,strlen($enable_time)-1));
		if ($enable_time[strlen($enable_time)-1] == 'd' || $enable_time[strlen($enable_time)-1] == 'D'){
			$time_hour = $time_hour * 24;
			
		}else if ($enable_time[strlen($enable_time)-1] == 'h' || $enable_time[strlen($enable_time)-1] == 'H'){
			$time_hour = 1;
		}

		$disabled_ads = $this->user_enable_scheduler_model->get_disabled_ads();
		$id = '';
		foreach ($disabled_ads as $row) {
			$click_time = ($row->click_time);
			$timediff = (time())-$click_time;
			$timediff_h = $timediff / 60;
			//$timediff_h = $timediff_h / 60;
			if($timediff_h > $time_hour){
				$id .= $row->manage_client_advertise_id.',';
			}
		}
		$id = substr($id,0,strlen($id)-1);
		
		$this->user_enable_scheduler_model->enable_ads($id);
		

	}

	public function test()
	{
		$date1 = new DateTime('2016-05-20 12:00:49 PM');
		$date2 = new DateTime('2016-05-30 7:15:49 PM');

		 $diff = $date2->diff($date1);
		$hours = $diff->h;
		$date3 =1464613779-1463738449;
		$date4=($date3/60);

		$date4=($date4/60);
		// $diff = $date2->diff($date3);
		echo $date4. ', ';
		// $hours = $diff->h;
		echo $hours.'<br>';
		echo date('Y-m-d h:i:s A', 1463738449).'<br>';
		// echo $hours.'<br>';
		// echo $diff->format('%h');
	}


}
?>