<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mybitshares extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('category_model', 'mybitshares_model', 'referral_model', 'cron_model','admin_model','user_transaction_update_scheduler_model','transaction_update_scheduler_model'));
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
    	$this->load->library('cart');
    	$this->load->library('session');
		$this->session->keep_flashdata('MSGS');
	}

	public function index()
	{
		$data = array();
		if($this->session->userdata('userID')){
			redirect('mybitshares/dashboard');
		}else{
			// $this->load->view('mybitshares/index',$data);
			redirect('mybitshares/dashboard');
		}
	}

	public function dashboard()
	{
		$MSG = $this->session->flashdata('MSGS');
		//echo $MSG;
		if($this->session->userData('guest'))
		{
			$data = array();
			$this->load->view('info/upgrade',$data);
				
		}
		else {
			$data = array();
			$data['MSG'] = $MSG;
			
            $this->load->view('mybitshares/dashboard',$data);
				
		}
	}


	public function profile()
	{
		$id = $this->session->userdata('userID');
		$data['user_details'] = $this->mybitshares_model->get_user_details_by_id($id);
		$data['balance_total'] = $this->mybitshares_model->get_all_balance_total($id);
		$data['share_total'] = $this->mybitshares_model->get_all_share_total($id);
		
		
		
		
		if($this->session->userData('guest'))
		{
			$data = array();
			$this->load->view('info/upgrade',$data);
			
		}
		else {
			$this->load->view('mybitshares/profile',$data);
			
		}
		
		
		
	}

	public function edit_profile()
	{
		$data['edit'] = true;
		if($this->input->post('userID')){
			$this->form_validation->set_rules('firstName','First Name','trim|required');
			$this->form_validation->set_rules('lastName','Last Name','trim|required');
			$this->form_validation->set_rules('town','Town','trim|required');
			// if($this->form_validation->run()==TRUE){
				$this->mybitshares_model->update_user_data($this->input->post('userID'));
				redirect('mybitshares/profile');
			// }
		}
		$id = $this->session->userdata('userID');
		$data['user_detail'] = $this->mybitshares_model->get_user_details_by_id($id);
		$data['country_list'] = $this->mybitshares_model->get_country_list();
		// $data['share_total'] = $this->mybitshares_model->get_all_share_total($id);
		$this->load->view('mybitshares/edit_profile',$data);
	}

	public function click_ad()
	{
		$new_clicked_ad_id = $this->mybitshares_model->ad_clicked();
		echo $new_clicked_ad_id;
	}

	
	public function myCheck()
	{
		$result = array();
		//$qry = "SELECT MAX(click_date) as click_date FROM client_cash WHERE client_cash_publish = 1 AND client_id = 1 ";
		//$qry = "SELECT * FROM client_shares";
		$qry ="SELECT * FROM advertise";
		//$qry = "SELECT * FROM client_cash ";
		$query = $this->db->query($qry);
		$result = $query->result_array();
				echo '<pre>';
            print_r($result);
        echo '</pre>';
		
		// if($result){
			// foreach($result as $arr){
				// $results[$arr['site_server_code']] = $arr;
			// }

		// }else
			// return false;
		

	}

	
	
	public function share_to_click($value='')
	{
		$all_data = array();
		$site_server_ads_data = array();
        $site_servers = $this->admin_model->get_all_site_servers();
//        echo "<pre>";print_r($site_servers);die;
        if(!empty($site_servers)){
            foreach ($site_servers as $site_server) {
                $site_server_url_exist = $this->general->urlExists($site_server->site_server_url."v1/");
                if(!$site_server_url_exist) continue;

                $site_server_url = $site_server->site_server_url;
                $handle = curl_init();
                curl_setopt_array($handle, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $site_server_url."v1/advertise/advertise"
                ));

                $res = curl_exec($handle);
                curl_close($handle);

                $site_server_ads_data = json_decode($res);
            }
        }
       // $data['obj'] = $site_server_ads_data;
		
		$menu_id =0;
		$newSite_server_ads=array();
		//$advertise_publish = "";
		$rVal = 0;
		$index = 0;
		if(!is_null($site_server_ads_data))
		{
			foreach($site_server_ads_data as $row1)
			{
				
				foreach ($row1 as $row) {
				
				$menu1_id = $this->admin_model->getMenu1ID($row->site_server_code, $row->site_advertise_id);
				$menu2_id = $this->admin_model->getMenu2ID($row->site_server_code, $row->site_advertise_id);
				$advertise_publish_main = $this->admin_model->get_advertise_publish($row->site_server_code, $row->site_advertise_id);
				$share_to_click_publish = $this->admin_model->get_share_to_click_publish($row->site_server_code, $row->site_advertise_id);
				//array_push($newSite_server_ads,$menu2_id);

				if(($menu1_id == 4 || $menu2_id == 4 )&& $row->advertise_publish == 1 && $advertise_publish_main == 1 && $share_to_click_publish == 1)
				   {
					   $rVal = 0;
				   }else{
					   $rVal = 1;
					   unset($row1[$index]);
				   }
				  $index =$index+1;
				}

				 $site_server_ads_data=$row1;
			}
		}
		//$site_server_ads_data =$newSite_server_ads;
				 //echo '<pre>';
				    // print_r($site_server_ads_data);
					////print_r(gettype($row1));
					// die();
				//echo '</pre>';

	    $data2 = array();
		$data2['category_detail'] = $this->mybitshares_model->getCategoryDetail(4);
		$data2['already_clicked_ads'] = $this->mybitshares_model->get_clicked_ads(4);
		
		
		$data2["all_data"] = $site_server_ads_data;
		$this->load->view('mybitshares/view_menu1',$data2);
	}

	public function paid_to_click($value='')
	{
		// $all_data = array();
		
		// $site_server_code_arr  = $this->mybitshares_model->get_all_site_server_URL();
		//if (sizeof($site_server_code_arr) > 0) {
			// foreach($site_server_code_arr as $arr){
				// $data = array();
				// $site_server_url = $arr->site_server_url;
				// $handle = curl_init();
				// curl_setopt_array($handle, array(
				// CURLOPT_RETURNTRANSFER => 1,
				// CURLOPT_URL => $site_server_url."advertise/advertise"
				// ));
				// $res = curl_exec($handle);
				// curl_close($handle);
				// $data['obj'] = json_decode($res);
				// $data['site_server_url'] = $site_server_url;
				// $data['site_server_code'] = $arr->site_server_code;
				
				// array_push($all_data, $data);
			// }
		
		
			// $data2 = array();
		// $data2['category_detail'] = $this->mybitshares_model->getCategoryDetail(5);
		
		
		// $data2["all_data"] = $all_data;
		// $this->load->view('mybitshares/view_menu2',$data2);
		
		$all_data = array();
		$site_server_ads_data = array();
        $site_servers = $this->admin_model->get_all_site_servers();
//        echo "<pre>";print_r($site_servers);die;
        if(!empty($site_servers)){
            foreach ($site_servers as $site_server) {
                $site_server_url_exist = $this->general->urlExists($site_server->site_server_url."v1/");
                if(!$site_server_url_exist) continue;

                $site_server_url = $site_server->site_server_url;
                $handle = curl_init();
                curl_setopt_array($handle, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $site_server_url."v1/advertise/advertise"
                ));

                $res = curl_exec($handle);
                curl_close($handle);

                $site_server_ads_data = json_decode($res);
            }
        }
       // $data['obj'] = $site_server_ads_data;
		

		$menu_id =0;
		$newSite_server_ads=array();
		$advertise_publish = "";
		$rVal = 0;
		$index = 0;
		if(!is_null($site_server_ads_data))
		{
			foreach($site_server_ads_data as $row1)
			{
				
				foreach ($row1 as $row) {
				
				$menu1_id = $this->admin_model->getMenu1ID($row->site_server_code, $row->site_advertise_id);
				$menu2_id = $this->admin_model->getMenu2ID($row->site_server_code, $row->site_advertise_id);
				$advertise_publish_main = $this->admin_model->get_advertise_publish($row->site_server_code, $row->site_advertise_id);
				$paid_to_click_publish = $this->admin_model->get_paid_to_click_publish($row->site_server_code, $row->site_advertise_id);
				//array_push($newSite_server_ads,$menu2_id);

				if(($menu1_id == 4 || $menu2_id == 4 )&& $row->advertise_publish == 1 && $advertise_publish_main == 1 && $paid_to_click_publish == 1)
				   {
					   $rVal = 0;
				   }else{
					   $rVal = 1;
					   unset($row1[$index]);
				   }
				  $index =$index+1;
				}

				 $site_server_ads_data=$row1;
			}
		}
         $data2 = array();
		 $data2['category_detail'] = $this->mybitshares_model->getCategoryDetail(5);
		 $data2['already_clicked_ads'] = $this->mybitshares_model->get_clicked_ads(5);
		
		 $data2["all_data"] = $site_server_ads_data;
		 $this->load->view('mybitshares/view_menu2',$data2);
	}

	public function view($subcategory_id)
	{
		if ($subcategory_id == 4){
			redirect('mybitshares/share_to_click');
		}
		else if ($subcategory_id == 5){
			redirect('mybitshares/paid_to_click');
		}
		else if ($subcategory_id == 8){
			redirect('mybitshares/purchase_product');
		}else if($subcategory_id == 9){
			echo 'Work On Progress';
		}else if($subcategory_id == 10){
			redirect('mybitshares/purchase_product');
		}else if($subcategory_id == 11){
			echo 'Work On Progress';
		}else if($subcategory_id == 12){
			echo 'Work On Progress';
		}else{
			$data = array();
			$site_server_code_arr  = $this->mybitshares_model->get_site_server_code_by_menuID($subcategory_id);
			if ($site_server_code_arr) {
				foreach($site_server_code_arr as $arr){
					$site_server_url = $this->general->getSiteServerURL($arr['site_server_code']);
					$handle = curl_init();
					curl_setopt_array($handle, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $site_server_url."advertise/advertise"
					));
					$res = curl_exec($handle);
					curl_close($handle);
					$data['obj'] = json_decode($res);
					$data['site_server_url'] = $site_server_url;
				}
			}
			$data['category_detail'] = $this->mybitshares_model->getCategoryDetail($subcategory_id);
			$this->load->view('mybitshares/view_menu',$data);
		}
	}

	public function purchase_product()
	{
		//$this->mybitshares_model->clear_old_carts();
		if (!$this->session->userdata('cart_id'))
		{
			if($this->mybitshares_model->has_saved_cart($this->session->userdata('userID')))
			{
				$saved_data = $this->mybitshares_model->get_saved_cart($this->session->userdata('userID'));
				$data = '';
				if(sizeof($saved_data) > 0)
				{
					$this->session->set_userdata('cart_id', $saved_data[0]->secret);
				}
			}
			else 
			{
				$this->session->set_userdata("cart_id",md5($this->session->userdata('userID').date('Y-m-d-h-i-s')));
				
			}
			
		}
		
		$data['product_list'] = $this->mybitshares_model->getProductListByCategory('All');
		// $data['category_detail'] = $this->mybitshares_model->getCategoryDetail('All');
		$this->load->view('mybitshares/purchase_product',$data);
	}

	public function add_to_cart($product_id, $product_quantity, $unit_price)
	{
		//design a page and echo that
		$cart_contents = $this->mybitshares_model->add_to_cart($product_id, $product_quantity, $unit_price);
		echo "Items: ".$cart_contents[0]->item_count." Amount: ".$cart_contents[0]->amount.' in Cart <a href="'.base_url().'mybitshares/view_cart">Checkout</a>';
	}

	public function earning_stats()
	{
		$data['id'] = $id = $this->session->userData('userID');
		$data['referral_income'] = $this->cron_model->update_user_price_history($data['id']);
		$data['client_cash_history'] = $this->mybitshares_model->get_all_balance_history_by_id($id);
		$data['client_share_history'] = $this->mybitshares_model->get_all_share_history_by_id($id);
		
		
			
			$this->load->view('mybitshares/earning_stats',$data);
		
		
		
	}

	

	public function withdraw_cashout()
	{
		$total_income = 0;
		$data = array();
		$tempQueryData = $this->mybitshares_model->get_total_income($this->session->userdata('userID'));
		
		if($tempQueryData)
		{
			$total_income = $tempQueryData[0]->current_amount;
			
		}
		$data['total_amount'] = $total_income;// get the total income or current cash amount
		
		$this->session->set_userdata('total_income',$total_income);
		
		$this->load->view('mybitshares/withdraw_cashout', $data);
		
		
	}
	
	public function process_withdraw()
	{
		//echo "in progress";
		//insert data to withdraw history
		//adjust total income amount
		
	if($this->input->post('withdraw_amount')){
			$data = array(
					'user_id' => $this->session->userdata('userID'),
					'amount' => $this->input->post('withdraw_amount'),
					'date' => time(),
					'payment_method' => $this->input->post('payment_method'),
					'transactionID' => $this->input->post('transaction_id'),
					'other_infos' => $this->input->post('other_infos')
			);
			
			//add entry in cash withdraw history table
			$this->mybitshares_model->add_cash_withdraw($data);
			
			$total_income = $this->session->userdata('total_income');
			
			$total_income = $total_income - $this->input->post('withdraw_amount');
			
			//adjust total
			$this->mybitshares_model->update_total_income($total_income, $this->session->userdata('userID'));
			
			$data = array();
			$this->load->view('mybitshares/withdraw_cash_confirm');
			
		}
	}
	
	public function withdraw_cash_confirm()
	{
		$this->load->view('mybitshares/withdraw_cash_confirm');
	}

	public function last_daily_earnings()
	{
		$data = array();
		$data['users_income'] = $this->mybitshares_model->get_daily_income_of_user($this->session->userdata('userID'));
		// echo '<pre>';
		   // print_r($users_income);
		// echo '</pre>';
		$this->load->view('mybitshares/last_daily_earnings',$data);
	}

	public function fund_add()
	{
		//echo "Working in progress";
		$data = array();
		$this->load->view('mybitshares/share_info',$data);
	}

	public function cancel_account()
	{
		$data = array();
		//echo "Working in progress";
		$id = $this->session->userdata('userID');
		$this->admin_model->enable_disable_user($id);
		$this->authorisation->processLogout();
		redirect('dashboard/dashboard');
		
	}

	public function log_out()
	{
		redirect('logout');
	}

	public function account_info()
	{
		$id = $this->session->userdata('userID');
		$data['user_details'] = $this->mybitshares_model->get_user_details_by_id($id);
		$data['balance_total'] = $this->mybitshares_model->get_all_balance_total($id);
		$data['share_total'] = $this->mybitshares_model->get_all_share_total($id);
		$this->load->view('mybitshares/profile',$data);
	}

	public function referral_link()
	{
		$id = $this->session->userdata('userID');
		$data['user_details'] = $this->mybitshares_model->get_user_details_by_id($id);
		$this->load->view('mybitshares/referral_link',$data);
	}


	

	public function process_payment($final_price)
	{
		//call payment gateway here
		return 1;
	}

	

	

	public function add_fund($bonus_id)
	{
		$id = $this->session->userdata('userID');
		$data['bonus_d'] = $this->mybitshares_model->get_bonus_by_id($bonus_id);
		$data['user_details'] = $this->mybitshares_model->get_user_details_by_id($id);
		$this->load->view('mybitshares/add_fund',$data);
	}

	public function confirm_fund_add($value='')
	{
		$id = $this->session->userdata('userID');
		$data = array('user_id' => $id, 
							'fund_amount' => $this->input->post('fund_amount'), 
							'bonus_id' =>$this->input->post('bonus_id'), 
							'payment_method' =>$this->input->post('payment_method'), 
							'transaction_id' =>$this->input->post('transaction_id'), 
							'gateway_company' =>$this->input->post('gateway_company'), 
							'date' => time()
				);
		$this->mybitshares_model->add_fund($data);
		echo 'Fund Added Successfully.<a href="'.base_url().'"> Click here</a>  to go to dashboard page';
	}

	public function my_referrals()
	{
		$id = $this->session->userdata('userID');
		$data['user_details'] = $this->mybitshares_model->get_user_details_by_id($id);
		$data['level1_count'] = $this->mybitshares_model->get_referrals_count_by_id($id);
		$data['level1_users'] = $this->mybitshares_model->get_referrals_by_id($id);
		$this->load->view('mybitshares/my_referrals',$data);
	}

	public function get_referrals($id, $level)
	{
		$data['user_details'] = $this->mybitshares_model->get_user_details_by_id($id);
		$data['level'] = $level;
		$data['level1_count'] = $this->mybitshares_model->get_referrals_count_by_id($id);
		$data['level1_users'] = $this->mybitshares_model->get_referrals_by_id($id);
		$this->load->view('mybitshares/get_referrals',$data);
	}

	
	public function product_buy()
	{
		if($this->input->post('name')){
			
			//process payment
			
			//insert data to product_order table with payment and shipping details
			
			
			$data = array(
					'name' => $this->input->post('name'),
					'address' => $this->input->post('address'),
					'phone' => $this->input->post('phone'),
					'city' => $this->input->post('city'),
					'country' => $this->input->post('country'),
					'zip_code' => $this->input->post('zip_code')				
					
			);
			//add the address in the table
			$this->mybitshares_model->add_shipping_address($data);
			$cart_contents = $this->mybitshares_model->get_cart_contents($this->session->userdata('cart_id'));
			$number_of_shares = 0;
			foreach ($cart_contents as $row) {
				$number_of_shares += ($row->quantity*$row->product_shares);
			}
			//add product shares
			$this->mybitshares_model->add_purchase_share_to_user($number_of_shares);
			
			// clear entry of this user in saved_cart
			$this->mybitshares_model->delete_saved_cart($this->session->userdata('userID'));
			
			//unset cart_id
			$this->session->unset_userdata('cart_id');
			
			$tempPayData = array(
					'payment_getway' => $this->input->post('name'),
					'card_type' => $this->input->post('card_type'),
					'cardholder_name' => $this->input->post('cardholder_name'),
					'card_number' => $this->input->post('card_number'),
					'expire_date' => $this->input->post('expire_date'),
					'subtotal_amount' => $this->input->post('total_amount'),
					'tax_amount' => 0,
					'coupon_value' => 0,
					'deposit_value' => 0,
					'tip_value' => 0,
					'delivery_value' => 0,
					'payment_getway_amount' => 0,
					'total_amount' => $this->input->post('total_amount'),
					'order_status' => 1,
					'shipping_status' => 1,
					'services_name' => "test",					
					'security_code' => $this->input->post('security_code')
			);
			
			$data['email'] = "";
			$data['delivery_date'] = "";
			$data['state'] = "";
			$data['day_phone_a'] = "";
						
			$tempData = "";
			$tempData["cart_contents"] = $cart_contents;
			$tempData["shipping_info"] = $data;
			$tempData["payment_info"] = $tempPayData;
			
			$this->mybitshares_model->save_purchase_info($data, $tempPayData);
			
			$this->load->view('mybitshares/purchase_confirm',$tempData);
			//echo $number_of_shares;
			//show buy details, shipping location ,estimated time
		}
	}
	

	public function share_buy($id)
	{
		$userID=$this->session->userdata('userID');
		$data['share_info'] = $this->mybitshares_model->get_share_info_by_id($id);
		$data['main_cash_amount'] = $this->user_transaction_update_scheduler_model->get_Total_cash_amount_of_user($userID);
		$this->load->view('mybitshares/share_buy',$data);
	}
	
	public function share_cancel($id)
	{	
		$userID=$this->session->userdata('userID');
		$share_to_add_for_sell = $this->mybitshares_model->get_share_sell_amount($id);
		$message = 'Cancel_Selled_Share';
		//echo'<pre>';print_r($share_to_add_for_sell);echo'</pre>';die();
		$this->update_user_history($userID,($share_to_add_for_sell),$message);
		$this->mybitshares_model->cancel_added_share_sell($id);
		//$this->buy_sell();
		redirect('mybitshares/buy_sell');
	}
	
	public function buy_sell()
	{
		$client_cash_history = $this->mybitshares_model->get_all_balance_history_by_id($this->session->userdata('userID'));
		$total = 0;
		foreach ($client_cash_history as $row){
			$total += $row->cash_amount;
		}
		$min_amount = $this->mybitshares_model->get_minimum_amount_to_see_sell();
		if($total<$min_amount){
			echo "You don't have minimum amount of ".$min_amount." taka to see sellable shares";
		}else{
			$id = $this->session->userdata('userID');
			$data['share_buy_percentage'] = $this->mybitshares_model->get_share_buy_percentage();
			$data['share_sell_percentage'] = $this->mybitshares_model->get_share_sell_percentage();
			$data['sellable_shares'] = $this->mybitshares_model->get_sellable_shares($id);
			$package_quantity = $this->mybitshares_model->get_share_sell_count_by_seller_id($id);
			$data['package_quantity'] =$package_quantity;
			$data['share_detail'] = $this->mybitshares_model->user_total_share_amount_by_id($id);
			if(sizeof($data['share_detail']) > 0)
			{
				$this->session->set_userdata('current_share_number', $data['share_detail'][0]->share_amount);
			}
			else {
				$this->session->set_userdata('current_share_number', 0);
			}
			
			
			$this->load->view('mybitshares/sellable_shares',$data);
		}
	}
	
	public function confirm_share_to_sell()
	{
		$data = '';
		$id = $this->session->userdata('userID');
		$share_sell_percentage = $this->mybitshares_model->get_share_sell_percentage();
		//$share_detail = $this->mybitshares_model->user_share_detail_by_id($id);
		//$current_share_balance = $share_detail[0]->current_share_balance;
		//$previous_per_share_value = $share_detail[0]->per_share_price;
		$share_price = $this->input->post('amount');
		$share_to_add_for_sell = $this->input->post('share_to_add_for_sell');
	
		//$calculate_price = ($share_to_add_for_sell*$per_share_value);
		//$calculate_price = $calculate_price + (($calculate_price*$share_sell_percentage)/100);
		$current_share_amount = $this->session->userdata('current_share_number');
		
		$package_quantity = $this->mybitshares_model->get_share_sell_count_by_seller_id($id);
		//echo'<pre>';print_r($package_quantity);echo'<pre>';die();
		if($share_to_add_for_sell >=1 && ($share_to_add_for_sell <= $current_share_amount)  && $package_quantity<3  && $share_to_add_for_sell!= null && $share_to_add_for_sell !="" && $share_to_add_for_sell>0){
			$data = array(
					'seller_id' => $id,
					'number_of_shares' => $share_to_add_for_sell,
					'amount' => $share_price,
					'share_sell_publish' => 1,
					'package_quantity' => $package_quantity+1,
					'date' => time()
			);
			$this->mybitshares_model->add_share_sell($data);
			$message='Selled_Share';
			$this->update_user_history($id,$share_to_add_for_sell,$message);
			echo 'Share added to our Share Selling List. Click <a href="'.base_url('mybitshares/dashboard').'">here</a> to go back';
		}
		else
		{
			redirect("/mybitshares/buy_sell");
		}
	
		//if ($previous_per_share_value != $per_share_value){
		//	$this->mybitshares_model->change_user_current_share_price($previous_per_share_value, $per_share_value);
		//}
	
		
	}
	public function update_user_history($userID,$share_to_add_for_sell,$message)
	{
	    //Users History Table
		    $time =time();
			$data_users = array(
			'user_id' => $userID,
			'transaction_message' => $message,
			'transaction_amount' =>$share_to_add_for_sell,
			'transaction_history_date' => $time				
			);
			$this->db->insert('user_transaction_history', $data_users);
		//Main table
			$main_share_amount= $this->user_transaction_update_scheduler_model->get_client_shares_Amount_transaction_history($userID,time());
			
			$selled_shares_Amount= $this->user_transaction_update_scheduler_model->get_selled_shares_Amount_by_time($userID,$time);
			if($message == 'Cancel_Selled_Share')
			{
				$share_to_add_for_sell=0;
			}
			$data_users_main = array(
				'share_amount' => ($main_share_amount-$selled_shares_Amount-$share_to_add_for_sell),
				'cash_last_update_date' => time()
			);
			$this->db->where('user_id', $userID);
			$this->db->update('user_transaction_main', $data_users_main);			
	}
	public function add_shares_to_sell()
	{
		$id = $this->session->userdata('userID');
		$data['share_detail'] = $this->mybitshares_model->user_share_detail_by_id($id);
		$this->load->view('mybitshares/add_shares_to_sell',$data);
	}
	
	public function process_buy($id)
	{
		
		$total_share_price = $this->session->userdata('total_price');
		$users_cash_amount = $this->session->userdata('users_cash_amount');
		
		if(($this->session->userdata('userID') != $this->session->userdata('seller_id')) && ($users_cash_amount > $total_share_price))
		{

		   $userID = $this->session->userdata('userID'); 
		   $share_to_add_for_sell = $this->session->userdata('number_of_shares');
		   $total_share_price = $this->session->userdata('total_price');
		   $seller_id = $this->session->userdata('seller_id');
		   $this->share_buy_update_history_buyer($userID,$share_to_add_for_sell,$total_share_price);
		   $this->share_buy_update_history_seller($seller_id,$total_share_price);
		   //Publish Flag
			$data_share_sell = array(
				'share_sell_publish' =>  0
			);
			$this->db->where('share_sell_id', $id);
			$this->db->update('share_sell', $data_share_sell);	
			
				//echo "Your request processed successfully. .";
				echo 'Your request processed successfully. Click <a href="'.base_url('mybitshares/dashboard').'">here</a> to go back';
		}
		
	}
	
	public function share_buy_update_history_buyer($userID,$share_to_add_for_sell,$cash_amount)
	{
		
		//buyer
	    //Users History Table for Share
		    $time =time();
			$data_users_share = array(
			'user_id' => $userID,
			'transaction_message' => 'Buying_Share',
			'transaction_amount' =>$share_to_add_for_sell,
			'transaction_history_date' => $time				
			);
			$this->db->insert('user_transaction_history', $data_users_share);
		//Users History Table for cash
		    $time =time();
			$data_users_cash = array(
			'user_id' => $userID,
			'transaction_message' => 'Share_Buy_Cash',
			'transaction_amount' =>$cash_amount,
			'transaction_history_date' => $time				
			);
			$this->db->insert('user_transaction_history', $data_users_cash);
		//Admin Percentage
			$share_sell_percentage = $this->mybitshares_model->get_share_buy_percentage();
			$admin_cash_percentage = (($cash_amount *$share_sell_percentage)/100); 
			$data_admin_cash_percentage = array(
				'user_id' => $userID,
				'transaction_message' => 'Admin_Percentage_from_buyer',
				'transaction_amount' =>$admin_cash_percentage,
				'transaction_history_date' => $time				
			);
			$this->db->insert('user_transaction_history', $data_admin_cash_percentage);
		//Main table
			$main_share_amount= $this->user_transaction_update_scheduler_model->get_client_shares_Amount_transaction_history($userID,time());
			
			$selled_shares_Amount= $this->user_transaction_update_scheduler_model->get_selled_shares_Amount_by_time($userID,$time);

			$main_cash_amount= $this->user_transaction_update_scheduler_model->get_client_cash_Amount_transaction_history($userID,time());
			
			$buy_share_deduct_cash_Amount = $this->user_transaction_update_scheduler_model->get_client_buy_share_deduct_cash_Amount($userID,time());
			
			$data_users_main = array(
				'share_amount' => ($main_share_amount-$selled_shares_Amount),
				'cash_amount' =>  ($main_cash_amount - $buy_share_deduct_cash_Amount),
				'cash_last_update_date' => time()
			);
			$this->db->where('user_id', $userID);
			$this->db->update('user_transaction_main', $data_users_main);
		
			//Admin Process?????
			//share_amount 10%
			$share_amount=(($cash_amount *$share_sell_percentage)/100);
			$this->update_share_to_click_transaction($share_amount);
			
	}
	
	public function share_buy_update_history_seller($userID,$cash_amount)
	{
		
		//seller
		//Users History Table for cash
		    $time =time();
			$data_users_cash = array(
				'user_id' => $userID,
				'transaction_message' => 'Share_selled_Cash',
				'transaction_amount' =>$cash_amount,
				'transaction_history_date' => $time				
			);
			$this->db->insert('user_transaction_history', $data_users_cash);
		//Admin Percentage
			$share_sell_percentage = $this->mybitshares_model->get_share_sell_percentage();
			$admin_cash_percentage = (($cash_amount *$share_sell_percentage)/100); 
			$data_admin_cash_percentage = array(
				'user_id' => $userID,
				'transaction_message' => 'Admin_Percentage_from_seller',
				'transaction_amount' =>$admin_cash_percentage,
				'transaction_history_date' => $time				
			);
			$this->db->insert('user_transaction_history', $data_admin_cash_percentage);
		//Main table
			$main_cash_amount= $this->user_transaction_update_scheduler_model->get_client_cash_Amount_transaction_history($userID,time());
			
			$buy_share_deduct_cash_Amount = $this->user_transaction_update_scheduler_model->get_client_buy_share_deduct_cash_Amount($userID,time());
			
			$data_users_main = array(
				'cash_amount' =>  ($main_cash_amount - $buy_share_deduct_cash_Amount),
				'cash_last_update_date' => time()
			);
			$this->db->where('user_id', $userID);
			$this->db->update('user_transaction_main', $data_users_main);	
			
			//Admin Process?????
			//share_amount 10%
			$share_amount=(($cash_amount *$share_sell_percentage)/100);
			$this->update_share_to_click_transaction($share_amount);

	}
	
	public function update_share_to_click_transaction($share_amount){

	    //$userID=$this->session->userData('userID');
		$userID=1;
	    
			//client_shares Share_To_Click		
			$data2 = array(
				'user_id' => $userID,
				'transaction_message' => "Share_Buy_and_Sell_Amount",
				'transaction_amount' => $share_amount,
				'transaction_history_date' => time()				
			
			);
			$this->db->insert('transaction_history', $data2);

			$share_last_update_date= $this->transaction_update_scheduler_model->get_client_shares_latest_date($userID);
			//$gifted_amount= $this->transaction_update_scheduler_model->get_gifted_shares_Amount($userID);
			$main_share_amount= $this->transaction_update_scheduler_model->get_cash_Amount_transaction_history_for_admin($userID,time());

			$data3 = array(
					'cash_amount' => ($main_share_amount),
					'share_last_update_date' => time()
			);

			 $this->db->where('user_id', $userID);
			 $this->db->update('transaction_main', $data3);

	}
	
	public function view_cart($value='')
	{
		if($value == 1)
		{
			// check if the user has saved cart, then show saved cart
			$saved_data = $this->mybitshares_model->get_saved_cart($this->session->userdata('userID'));
			$data = '';
			if(sizeof($saved_data) > 0)
			{
				$this->session->set_userdata('cart_id', $saved_data[0]->secret);
				$data['cart_contents'] = $saved_data;
				$data['country_list'] = $this->mybitshares_model->get_country_list();
				$data['address'] = $this->mybitshares_model->get_shipping_address();
				if(sizeof($data['address'])>0){
					$data['show']=true;
				}else{
					$data['show']=false;
				}
			}
		}
		else {
				
	
			//check if there is cart id in the session. if not, assign a new
				
			if ($this->session->userdata('cart_id')){
				$data['cart_contents'] = $this->mybitshares_model->get_cart_contents($this->session->userdata('cart_id'));
				$data['country_list'] = $this->mybitshares_model->get_country_list();
				$data['address'] = $this->mybitshares_model->get_shipping_address();
				if(sizeof($data['address'])>0){
					
					if($this->session->userdata('guest'))
					{
						$data['show']=false;
					}
					else {
						$data['show']=true;
					}
				}else{
					$data['show']=false;
				}
			}
		}
		$this->load->view('mybitshares/view_cart', $data);
	}
	
	
	public function save_cart()
	{
		if($this->session->userdata('guest'))
		{
			redirect ('users/join_user');
		}
		else {
			if ($this->session->userdata('cart_id'))
			{
				$this->mybitshares_model->save_cart($this->session->userdata('userID'),$this->session->userdata('cart_id'));
				redirect ('mybitshares/purchase_product');
			}
			else {
				redirect ('mybitshares/login');
			}
			
		}
	
	}
	
	public function delete_save_cart()
	{
		$this->mybitshares_model->delete_saved_cart($this->session->userdata('userID'));
		
		if($this->session->userdata('cart_id'))
		{
		
			$this->mybitshares_model->delete_current_cart($this->session->userdata('cart_id'));
			
			$this->session->unset_userdata('cart_id');
		}
		
		
		redirect ('mybitshares/purchase_product');
		
	}
	
	public function menu2_click_ajax($site_server_url = '', $site_server_code = '', $site_advertise_id = 0, $price_advertise = 0)
	{
		$ip_address = "";
		$client_id="";
		
		if($this->session->userdata('ip_address'))
		{
			$ip_address = $this->session->userdata('ip_address');
		}
		else 
		{
			$ip_address = $_SERVER['REMOTE_ADDR'];
		}
		
		$userID=$this->session->userdata('userID');
        // if( !isset($userID) && !empty($userID) )
		// {
			// $client_id=userID;
		// }else{
			// $client_id=1 ;
		// }
		$siteServer_url=base64_decode(urldecode($site_server_url));
		//insert data on client_cash
		$data = array(
				'site_server_code' => $site_server_code,
				'site_server_advertise_id' => $site_advertise_id,
				'client_id' => $userID,
				'cash_amount' => $price_advertise,
				'client_cash_publish' => 0,
				'click_date' => time(),				
				'ip_address' => $ip_address
		
		);
		
		//$this->db->insert('client_cash', $data);
		
		
		
		$inserted_row_id = $this->mybitshares_model->insert_in_client_cash($data);
		
		$this->mybitshares_model->ad_clicked2($siteServer_url,$site_server_code,$site_advertise_id, $price_advertise);
		
		//call ad_clicked method in model
		
		$siteTxt = $siteServer_url.'v1/siteserver/runner/'.$inserted_row_id.'/'.$site_advertise_id.'/paid';
		
		//$siteTxt = "http://localhost/mybitshares/mybitshares/menu1_click";
		
		echo $siteTxt;
		
		/*
		
		$site_send_url = $site_server_url.'siteserver/runner/'.$inserted_row_id;
		
		//send id to site server to turn on client_cash_publish for this data
		
		$handle = curl_init();
		curl_setopt_array($handle, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $site_send_url
		));
		$res = curl_exec($handle);
		curl_close($handle);
		
		*/
		
		//$data = array();
		//$this->load->view('mybitshares/view_menu2_click', $data);
		
		
	
	}
	
	public function menu2_click()
	{
		
	
		$data = array();
		$this->load->view('mybitshares/view_menu2_click', $data);
	
	}
	
	
	public function menu1_click_ajax($site_server_url = '', $site_server_code = '', $site_advertise_id = 0, $price_advertise = 0)
	{
		$ip_address = "";
		$client_id="";
		if($this->session->userdata('ip_address'))
		{
			$ip_address = $this->session->userdata('ip_address');
		}
		else
		{
			$ip_address = $_SERVER['REMOTE_ADDR'];
		}
		
		$userID=$this->session->userdata('userID');
        // if( !isset($userID) && !empty($userID) )
		// {
			// $client_id=userID;
		// }else{
			// $client_id=1 ;
		// }
	

	   $siteServer_url=base64_decode(urldecode($site_server_url));
		//insert data on client_cash
		$data = array(
				'site_server_code' => $site_server_code,
				'site_server_advertise_id' => $site_advertise_id,
				'client_id' =>$userID,
				'shares_amount' => $price_advertise,
				'client_shares_publish' => 0,
				'click_date' => time(),
				'ip_address' => $ip_address
	
		);
		            // echo '<pre>';
                    //print_r($data);
					 //print_r(base64_decode(urldecode($site_server_url)));
					//die();
                    //echo '</pre>';
		//$this->db->insert('client_cash', $data);
	
	
		$inserted_row_id = $this->mybitshares_model->insert_in_client_share($data);
		

		
		
		$this->mybitshares_model->ad_clicked3($siteServer_url,$site_server_code,$site_advertise_id, $price_advertise);
		

		//$siteTxt = $siteServer_url.'v1/siteserver/runner/'.$inserted_row_id;
		
		
		$siteTxt = $siteServer_url.'v1/siteserver/runner/'.$inserted_row_id.'/'.$site_advertise_id.'/share';
		
				 // echo '<pre>';
					 // print_r($siteTxt);
					// die();
                    // echo '</pre>';
		
		echo $siteTxt;
		
		//redirect($siteTxt);
	
		/*
		$site_send_url = $site_server_url.'siteserver/runner/'.$inserted_row_id;
	
		//send id to site server to turn on client_cash_publish for this data
	
		$handle = curl_init();
		curl_setopt_array($handle, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $site_send_url
		));
		$res = curl_exec($handle);
		curl_close($handle);
		
		*/
	
		//$data = array();
		//$this->load->view('mybitshares/view_menu1_click', $data);
		
		
	}
	
	public function menu1_click()
	{
				
		$data = array();
		$this->load->view('mybitshares/view_menu1_click', $data);
	
	
	}
	
	
		
	
	

	
}