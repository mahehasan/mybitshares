<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_update_scheduler extends CI_Controller {
   private $timestamp;
   private $adminID;
	function __construct()
	{		
		parent::__construct();
		$this->load->model(array('transaction_update_scheduler_model'));
		$this->load->model(array('user_transaction_update_scheduler_model'));
	}
	
	public function enable_scheduler(){

	    $userID=$this->session->userData('userID');
		//$userID=4;
		//$checkhistory = $this->transaction_update_scheduler_model->user_transaction_history_existence($userID);
		$checmain = $this->transaction_update_scheduler_model->user_transaction_main_existence($userID);
		
		if(!empty($checmain))
		{
			
			//Client_cash Paid_To_Click
			$cash_last_update_date= $this->transaction_update_scheduler_model->get_client_cash_latest_date($userID);		
			$cash_last_update_history_date= $this->transaction_update_scheduler_model->get_client_cash_history_date($userID);	

			$cash_amount=0;
			if($cash_last_update_date>0){
				$cash_amount= $this->transaction_update_scheduler_model->get_client_cash_Amount($userID,$cash_last_update_date,$cash_last_update_history_date);
			}
			else{
				$cash_amount= $this->transaction_update_scheduler_model->get_client_cash_Amount($userID,time(),$cash_last_update_history_date);
				$cash_last_update_date=time();
				
			}
			if($cash_amount>0)
			{
				$data = array(
					'user_id' => $userID,
					'transaction_message' => "Paid_To_Click",
					'transaction_amount' => $cash_amount,
					'transaction_history_date' => $cash_last_update_date				
					);
				$this->db->insert('transaction_history', $data);
			}	
			//client_shares Share_To_Click
			$share_last_update_date= $this->transaction_update_scheduler_model->get_client_shares_latest_date($userID);
			$share_last_update_history_date= $this->transaction_update_scheduler_model->get_client_shares_history_date($userID);
			
			$share_amount=0;
			if($share_last_update_date>0){
				$share_amount= $this->transaction_update_scheduler_model->get_client_share_Amount($userID,$share_last_update_date,$share_last_update_history_date);
			}
			 else{
				$share_amount= $this->transaction_update_scheduler_model->get_client_share_Amount($userID,time(),$share_last_update_history_date);
				$share_last_update_date=time();
			}
	
			if($share_amount>0)
			{
				$data2 = array(
				'user_id' => $userID,
				'transaction_message' => "Share_To_Click",
				'transaction_amount' => $share_amount,
				'transaction_history_date' => $share_last_update_date				
				 );
				$this->db->insert('transaction_history', $data2);
			}
			$gifted_amount= $this->transaction_update_scheduler_model->get_gifted_shares_Amount($userID);
			$main_share_amount= $this->transaction_update_scheduler_model->get_client_shares_Amount_transaction_history($userID,time());
			$main_cash_amount= $this->transaction_update_scheduler_model->get_cash_Amount_transaction_history_for_admin($userID,time());
			//echo '<pre>';print_r($gifted_amount);echo '</pre>';	die();
			//transaction_main
			if($gifted_amount>0)
			{				
				$share_last_update_date=time();
			}
			
			if($cash_amount>0 || $share_amount>0){
			$data3 = array(
					'share_amount' => ($main_share_amount-$gifted_amount),
					'cash_amount' => $main_cash_amount,
					'share_last_update_date' => $share_last_update_date,
					'cash_last_update_date' => $cash_last_update_date
			
			);
			 $this->db->where('user_id', $userID);
			 $this->db->update('transaction_main', $data3);
			}
			
		}else{
			
			//Client_cash Paid_To_Click
			$cash_last_update_date= $this->transaction_update_scheduler_model->get_client_cash_latest_date($userID);			
			//$cash_amount= $this->transaction_update_scheduler_model->get_client_cash_Amount_First($userID,$cash_last_update_date);
			$cash_amount= $this->transaction_update_scheduler_model->get_client_cash_Amount_First($userID,time());
			if($cash_amount>0)
			{
				$data = array(
					'user_id' => $userID,
					'transaction_message' => "Paid_To_Click",
					'transaction_amount' => $cash_amount,
					'transaction_history_date' => $cash_last_update_date						
				);
				$this->db->insert('transaction_history', $data);
			}
		
		//client_shares Share_To_Click
		    $share_last_update_date= $this->transaction_update_scheduler_model->get_client_shares_latest_date($userID);			
			$share_amount= $this->transaction_update_scheduler_model->get_client_shares_Amount_First($userID,time());
			if($share_amount>0)
			{
				$data2 = array(
					'user_id' => $userID,
					'transaction_message' => "Share_To_Click",
					'transaction_amount' => $share_amount,
					'transaction_history_date' => $share_last_update_date				
			
					);
				$this->db->insert('transaction_history', $data2);
			}
		//transaction_main
		if($share_last_update_date == null || $share_last_update_date == "")
		{
			$share_last_update_date=time();
		}
		if($cash_last_update_date == null || $cash_last_update_date == "")
		{
			$cash_last_update_date=time();
		}
		$gifted_amount= $this->transaction_update_scheduler_model->get_gifted_shares_Amount($userID);
		if($cash_amount>0 || $share_amount>0){
			$data3 = array(
					'user_id' => $userID,
					'share_amount' => ($share_amount-$gifted_amount),
					'cash_amount' => $cash_amount,
					'share_last_update_date' => $share_last_update_date,
					'cash_last_update_date' => $cash_last_update_date
			
			);
			$this->db->insert('transaction_main', $data3);
			
		  }
		}
		
		
		//$checmain = $this->transaction_update_scheduler_model->user_transaction_main_existence(1);
		 redirect('/admin/system_earnings');
		
			
		

	}
	
		public function enable_daily_earnings(){

	    $userID=$this->session->userData('userID');
		//$userID=4;

		$checmain = $this->transaction_update_scheduler_model->user_daily_transaction_history_existence($userID);
		//echo '<pre>';print_r($checmain);echo '</pre>';	die();
		if(!empty($checmain))
		{

			//Client_cash 
			$cash_first_update_date= $this->transaction_update_scheduler_model->get_daily_transaction_history_latest_date_Paid_To_Click($userID);
			$cash_last_update_date= $this->transaction_update_scheduler_model->get_client_cash_latest_date($userID);
			
			$interval = (($cash_last_update_date-$cash_first_update_date)/86400);
			
			$cash_amount=0;
			$fm=date("Y-m-d",$cash_first_update_date);
			$fm1=new DateTime($fm.'00:00:00');
			
			$d1=date($fm1->format("Y-m-d H:i:s"));//(floor($interval)+1)
			$firstdate=$cash_first_update_date;
			//print_r( number_format($interval));
			if(number_format($interval)>0){
			for($i=1; $i<(number_format($interval)+2);$i++)
			{

				$checkDate= date("Y-m-d H:i:s",strtotime($d1 . "+".$i." days"));
				if($firstdate>0){
				$cash_amount= $this->transaction_update_scheduler_model->get_client_cash_Amount_by_date($userID,$firstdate,strtotime($checkDate));
				}
				
				if($cash_amount>0)
				{
					// print_r(date("Y-m-d", $firstdate)."=");
					// print_r( $cash_amount.",");
					
					$data = array(
					'user_id' => $userID,
					'transaction_message' => "Paid To Click",
					'transaction_amount' => $cash_amount,
					'transaction_history_date' => $firstdate				
					);
					$this->db->insert('daily_transaction_history', $data);
				}
				$firstdate = strtotime($checkDate);
			}
			}
			//Client_Share
			$share_last_update_date= $this->transaction_update_scheduler_model->get_client_shares_latest_date($userID);
			$share_first_update_date= $this->transaction_update_scheduler_model->get_daily_transaction_history_latest_date_Share_To_Click($userID);
			
			$interval_share = (($share_last_update_date-$share_first_update_date)/86400);
			//echo '<pre>';print_r(number_format($interval_share));echo '</pre>';	die();
			$share_amount=0;
			$fm_share=date("Y-m-d",$share_first_update_date);
			$fm_share1=new DateTime($fm_share.'00:00:00');
			
			$share_date=date($fm_share1->format("Y-m-d H:i:s"));//(floor($interval)+1)
			$first_date_share=$share_first_update_date;
			//print_r( number_format($interval));
			if(number_format($interval_share)>0){
			for($i=1; $i<(number_format($interval_share)+2);$i++)
			{

				$check_Date_share= date("Y-m-d H:i:s",strtotime($share_date . "+".$i." days"));
				if($first_date_share>0){
				$share_amount= $this->transaction_update_scheduler_model->get_client_share_Amount_by_date($userID,$first_date_share,strtotime($check_Date_share));
				}
				
				if($share_amount>0)
				{
					// print_r(date("Y-m-d", $first_date_share)."=");
					// print_r( $share_amount.",");
					
					$data = array(
					'user_id' => $userID,
					'transaction_message' => "Share To Click",
					'transaction_amount' => $share_amount,
					'transaction_history_date' => $first_date_share				
					);
					$this->db->insert('daily_transaction_history', $data);
				}
				$first_date_share = strtotime($check_Date_share);
			}
			}
			//Share Buy & sell
			
			//$userID=1;

			$buy_sell_last_update_date= $this->transaction_update_scheduler_model->get_transaction_history_latest_date($userID);
			$buy_sell_first_update_date= $this->transaction_update_scheduler_model->get_daily_transaction_history_latest_date_Share_Buy_and_Sell_Amount($userID);

			$buy_sell_interval_share = (($buy_sell_last_update_date-$buy_sell_first_update_date)/86400);
			
			//print_r($buy_sell_last_update_date);
			$buy_sell_cash_amount=0;
			$buy_sell_fm_share=date("Y-m-d",$buy_sell_first_update_date);
			$buy_sell_fm_share1=new DateTime($buy_sell_fm_share.'00:00:00');
			
			$buy_sell_date=date($buy_sell_fm_share1->format("Y-m-d H:i:s"));//(floor($interval)+1)
			$buy_sell_first_date=$buy_sell_first_update_date;
			if(number_format($buy_sell_interval_share)>0){
			for($i=1; $i<(number_format($buy_sell_interval_share)+2);$i++)
			{

				$buy_sell_check_Date= date("Y-m-d H:i:s",strtotime($buy_sell_date . "+".$i." days"));
				if($buy_sell_first_date>0){
				$buy_sell_cash_amount= $this->transaction_update_scheduler_model->get_admin_sharebuy_and_sell_Amount_by_date($userID,$buy_sell_first_date,strtotime($buy_sell_check_Date));
				}
				
				if($buy_sell_cash_amount>0)
				{
					//print_r(date("Y-m-d", $buy_sell_first_date)."=");
					//print_r( $buy_sell_cash_amount.",");
					
					$data = array(
					'user_id' => $userID,
					'transaction_message' => "Share Buy and Sell Amount",
					'transaction_amount' => $buy_sell_cash_amount,
					'transaction_history_date' => $buy_sell_first_date				
					);
					$this->db->insert('daily_transaction_history', $data);
				}
				$buy_sell_first_date = strtotime($buy_sell_check_Date);
			}
			
			}
			//Advertise Add
			

			$adverise_add_last_update_date= $this->transaction_update_scheduler_model->get_adverise_add_latest_date($userID);
			$adverise_add_first_update_date= $this->transaction_update_scheduler_model->get_daily_transaction_history_latest_date_Advertise_Add($userID);

			$adverise_add_interval_share = (($adverise_add_last_update_date-$adverise_add_first_update_date)/86400);

			$adverise_add_cash_amount=0;
			$adverise_add_fm_share=date("Y-m-d",$adverise_add_first_update_date);
			$adverise_add_fm_share1=new DateTime($adverise_add_fm_share.'00:00:00');
			
			$adverise_add_date=date($adverise_add_fm_share1->format("Y-m-d H:i:s"));//(floor($interval)+1)
			$adverise_add_first_date=$adverise_add_first_update_date;
			if(number_format($adverise_add_interval_share)>0){
			for($i=1; $i<(number_format($adverise_add_interval_share)+2);$i++)
			{

				$adverise_add_check_Date= date("Y-m-d H:i:s",strtotime($adverise_add_date . "+".$i." days"));
				//print_r( date("Y-m-d H:i:s",$adverise_add_first_date));
				if($adverise_add_first_date>0){
				$adverise_add_cash_amount= $this->transaction_update_scheduler_model->get_adverise_add_Amount_by_date($userID,$adverise_add_first_date,strtotime($adverise_add_check_Date));
				}
				
				if($adverise_add_cash_amount>0)
				{
					//print_r(date("Y-m-d", $adverise_add_first_date)."=");
					//print_r( $adverise_add_cash_amount.",");
					
					$data = array(
					'user_id' => $userID,
					'transaction_message' => "Advertise Add",
					'transaction_amount' => $adverise_add_cash_amount,
					'transaction_history_date' => $adverise_add_first_date				
					);
					$this->db->insert('daily_transaction_history', $data);
				}
				$adverise_add_first_date = strtotime($adverise_add_check_Date);
			}
			}

			}else{

			//Client_cash 
			$cash_first_update_date= $this->transaction_update_scheduler_model->get_client_cash_first_date($userID);
			$cash_last_update_date= $this->transaction_update_scheduler_model->get_client_cash_latest_date($userID);
			
			$interval = (($cash_last_update_date-$cash_first_update_date)/86400);

			$cash_amount=0;
			$fm=date("Y-m-d",$cash_first_update_date);
			$fm1=new DateTime($fm.'00:00:00');
			
			$d1=date($fm1->format("Y-m-d H:i:s"));//(floor($interval)+1)
			$firstdate=$cash_first_update_date;
			//print_r( number_format($interval));
			for($i=1; $i<(number_format($interval)+2);$i++)
			{

				$checkDate= date("Y-m-d H:i:s",strtotime($d1 . "+".$i." days"));
				if($firstdate>0){
				$cash_amount= $this->transaction_update_scheduler_model->get_client_cash_Amount_by_date($userID,$firstdate,strtotime($checkDate));
				}
				
				if($cash_amount>0)
				{
					// print_r(date("Y-m-d", $firstdate)."=");
					// print_r( $cash_amount.",");
					
					$data = array(
					'user_id' => $userID,
					'transaction_message' => "Paid To Click",
					'transaction_amount' => $cash_amount,
					'transaction_history_date' => $firstdate				
					);
					$this->db->insert('daily_transaction_history', $data);
				}
				$firstdate = strtotime($checkDate);
			}
			
			//Client_Share
			$share_last_update_date= $this->transaction_update_scheduler_model->get_client_shares_latest_date($userID);
			$share_first_update_date= $this->transaction_update_scheduler_model->get_client_shares_first_date($userID);
			
			$interval_share = (($share_last_update_date-$share_first_update_date)/86400);

			$share_amount=0;
			$fm_share=date("Y-m-d",$share_first_update_date);
			$fm_share1=new DateTime($fm_share.'00:00:00');
			
			$share_date=date($fm_share1->format("Y-m-d H:i:s"));//(floor($interval)+1)
			$first_date_share=$share_first_update_date;
			//print_r( number_format($interval));
			for($i=1; $i<(number_format($interval_share)+2);$i++)
			{

				$check_Date_share= date("Y-m-d H:i:s",strtotime($share_date . "+".$i." days"));
				if($first_date_share>0){
				$share_amount= $this->transaction_update_scheduler_model->get_client_share_Amount_by_date($userID,$first_date_share,strtotime($check_Date_share));
				}
				
				if($share_amount>0)
				{
					//print_r(date("Y-m-d", $first_date_share)."=");
					//print_r( $share_amount.",");
					
					$data = array(
					'user_id' => $userID,
					'transaction_message' => "Share To Click",
					'transaction_amount' => $share_amount,
					'transaction_history_date' => $first_date_share				
					);
					$this->db->insert('daily_transaction_history', $data);
				}
				$first_date_share = strtotime($check_Date_share);
			}
			//Share Buy & sell
			
			//$userID=1;

			$buy_sell_last_update_date= $this->transaction_update_scheduler_model->get_transaction_history_latest_date($userID);
			$buy_sell_first_update_date= $this->transaction_update_scheduler_model->get_transaction_history_first_date($userID);

			$buy_sell_interval_share = (($buy_sell_last_update_date-$buy_sell_first_update_date)/86400);
			
			//print_r($buy_sell_last_update_date);
			$buy_sell_cash_amount=0;
			$buy_sell_fm_share=date("Y-m-d",$buy_sell_first_update_date);
			$buy_sell_fm_share1=new DateTime($buy_sell_fm_share.'00:00:00');
			
			$buy_sell_date=date($buy_sell_fm_share1->format("Y-m-d H:i:s"));//(floor($interval)+1)
			$buy_sell_first_date=$buy_sell_first_update_date;
			
			for($i=1; $i<(number_format($buy_sell_interval_share)+2);$i++)
			{

				$buy_sell_check_Date= date("Y-m-d H:i:s",strtotime($buy_sell_date . "+".$i." days"));
				if($buy_sell_first_date>0){
				$buy_sell_cash_amount= $this->transaction_update_scheduler_model->get_admin_sharebuy_and_sell_Amount_by_date($userID,$buy_sell_first_date,strtotime($buy_sell_check_Date));
				}
				
				if($buy_sell_cash_amount>0)
				{
					//print_r(date("Y-m-d", $buy_sell_first_date)."=");
					//print_r( $buy_sell_cash_amount.",");
					
					$data = array(
					'user_id' => $userID,
					'transaction_message' => "Share Buy and Sell Amount",
					'transaction_amount' => $buy_sell_cash_amount,
					'transaction_history_date' => $buy_sell_first_date				
					);
					$this->db->insert('daily_transaction_history', $data);
				}
				$buy_sell_first_date = strtotime($buy_sell_check_Date);
			}
			//Advertise Add
			

			$adverise_add_last_update_date= $this->transaction_update_scheduler_model->get_adverise_add_latest_date($userID);
			$adverise_add_first_update_date= $this->transaction_update_scheduler_model->get_adverise_add_first_date($userID);

			$adverise_add_interval_share = (($adverise_add_last_update_date-$adverise_add_first_update_date)/86400);

			$adverise_add_cash_amount=0;
			$adverise_add_fm_share=date("Y-m-d",$adverise_add_first_update_date);
			$adverise_add_fm_share1=new DateTime($adverise_add_fm_share.'00:00:00');
			
			$adverise_add_date=date($adverise_add_fm_share1->format("Y-m-d H:i:s"));//(floor($interval)+1)
			$adverise_add_first_date=$adverise_add_first_update_date;

			for($i=1; $i<(number_format($adverise_add_interval_share)+2);$i++)
			{

				$adverise_add_check_Date= date("Y-m-d H:i:s",strtotime($adverise_add_date . "+".$i." days"));
				//print_r( date("Y-m-d H:i:s",$adverise_add_first_date));
				if($adverise_add_first_date>0){
				$adverise_add_cash_amount= $this->transaction_update_scheduler_model->get_adverise_add_Amount_by_date($userID,$adverise_add_first_date,strtotime($adverise_add_check_Date));
				}
				
				if($adverise_add_cash_amount>0)
				{
					//print_r(date("Y-m-d", $adverise_add_first_date)."=");
					//print_r( $adverise_add_cash_amount.",");
					
					$data = array(
					'user_id' => $userID,
					'transaction_message' => "Advertise Add",
					'transaction_amount' => $adverise_add_cash_amount,
					'transaction_history_date' => $adverise_add_first_date				
					);
					$this->db->insert('daily_transaction_history', $data);
				}
				$adverise_add_first_date = strtotime($adverise_add_check_Date);
			}

			}
			redirect('/admin/last_daily_earnings');
		}
		
			
		

	
	public function enable_scheduler_for_user(){
		
		$user_id_list = $this->user_transaction_update_scheduler_model->getAllUserID();
		//echo '<pre>';print_r($user_id_list);echo '</pre>';	die();
		if(!empty($user_id_list))
		{
		  foreach($user_id_list as $var)
		  {
			  $this->execute_scheduler_for_all_user($var->userID);
		  }		  
		}
		redirect('/admin/system_earnings');
	}
	
	
	public function execute_scheduler_for_all_user($userID){
		//$userID=8;
		$checmain = $this->user_transaction_update_scheduler_model->user_transaction_main_existence($userID);
		//echo '<pre>';print_r($checmain);echo '</pre>';
		if(!empty($checmain))
		{
			
			//Client_cash Paid_To_Click
			$cash_last_update_date= $this->user_transaction_update_scheduler_model->get_client_cash_latest_date($userID);		
			$cash_last_update_history_date= $this->user_transaction_update_scheduler_model->get_client_cash_history_date($userID);	

			$cash_amount=0;
			if($cash_last_update_date>0){
				$cash_amount= $this->user_transaction_update_scheduler_model->get_client_cash_Amount($userID,$cash_last_update_date,$cash_last_update_history_date);
			}
			else{
				$cash_amount= $this->user_transaction_update_scheduler_model->get_client_cash_Amount($userID,time(),$cash_last_update_history_date);
				$cash_last_update_date=time();
				
			}
					
			//echo '<pre>';print_r($cash_last_update_history_date);echo '</pre>';
			if($cash_amount>0)
			{
				$data = array(
				'user_id' => $userID,
				'transaction_message' => "Paid_To_Click",
				'transaction_amount' => $cash_amount,
				'transaction_history_date' => $cash_last_update_date				
			     );
				$this->db->insert('user_transaction_history', $data);
			}	
			//client_shares Share_To_Click
			$share_last_update_date= $this->user_transaction_update_scheduler_model->get_client_shares_latest_date($userID);
			$share_last_update_history_date= $this->user_transaction_update_scheduler_model->get_client_shares_history_date($userID);
			
			$share_amount=0;
			if($share_last_update_date>0){
				$share_amount= $this->user_transaction_update_scheduler_model->get_client_share_Amount($userID,$share_last_update_date,$share_last_update_history_date);
			}
			 else{
				$share_amount= $this->user_transaction_update_scheduler_model->get_client_share_Amount($userID,time(),$share_last_update_history_date);
				$share_last_update_date=time();
			}
			

			if($share_amount>0) 
			{
			   $data2 = array(
				'user_id' => $userID,
				'transaction_message' => "Share_To_Click",
				'transaction_amount' => $share_amount,
				'transaction_history_date' => $share_last_update_date				
				);
				$this->db->insert('user_transaction_history', $data2);
			}
			//$gifted_amount= $this->user_transaction_update_scheduler_model->get_gifted_shares_Amount($userID);
			$share_add_for_sell= $this->user_transaction_update_scheduler_model->get_selled_shares_Amount($userID);
			//$cancel_selled_shares_Amount= $this->user_transaction_update_scheduler_model->get_cancel_selled_shares_Amount($userID);
			$main_share_amount= $this->user_transaction_update_scheduler_model->get_client_shares_Amount_transaction_history($userID,time());

			$main_cash_amount= $this->user_transaction_update_scheduler_model->get_client_cash_Amount_transaction_history($userID,time());
			
			$buy_share_deduct_cash_Amount = $this->user_transaction_update_scheduler_model->get_client_buy_share_deduct_cash_Amount($userID,time());
			//echo '<pre>';print_r($main_cash_amount);echo '</pre>';	
			//transaction_main
			// if($gifted_amount>0)
			// {				
				// $share_last_update_date=time();
			// }
			
			if($cash_amount>0 || $share_amount>0 )
			{
			$data3 = array(
					'share_amount' => ($main_share_amount-$share_add_for_sell),
					'cash_amount' => ($main_cash_amount - $buy_share_deduct_cash_Amount),
					'share_last_update_date' => $share_last_update_date,
					'cash_last_update_date' => $cash_last_update_date
			
			);
			 $this->db->where('user_id', $userID);
			 $this->db->update('user_transaction_main', $data3);
			}
			
			
		}else{
			
			//Client_cash Paid_To_Click
			$cash_last_update_date= $this->user_transaction_update_scheduler_model->get_client_cash_latest_date($userID);			
			//$cash_amount= $this->user_transaction_update_scheduler_model->get_client_cash_Amount_First($userID,$cash_last_update_date);
			$cash_amount= $this->user_transaction_update_scheduler_model->get_client_cash_Amount_First($userID,time());
			
			//echo '<pre>';print_r($cash_amount);echo '</pre>';
			if($cash_amount>0)
			{
			  $data = array(
				'user_id' => $userID,
				'transaction_message' => "Paid_To_Click",
				'transaction_amount' => $cash_amount,
				'transaction_history_date' => $cash_last_update_date				
		        );
		      $this->db->insert('user_transaction_history', $data);
			}
		
		//client_shares Share_To_Click
		    $share_last_update_date= $this->user_transaction_update_scheduler_model->get_client_shares_latest_date($userID);			
			//$share_amount= $this->user_transaction_update_scheduler_model->get_client_shares_Amount_First($userID,$share_last_update_date);
			$share_amount= $this->user_transaction_update_scheduler_model->get_client_shares_Amount_First($userID,time());
			
			if($share_amount>0)
			{
			   $data2 = array(
				'user_id' => $userID,
				'transaction_message' => "Share_To_Click",
				'transaction_amount' => $share_amount,
				'transaction_history_date' => $share_last_update_date				
		       );
		     $this->db->insert('user_transaction_history', $data2);
			}
		//transaction_main
		if($share_last_update_date == null || $share_last_update_date == "")
		{
			$share_last_update_date=time();
		}
		if($cash_last_update_date == null || $cash_last_update_date == "")
		{
			$cash_last_update_date=time();
		}
		$buy_share_deduct_cash_Amount = $this->user_transaction_update_scheduler_model->get_client_buy_share_deduct_cash_Amount($userID,time());
		//$gifted_amount= $this->user_transaction_update_scheduler_model->get_gifted_shares_Amount($userID);
		$share_add_for_sell= $this->user_transaction_update_scheduler_model->get_selled_shares_Amount($userID);
		$cancel_selled_shares_Amount= $this->user_transaction_update_scheduler_model->get_cancel_selled_shares_Amount($userID);
		if($cash_amount>0 || $share_amount>0)
		{
			$data3 = array(
					'user_id' => $userID,
					'share_amount' => ($share_amount-$share_add_for_sell),
					'cash_amount' => ($cash_amount - $buy_share_deduct_cash_Amount),
					'share_last_update_date' => $share_last_update_date,
					'cash_last_update_date' => $cash_last_update_date
			
			);
			$this->db->insert('user_transaction_main', $data3);
		}
		
		}
	}

}
?>