<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct(){
		parent::__construct();
 		$this->load->model(array('user_model'));
//         $this->load->library('user_agent');
         $this->load->library('session');
//         $loggedIn = $this->usersecurity->sessionCheck();
//         if($loggedIn){
//             $this->general->redirect(DEFAULT_PAGE);
//         }
	}
	
	public function index()
	{
		$data = array();
		//ip_check
		$ip_address = "";
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$MESG = "";
		if($this->input->post('ssubmit') && $this->input->post('ssubmit') == 'Login' && $this->input->post('email')){				
				
				$userData = $this->authorisation->processLogin($this->input->post('email'),$this->input->post('password'));
				if(!empty($userData)){
				  //---- validate if ip address already exists
				  if ($this->user_model->checkIPAddress($ip_address, $userData['userID'])) 
						 {
							$MESG = "Another user using this IP address.!";
						 }
				  else{	
					if($this->user_model->validateIPAddress($ip_address)) 
					{ 
				
					}else{
						if($userData['userRoleID']==1)
						{
							
						}else{
							$data = array();
							$data = array(
										'userID' => $userData['userID'],
										'ip_address' => $ip_address,
										);
							$this->user_model->addUserIP($data);
						 }
					}
				  }
					
// 					//---- generate login stats
// 					$this->user_model->generateLoginStats("login");
//                     $pastDays60  = mktime(0, 0, 0, date("m")  , date("d")-60, date("Y"));
//                     $passwordUpdateDate =  strtotime($userData['password_date']);
					
// 					if(trim($userData['password_date']) == '' || $pastDays60 > $passwordUpdateDate){
//                         $this->session->set_userdata(array('foreceUpdatePassword' => true));
//                         $this->general->redirect('my_profile/?ch=true#tab_1_2');
//                         exit;
//                     }
//                     else
//                     {
//                         if($this->session->userdata('pageRedirectUrl')){
//                             $redirect_url = $this->session->userdata('pageRedirectUrl');
//                             $this->session->set_userdata(array('pageRedirectUrl' =>''));
//                             $this->session->unset_userdata(array('pageRedirectUrl' =>''));
//                             $this->general->redirect($redirect_url);
//                         }
//                         else
//                         {
//                             $this->general->redirect(DEFAULT_PAGE);
//                         }
//                     }
					
					//$data['MSG'] = $MSG;
					if($userData['userRoleID']==1)
					{ 
				        $this->session->set_flashdata('MSGS',$MESG);
						$this->general->redirect('admin/dashboard');
					}
					else 
					{
						$this->session->set_flashdata('MSGS', $MESG);
					    $this->general->redirect('mybitshares');
					}
				}
				else{
					$data['MSG'] = "Username or password invalid.";
					$data['err'] = 1;
				}
		}
		$this->load->view('users/login',$data);
	}
	public function process_forgot_password(){
		if($_POST['forgot_password']=="Submit"){
			$user_info = $this->user_model->getUserByEmail($_POST["user_email"]);	
			if($user_info){
				//Prepare password reset url and link
				$password_reset_url = SITE_URL."login/reset_password/?pid=reset_password&uid=".$user_info['userID']."&cid=".$user_info['password'];
				$password_reset_link = "<a href='".$password_reset_url."' target='_blank'>Click Here</a> to reset your password.";
				//Prepare to Send Email to The User
				$emailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								 <html xmlns="http://www.w3.org/1999/xhtml">
								 <head></head>
								 <body style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
								   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
									 Dear '.$user_info['firstName'].' '.$user_info['LastName'].',
								   </p>
								   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
									You have requested to reset your password.
								   </p>
								   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
									 '.$password_reset_link.'
								   </p>
								   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
									  Regards<br/><br/>
									  IT Support Team<br/>
									  Amey Streetworks
								   </p>
								 </body>
								 </html>';
				$emailSubject = 'Amey Streetworks - Reset Your Password';
				$recipients  = $user_info['email'];		
				
				// Send Email
				$mail_sent = $this->usersecurity->sendEmail($emailBody, $emailSubject, $recipients);
				
				if($mail_sent){
					$this->general->redirect('login/?msg=email_sent');
				}
				else{
					$this->general->redirect('login');
				}
			}
			else{
				$this->general->redirect('login/?msg=email_not_found');
			}
		}
		
	}
	
	public function reset_password(){
		
		$this->load->view('reset_password');
	}
	public function process_reset_password(){
		
		if(!empty($_POST["password"]) && !empty($_POST["confirm_password"]) && ($_POST["password"]==$_POST["confirm_password"])){

			$userInfo = $this->user_model->getUserByCode($_POST['uid'], $_POST['cid']);
			//---- update password
			if(!empty($userInfo)){
				$result = $this->user_model->updatePassword($_POST['uid'], $_POST['password']);		
				
				if($result){
					//---- Prepare login url and link
					$login_url = $this->url->login();
					$login_link = "<a href='".$login_url."' target='_blank'>Click Here</a> to visit login page.";
					
					//---- Prepare to Send Email to The User
					$emailBody = '<!DOCTYPE >
									 <html xmlns="http://www.w3.org/1999/xhtml">
									 <head></head>
									 <body style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
									   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
										 Dear '.$userInfo['firstName'].' '.$userInfo['LastName'].',
									   </p>
									   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
										Your password has been successfully changed.
									   </p>
									   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
										 '.$login_link.'
									   </p>
									   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
										  Regards<br/><br/>
										  IT Support Team<br/>
										  Amey Streetworks
									   </p>
									 </body>
									 </html>';
					$emailSubject = 'Amey Streetworks - Your Password Changed';
					$recipients  = $userInfo['email'];
					
					//---- get Email Object
					$mail_sent = $this->usersecurity->sendEmail($emailBody, $emailSubject, $recipients);			
					
					if($mail_sent){
						//---- Redirect User to Reset Password Page with a success message
						$this->general->redirect('login/?msg=pasw_reset_success');
					}
					else{
						//---- Redirect User to Reset Password Page with an error message
						$this->general->redirect('login/?msg=pasw_reset_fail');
					}
				}
			}
			else{
				//---- Redirect User to Reset Password Page with an error message
				$this->general->redirect('login/?msg=pasw_reset_fail');
			}
		}
	}
	
	public function cookie_policy(){
		
		
		$this->load->view('cookie_policy');	
	}
	
}
