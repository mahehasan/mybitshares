<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cron extends CI_Controller {

	function __construct()
	{		
		parent::__construct();
		$this->load->library('form_validation');
     	$this->load->helper('form');
		$this->load->model(array('cron_model', 'referral_model'));
		$this->timestamp = time();
	}

	public function index(){
		$this->cron_model->update_all_user_price_history();
		//$this->system_income_bonus_distribute();
	}

	public function system_income_bonus_distribute()
	{
		$all_users_income = $this->cron_model->get_all_users_last_24_hours_income();
		$total_income_24hr=0;
		foreach ($all_users_income as $key => $value) {
			$total_income_24hr+=$value;
		}
		$system_share_percentage = $this->cron_model->get_daily_income_system_share();
		$this->cron_model->update_system_daily_income($total_income_24hr);
		$system_share = ($total_income_24hr*$system_share_percentage)/100;
		$total_shares_of_system = $this->cron_model->get_total_shares_of_system();
		$per_share_value = $system_share / $total_shares_of_system;
		$all_shares = $this->cron_model->get_all_shares_of_user();
		foreach ($all_shares as $row) {
			$system_income_bonus = ($row->current_share_number*$per_share_value);
			$this->cron_model->update_system_income_bonus_of_user($row->user_id,$system_income_bonus);
		}
	}


}
?>