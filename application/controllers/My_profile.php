<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_profile extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model(array('user_model'));
		$p = $this->uri->uri_string();
		$this->usersecurity->performPremissionChecks($p);
	}
	
	public function index()
	{
		$id = $this->session->userData('userID');
		$resultUser = $this->user_model->getUserById($id);
		$this->session->set_userdata('password',$resultUser["password"]);
		$data['id']  = $id;
		$data['resultUser']  = $resultUser;
		$MSG  = '';
			
		//---- Reset Password
		if(isset($_REQUEST['ssubmit']) && $_REQUEST['ssubmit'] == "PostReplyResetPass"){
				$DB = $this -> input->post('DB');
				$email = $DB['email'];
				$data['email']  = $email;
							
		
				if(isset($_POST['generateTempPass']) && $_POST['generateTempPass'] == 'Generate Temporary Password'){
					//---- Generate Random Password
					
					$characters = 'abcdefgh45ij345kl345mn345o34pqrs6tuvwxyz0123456789ABFDYTU8899IEJKASIU3213567987564SHASIYQZXMKOIUYahdye8754322reiuyh5489i2342349867fASUYJhu7090230936598yasjhaseuyoqpppiurytybvldmAWYUQYRNGLIOYTO';
					$tempPass = '';
					$random_string_length = 8;
					for ($i = 0; $i < $random_string_length; $i++) {
						$tempPass .= $characters[rand(0, strlen($characters) - 1)];
					}
			
					$randPass = md5($tempPass);
					if($id==$this->session->userData('userID')){
						$this->session->set_userdata('password',$randPass) ;
						$this->session->set_userdata(array('foreceUpdatePassword' => false));
					}
					
					unset($DB['password_confirm']);
					unset($DB['password']);
					unset($DB['email']);
					$DB['password'] = $randPass;
					$DB['userID'] = $id;
					$DB["password_date"] = date("Y-m-d");	
					$this->query_manager->selectTbl(APP_USERS);
					$this->query_manager->updateRecordV2($DB,"userID");
					
					$user_info = $this->user_model->getUserByEmail($email);
			
					if($user_info){
						//---- Prepare password reset url and link
						$password_reset_url = SITE_URL."reset_password/?pid=reset_password_match&uid=".$user_info['userID']."&cid=".$randPass;
						$password_reset_link = "<a href='".$password_reset_url."' target='_blank'>Click Here</a> to reset your password.";
						
						//---- Prepare to Send Email to The User
						$emailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
									 <html xmlns="http://www.w3.org/1999/xhtml">
									 <head></head>
									 <body style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
									   <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
										 Dear <b>'.$user_info['firstName'].' '.$user_info['LastName'].'</b>,
									   </p>
									   <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
										This email contains temporary password for your account generated by one of our admins.
									   </p>
									   <p>
									   Please note:
										<ul>
											<li>When distributing IDs and passwords to individual users, be sure to do so in a safe and secure manner.</li>
											<li>This Password is valid for 60 Days.</li>
										</ul>
									   </p>
									   <p>
									   <b>User Name: </b> '.$email.' <br />
									   <b>Password: </b>'.$tempPass.'
									   </p>
									   <p>Once you successfully signed in with the credentials mentioned above, you can create new passwords by following the steps below.</p>
									   <h3 style="color:#3772C7">To sign in and change your password</h3>
									   <ol>
										<li>Click on password reset link under admin menu.</li>
										<li>Enter corresponding temporary or current password.</li>
										<li>Follow the instruction on the screen to reset password.</li>
									   </ol>
									   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
										  Regards<br/><br/>
										  IT Support Team<br/>
									   </p>
									 </body>
									 </html>';
			
						$emailSubject = 'Defect Manager - Password for '.$user_info['firstName'].' '.$user_info['LastName'];
						$recipients  = $user_info['email'];
			
						//---- prepare and send Email
						$mailer = $this->usersecurity->getEmailObject();
						$mailer->Body = $emailBody;
						$mailer->Subject = $emailSubject;
						$mailer->AddAddress($recipients);
						$mailer->Send();
						$mail_sent=1;
						$invalidPass=0;
						$MSG = "Password has been sent Successfully!";
					}
				}
				else{
					
					$md5Pass = md5($_POST['currPass']);
					$invalidPass = 0;
					if($invalidPass==0){
						unset($DB['password_confirm']);
						unset($DB['email']);
					  	
						$DB['password'] = md5($DB['password']);
						$DB["password_date"] = date("Y-m-d");	
						if($id==$this->session->userData('userID')){
							$this->session->set_userdata('password',$DB["password"]) ;
							$this->session->set_userdata(array('foreceUpdatePassword' => false));
						}
						
						$DB['userID'] = $id;
						$this->query_manager->selectTbl(APP_USERS);
						$this->query_manager->updateRecordV2($DB,"userID");
						
						$MSG = "Password has been reset successfully!";
					}	
				}
			}
					
			//---- update user info		
			if(isset($_REQUEST['ssubmit']) && $_REQUEST['ssubmit'] == "editInfo") { 
			  	$DB = $this->input->post('DB');
				$DB['lastUpdated'] =  time();
				$DB['lastUpdatedBy'] = $this->session->userData('userID');
			
				$DB['userID'] = $this->session->userData('userID');;
				$this->query_manager->selectTbl(APP_USERS);
				$this->query_manager->updateRecordV2($DB,"userID");
			
				$MSG = "Information has been updated successfully!";
				$err = 0;
				
				$data['err']  = $err;
			  
			}
		
		  $DB = $rsUserInfo =  $this->user_model->getUserById($this->session->userData('userID'));
		  $data['MSG']  = $MSG;
		  $data['DB'] = $DB;
		  $data['invalidPass']  = @$invalidPass;
		  
		  
		  		  
				$this->load->view('my_profile',$data);
		  
	}
}
