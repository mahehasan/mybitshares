<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		//$this->load->model(array('user_model', 'General_model'));
		//$this->load->model(array('defect_model', 'Dashboard_model', 'Defect_costs_model', 'Defect_inspections_model'));
		//$p = $this->uri->uri_string();
		//$this->usersecurity->performPremissionChecks($p);
		$this->load->library('session');
		$this->session->keep_flashdata('mesg');
	}
	
	public function index()
	{
		

		$data = array();		

		$MSG = $this->session->flashdata('mesg');
		$data['MSG'] = $MSG;
		$this->load->view('dashboard/dashboard',$data);
				
	}
	public function dashboard()
	{
		//$this->authorisation->processLogout();
		$data['MSG'] ="Your Account has been successfully deactivated.!";
		$this->load->view('dashboard/dashboard',$data);
				
	}
	
	public function permissionError()
	{
		$this->message->display();	
		
		$this->load->view('permission_error');
	}
	
	public function __list_slg_nonslg_summary_ajax_call() {			
		
		$thisTime = date("Y-m-d G:i:s");		
		$post = $this->input->post(NULL, TRUE);
		$get  = $this->input->get(NULL, TRUE);
		
		$dateStart = @$post['dateStart'];
		$dateEnd = @$post['dateEnd'];
		
		// Get contracts belongs to user if there roles are limited
        $contract_qry = "";
        if(in_array($_SESSION['userRoleID'],array("3","4"))){
             $contract_qry .= $this->general->default_contract_query();
		}


		/*---------------------------------------------------------------------------
			START:   SLG Dashboard Data
		-----------------------------------------------------------------------------*/		
		
		$where = DM_DEFECTS.".defectTypeID IN(1, 3)";
		$where .= " AND (". DM_DEFECTS.".dateTimeReceivedWithAmey BETWEEN '".$dateStart."' AND '".$dateEnd."')";
        $where .= $contract_qry;

        $order_by = DM_DEFECTS.".dateTimeReceivedWithAmey";
		$slgDefects = $this->Dashboard_model->getDashboardDefects($where, $order_by, "DESC" );
		
		//-- Initilizing
		$__slgCountData["totalSLGDefects"] = 0;
		$__slgCountData["totalCompletions"] = 0;
		$__slgCountData["total2Hours"] = 0;
		$__slgCountData["total4Hours"] = 0;
		
		$__slgCountData["totalSLGDefects"] = count($slgDefects);
		
		$slgReportData = array();
		$slg2HoursData = array();
		$slg4HoursData = array();
		
		if($slgDefects) {
			foreach($slgDefects as $i => $defect) {
								
				//-- Check if closed/completed
				if($defect["status"] == 'Closed' && $defect["subStatus"] == "Completed") {
					$__slgCountData["totalCompletions"]++;					
				}
				
				//-- Check 2 Hours or 4 Hours
				if($defect["defectTypeID"] == '1') {
					$__slgCountData["total2Hours"]++;
					$slg2HoursData[] = $defect;
				} else {
					$__slgCountData["total4Hours"]++;
					$slg4HoursData[] = $defect;
				}
			}
		}
		
		
		//-- 2 Hours classification	
		$__slg2HoursData = array();
		if(count($slg2HoursData)) {
			
			foreach($slg2HoursData as $row) {				
				if($row["status"] == 'Closed' && $row["subStatus"] == "Completed") {					
					$__slg2HoursData["completed"][] = $row;
				} elseif( $this->general->calculateHours($thisTime, $row["dateTimeReceivedWithAmey"]) > 2 ) {
					$row["highway_authority"] = $row["highway_authority"].'('.$this->general->calculateHours($thisTime, $row["dateTimeReceivedWithAmey"]).')';
					$__slg2HoursData["overdue"][] = $row;
				}			
			}
		}
		
		
		//-- 4 Hours classification	
		$__slg4HoursData = array();
		if(count($slg4HoursData)) {	
			
			foreach($slg4HoursData as $row) {				
				if($row["status"] == 'Closed' && $row["subStatus"] == "Completed") {
					$__slg4HoursData["completed"][] = $row;
				} elseif( $this->general->calculateHours($thisTime, $row["dateTimeReceivedWithAmey"]) > 4 ) {
					$__slg4HoursData["overdue"][] = $row;
				}			
			}
		}		
		
		$data_slg["slgCountData"] = $__slgCountData;		
		
		//-- Getting onsite Data
		$where = DM_DEFECTS.".defectTypeID IN(1, 3)";
        $where .= " AND (". DM_DEFECTS.".dateTimeReceivedWithAmey BETWEEN '".$dateStart."' AND '".$dateEnd."')";
        //$where .= " AND ".DM_WORK_ALLOCATIONS.".offSiteDateTime = '0000-00-00'";
        $where .= $contract_qry;

	    $order_by = DM_DEFECTS.".dateTimeReceivedWithAmey";

		$onSiteDefects = $this->Dashboard_model->getOnSiteDefects($where, $order_by);
		if($onSiteDefects) {
			foreach($onSiteDefects as $row) {
				
				//-- Check 2 Hours or 4 Hours
				if($row["defectTypeID"] == '1') {					
					$__slg2HoursData["onsite"][] = $row;
				} else {					
					$__slg4HoursData["onsite"][] = $row;
				}
			}
		}
		$data_slg["slg2HoursData"] = $__slg2HoursData;
		$data_slg["slg4HoursData"] = $__slg4HoursData;		
		
		$slg_summary = $data["slg_summary"] = $this->load->view('dashboard/slg_summary',$data_slg, TRUE);
		/*---------------------------------------------------------------------------
			END:     SLG Dashboard Data Starts
		-----------------------------------------------------------------------------*/
		
		
		
		/*---------------------------------------------------------------------------
			START:   Non SLG Dashboard Data
		-----------------------------------------------------------------------------*/		
		
		$where = DM_DEFECTS.".defectTypeID IN(5, 6)";
		$where .= " AND (". DM_DEFECTS.".dateTimeReceivedWithAmey BETWEEN '".$dateStart."' AND '".$dateEnd."')";
        $where .= $contract_qry;

		$order_by = DM_DEFECTS.".dateTimeReceivedWithAmey";
		$nonSlgDefects = $this->Dashboard_model->getDashboardDefects($where, $order_by, "DESC" );
		
		//-- Initilizing
		$__nonSlgCountData["totalNonSLGDefects"] = 0;
		$__nonSlgCountData["totalCompletions"] = 0;
		$__nonSlgCountData["totalVisual"] = 0;
		$__nonSlgCountData["totalCore"] = 0;
		
		$__nonSlgCountData["totalNonSLGDefects"] = count($nonSlgDefects);
		
		$nonSlgReportData = array();
		$visualData = array();
		$coreData = array();
		
		if($nonSlgDefects) {
			foreach($nonSlgDefects as $i => $defect) {
								
				//-- Check if closed/completed
				if($defect["status"] == 'Closed' && $defect["subStatus"] == "Completed") {
					$__nonSlgCountData["totalCompletions"]++;					
				}
				
				//-- Check Visual or Core
				if($defect["defectTypeID"] == '5') {
					$__nonSlgCountData["totalVisual"]++;
					$visualData[] = $defect;
				} else {
					$__nonSlgCountData["totalCore"]++;
					$coreData[] = $defect;
				}
			}
		}
				
		//-- Visual classification	
		$__visualData = array();
		$__coreData = array();
		if(count($visualData)) {
			
			foreach($visualData as $row) {				
				if($row["status"] == 'Closed' && $row["subStatus"] == "Completed") {					
					$__visualData["completed"][] = $row;
				} elseif( $this->general->calculateDays($thisTime, $row["dateTimeReceivedWithAmey"], false) > 17 ) {
					$row["highway_authority"] = $row["highway_authority"].'('.$this->general->calculateDays($thisTime, $row["dateTimeReceivedWithAmey"], false).')';
					$__visualData["overdue"][] = $row;
				}			
			}
		}		
		
		//-- Core classification	
		$coreData = array();
		if(count($coreData)) {			
			foreach($coreData as $row) {				
				if($row["status"] == 'Closed' && $row["subStatus"] == "Completed") {
					$__visualData["completed"][] = $row;
				} elseif( $this->general->calculateDays($thisTime, $row["dateTimeReceivedWithAmey"], false) > 17 ) {
					$row["highway_authority"] = $row["highway_authority"].'('.$this->general->calculateDays($thisTime, $row["dateTimeReceivedWithAmey"], false).')';
					$__visualData["overdue"][] = $row;
				}			
			}
		}
		
		$data_nonslg["nonSlgCountData"] = $__nonSlgCountData;
		//-- Getting onsite Data
		
		$where = DM_DEFECTS.".defectTypeID IN(5, 6)";
        $where .= " AND (". DM_DEFECTS.".dateTimeReceivedWithAmey BETWEEN '".$dateStart."' AND '".$dateEnd."')";
        $where .= $contract_qry;

		$order_by = DM_DEFECTS.".dateTimeReceivedWithAmey";
		
		$onSiteNonSGLDefects = $this->Dashboard_model->getOnSiteDefects($where, $order_by);
		if($onSiteNonSGLDefects) {
			foreach($onSiteNonSGLDefects as $row) {
				
				//-- Check visual or Core
				if($row["defectTypeID"] == '4') {					
					$__visualData["onsite"][] = $row;
				} else {					
					$__coreData["onsite"][] = $row;
				}
			}
		}
		$data_nonslg["visualData"] = $__visualData;
		$data_nonslg["coreData"] = $__coreData;		
		
		$visual_core_sumary = $data["visual_core_summary"] = $this->load->view('dashboard/visual_core_summary',$data_nonslg, TRUE);
		/*---------------------------------------------------------------------------
			END:     Non-SLG Dashboard Data Starts
		-----------------------------------------------------------------------------*/
		
		echo $slg_summary . $visual_core_sumary ;
	}
	
	// KPI Management Report
	public function __kpi_management() {
		
	  if($_SESSION['userRoleID'] == 1) {
		  
        $post = $this->input->post(NULL, TRUE);
		$get  = $this->input->get(NULL, TRUE);
		$year = @$post['year'];
		
		$year = $year == '' ? date('Y') : $year;
		$dateStart =  $year.'-01-01 00:00:00';
		$dateEnd = $year.'-12-31 23:59:59';
		
		$data['year'] = $year;
		//-- Counting total defects
		$where = " (". DM_DEFECTS.".dateTimeReceivedWithAmey BETWEEN '".$dateStart."' AND '".$dateEnd."')";
		$totalDefects = $this->defect_model->count_defects($where);
		$kpi['totalDefects'] = $totalDefects;
		
		//-- Re Defect Percentage		
		$where = " (". DM_DEFECTS.".clonedFrom > 0)";
		$totalReDefects = $this->defect_model->count_defects($where);
		$reDefectPercentage = '0';
		if($totalDefects > 0) {
			$reDefectPercentage = ($totalReDefects / $totalDefects) * 100;
		}
		$kpi['reDefectPercentage'] = number_format($reDefectPercentage, 2) . '%';
		
		// -- Total Cost
		$where = " timeStamp >= '".strtotime($dateStart)."' AND  timeStamp<= '".strtotime($dateEnd)."'";
		$defectCostRes = $this -> Defect_costs_model -> totalDefectsCost($where);
		$total_defect_cost = $defectCostRes[0]['total_defect_costs'];
		
		$where = " (inspectionDateTime BETWEEN '".$dateStart."' AND '".$dateEnd."')";
		
		$total_inspection_cost = $this->Defect_inspections_model -> totalInspectionsCost($where);
		$anualCost = $total_defect_cost + $total_inspection_cost;
		
		$kpi['anualCost'] = $anualCost;
		
		$kpi['avgCostPerMonth'] = $anualCost / 12;
		if($totalDefects > 0) {
			$kpi['avgCostPerDefect'] = $anualCost / $totalDefects;
		} else {
			$kpi['avgCostPerDefect'] = 0;
		}
		
		$where = " (". DM_DEFECTS.".dateTimeReceivedWithAmey BETWEEN '".$dateStart."' AND '".$dateEnd."')";
		$topUsedHA = $this->defect_model->topUsedHA($where, 10);
		$kpi['topUsedHA'] = $topUsedHA;
				
		$data['kpi'] = $kpi;
		
		echo $this->load->view('dashboard/kpi_management',$data, TRUE);
      }
	}
	
	
	public function slaStatusManagement() {
		
		$data = array();
		
		$defectsOnStage = $this->defect_model->countDefectsOnStages();
		
		//-- Defect Input
		$sla_status["stage_1"] = $defectsOnStage['Defect'];
				
		//-- Awaiting JSM
		$sla_status["stage_2"] = $defectsOnStage['JSM'];
		
		//-- Quotes Required
		$sla_status["stage_3"] = $defectsOnStage['Quotes'];
		
		//-- Scheduling
		$sla_status["stage_4"] = $defectsOnStage['Scheduling'];
		
		//-- Work in Progress
		$sla_status["stage_5"] = $defectsOnStage['WIP'];
		
		// Completed
		$sla_status["stage_6"] = $defectsOnStage['Completed'];
		
		
		$data['sla_status'] = $sla_status;
		
		return $this->load->view('dashboard/sla_status',$data, TRUE);				
	}
	
	public function ___ageSLA() {
		
		$post = $this->input->post(NULL, TRUE);
		$get  = $this->input->get(NULL, TRUE);
		
		$defectAgeProfileWithSLA = $this->Dashboard_model->getDefectAgeProfileWithSLA($post['startDate'], $post['endDate']);		
		if(empty($defectAgeProfileWithSLA)) {
				$data['isEmpty'] = 1;
		} else {
			$data["defectAgeProfileWithSLA"] = $defectAgeProfileWithSLA;
			$data['isEmpty'] = 0;
		}
		
		echo $this->load->view('dashboard/aga_sla',$data, TRUE);
	}

   

    public function __getSiteWorkChartData(){
		
        $result = array("plannedWA"=>"0","completedWA"=>"0","abortedWA"=>"0");

        $dateToday = date('Y-m-d');
        $WeekStartDate = date('Y-m-d', strtotime("monday this week"));
        $LastDayOfWeek = date('Y-m-d', strtotime("sunday this week"));

        $post = $this->input->post(NULL, TRUE);
        if ( !empty($post) ){
            $dateStart = !empty($post['dateStart'])?$post['dateStart']:"";
            $dateEnd = !empty($post['dateEnd'])?$post['dateEnd']:"";
        }

        // Get contracts belongs to user if there roles are limited
        $contract_qry = "";
        if(in_array($_SESSION['userRoleID'],array("3","4"))){
           $contract_qry .= $this->general->default_contract_query();
        }

        $plannedWhere = DM_DEFECTS.".status != 'Closed' ";
        //$plannedWhere .= " AND ".DM_WORK_ALLOCATIONS.".workEndDate BETWEEN '".(!empty($dateStart)?$dateStart:$WeekStartDate)."' AND '".(!empty($dateEnd)?$dateEnd:$LastDayOfWeek)."'";
        if(!empty($dateStart))
            $plannedWhere .= " AND ".DM_WORK_ALLOCATIONS.".workEndDate BETWEEN '".$dateStart."' AND '".$dateEnd."'";
        $plannedWhere .= " AND ".DM_WORK_ALLOCATIONS.".workEndDate > ".$dateToday;
        $plannedWhere .= " AND ".DM_WORK_ALLOCATIONS.".onSiteDateTime = '0000-00-00 00:00:00' AND ".DM_WORK_ALLOCATIONS.".offSiteDateTime = '0000-00-00 00:00:00'";
        $plannedWhere .= $contract_qry;
        $plannedWA = $this->defect_model->count_defects_work_allocation_to_terms($plannedWhere);
        $result['plannedWA'] = !empty($plannedWA)?$plannedWA:0;

        $completedWhere = DM_DEFECTS.".status != 'Closed' ";
        //$completedWhere .= " AND ".DM_WORK_ALLOCATIONS.".workEndDate BETWEEN '".(!empty($dateStart)?$dateStart:$WeekStartDate)."' AND '".(!empty($dateEnd)?$dateEnd:$LastDayOfWeek)."'";
        if(!empty($dateStart))
            $completedWhere .= " AND ".DM_WORK_ALLOCATIONS.".workEndDate BETWEEN '".$dateStart."' AND '".$dateEnd."'";
        $completedWhere .= " AND ".DM_WORK_ALLOCATIONS.".onSiteDateTime != '0000-00-00 00:00:00' AND ".DM_WORK_ALLOCATIONS.".offSiteDateTime != '0000-00-00 00:00:00'";
        $completedWhere .= $contract_qry;
        $completedWA = $this->defect_model->count_defects_work_allocation_to_terms($completedWhere);
        $result['completedWA'] = !empty($completedWA)?$completedWA:0;

        $abortedWhere = DM_DEFECTS.".status = 'Open' AND ".DM_DEFECTS.".subStatus = 'Aborted Visit' ";
        //$abortedWhere .= "AND ".DM_WORK_ALLOCATIONS.".workEndDate BETWEEN '".(!empty($dateStart)?$dateStart:$WeekStartDate)."' AND '".(!empty($dateEnd)?$dateEnd:$dateToday)."'";
        if(!empty($dateStart))
            $abortedWhere .= "AND ".DM_WORK_ALLOCATIONS.".workEndDate BETWEEN '".$dateStart."' AND '".$dateEnd."'";
        $abortedWhere .= $contract_qry;
        $abortedWA = $this->defect_model->count_defects_work_allocation_to_terms($abortedWhere);
        $result['abortedWA'] = !empty($abortedWA)?$abortedWA:0;

		echo $this->load->view('dashboard/ajax_includes/site_work_chart_data',$result, true);
    }
	
	public function __getJoinSiteMeetingsData() {
		
		$dateTimeNOW = date('Y-m-d H:i:s');
        $dateStart = date('Y-m-d 00:00:00', strtotime("monday this week"));
        $dateEnd = date('Y-m-d 23:59:59', strtotime("sunday this week"));
		
		$post = $this->input->post(NULL, TRUE);
        if ( !empty($post) ){
            $dateStart = !empty($post['dateStart'])?$post['dateStart']:"";
            $dateEnd = !empty($post['dateEnd'])?$post['dateEnd']:"";
        }


		
		 // Get contracts belongs to user if there roles are limited
        $contract_qry = "";
        if(in_array($_SESSION['userRoleID'],array("3","4"))){
            $contract_qry .= $this->general->default_contract_query();
        }
		
		$plannedWhere = DM_DEFECTS.".status <> 'Closed'";
        if(!empty($dateStart))
            $plannedWhere .= " AND ".DM_DEFECTS_JSM_LIABILITY.".JSMDateTime BETWEEN '".$dateStart."' AND '".$dateEnd."' ";
        $plannedWhere .= " AND ".DM_DEFECTS_JSM_LIABILITY.".JSMDateTime > '".$dateTimeNOW."'";
		
        $plannedWhere .= $contract_qry;
        $plannedJSM = $this->Dashboard_model->getJoinSiteMeetingData( $plannedWhere);
        $data['planned'] = !empty($plannedJSM[0]['total'])?$plannedJSM[0]['total']:0;

        $completedWhere = DM_DEFECTS.".status <> 'Closed'";
        if(!empty($dateStart))
            $completedWhere .= " AND ".DM_DEFECTS_JSM_LIABILITY.".JSMDateTime BETWEEN '".$dateStart."' AND '".$dateEnd."'";
        $completedWhere .= " AND ".DM_DEFECTS_JSM_LIABILITY.".JSMDateTime <= '".$dateTimeNOW."'";
		
        $completedWhere .= $contract_qry;
        $completedJSM = $this->Dashboard_model->getJoinSiteMeetingData( $completedWhere);
        $data['completed'] = !empty($completedJSM[0]['total'])?$completedJSM[0]['total']:0;
		
		echo $this->load->view('dashboard/ajax_includes/join_site_meetings_chart_data',$data, true);
	}
}