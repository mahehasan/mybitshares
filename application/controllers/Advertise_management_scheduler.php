<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Advertise_management_scheduler extends CI_Controller {
   private $timestamp;
   private $adminID;
	function __construct()
	{		
		parent::__construct();
		$this->load->model(array('advertise_management_scheduler_model'));
	}
	
	public function disable_share_advertise_scheduler(){
		$newSite_server_ads=array();
		$all_advertise = $this->advertise_management_scheduler_model->get_all_sahre_to_click_advertise();
		
		if(!empty($all_advertise)){
			foreach ($all_advertise as $advertise){
				$client_click_share_amount =$this->advertise_management_scheduler_model->get_client_total_click_share($advertise->advertise_id);
				$get_server_total_click_share =$this->advertise_management_scheduler_model->get_server_total_click_share($advertise->advertise_id);
				
				//array_push($newSite_server_ads,$advertise->advertise_id,$client_click_share_amount,$get_server_total_click_share);

				if($client_click_share_amount >= $get_server_total_click_share)
				{
					$updateData = array('share_to_click_publish' => 0);
					 $this->advertise_management_scheduler_model->desable_advertise($advertise->advertise_id,$updateData);					 
				}
				else
				{
					//echo($client_click_share_amount);
					
				}
			}
			
			//print_r($newSite_server_ads);
		}

	}
	public function disable_paid_advertise_scheduler(){
		
		$all_advertise = $this->advertise_management_scheduler_model->get_all_paid_to_click_advertise();
		
		if(!empty($all_advertise)){
			foreach ($all_advertise as $advertise){
				$client_click_paid_amount =$this->advertise_management_scheduler_model->get_client_total_click_paid($advertise->advertise_id);
				$get_server_total_click_paid =$this->advertise_management_scheduler_model->get_server_total_click_paid($advertise->advertise_id);
				
				if($client_click_paid_amount >= $get_server_total_click_paid)
				{
					 $updateData = array('paid_to_click_publish' => 0);
					 $this->advertise_management_scheduler_model->desable_advertise($advertise->advertise_id,$updateData);					 
				}
				else
				{
					//echo($client_click_share_amount);
					
				}
			}
		}

	}
	public function disable_all_desable_advertise_scheduler(){
		
		$all_advertise = $this->advertise_management_scheduler_model->get_desable_advertise();
		
		if(!empty($all_advertise)){
			foreach ($all_advertise as $advertise){

					 $updateData = array('advertise_publish' => 0);
					 $this->advertise_management_scheduler_model->desable_advertise($advertise->advertise_id,$updateData);					 
			}
		}

	}
	
	public function test()
	{
		$date1 = new DateTime('2016-05-20 12:00:49 PM');
		$date2 = new DateTime('2016-05-30 7:15:49 PM');

		 $diff = $date2->diff($date1);
		$hours = $diff->h;
		$date3 =1464613779-1463738449;
		$date4=($date3/60);

		$date4=($date4/60);
		// $diff = $date2->diff($date3);
		echo $date4. ', ';
		// $hours = $diff->h;
		echo $hours.'<br>';
		echo date('Y-m-d h:i:s A', 1463738449).'<br>';
		// echo $hours.'<br>';
		// echo $diff->format('%h');
	}

}
?>