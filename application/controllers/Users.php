<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('user_model', 'User_model'));
		$this->load->library(array('pagination','Query_manager'));

// 		$p = $this->uri->uri_string();
// 		$this->usersecurity->performPremissionChecks($p);
	}

	public function index()
	{
		$data['cur_page_back'] =  (isset($_REQUEST['cur_page']))?$_REQUEST['cur_page']:0;
		$data['page_limit_back'] =  (isset($_REQUEST['page_limit']))?$_REQUEST['page_limit']:0;
		$data['user_newly_edit_id']=  (isset($_REQUEST['user_newly_edit_id']))?$_REQUEST['user_newly_edit_id']:0;
		$data['filterUserType'] =  (isset($_REQUEST['filterUserType']))?$_REQUEST['filterUserType']:'';
		$data['name'] =  (isset($_REQUEST['name']))?$_REQUEST['name']:'';
		$EditBox = (isset($_REQUEST['EditBox']))?$_REQUEST['EditBox']:'';
		//---- Delete Selected Users
		if(isset($_REQUEST['assetAction']) && $_REQUEST['assetAction'] == "delete"){
			for ($j = 0; $j < count($EditBox); $j++) {
				$this->user_model->deleteUserById($EditBox[$j]);
			}
			$data['MSG'] = "Selected User(s) have been deleted successfully.";
		}
		/*----------------------------------------------------------------------------------------------------------------------------------------------------
		| Process default Filters
		------------------------------------------------------------------------------------------------------------------------------------------------------*/
		$data['userTypes'] = $this->user_model->getUserLevels();

		//---- default date filters
		$this->load->view('admin/users/list_users',$data);
	}

	public function list_users(){
		
		// If ajax request then list data
		if ( $this->input->is_ajax_request() )
		{
			$this->__list_users_view_ajax();
			exit();
		}
		
		// If download CSV requested
		if ( $this->input->get('download_csv') ){
			$this->__download_users_CSV();
			exit();
		}
		
		
		$this->index();

	}

	public function join_user(){
		//$data['rsUserTypes'] = $this->user_model->getUserLevels();
		$data = array();
// 		echo "<pre>";
// 		print_r($_REQUEST);
// 		echo "</pre>";
// 		die;
		if( isset($_REQUEST['ssubmit']) ) {
			$postData = $this->input->post(NULL, TRUE);
			//print_r($postData);die;

            //ip_check
			$ip_address = "";
			$ip_address = $_SERVER['REMOTE_ADDR'];
			//$ip_address =$this->input->ip_address();

			if(!empty($postData['sign_up_email'])){
				unset($postData['ssubmit']);
				//---- validate if email already exists
				if ($this->user_model->validateEmailAddress($postData['sign_up_email'])) {
					$MSG = "Email already exists!";
				    $mid = 1;
					$err = 1;
				}
				else{
					//---- validate if ip address already exists

					if($this->user_model->validateIPAddress($ip_address)) 
					{
						$MSG = "IP Address already exists!";
						$mid = 1;
					    $err = 1;
					}
				}
			}
			
			if (!isset($MSG) && !empty($postData['sign_up_email'])) {
// 				print_r($postData);die;
				$this->session->set_userdata('verified_email', $postData['sign_up_email']);
				$this->general->redirect( 'users/user_details' );
			}
			if(!empty($postData['verified_email'])){
				//---- Create a Merchant client folder
				$email = $postData['verified_email'];
				//--- If auto generate password
				if (isset($DB['sendPass']) && $DB['sendPass']) {
					$characters = 'abcdefgh45ij345kl345mn345o34pqrs6tuvwxyz0123456789ABFDYTU8899IEJKASIU3213567987564SHASIYQZXMKOIUYahdye8754322reiuyh5489i2342349867fASUYJhu7090230936598yasjhaseuyoqpppiurytybvldmAWYUQYRNGLIOYTO';
					$temp_pass = '';
					$random_string_length = 8;
					for ($i = 0; $i < $random_string_length; $i++) {
						$temp_pass .= $characters[rand(0, strlen($characters) - 1)];
					}
					$DB['password'] = $temp_pass;
					$password = $DB['password'];


				} else {
					$password = $postData['password'];
				}
				
				$postData['email'] = $email;
				$postData['password'] = md5($password);
				$postData['timeStamp'] =  time();
				$postData["password_date"] = date("Y-m-d");

				unset($postData['ssubmit']);
				unset($postData['cpassword']);
				unset($postData['verified_email']);
				unset($postData['send_emails_to']);
// 				echo "<pre>";
// 				print_r($postData);
// 				echo "</pre>";
// 				die;
				$this->query_manager->selectTbl(APP_USERS);
				$user_id = $this->query_manager->saveRecordV3($postData);
				if ($this->session->userdata('referral_id')){
					$this->user_model->add_referral_relation($user_id);
				}
				$MSG = "User has been added successfully!";
				$mid = 1;
				$err = 0;

				$user_info = $this->user_model->getUserByEmail($email);

				//---- Prepare password reset url and link
// 				$password_reset_url = $this->url->manage_users("reset_password/?pid=reset_password&uid=" . $user_info['userID'] . "&cid=" . $user_info['password']);
// 				$password_reset_link = "<a href='" . $password_reset_url . "' target='_blank'>Click Here</a> to reset your password.";

				//---- Prepare to Send Email to The User
				$emailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							 <html xmlns="http://www.w3.org/1999/xhtml">
							 <head></head>
							 <body style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
							   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
								 Dear ' . $user_info['firstName'] . ' ' . $user_info['lastName'] . ',
							   </p>
							   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
								See below for your login details:
								<br />
								<span style="color:#d4d4d4; width:45px;">URL: </span> <span style="font-weight: normal"> ' . SITE_URL . ' </span><br />
								<span style="color:#d4d4d4; width:45px;">Username: </span> <span style="font-weight: normal">' . $user_info['email'] . '</span><br />
								<span style="color:#d4d4d4; width:45px;">Password: </span> <span style="font-weight: normal">' . $password . '</span><br />
							   </p>
							   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
								  Regards<br/><br/>
								  IT Support Team<br/>
								  
							   </p>
							 </body>
							 </html>';

				$emailSubject = 'Mybitshares - Login Details for ' . $user_info['firstName'] . ' ' . $user_info['lastName'];
				$recipients = $user_info['email'];

				// Send Email

				//--- get Email Object
//				$mailer = $this->general->getEmailObject();
//				//--- prepare and send emails
//				$mailer->Body = $emailBody;
//				$mailer->Subject = $emailSubject;
//				$mailer->AddAddress($recipients);
//				$mailer->Send();
				//send the message, check for errors
// 				if (!$mailer->send()) {
// 					echo "Mailer Error: " . $mailer->ErrorInfo;
// 				} else {
// 					echo "Message sent!";
// 				}
				$mail_sent = 1;
				//$this->message->set('User has been added successfully!', 'success', TRUE);
                $userData = $this->authorisation->processLogin($postData['email'],$password);
                if(!empty($userData)){
                    $this->general->redirect('mybitshares');
                }
			}
			$data['MSG'] = $MSG;
			$data['err'] = $err;
			//$data['DB'] = $DB;
		}
		$this->load->view('users/add_user_email',$data);
	}

	public function user_details(){
		$data = array();
		$data['verified_email'] = $this->session->userdata('verified_email');
		$data['countryList'] = $this->general->getCountryList();
		

		$this->load->view('users/user_details',$data);
	}

	public function delete_user($userID = '') {		
		
		if($userID == '') {
			$userID = @$_REQUEST['userID'];
		}
		
		if($userID != '') {
			//---- Delete Single User Entry
			//$data['active'] = '0';
			$data['publish_flag'] = 0;
			$this->user_model->updateUser($userID, $data);
		}
		
		redirect('admin/manage_users');
	}
	
	public function edit_user($id)
	{
		$this->session->set_userdata('admin_edit_user', $id);
		$data['user_detail']  = $this->user_model->getUserDetails($id);
		$data['countryList'] = $this->general->getCountryList();
		//$data['edit']  = true;
		$this->load->view('users/admin/user_edit',$data);
		
		//echo "hi";
	}
	
public function process_user_edit(){
		
	
	if($this->input->post('firstName'))
	{
		$data = array(
				'firstName' => $this->input->post('firstName'),
				'lastName' => $this->input->post('lastName'),
				'email' => $this->input->post('email'),
				'address1' => $this->input->post('address1'),
				'town' => $this->input->post('town'),
				'postCode' => $this->input->post('postCode'),
				'country' => $this->input->post('country'),
				'pay_type' => $this->input->post('pay_type')
				
		);
		//add the address in the table
		$this->user_model->updateUser($this->session->userData('admin_edit_user'), $data);
		redirect('admin/manage_users');
	}
	
	
	
	
	}
	

	public function password_reset(){
		if(!empty($_REQUEST['id'])){
			$id = $_REQUEST['id'];
		}
		else{
			$id = $this->session->userData('userID');
		}
		/* state back button */
		$data['cur_page'] =  (isset($_REQUEST['cur_page']))?$_REQUEST['cur_page']:0;
		$data['page_limit'] =  (isset($_REQUEST['page_limit']))?$_REQUEST['page_limit']:0;
		$data['filterUserName'] =  (isset($_REQUEST['filterUserName']))?$_REQUEST['filterUserName']:'';
		$data['filterUserType'] =  (isset($_REQUEST['filterUserType']))?$_REQUEST['filterUserType']:'';
		$data['rsUserTypes'] = $this->user_model->getUserLevels();
		$data['id'] =$id;
		$invalidPass = 0;

		$resultUser = $this->user_model->getUserById($id);
		$this->session->set_userdata('password',$resultUser["password"]) ;

		//---- Reset Password
		if(isset($_POST['ssubmit']) && $_POST['ssubmit'] == "PostReplyResetPass") {
			$DB = $this -> input->post('DB');
			$email = $DB['email'];

			if(isset($_POST['generateTempPass']) && $_POST['generateTempPass'] == 'Generate Temporary Password'){
				//---- Generate Random Password
				$characters = 'abcdefgh45ij345kl345mn345o34pqrs6tuvwxyz0123456789ABFDYTU8899IEJKASIU3213567987564SHASIYQZXMKOIUYahdye8754322reiuyh5489i2342349867fASUYJhu7090230936598yasjhaseuyoqpppiurytybvldmAWYUQYRNGLIOYTO';
				$tempPass = '';
				$random_string_length = 8;
				for ($i = 0; $i < $random_string_length; $i++) {
					$tempPass .= $characters[rand(0, strlen($characters) - 1)];
				}

				$randPass = md5($tempPass);
				if($id == $this->session->userData('userID')){
					$this->session->set_userdata('password',$randPass) ;
				}
				unset($DB['password_confirm']);
				unset($DB['password']);
				unset($DB['email']);
				$DB['password'] = $randPass;
				$DB["password_date"] = date("Y-m-d");
				$DB['userID'] = $id;
				$this->query_manager->selectTbl(APP_USERS);
				$this->query_manager->updateRecordV2($DB,"userID");

				$user_info = $this->user_model->getUserByEmail($email);

				if($user_info){
					//---- Prepare password reset url and link
					$password_reset_url = SITE_URL."reset_password/?pid=reset_password_match&uid=".$user_info['userID']."&cid=".$randPass;
					$password_reset_link = "<a href='".$password_reset_url."' target='_blank'>Click Here</a> to reset your password.";
					$to      = $user_info['email'];
					$subject = 'Defect Manager - Password for '.$user_info['firstName'].' '.$user_info['LastName'];
					$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
													 <html xmlns="http://www.w3.org/1999/xhtml">
													 <head></head>
													 <body style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
													   <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
														 Dear <b>'.$user_info['firstName'].' '.$user_info['LastName'].'</b>,
													   </p>
													   <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
														This email contains password for your account generated by one of our admins.
													   </p>
													   <p>
													   Please note:
														<ul>
															<li>When distributing IDs and passwords to individual users, be sure to do so in a safe and secure manner.</li>
															<li>This Password is valid for 60 Days.</li>
														</ul>
													   </p>
													   <p>
													   <b>User Name: </b> '.$email.' <br />
													   <b>Password: </b>'.$tempPass.'
													   </p>
													   <p>Once you successfully signed in with the credentials mentioned above, you can create new passwords by following the steps below.</p>
													   <h3 style="color:#3772C7">To sign in and change your password</h3>
													   <ol>
														<li>Click on password reset link under admin menu.</li>
														<li>Enter corresponding temporary or current password.</li>
														<li>Follow the instruction on the screen to reset password.</li>
													   </ol>
													   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
														  Regards<br/><br/>
														  IT Support Team<br/>
														  Defect Manager
													   </p>
													 </body>
													 </html>';

					$this->load->library('email');

					$this->email->from('admin@mybitshares.com', 'Administrator');
					$this->email->to($to); 
					// $this->email->cc('another@another-example.com'); 
					// $this->email->bcc('them@their-example.com'); 
					$this->email->subject($subject);
					$this->email->message($message);	
					$this->email->send();
					// echo $this->email->print_debugger();
					$invalidPass = 0;
					$MSG = "Password has been sent Successfully!";
				}
			}
			else{
				if(isset($_POST['currPass']))
				$md5Pass = md5($_POST['currPass']);
				else
				$md5Pass = '';
				if($invalidPass==0){
					unset($DB['password_confirm']);
					unset($DB['email']);

					$DB['password'] = md5($DB['password']);
					$DB["password_date"] = date("Y-m-d");
					if($id ==  $this->session->userData('userID')){
						$this->session->set_userdata('password', $DB['password']) ;
					}

					$DB['userID'] = $id;
					$this->query_manager->selectTbl(APP_USERS);
					$this->query_manager->updateRecordV2($DB,"userID");

					$MSG = "Password has been reset successfully!";
				}
			}
			$data['MSG'] = $MSG;
			//$data['err'] = $err;
		}
		$data['resultUser'] = $resultUser;
		$data['userID'] = $id;
		$data['invalidPass'] = $invalidPass;
		$this->load->view('admin/users/password_reset',$data);
	}

	public function __list_users_view_ajax()
	{
		
		$userLevel = $this->session->userData('userLevel');
		$userID = $this->session->userData('userID');
		//$masterClientCondition = security::applyMasterClientAccessRule("full");
		$masterClientCondition  = '';
		$nonSuperAdminConditions = ( $userLevel != 0 ) ? " AND userRoleID != 1 " : "";
		$qry_options = " WHERE `active` = '1' AND userID != ".$userID." ". $masterClientCondition . $nonSuperAdminConditions;
		
		/*----------------------------------------------------------------------------------------------------------------------------------------------------
		| process user filters
		------------------------------------------------------------------------------------------------------------------------------------------------------*/
		//---- Select User Type condition
		if (isset($_REQUEST['filterUserType']) && $_REQUEST['filterUserType'] != "") {
			$qry_options .= " AND userRoleID='" . $_REQUEST['filterUserType'] . "'";
		}
		
		//---- Search by name
		if (isset($_REQUEST['name']) && $_REQUEST['name'] != "" ) {
			$qry_options .= " AND (firstName LIKE '%" . $_REQUEST['name'] . "%' OR lastName LIKE '%".$_REQUEST['name']."%')";
		}

		$data['userTypes'] = $this -> user_model -> getUserLevels();

		$data['user_newly_edit_id'] = isset($_REQUEST['user_newly_edit_id'])?$_REQUEST['user_newly_edit_id']:0;

		/*----------------------------------------------------------------------------------------------------------------------------------------------------
		| Main Qry Processing
		------------------------------------------------------------------------------------------------------------------------------------------------------*/
		/*$paginate = new Pagination();
		$paginate -> per_page = empty($_REQUEST['per_page']) ? PER_PAGE : $_REQUEST['per_page'];

		//---- following variable is being used to change per page records on user request
		$data['per_page_option'] = ($paginate -> per_page == PER_PAGE) ? 250 : PER_PAGE;

		$cur_page = (empty($_REQUEST['cur_page'])) ? 1 : $_REQUEST['cur_page'];
		$start_LIMIT = $paginate->per_page * ($cur_page -1);
		$end_LIMIT = $paginate->per_page;
		$qry_LIMIT = " LIMIT $start_LIMIT , $end_LIMIT";
		$data['cur_page'] = $cur_page;*/
		
		//---- all records processing
		$dataAll = $this->user_model->getAllUsers($qry_options);
		$totalRecords = ( ! empty($dataAll) ) ? count($dataAll) : 0;
		$data['totalRecords'] = ($totalRecords <= 0) ? 0 : $totalRecords;
		//$data['paginate'] = $paginate;
		
		//---- Order By Compile
		
		//-- Pagination Variables ...
		$qry_orderBy = $_REQUEST['columns'][$_REQUEST['order'][0]['column']]['data'];
		if($qry_orderBy == 'lastLoginDays') {
			$qry_orderBy = 'lastLogin';
		}
		$qry_sortOrder = $_REQUEST['order'][0]['dir'];
		$draw = $_REQUEST['draw'];
		$length = $_REQUEST['length'];
		$start = $_REQUEST['start'];
		
		$qry_LIMIT = " LIMIT $start , $length";
		
		
		if(!empty($qry_orderBy)) {
			$qry_orderBy = " ORDER BY ".$qry_orderBy." ";

			
			if(!empty($qry_sortOrder)) {
				$qry_sortOrder = $qry_sortOrder;
			}
			$qry_orderBy .= $qry_sortOrder;
		}
		
		//---- set records for CSV
		$_SESSION["users_csv"]["filename"] = 'users.csv';
		$_SESSION["users_csv"]["where"] 	= serialize($qry_options);
		
		//---- get report data with query limit applied
		$data['usersData'] = $this->user_model->getAllUsers($qry_options.$qry_orderBy.$qry_LIMIT);

		/*----------------------------------------------------------------------------------------------------------------------------------------------------
		| MISC Ops and other report related data
		------------------------------------------------------------------------------------------------------------------------------------------------------*/
		//---- variables
		$data['prevProjectID']  = 0;
		$data['sectionStyle']	= "";
		$data['today'] = time();

		//---- default date filters
		echo $this->load->view('admin/users/ajax_includes/list_users_view', $data,true);
	}
	/**
	 * @author MH
	 */
	public function __download_users_CSV()
	{
		$filename = $_SESSION["users_csv"]["filename"];
		$where = unserialize($_SESSION["users_csv"]["where"]);
		$data = $this->user_model->getAllUsers($where);
		$userTypes = $this->user_model->getUserLevels();
	
		$csvDataHeading = array("ID", "Company", "Email", "User Type", "Last Login Date", "Last Login Days");
		$this->general->csvHeaders($filename);
		$out = fopen('php://output', 'w');
		fputcsv($out,$csvDataHeading);
		if ( ! empty($data) ){
			foreach($data as $row){
				$csvData = array();
				$csvData[] = $row["userID"];
				$csvData[] = $row["company"];
				$csvData[] = $row["email"];
				$csvData[] = $userTypes[$row['userRoleID']]['userTypeName'];
				$csvData[] = !empty($row["lastLogin"]) ? $this->general->showDateTime($row["lastLogin"],'timestamp') : "N/A";
				$csvData[] = !empty($row["lastLogin"]) ? $this->general->calculateDays($row["lastLogin"], time()) : "N/A";
				fputcsv($out,$csvData);
			}
		}
		fclose($out);
	
	}
	
	/**
     * This function will show list of all Contract info to assign them to users.
     *
     * @author usman
     */
	public function assignContracts()
	{
		$post = $this->input->post(NULL, TRUE);
		$user_id = $_REQUEST['user_id'];

		$data = array();
		$dbArray = array();
		if(isset($post['contractAction']) && $post['contractAction'] == "assign"){
			$dbArray['userID'] = $user_id;
			foreach($post['unAssignedBox'] as $assign){
				 $dbArray['contractID'] = $assign;
				 $this->Clients_model->assignContract($dbArray);
			}
		}
		if(isset($post['contractAction']) && $post['contractAction'] == "unAssign"){
			if(isset($post['assignedBox'])){
				$where = " userID = ".$user_id." AND contractID IN (".implode(',',$post['assignedBox']).') ';
				$this->Clients_model->deleteAssignmentContract($where);
			}
		}
		// If ajax request then list data
		if ( $this->input->is_ajax_request() ){
			$this->__assign_contracts_ajax_call();
			exit();
		}
		$data['user_id'] = $user_id ;

		$this->load->view('admin/users/assign_contracts', $data);
	}

	/**
     * This function will return list of Contracts list by call through Ajax.
     *
     * @author usman
     */
	public function __assign_contracts_ajax_call()
	{
		$post = $this->input->post(NULL, TRUE);
		$get = $this->input->get(NULL, TRUE);
		
		if (is_null($post['user_id']) || empty($post['user_id'])) {
			die('Please provide a user ID');
		}
		
		$user_detail = $this->user_model->getUserById($post['user_id']);
		
		if (empty($user_detail)) {
			die('Please provide a valid user ID');
		}
		
		/*----------------------------------------------------------------------------------------------------------------------------------------------------
		| Main Qry Processing
		------------------------------------------------------------------------------------------------------------------------------------------------------*/

		//---- get report data for assigned contracts
		$contracts = array();
		$where = '1 AND '.APP_USERS.'.userID = '.$post['user_id'];
		$data['assginedContracts'] 			= $this->general_model->getAssignmentContracts($where);
		foreach($data['assginedContracts'] as $contract){
			$contracts[] = $contract['contractID'];
		}
		
		/*----------------------------------------------------------------------------------------------------------------------------------------------------
		| process search filters
		------------------------------------------------------------------------------------------------------------------------------------------------------*/
		$where = ' 1 = 1 ';
		
		if(count($contracts)>0){
			$where .= ' AND contractID NOT IN('.implode(',',$contracts).') ';
		}
		//---- get report data for Un-Assigned contracts
		$per_page_limit  = ! $this->input->get('per_page_limit', true) ? PER_PAGE : $this->input->get('per_page_limit', true);
		$per_page_option = ($per_page_limit==PER_PAGE) ? 250 : PER_PAGE;

		//---- Order By Compile
		$qry_orderBy = "";
		$qry_sortOrder = "";
		if(isset($post['sort_by']) && !empty($post['sort_by'])) {
			$qry_orderBy = $post['sort_by'];
			$qry_sortOrder = " ASC ";
			if(!empty($_REQUEST['sort_order'])) {
				$qry_sortOrder = $post['sort_order'];
			}
		}

		//---- get report data with query limit applied
		$data['results'] 			= $this->Clients_model->get_contracts($where, '', 0,$qry_orderBy, $qry_sortOrder);
		$data['per_page_limit']  	= $per_page_limit;
		$data['per_page_option'] 	= $per_page_option;
		
		$data['user_detail'] = $user_detail;
		$data['user_id'] 	= $post['user_id'];
		$data['activeTab'] 	= isset($post['activeTab'])?$post['activeTab']:'tab_1';

		echo $this->load->view('admin/users/ajax_includes/assign_contracts_data',$data,true);
	}

    public function add_user_by_ajax_call(){
        $data['rsUserTypes'] = $this->user_model->getUserLevels();
        if( isset($_REQUEST['ssubmit']) ) {
            $DB = $this -> input->post('DB');
            $DB['email'] = trim(@$DB['email']);

            //---- validate if email already exists
            if ($this->user_model->validateEmailAddress($DB['email'])) {
                $MSG = "Email already exists!";
                $mid = 1;
                $err = 1;
            }

            if (!isset($MSG) || $MSG == "") {

                //---- Create a Merchant client folder
                $email = $DB['email'];
                //--- If auto generate password
                if (isset($DB['sendPass']) && $DB['sendPass']) {
                    $characters = 'abcdefgh45ij345kl345mn345o34pqrs6tuvwxyz0123456789ABFDYTU8899IEJKASIU3213567987564SHASIYQZXMKOIUYahdye8754322reiuyh5489i2342349867fASUYJhu7090230936598yasjhaseuyoqpppiurytybvldmAWYUQYRNGLIOYTO';
                    $temp_pass = '';
                    $random_string_length = 8;
                    for ($i = 0; $i < $random_string_length; $i++) {
                        $temp_pass .= $characters[rand(0, strlen($characters) - 1)];
                    }
                    $DB['password'] = $temp_pass;
                    $password = $DB['password'];


                } else {
                    $password = $DB['password'];
                }

                $DB['password'] = md5($password);

                $DB['timeStamp'] =  time();
                $DB["password_date"] = date("Y-m-d");

                $this->query_manager->selectTbl(APP_USERS);
                $insertedData = $this->query_manager->saveRecordV3($DB);
                $MSG = "User has been added successfully!";
                $mid = 1;
                $err = 0;

                $user_info = $this->user_model->getUserByEmail($email);
                if ($user_info) {

                    if (isset($_POST['user_master_clients']) && !empty($_POST['user_master_clients'])) {
                        foreach ($_POST['user_master_clients'] as $user_master_client) {
                            $this->query_manager->selectTbl("app_user_master_clients");
                            $this->query_manager->saveRecord(array('userID' => $user_info['userID'], 'masterClientID' => $user_master_client));
                        }
                    }

                    //---- Prepare password reset url and link
                    $password_reset_url = $this->url->manage_users("reset_password/?pid=reset_password&uid=" . $user_info['userID'] . "&cid=" . $user_info['password']);
                    $password_reset_link = "<a href='" . $password_reset_url . "' target='_blank'>Click Here</a> to reset your password.";

                    //---- Prepare to Send Email to The User
                    $emailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								 <html xmlns="http://www.w3.org/1999/xhtml">
								 <head></head>
								 <body style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
								   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
									 Dear ' . $user_info['firstName'] . ' ' . $user_info['LastName'] . ',
								   </p>
								   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
									See below for your login details:
									<br />
									<span style="color:#d4d4d4; width:45px;">URL: </span> <span style="font-weight: normal"> ' . SITE_URL . ' </span><br />
									<span style="color:#d4d4d4; width:45px;">Username: </span> <span style="font-weight: normal">' . $user_info['email'] . '</span><br />
									<span style="color:#d4d4d4; width:45px;">Password: </span> <span style="font-weight: normal">' . $password . '</span><br />
								   </p>
								   <p style="padding:4px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
									  Regards<br/><br/>
									  IT Support Team<br/>

								   </p>
								 </body>
								 </html>';

                    $emailSubject = 'Defect Manager - Login Details for ' . $user_info['firstName'] . ' ' . $user_info['LastName'];
                    $recipients = $user_info['email'];

                    // Send Email

                    //--- get Email Object
                    $mailer = $this->usersecurity->getEmailObject();
                    //--- prepare and send emails
                    $mailer->Body = $emailBody;
                    $mailer->Subject = $emailSubject;
                    $mailer->AddAddress($recipients);
                    $mailer->Send();
                    $mail_sent = 1;
                    //$this->message->set('User has been added successfully!', 'success', TRUE);
                    //$this->general->redirect( 'admin/users/list_users' );
                }
            }
            $data['MSG'] = $MSG;
            $data['err'] = $err;
            $data['DB'] = $DB;

            /*$ispectorsResult = $this->Inspector_model->get_inspectors($where);

            $inspectorsList = array();

            foreach($ispectorsResult as $inspector){
                $inspectorsList[$inspector['haInspectorID']] = $inspector['name'];
            }*/
            $jsonArr = array();
            if($err != 1) {
                $jsonArr['dsp_users'] = $insertedData; // $this->general_model->getOptionsList(APP_USERS, 'CONCAT(firstName, " ",LastName)', 'userID', ' userRoleID = ' . $this->general->getDSPRoleID());
                $jsonArr['MSG'] = $MSG;
                $jsonArr['err'] = $err;
            }else{
                $jsonArr['MSG'] = $MSG;
                $jsonArr['err'] = $err;
            }
            echo json_encode($jsonArr);
        }
        //$this->load->view('admin/users/add_user',$data);
    }

    public function get_user_by_ajax_call(){
		
        $dsp_users_list = $this->general_model->getOptionsList(APP_USERS, 'CONCAT(firstName, " ",LastName)', 'userID', ' userRoleID = ' . $this->general->getDSPRoleID());

        echo json_encode($dsp_users_list);
    }
}