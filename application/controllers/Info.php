<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('category_model', 'mybitshares_model'));
		$this->load->helper('form');
		$this->load->helper('url');
    	$this->load->library('session');
	}

	public function index()
	{
		$data = array();
		if($this->session->userdata('userID')){
			redirect('mybitshares/dashboard');
		}else{
			$this->load->view('mybitshares/index',$data);
		}
	}

	public function upgrade()
	{
		$data = array();
		$this->load->view('info/upgrade',$data);
	}

	public function advertise()
	{
		$data = array();
		$this->load->view('info/advertise',$data);
	}

	public function been_paid()
	{
		$data = array();
		$this->load->view('info/been_paid',$data);
	}

	public function terms()
	{
		$data = array();
		$this->load->view('info/terms',$data);
	}

	public function privacy()
	{
		$data = array();
		$this->load->view('info/privacy',$data);
	}

	public function help()
	{
		$data = array();
		$this->load->view('info/help',$data);
	}

	public function contact()
	{
		$data = array();
		$this->load->view('info/contact',$data);
	}

	public function referred_by($id)
	{
		$this->session->set_userdata('referral_id', $id);
		redirect ('users/join_user');
	}
	
}